# Processing script

import pydub
import pydub.generators
import pydub.effects

# import pysndfx
from pedalboard import Pedalboard, Chorus, Reverb, Clipping, Distortion, Bitcrush, LowpassFilter, Compressor, Gain, Phaser, LadderFilter, Convolution
from pedalboard.io import AudioFile


def ProcessFile(InputName, OutputName):

    print(f"Processing File '{InputName}' To Export Location '{OutputName}'")


    Sound = pydub.AudioSegment.from_wav(InputName)


    # Add Whitenoise To Sound
    Noise = pydub.generators.WhiteNoise().to_audio_segment(duration=len(Sound))
    Noise -= 18 # Make it 24 db quieter
    Sound = Sound.overlay(Noise)

    # Filter shenanigans
    HighFiltered = pydub.effects.high_pass_filter(Sound, 400)
    HighFiltered += 2
    LowFiltered = pydub.effects.low_pass_filter(Sound, 600)
    LowFiltered += 9
    Sound = HighFiltered.overlay(LowFiltered)
    Sound += 5
    Sound += pydub.AudioSegment.silent(duration=1000)

    pydub.AudioSegment.export(Sound, "/tmp/file.wav")




    # Make a Pedalboard object, containing multiple audio plugins:
    board = Pedalboard([
        Distortion(),
        Bitcrush(),
        Clipping(-9),
        LowpassFilter(300),
        Chorus(),
        Reverb(room_size=0.45),
        Compressor(threshold_db=-50, ratio=25),
        Gain(gain_db=30),
        Chorus(),
        LadderFilter(mode=LadderFilter.Mode.HPF12, cutoff_hz=900),
        Phaser(),
        # Convolution("./guitar_amp.wav", 1.0),
        Reverb(room_size=0.25)
        ])

    # Open an audio file for reading, just like a regular file:
    with AudioFile("/tmp/file.wav") as f:
    
        # Open an audio file to write to:
        with AudioFile(OutputName, 'w', f.samplerate, f.num_channels) as o:
        
            # Read one second of audio at a time, until the file is empty:
            while f.tell() < f.frames:
                chunk = f.read(f.samplerate)
                
                # Run the audio through our pedalboard:
                effected = board(chunk, f.samplerate, reset=False)
                
                # Write the output to our output file:
                o.write(effected)

    


    # EffectChain = (pysndfx.AudioEffectsChain()
    #                 .overdrive()
    #                 .reverb()
    #                 .highshelf()
    #                 .reverb()
    #                 .phaser()
    #                 .lowshelf()
    #             )
    # EffectChain(InputName, OutputName)



def Main():

    ProcessFile("Test.wav", "OutTest.wav")


if __name__ == "__main__":
    Main()