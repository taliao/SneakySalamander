import glob
import os

def Main():
    print("Starting Recursive Audio Search")
    Files = glob.glob('**/*.wav', recursive=True);
    print(f"Found {len(Files)} Files In Audio Search")
    for File in Files:
        print(f"Compressing Audio File {File}")
        os.system(f"ffmpeg -i {File} -y -c:a libvorbis -ab 490k -ar 44100 {File.split('.')[0]}.ogg")

    print("Done Compressing Audio")

if __name__ == "__main__":
    Main()
