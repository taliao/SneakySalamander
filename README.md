# Project being rewritten to use Godot.
Please go to new repo on gitlab:

https://gitlab.com/sneakysalamander/SneakySalamander











# New plan (moved from plan.txt)

enemies:
	- flying demon (vertical moving)
	- fire demon
	- horiztonal demon
	- tack shooter (spike demon)
	- stalagtite
	- stalagmite
	- turret
	- laser fence

platforms:
	- static
	- moving horizontally
	- moving vertically
	- flipping triangle
	- spinning
	- rotating
	- tilted
	- ladders

other challenges:
	- hard jumps
	- precise jumps? like if you land too far you hit an enemy
	- minor bullet hell

levels: which things are introduced in which level
	- DONE tutorial: flying demon, checkpoints, static, fire demon
	- DONE 1: platform moving horizontally, ladders
	- DONE 2: tilted platforms, platform moving vertically, horizontal demon
	- DONE 3: tack shooter
	- DONE 4: hard level 1, spinning moving platform
	- DONE 5: flipping triangle, stalagtite, stalagmite
	- DONE 6: staggered stalagmites and stalagtites, staggered flipping platforms, staggered tack shooters, spinning rotating moving platform, turret, chaos level 1
	- DONE 7: laser fence, staggered fences, both directions moving and rotating platforms, floating ladders
	- DONE 8: staggered turrets, shockwave
	- DONE 9: mites, hard level 2
	- DONE 10: chaos level 2, also replicate exact tutorial layout in one part except make jumps harder and no text
	- 11: Dylan: confidence bar starts out full, killing enemies no longer recharges it. IDEA: cool neon colors for this level (could rgb the entire map with color tweening)

boss fight ideas:
	to emphasize fighting inner demons theme: could have evil salamander on middle of screen mirroring your inputs (same direction too; if somehow they reach right bound while youre still at left bound though they will be prevented from moving further right until you do), they start with 5 lives and 0 confidence (displayed on top right). only you get confidence from killing enemies though, so they’re always stuck on 0. your goal is to deplete their five lives without dying yourself. boss doesnt fight you, but when its falling (to detect: if falling and if you haven’t pressed space in the last few seconds global timer and if nearest platform is not under it (check if its x pos is within center - width/2, center + width/2 of platform)) it can summon a flying demon temporarily under it to try and bounce multiple times too. when its about to run into an enemy it can sometimes (maybe just rng? if closest enemy every 0.1 secs (new global group?) within certain radius, check half second later and if distance to this enemy significatntly descreased and not falling onto it (rough leniency for x posiroon) trigger. if falling onto, summon flying if fire demon with fire = true, and this logic should keep bouncing it until fire off) summon a fire enemy in front of you in that direction (still with some reaction buffer though) so you would run into the enemy before you could get boss to. have 1.5 ish sec cooldown for that one so moving  platform solution = only one spawns. turrets don’t target boss, stalag do not shoot if they’d hit the boss (keep timer normal though, just no shaking and shooting). boss summons permanent laser fence temporarily to block tack shooter (use same detection radius thingy), do math for direction. boss walks through laser fences and does not get killed by their bases. 5 checkpoints, designed to provide safe boss spawning space. when boss dies, it respawns at next checkpoint’s designated spawning space and waits until your x position crosses x position of checkpoint, then it makes some animation effect and starting mirroring again. maybe evil laughing sounds play even if off screen to remind player it’s still there? player cannot normally cross x position of next checkpoint before boss has been killed (match checkpoint with boss life count since it can’t heal); walled off by permanent laser fences to the left of checkpoint that only disappear when boss dies (variables in laser beam and base like bosslifecounttodespawb). alrhough boss can never go left of the x position of its  corresponding checkpoint so player is forced to progress forward. when you die, boss goes to your checkpoint’s safe spawn spot.

	intended ways to kill boss, don’t allow too many of each at MOST 2 chances each:
	shove into path of stalag shooting
	place on moving platform where it can’t move out and gets shoved into enemy
	let them summon flying underneath them and bounce into something like a stalactite or another enemy by moving horizontally


quirky dlc level ideas:
	- checkpoint spam
	- vertical level just falling onto enemies, make sure dont fall too fast or else camera can't catch up
	- tack shooter spam or faster turret spam
	- moving platforms spam and breaking physics and lag
	- tiny platforms and huge platforms
	- pixel perfect jumps
	- super stretched and deranged aspect ratios (Dylan style)




# Old Plan

# Plan
    - Story is that the main character (MC) has faced rejection many times.
    - Therefore, he's afraid of reaching out socially
    - The salamander is a metaphor for the part of his brain that still does want to reach out
    - You've got to help him face his rejection and overcome it

# Level Plan
    - Each level has a different theme, based around an event of rejection
    - After each level, a small cutscene will play that shows what happened
    - This is supposed to show that the MC is finally accepting and moving on from the rejection
    - The background of each level (and memory orb) will have a unique theme

# Death Screen
    - Quotes that show rejection after dying
    - each level might have a few different ones


# Levels

## L1 (Tutorial Level)
    - Very short and sweet - focuses on a mountain theme
    - Teaches player movement and the first two enemies

    - Cutscene:
        - (very young) MC goes hiking with his crush (Person  1)
        - A bit of banter, then he asks if his feelings are mutual
        - bit of silence
        - relatively soft rejection (the beginning of his spiral)
        - end of Cutscene

## L2 (Forest Level)
    - Still a shorter level, focuses on the player trying to get used to some challenge
    - Has two ways to do things, an easy and hard way

    - Cutscene:
        - (Young) MC is walking in the forest with crush (Person 2)
        - They're talking about trees and the forest
        - A short comfortable silence falls
        - MC says that they enjoy being friends
        - Asks if crush is ever attracted to people of same gender
        - Crush laughs and says no cause he'd go to hell
        - end of Cutscene

## L3 (Beach Level)
    - Introduces third enemy type (moves horizontally)

- Person 2 ark:
    - Intro them as crush
        - enjoy beign friends
        - person misinterprets this as friendship not romance
    - then MC asks if p2 is gay,
        - they are homophobic and say no citing religion
    - Finally MC (not knowing better) asks them out
        - gets hard rejected, other person yells at them and leaves

- Person 3 ark:
    - Intro them as known gay person
        - meet at school
        - MC asks to talk abouit sexuality with them
        - they agree
    - Show them talking
        - they get to know eachother, and share interests
        - paint together (art themed level)
    - Finally, show rejection
        - P3 realizes that MC has a crush, he has a boyfriend unknown to MC
        - devestating rejection, is cut off from P3 (turns back)


# L(N) (Ocean)
    - A bit longer of a level, has more challenge here, and makes the player really work
    - Focused more on platforming and such
    - Still has two ways to do things, an easy and hard way

    - Cutscene:
        - (Young Adult) MC is on a platform in the ocean talking with crush (Person 3)
        -
        - end of Cutscene
