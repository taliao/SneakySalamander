Sound pack downloaded from Freesound
----------------------------------------

"Ambient Sounds &amp; Loops [Pack 7]"

This pack of sounds contains sounds by the following user:
 - Erokia ( https://freesound.org/people/Erokia/ )

You can find this pack online at: https://freesound.org/people/Erokia/packs/27235/


Pack description
----------------

A collection of ambient sounds, self made synth like sounds and piano loops.
As of May 16th 2020, no more sounds will be added to this pack unless edited or removed.
Pack is 1.1 Gigs in size.


Licenses in this pack (see below for individual sound licenses)
---------------------------------------------------------------

Creative Commons 0: http://creativecommons.org/publicdomain/zero/1.0/
Attribution NonCommercial 4.0: https://creativecommons.org/licenses/by-nc/4.0/


Sounds in this pack
-------------------

  * 516902__erokia__ambient-wave-56-msfxp7-78.wav
    * url: https://freesound.org/s/516902/
    * license: Attribution NonCommercial 4.0
  * 511616__erokia__ambient-wave-55-msfxp7-17-3-33.wav
    * url: https://freesound.org/s/511616/
    * license: Attribution NonCommercial 4.0
  * 511487__erokia__piano-ambiance-misc-drums-remix-165209-50101-41589-22129-100-bpm.wav
    * url: https://freesound.org/s/511487/
    * license: Attribution NonCommercial 4.0
  * 509444__erokia__ambient-wave-54-msfxp7-1-3-p-sess.wav
    * url: https://freesound.org/s/509444/
    * license: Attribution NonCommercial 4.0
  * 509157__erokia__dnb-synth-v2-100-bpm-fs-50101-61704-24930-399087.wav
    * url: https://freesound.org/s/509157/
    * license: Attribution NonCommercial 4.0
  * 508830__erokia__dnb-synth-fs-50101-61704.wav
    * url: https://freesound.org/s/508830/
    * license: Attribution NonCommercial 4.0
  * 505241__erokia__ambient-wave-53-msfxp6-96-9-2-p-sess.wav
    * url: https://freesound.org/s/505241/
    * license: Attribution NonCommercial 4.0
  * 504692__erokia__ambient-wave-52.wav
    * url: https://freesound.org/s/504692/
    * license: Attribution NonCommercial 4.0
  * 503577__erokia__ambient-wave-51.wav
    * url: https://freesound.org/s/503577/
    * license: Attribution NonCommercial 4.0
  * 502185__erokia__ambient-wave-50.wav
    * url: https://freesound.org/s/502185/
    * license: Attribution NonCommercial 4.0
  * 501632__erokia__synth-drums-90-bpm-stellar.wav
    * url: https://freesound.org/s/501632/
    * license: Attribution NonCommercial 4.0
  * 500139__erokia__ambient-wave-49.wav
    * url: https://freesound.org/s/500139/
    * license: Attribution NonCommercial 4.0
  * 496757__erokia__ambient-wave-48-tribute.wav
    * url: https://freesound.org/s/496757/
    * license: Creative Commons 0
  * 490555__erokia__ambient-wave-47.wav
    * url: https://freesound.org/s/490555/
    * license: Attribution NonCommercial 4.0
  * 485633__erokia__ambient-wave-46.wav
    * url: https://freesound.org/s/485633/
    * license: Attribution NonCommercial 4.0
  * 484280__erokia__soundscape-synth-ambiance-1-reverberation.wav
    * url: https://freesound.org/s/484280/
    * license: Attribution NonCommercial 4.0
  * 484199__erokia__august-dnb-loop-4-60-120-bpm.wav
    * url: https://freesound.org/s/484199/
    * license: Attribution NonCommercial 4.0
  * 482706__erokia__ambient-wave-45.wav
    * url: https://freesound.org/s/482706/
    * license: Attribution NonCommercial 4.0
  * 481971__erokia__bassy-ambient-sounds.wav
    * url: https://freesound.org/s/481971/
    * license: Attribution NonCommercial 4.0
  * 478940__erokia__fresh-24.wav
    * url: https://freesound.org/s/478940/
    * license: Attribution NonCommercial 4.0
  * 473545__erokia__ambient-wave-compilation-by-erokia.mp3
    * url: https://freesound.org/s/473545/
    * license: Attribution NonCommercial 4.0


