gdjs.Level3Code = {};
gdjs.Level3Code.GDPlayerObjects3_1final = [];

gdjs.Level3Code.GDPortalObjects3_1final = [];

gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final = [];

gdjs.Level3Code.GDBackgroundPlantsObjects1= [];
gdjs.Level3Code.GDBackgroundPlantsObjects2= [];
gdjs.Level3Code.GDBackgroundPlantsObjects3= [];
gdjs.Level3Code.GDBackgroundPlantsObjects4= [];
gdjs.Level3Code.GDBackgroundPlantsObjects5= [];
gdjs.Level3Code.GDBackgroundPlantsObjects6= [];
gdjs.Level3Code.GDLeftBoundaryObjects1= [];
gdjs.Level3Code.GDLeftBoundaryObjects2= [];
gdjs.Level3Code.GDLeftBoundaryObjects3= [];
gdjs.Level3Code.GDLeftBoundaryObjects4= [];
gdjs.Level3Code.GDLeftBoundaryObjects5= [];
gdjs.Level3Code.GDLeftBoundaryObjects6= [];
gdjs.Level3Code.GDRightBoundaryObjects1= [];
gdjs.Level3Code.GDRightBoundaryObjects2= [];
gdjs.Level3Code.GDRightBoundaryObjects3= [];
gdjs.Level3Code.GDRightBoundaryObjects4= [];
gdjs.Level3Code.GDRightBoundaryObjects5= [];
gdjs.Level3Code.GDRightBoundaryObjects6= [];
gdjs.Level3Code.GDTopBoundaryObjects1= [];
gdjs.Level3Code.GDTopBoundaryObjects2= [];
gdjs.Level3Code.GDTopBoundaryObjects3= [];
gdjs.Level3Code.GDTopBoundaryObjects4= [];
gdjs.Level3Code.GDTopBoundaryObjects5= [];
gdjs.Level3Code.GDTopBoundaryObjects6= [];
gdjs.Level3Code.GDBottomBoundaryObjects1= [];
gdjs.Level3Code.GDBottomBoundaryObjects2= [];
gdjs.Level3Code.GDBottomBoundaryObjects3= [];
gdjs.Level3Code.GDBottomBoundaryObjects4= [];
gdjs.Level3Code.GDBottomBoundaryObjects5= [];
gdjs.Level3Code.GDBottomBoundaryObjects6= [];
gdjs.Level3Code.GDBoundaryJumpThroughObjects1= [];
gdjs.Level3Code.GDBoundaryJumpThroughObjects2= [];
gdjs.Level3Code.GDBoundaryJumpThroughObjects3= [];
gdjs.Level3Code.GDBoundaryJumpThroughObjects4= [];
gdjs.Level3Code.GDBoundaryJumpThroughObjects5= [];
gdjs.Level3Code.GDBoundaryJumpThroughObjects6= [];
gdjs.Level3Code.GDPlayerObjects1= [];
gdjs.Level3Code.GDPlayerObjects2= [];
gdjs.Level3Code.GDPlayerObjects3= [];
gdjs.Level3Code.GDPlayerObjects4= [];
gdjs.Level3Code.GDPlayerObjects5= [];
gdjs.Level3Code.GDPlayerObjects6= [];
gdjs.Level3Code.GDFlyingDemonObjects1= [];
gdjs.Level3Code.GDFlyingDemonObjects2= [];
gdjs.Level3Code.GDFlyingDemonObjects3= [];
gdjs.Level3Code.GDFlyingDemonObjects4= [];
gdjs.Level3Code.GDFlyingDemonObjects5= [];
gdjs.Level3Code.GDFlyingDemonObjects6= [];
gdjs.Level3Code.GDFireDemonObjects1= [];
gdjs.Level3Code.GDFireDemonObjects2= [];
gdjs.Level3Code.GDFireDemonObjects3= [];
gdjs.Level3Code.GDFireDemonObjects4= [];
gdjs.Level3Code.GDFireDemonObjects5= [];
gdjs.Level3Code.GDFireDemonObjects6= [];
gdjs.Level3Code.GDCheckpointObjects1= [];
gdjs.Level3Code.GDCheckpointObjects2= [];
gdjs.Level3Code.GDCheckpointObjects3= [];
gdjs.Level3Code.GDCheckpointObjects4= [];
gdjs.Level3Code.GDCheckpointObjects5= [];
gdjs.Level3Code.GDCheckpointObjects6= [];
gdjs.Level3Code.GDStaticPlatform3Objects1= [];
gdjs.Level3Code.GDStaticPlatform3Objects2= [];
gdjs.Level3Code.GDStaticPlatform3Objects3= [];
gdjs.Level3Code.GDStaticPlatform3Objects4= [];
gdjs.Level3Code.GDStaticPlatform3Objects5= [];
gdjs.Level3Code.GDStaticPlatform3Objects6= [];
gdjs.Level3Code.GDStaticPlatform2Objects1= [];
gdjs.Level3Code.GDStaticPlatform2Objects2= [];
gdjs.Level3Code.GDStaticPlatform2Objects3= [];
gdjs.Level3Code.GDStaticPlatform2Objects4= [];
gdjs.Level3Code.GDStaticPlatform2Objects5= [];
gdjs.Level3Code.GDStaticPlatform2Objects6= [];
gdjs.Level3Code.GDHorizontalMovingPlatformObjects1= [];
gdjs.Level3Code.GDHorizontalMovingPlatformObjects2= [];
gdjs.Level3Code.GDHorizontalMovingPlatformObjects3= [];
gdjs.Level3Code.GDHorizontalMovingPlatformObjects4= [];
gdjs.Level3Code.GDHorizontalMovingPlatformObjects5= [];
gdjs.Level3Code.GDHorizontalMovingPlatformObjects6= [];
gdjs.Level3Code.GDStaticPlatform1Objects1= [];
gdjs.Level3Code.GDStaticPlatform1Objects2= [];
gdjs.Level3Code.GDStaticPlatform1Objects3= [];
gdjs.Level3Code.GDStaticPlatform1Objects4= [];
gdjs.Level3Code.GDStaticPlatform1Objects5= [];
gdjs.Level3Code.GDStaticPlatform1Objects6= [];
gdjs.Level3Code.GDPortalObjects1= [];
gdjs.Level3Code.GDPortalObjects2= [];
gdjs.Level3Code.GDPortalObjects3= [];
gdjs.Level3Code.GDPortalObjects4= [];
gdjs.Level3Code.GDPortalObjects5= [];
gdjs.Level3Code.GDPortalObjects6= [];
gdjs.Level3Code.GDLadderObjects1= [];
gdjs.Level3Code.GDLadderObjects2= [];
gdjs.Level3Code.GDLadderObjects3= [];
gdjs.Level3Code.GDLadderObjects4= [];
gdjs.Level3Code.GDLadderObjects5= [];
gdjs.Level3Code.GDLadderObjects6= [];
gdjs.Level3Code.GDHeartObjects1= [];
gdjs.Level3Code.GDHeartObjects2= [];
gdjs.Level3Code.GDHeartObjects3= [];
gdjs.Level3Code.GDHeartObjects4= [];
gdjs.Level3Code.GDHeartObjects5= [];
gdjs.Level3Code.GDHeartObjects6= [];
gdjs.Level3Code.GDMonsterParticlesObjects1= [];
gdjs.Level3Code.GDMonsterParticlesObjects2= [];
gdjs.Level3Code.GDMonsterParticlesObjects3= [];
gdjs.Level3Code.GDMonsterParticlesObjects4= [];
gdjs.Level3Code.GDMonsterParticlesObjects5= [];
gdjs.Level3Code.GDMonsterParticlesObjects6= [];
gdjs.Level3Code.GDSpikeParticlesObjects1= [];
gdjs.Level3Code.GDSpikeParticlesObjects2= [];
gdjs.Level3Code.GDSpikeParticlesObjects3= [];
gdjs.Level3Code.GDSpikeParticlesObjects4= [];
gdjs.Level3Code.GDSpikeParticlesObjects5= [];
gdjs.Level3Code.GDSpikeParticlesObjects6= [];
gdjs.Level3Code.GDDoorParticlesObjects1= [];
gdjs.Level3Code.GDDoorParticlesObjects2= [];
gdjs.Level3Code.GDDoorParticlesObjects3= [];
gdjs.Level3Code.GDDoorParticlesObjects4= [];
gdjs.Level3Code.GDDoorParticlesObjects5= [];
gdjs.Level3Code.GDDoorParticlesObjects6= [];
gdjs.Level3Code.GDDustParticleObjects1= [];
gdjs.Level3Code.GDDustParticleObjects2= [];
gdjs.Level3Code.GDDustParticleObjects3= [];
gdjs.Level3Code.GDDustParticleObjects4= [];
gdjs.Level3Code.GDDustParticleObjects5= [];
gdjs.Level3Code.GDDustParticleObjects6= [];
gdjs.Level3Code.GDLivesBarObjects1= [];
gdjs.Level3Code.GDLivesBarObjects2= [];
gdjs.Level3Code.GDLivesBarObjects3= [];
gdjs.Level3Code.GDLivesBarObjects4= [];
gdjs.Level3Code.GDLivesBarObjects5= [];
gdjs.Level3Code.GDLivesBarObjects6= [];
gdjs.Level3Code.GDLifeForceBarObjects1= [];
gdjs.Level3Code.GDLifeForceBarObjects2= [];
gdjs.Level3Code.GDLifeForceBarObjects3= [];
gdjs.Level3Code.GDLifeForceBarObjects4= [];
gdjs.Level3Code.GDLifeForceBarObjects5= [];
gdjs.Level3Code.GDLifeForceBarObjects6= [];
gdjs.Level3Code.GDMemoryObjects1= [];
gdjs.Level3Code.GDMemoryObjects2= [];
gdjs.Level3Code.GDMemoryObjects3= [];
gdjs.Level3Code.GDMemoryObjects4= [];
gdjs.Level3Code.GDMemoryObjects5= [];
gdjs.Level3Code.GDMemoryObjects6= [];
gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects1= [];
gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects2= [];
gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects3= [];
gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects4= [];
gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects5= [];
gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects6= [];
gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects1= [];
gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects2= [];
gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3= [];
gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects4= [];
gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects5= [];
gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects6= [];
gdjs.Level3Code.GDUI_9595Sinage_9595LivesObjects1= [];
gdjs.Level3Code.GDUI_9595Sinage_9595LivesObjects2= [];
gdjs.Level3Code.GDUI_9595Sinage_9595LivesObjects3= [];
gdjs.Level3Code.GDUI_9595Sinage_9595LivesObjects4= [];
gdjs.Level3Code.GDUI_9595Sinage_9595LivesObjects5= [];
gdjs.Level3Code.GDUI_9595Sinage_9595LivesObjects6= [];
gdjs.Level3Code.GDUI_9595Sinage_9595LifeForceObjects1= [];
gdjs.Level3Code.GDUI_9595Sinage_9595LifeForceObjects2= [];
gdjs.Level3Code.GDUI_9595Sinage_9595LifeForceObjects3= [];
gdjs.Level3Code.GDUI_9595Sinage_9595LifeForceObjects4= [];
gdjs.Level3Code.GDUI_9595Sinage_9595LifeForceObjects5= [];
gdjs.Level3Code.GDUI_9595Sinage_9595LifeForceObjects6= [];
gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects1= [];
gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects2= [];
gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects3= [];
gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects4= [];
gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects5= [];
gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects6= [];
gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects1= [];
gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects2= [];
gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects3= [];
gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects4= [];
gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects5= [];
gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects6= [];
gdjs.Level3Code.GDHorizontalDemonObjects1= [];
gdjs.Level3Code.GDHorizontalDemonObjects2= [];
gdjs.Level3Code.GDHorizontalDemonObjects3= [];
gdjs.Level3Code.GDHorizontalDemonObjects4= [];
gdjs.Level3Code.GDHorizontalDemonObjects5= [];
gdjs.Level3Code.GDHorizontalDemonObjects6= [];
gdjs.Level3Code.GDSpikeDemon_9595BaseObjects1= [];
gdjs.Level3Code.GDSpikeDemon_9595BaseObjects2= [];
gdjs.Level3Code.GDSpikeDemon_9595BaseObjects3= [];
gdjs.Level3Code.GDSpikeDemon_9595BaseObjects4= [];
gdjs.Level3Code.GDSpikeDemon_9595BaseObjects5= [];
gdjs.Level3Code.GDSpikeDemon_9595BaseObjects6= [];
gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects1= [];
gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects2= [];
gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects3= [];
gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects4= [];
gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects5= [];
gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects6= [];
gdjs.Level3Code.GDVerticalMovingPlatformObjects1= [];
gdjs.Level3Code.GDVerticalMovingPlatformObjects2= [];
gdjs.Level3Code.GDVerticalMovingPlatformObjects3= [];
gdjs.Level3Code.GDVerticalMovingPlatformObjects4= [];
gdjs.Level3Code.GDVerticalMovingPlatformObjects5= [];
gdjs.Level3Code.GDVerticalMovingPlatformObjects6= [];
gdjs.Level3Code.GDSpinningMovingPlatformObjects1= [];
gdjs.Level3Code.GDSpinningMovingPlatformObjects2= [];
gdjs.Level3Code.GDSpinningMovingPlatformObjects3= [];
gdjs.Level3Code.GDSpinningMovingPlatformObjects4= [];
gdjs.Level3Code.GDSpinningMovingPlatformObjects5= [];
gdjs.Level3Code.GDSpinningMovingPlatformObjects6= [];
gdjs.Level3Code.GDFlippingPlatformObjects1= [];
gdjs.Level3Code.GDFlippingPlatformObjects2= [];
gdjs.Level3Code.GDFlippingPlatformObjects3= [];
gdjs.Level3Code.GDFlippingPlatformObjects4= [];
gdjs.Level3Code.GDFlippingPlatformObjects5= [];
gdjs.Level3Code.GDFlippingPlatformObjects6= [];
gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects1= [];
gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects2= [];
gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects3= [];
gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects4= [];
gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects5= [];
gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects6= [];
gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects1= [];
gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects2= [];
gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3= [];
gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects4= [];
gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects5= [];
gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects6= [];


gdjs.Level3Code.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level3Code.GDFlippingPlatformObjects3);
{for(var i = 0, len = gdjs.Level3Code.GDFlippingPlatformObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDFlippingPlatformObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.2, 1, 1, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level3Code.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 2;
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level3Code.eventsList0(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 3;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level3Code.GDFlippingPlatformObjects3);
{for(var i = 0, len = gdjs.Level3Code.GDFlippingPlatformObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDFlippingPlatformObjects3[i].rotateTowardAngle(-(80), 0, runtimeScene);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 6;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level3Code.GDFlippingPlatformObjects3);
{for(var i = 0, len = gdjs.Level3Code.GDFlippingPlatformObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDFlippingPlatformObjects3[i].rotateTowardAngle(0, 0, runtimeScene);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


};gdjs.Level3Code.eventsList2 = function(runtimeScene) {

{


gdjs.Level3Code.eventsList1(runtimeScene);
}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects2});
gdjs.Level3Code.eventsList3 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "w");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);
{for(var i = 0, len = gdjs.Level3Code.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDPlayerObjects3[i].getBehavior("PlatformerObject").simulateControl("Up");
}
}{for(var i = 0, len = gdjs.Level3Code.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDPlayerObjects3[i].getBehavior("PlatformerObject").simulateControl("Ladder");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "a");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);
{for(var i = 0, len = gdjs.Level3Code.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDPlayerObjects3[i].getBehavior("PlatformerObject").simulateControl("Left");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "d");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);
{for(var i = 0, len = gdjs.Level3Code.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDPlayerObjects3[i].getBehavior("PlatformerObject").simulateControl("Right");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);
{for(var i = 0, len = gdjs.Level3Code.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDPlayerObjects3[i].getBehavior("PlatformerObject").simulateControl("Jump");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "s");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);
{for(var i = 0, len = gdjs.Level3Code.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDPlayerObjects3[i].getBehavior("PlatformerObject").simulateControl("Down");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "g");
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects2);
{gdjs.evtsExt__Player__HealPlayer.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level3Code.eventsList4 = function(runtimeScene) {

{


gdjs.Level3Code.eventsList3(runtimeScene);
}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects4Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level3Code.GDFlyingDemonObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects4Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level3Code.GDFlyingDemonObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects4Objects = Hashtable.newFrom({"FireDemon": gdjs.Level3Code.GDFireDemonObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects4Objects = Hashtable.newFrom({"FireDemon": gdjs.Level3Code.GDFireDemonObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects4Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level3Code.GDHorizontalDemonObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects4Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level3Code.GDHorizontalDemonObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level3Code.GDSpikeDemon_9595BaseObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level3Code.GDSpikeDemon_9595BaseObjects4});
gdjs.Level3Code.eventsList5 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.Level3Code.GDFlyingDemonObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDFlyingDemonObjects4 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level3Code.GDFireDemonObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDFireDemonObjects4 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.Level3Code.GDHorizontalDemonObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDHorizontalDemonObjects4 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level3Code.GDSpikeDemon_9595BaseObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDSpikeDemon_9595BaseObjects4 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.Level3Code.GDPlayerObjects3 */
{for(var i = 0, len = gdjs.Level3Code.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDPlayerObjects3[i].setVariableBoolean(gdjs.Level3Code.GDPlayerObjects3[i].getVariables().getFromIndex(4), false);
}
}}

}


};gdjs.Level3Code.eventsList6 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "AssetDev/Audio/Heartbeat_Amplified.wav", 2, true, 100, 1);
}{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level3Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level3Code.GDPlayerObjects4[i].getVariableNumber(gdjs.Level3Code.GDPlayerObjects4[i].getVariables().getFromIndex(1)) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level3Code.GDPlayerObjects4[k] = gdjs.Level3Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level3Code.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 60);
}}

}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDDustParticleObjects3Objects = Hashtable.newFrom({"DustParticle": gdjs.Level3Code.GDDustParticleObjects3});
gdjs.Level3Code.eventsList7 = function(runtimeScene) {

{


gdjs.Level3Code.eventsList6(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level3Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level3Code.GDPlayerObjects4[i].getBehavior("PlatformerObject").isJumping() ) {
        isConditionTrue_0 = true;
        gdjs.Level3Code.GDPlayerObjects4[k] = gdjs.Level3Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level3Code.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26897116);
}
}
if (isConditionTrue_0) {
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtsExt__Player__IsSteppingOnFloor.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects, "PlatformerObject", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26897396);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDPlayerObjects3 */
gdjs.Level3Code.GDDustParticleObjects3.length = 0;

{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "grass.mp3", 1, false, 20, gdjs.randomFloatInRange(0.7, 1.2));
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDDustParticleObjects3Objects, (( gdjs.Level3Code.GDPlayerObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDPlayerObjects3[0].getAABBCenterX()), (( gdjs.Level3Code.GDPlayerObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDPlayerObjects3[0].getAABBBottom()), "");
}{for(var i = 0, len = gdjs.Level3Code.GDDustParticleObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDDustParticleObjects3[i].setZOrder(-(1));
}
}{for(var i = 0, len = gdjs.Level3Code.GDDustParticleObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDDustParticleObjects3[i].setAngle(270);
}
}}

}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDCheckpointObjects3Objects = Hashtable.newFrom({"Checkpoint": gdjs.Level3Code.GDCheckpointObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDCheckpointObjects4Objects = Hashtable.newFrom({"Checkpoint": gdjs.Level3Code.GDCheckpointObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects3});
gdjs.Level3Code.eventsList8 = function(runtimeScene) {

{



}


{

gdjs.copyArray(gdjs.Level3Code.GDCheckpointObjects3, gdjs.Level3Code.GDCheckpointObjects4);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDCheckpointObjects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDCheckpointObjects4 */
{for(var i = 0, len = gdjs.Level3Code.GDCheckpointObjects4.length ;i < len;++i) {
    gdjs.Level3Code.GDCheckpointObjects4[i].getBehavior("Animation").setAnimationName("Inactive");
}
}}

}


{



}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.Level3Code.GDCheckpointObjects3 */
/* Reuse gdjs.Level3Code.GDPlayerObjects3 */
{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects, (( gdjs.Level3Code.GDCheckpointObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDCheckpointObjects3[0].getPointX("")), (( gdjs.Level3Code.GDCheckpointObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDCheckpointObjects3[0].getPointY("")), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.Level3Code.GDCheckpointObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDCheckpointObjects3[i].getBehavior("Animation").setAnimationName("Activate");
}
}}

}


};gdjs.Level3Code.eventsList9 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects4);
{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects4Objects, (( gdjs.Level3Code.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.Level3Code.GDPlayerObjects4[0].getPointX("")), (( gdjs.Level3Code.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.Level3Code.GDPlayerObjects4[0].getPointY("")), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Checkpoint"), gdjs.Level3Code.GDCheckpointObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDCheckpointObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level3Code.GDCheckpointObjects3.length;i<l;++i) {
    if ( !(gdjs.Level3Code.GDCheckpointObjects3[i].isCurrentAnimationName("Activate")) ) {
        isConditionTrue_0 = true;
        gdjs.Level3Code.GDCheckpointObjects3[k] = gdjs.Level3Code.GDCheckpointObjects3[i];
        ++k;
    }
}
gdjs.Level3Code.GDCheckpointObjects3.length = k;
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Checkpoint/Activate.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}
{ //Subevents
gdjs.Level3Code.eventsList8(runtimeScene);} //End of subevents
}

}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHeartObjects3Objects = Hashtable.newFrom({"Heart": gdjs.Level3Code.GDHeartObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeParticlesObjects3Objects = Hashtable.newFrom({"SpikeParticles": gdjs.Level3Code.GDSpikeParticlesObjects3});
gdjs.Level3Code.eventsList10 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Heart"), gdjs.Level3Code.GDHeartObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHeartObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDHeartObjects3 */
gdjs.Level3Code.GDSpikeParticlesObjects3.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeParticlesObjects3Objects, (( gdjs.Level3Code.GDHeartObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDHeartObjects3[0].getCenterXInScene()), (( gdjs.Level3Code.GDHeartObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDHeartObjects3[0].getCenterYInScene()), "");
}{for(var i = 0, len = gdjs.Level3Code.GDHeartObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDHeartObjects3[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(100);
}{gdjs.evtTools.sound.playSound(runtimeScene, "AssetDev/Audio/Squelch.wav", false, 50, 1);
}}

}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects2});
gdjs.Level3Code.eventsList11 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level3Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Level3Code.GDPlayerObjects2[i].getY() > gdjs.evtTools.camera.getCameraBorderBottom(runtimeScene, "", 0) ) {
        isConditionTrue_0 = true;
        gdjs.Level3Code.GDPlayerObjects2[k] = gdjs.Level3Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Level3Code.GDPlayerObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDPlayerObjects2 */
{gdjs.evtsExt__Player__TriggerDeath.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level3Code.eventsList12 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level3Code.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.Level3Code.GDPlayerObjects3[i].getVariableBoolean(gdjs.Level3Code.GDPlayerObjects3[i].getVariables().getFromIndex(4), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level3Code.GDPlayerObjects3[k] = gdjs.Level3Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level3Code.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level3Code.eventsList5(runtimeScene);} //End of subevents
}

}


{


gdjs.Level3Code.eventsList7(runtimeScene);
}


{


gdjs.Level3Code.eventsList9(runtimeScene);
}


{


gdjs.Level3Code.eventsList10(runtimeScene);
}


{


gdjs.Level3Code.eventsList11(runtimeScene);
}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level3Code.GDFlyingDemonObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level3Code.GDFlyingDemonObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level3Code.GDFlyingDemonObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.Level3Code.GDMonsterParticlesObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects2});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects2Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level3Code.GDFlyingDemonObjects2});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects2});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects2Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level3Code.GDFlyingDemonObjects2});
gdjs.Level3Code.eventsList13 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.Level3Code.GDFlyingDemonObjects2);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects2Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDFlyingDemonObjects2 */
/* Reuse gdjs.Level3Code.GDPlayerObjects2 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects, "PlatformerObject", gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level3Code.eventsList14 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.Level3Code.GDFlyingDemonObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.Level3Code.GDFlyingDemonObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects3Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDFlyingDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.Level3Code.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects3Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Level3Code.eventsList13(runtimeScene);
}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.Level3Code.GDFireDemonObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.Level3Code.GDFireDemonObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.Level3Code.GDFireDemonObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.Level3Code.GDMonsterParticlesObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects2});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects2Objects = Hashtable.newFrom({"FireDemon": gdjs.Level3Code.GDFireDemonObjects2});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects2});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects2Objects = Hashtable.newFrom({"FireDemon": gdjs.Level3Code.GDFireDemonObjects2});
gdjs.Level3Code.eventsList15 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level3Code.GDFireDemonObjects2);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects2Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDFireDemonObjects2 */
/* Reuse gdjs.Level3Code.GDPlayerObjects2 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects, "PlatformerObject", gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level3Code.eventsList16 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level3Code.GDFireDemonObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level3Code.GDFireDemonObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects3Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDFireDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.Level3Code.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects3Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level3Code.GDFireDemonObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level3Code.GDFireDemonObjects3.length;i<l;++i) {
    if ( gdjs.Level3Code.GDFireDemonObjects3[i].getBehavior("Animation").getAnimationName() == "Fire" ) {
        isConditionTrue_0 = true;
        gdjs.Level3Code.GDFireDemonObjects3[k] = gdjs.Level3Code.GDFireDemonObjects3[i];
        ++k;
    }
}
gdjs.Level3Code.GDFireDemonObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDFireDemonObjects3 */
{for(var i = 0, len = gdjs.Level3Code.GDFireDemonObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDFireDemonObjects3[i].returnVariable(gdjs.Level3Code.GDFireDemonObjects3[i].getVariables().getFromIndex(2)).setNumber(0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level3Code.GDFireDemonObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level3Code.GDFireDemonObjects3.length;i<l;++i) {
    if ( !(gdjs.Level3Code.GDFireDemonObjects3[i].getBehavior("Animation").getAnimationName() == "Fire") ) {
        isConditionTrue_0 = true;
        gdjs.Level3Code.GDFireDemonObjects3[k] = gdjs.Level3Code.GDFireDemonObjects3[i];
        ++k;
    }
}
gdjs.Level3Code.GDFireDemonObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDFireDemonObjects3 */
{for(var i = 0, len = gdjs.Level3Code.GDFireDemonObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDFireDemonObjects3[i].returnVariable(gdjs.Level3Code.GDFireDemonObjects3[i].getVariables().getFromIndex(2)).setNumber(1);
}
}}

}


{


gdjs.Level3Code.eventsList15(runtimeScene);
}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level3Code.GDHorizontalDemonObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level3Code.GDHorizontalDemonObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level3Code.GDHorizontalDemonObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.Level3Code.GDMonsterParticlesObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects2});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects2Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level3Code.GDHorizontalDemonObjects2});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects2});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects2Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level3Code.GDHorizontalDemonObjects2});
gdjs.Level3Code.eventsList17 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.Level3Code.GDHorizontalDemonObjects2);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects2Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDHorizontalDemonObjects2 */
/* Reuse gdjs.Level3Code.GDPlayerObjects2 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects, "PlatformerObject", gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level3Code.eventsList18 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.Level3Code.GDHorizontalDemonObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.Level3Code.GDHorizontalDemonObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects3Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDHorizontalDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.Level3Code.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects3Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Level3Code.eventsList17(runtimeScene);
}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level3Code.GDSpikeDemon_9595BaseObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level3Code.GDSpikeDemon_9595BaseObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level3Code.GDSpikeDemon_9595BaseObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.Level3Code.GDMonsterParticlesObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.Level3Code.GDMonsterParticlesObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level3Code.GDSpikeDemon_9595BaseObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level3Code.GDSpikeDemon_9595BaseObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects3});
gdjs.Level3Code.eventsList19 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level3Code.GDSpikeDemon_9595BaseObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects4Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDPlayerObjects4 */
/* Reuse gdjs.Level3Code.GDSpikeDemon_9595BaseObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595SpikeObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDPlayerObjects3 */
/* Reuse gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects3 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595SpikeObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level3Code.userFunc0xb61920 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
var NumSpikes = 6;
var SpikeScale = 64;

for (var x = 0; x < objects.length; x++) {
    var SpikeDemonBaseInstnace = objects[x];

    for (var i = 0; i < NumSpikes; i++) {
        var SpikeAngle = (360/NumSpikes)*i;

        const Spike = runtimeScene.createObject("SpikeDemon_Spike");
        Spike.setWidth(SpikeScale);
        Spike.setHeight(SpikeScale);
        var CenterX = SpikeDemonBaseInstnace.x + SpikeDemonBaseInstnace.getWidth()/4;
        var CenterY = SpikeDemonBaseInstnace.y + SpikeDemonBaseInstnace.getHeight()/4;
        Spike.setPosition(CenterX, CenterY);
        Spike.setAngle(SpikeAngle);
        
        Spike.setLayer("Base Layer");


    }
}
};
gdjs.Level3Code.eventsList20 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level3Code.GDSpikeDemon_9595BaseObjects4);

var objects = [];
objects.push.apply(objects,gdjs.Level3Code.GDSpikeDemon_9595BaseObjects4);
gdjs.Level3Code.userFunc0xb61920(runtimeScene, objects);

}


};gdjs.Level3Code.eventsList21 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level3Code.GDSpikeDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.Level3Code.GDSpikeDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDSpikeDemon_9595BaseObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(1.5, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level3Code.eventsList22 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy") >= 8;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy");
}
{ //Subevents
gdjs.Level3Code.eventsList20(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy") >= 5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level3Code.eventsList21(runtimeScene);} //End of subevents
}

}


};gdjs.Level3Code.eventsList23 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level3Code.GDSpikeDemon_9595BaseObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level3Code.GDSpikeDemon_9595BaseObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects3Objects);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.Level3Code.GDMonsterParticlesObjects3);
/* Reuse gdjs.Level3Code.GDSpikeDemon_9595BaseObjects3 */
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects3Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595SpikeObjects3Objects);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.Level3Code.GDMonsterParticlesObjects3);
/* Reuse gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects3 */
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595SpikeObjects3Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Level3Code.eventsList19(runtimeScene);
}


{


gdjs.Level3Code.eventsList22(runtimeScene);
}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy");
}}

}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.Level3Code.GDMonsterParticlesObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStaticPlatform1Objects3ObjectsGDgdjs_9546Level3Code_9546GDHorizontalMovingPlatformObjects3ObjectsGDgdjs_9546Level3Code_9546GDStaticPlatform2Objects3ObjectsGDgdjs_9546Level3Code_9546GDStaticPlatform3Objects3Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.Level3Code.GDStaticPlatform1Objects3, "HorizontalMovingPlatform": gdjs.Level3Code.GDHorizontalMovingPlatformObjects3, "StaticPlatform2": gdjs.Level3Code.GDStaticPlatform2Objects3, "StaticPlatform3": gdjs.Level3Code.GDStaticPlatform3Objects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeParticlesObjects3Objects = Hashtable.newFrom({"SpikeParticles": gdjs.Level3Code.GDSpikeParticlesObjects3});
gdjs.Level3Code.eventsList24 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{/* Unknown object - skipped. */}}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.Level3Code.GDHorizontalMovingPlatformObjects3);
/* Reuse gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3 */
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.Level3Code.GDStaticPlatform1Objects3);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.Level3Code.GDStaticPlatform2Objects3);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.Level3Code.GDStaticPlatform3Objects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595SpikeObjects3Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStaticPlatform1Objects3ObjectsGDgdjs_9546Level3Code_9546GDHorizontalMovingPlatformObjects3ObjectsGDgdjs_9546Level3Code_9546GDStaticPlatform2Objects3ObjectsGDgdjs_9546Level3Code_9546GDStaticPlatform3Objects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3 */
gdjs.Level3Code.GDSpikeParticlesObjects3.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeParticlesObjects3Objects, (( gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3[0].getPointX("")), (( gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3[0].getPointY("")), "");
}{for(var i = 0, len = gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3});
gdjs.Level3Code.eventsList25 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects4Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595BaseObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDPlayerObjects4 */
/* Reuse gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike"), gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595SpikeObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDPlayerObjects3 */
/* Reuse gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595SpikeObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level3Code.userFunc0x125ad58 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagtite = objects[x];
    const Spike = runtimeScene.createObject("StalagtiteDemon_Spike");
    Spike.setWidth(96);
    Spike.setHeight(48);
    var CenterX = Stalagtite.x + Stalagtite.getWidth()/3;
    var CenterY = Stalagtite.y + Stalagtite.getHeight() - Stalagtite.getHeight()/3;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(90);
    Spike.setLayer("Base Layer");
}
};
gdjs.Level3Code.eventsList26 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects4);

var objects = [];
objects.push.apply(objects,gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects4);
gdjs.Level3Code.userFunc0x125ad58(runtimeScene, objects);

}


};gdjs.Level3Code.eventsList27 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy") >= 6;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}
{ //Subevents
gdjs.Level3Code.eventsList26(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy") >= 4;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(1.5, 1, 1, 1, 0, 0.02, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level3Code.eventsList28 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595BaseObjects3Objects);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.Level3Code.GDMonsterParticlesObjects3);
/* Reuse gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects3 */
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595BaseObjects3Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike"), gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595SpikeObjects3Objects);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level3Code.eventsList24(runtimeScene);} //End of subevents
}

}


{


gdjs.Level3Code.eventsList25(runtimeScene);
}


{


gdjs.Level3Code.eventsList27(runtimeScene);
}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}}

}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPortalObjects4Objects = Hashtable.newFrom({"Portal": gdjs.Level3Code.GDPortalObjects4});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPortalObjects3Objects = Hashtable.newFrom({"Portal": gdjs.Level3Code.GDPortalObjects3});
gdjs.Level3Code.eventsList29 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.Level3Code.GDPlayerObjects3, gdjs.Level3Code.GDPlayerObjects4);

{for(var i = 0, len = gdjs.Level3Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level3Code.GDPlayerObjects4[i].activateBehavior("PlatformerObject", false);
}
}}

}


{

/* Reuse gdjs.Level3Code.GDPlayerObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level3Code.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.Level3Code.GDPlayerObjects3[i].getVariableNumber(gdjs.Level3Code.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level3Code.GDPlayerObjects3[k] = gdjs.Level3Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level3Code.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDPlayerObjects3 */
/* Reuse gdjs.Level3Code.GDPortalObjects3 */
{gdjs.evtsExt__Player__AnimateFallingIntoPortal.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects, "Tween", gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPortalObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level3Code.eventsList30 = function(runtimeScene) {

{


gdjs.Level3Code.eventsList29(runtimeScene);
}


};gdjs.Level3Code.eventsList31 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level3Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level3Code.GDPlayerObjects4[i].getVariableNumber(gdjs.Level3Code.GDPlayerObjects4[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level3Code.GDPlayerObjects4[k] = gdjs.Level3Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level3Code.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_DeathText"), gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects4);
{for(var i = 0, len = gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects4.length ;i < len;++i) {
    gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects4[i].hide();
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level3Code.GDPlayerObjects3.length;i<l;++i) {
    if ( !(gdjs.Level3Code.GDPlayerObjects3[i].getVariableNumber(gdjs.Level3Code.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 0) ) {
        isConditionTrue_0 = true;
        gdjs.Level3Code.GDPlayerObjects3[k] = gdjs.Level3Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level3Code.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_MemoryAcquired"), gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3);
{for(var i = 0, len = gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3[i].hide();
}
}}

}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDUI_95959595Sinage_95959595BackgroundObjects2Objects = Hashtable.newFrom({"UI_Sinage_Background": gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects2});
gdjs.Level3Code.eventsList32 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26938276);
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level3Code.eventsList31(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_Background"), gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects2);
{gdjs.evtsExt__UserInterface__StretchToFillScreen.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDUI_95959595Sinage_95959595BackgroundObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{}}

}


};gdjs.Level3Code.eventsList33 = function(runtimeScene) {

{


gdjs.Level3Code.eventsList32(runtimeScene);
}


};gdjs.Level3Code.eventsList34 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_Background"), gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects3);
{gdjs.evtTools.camera.hideLayer(runtimeScene, "EndScreen");
}{for(var i = 0, len = gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects3[i].setOpacity(180);
}
}}

}


{



}


{

gdjs.Level3Code.GDPlayerObjects3.length = 0;

gdjs.Level3Code.GDPortalObjects3.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.Level3Code.GDPlayerObjects3_1final.length = 0;
gdjs.Level3Code.GDPortalObjects3_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.Level3Code.GDPortalObjects4);
isConditionTrue_1 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects4Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPortalObjects4Objects, false, runtimeScene, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level3Code.GDPlayerObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level3Code.GDPlayerObjects3_1final.indexOf(gdjs.Level3Code.GDPlayerObjects4[j]) === -1 )
            gdjs.Level3Code.GDPlayerObjects3_1final.push(gdjs.Level3Code.GDPlayerObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level3Code.GDPortalObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level3Code.GDPortalObjects3_1final.indexOf(gdjs.Level3Code.GDPortalObjects4[j]) === -1 )
            gdjs.Level3Code.GDPortalObjects3_1final.push(gdjs.Level3Code.GDPortalObjects4[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects4);
for (var i = 0, k = 0, l = gdjs.Level3Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level3Code.GDPlayerObjects4[i].getVariableNumber(gdjs.Level3Code.GDPlayerObjects4[i].getVariables().getFromIndex(1)) <= 0 ) {
        isConditionTrue_1 = true;
        gdjs.Level3Code.GDPlayerObjects4[k] = gdjs.Level3Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level3Code.GDPlayerObjects4.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level3Code.GDPlayerObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level3Code.GDPlayerObjects3_1final.indexOf(gdjs.Level3Code.GDPlayerObjects4[j]) === -1 )
            gdjs.Level3Code.GDPlayerObjects3_1final.push(gdjs.Level3Code.GDPlayerObjects4[j]);
    }
}
}
{
gdjs.copyArray(gdjs.Level3Code.GDPlayerObjects3_1final, gdjs.Level3Code.GDPlayerObjects3);
gdjs.copyArray(gdjs.Level3Code.GDPortalObjects3_1final, gdjs.Level3Code.GDPortalObjects3);
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26934324);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.camera.showLayer(runtimeScene, "EndScreen");
}
{ //Subevents
gdjs.Level3Code.eventsList30(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "EndScreen");
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level3Code.eventsList33(runtimeScene);} //End of subevents
}

}


};gdjs.Level3Code.eventsList35 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("BoundaryJumpThrough"), gdjs.Level3Code.GDBoundaryJumpThroughObjects4);
gdjs.copyArray(runtimeScene.getObjects("LeftBoundary"), gdjs.Level3Code.GDLeftBoundaryObjects4);
gdjs.copyArray(runtimeScene.getObjects("RightBoundary"), gdjs.Level3Code.GDRightBoundaryObjects4);
{for(var i = 0, len = gdjs.Level3Code.GDLeftBoundaryObjects4.length ;i < len;++i) {
    gdjs.Level3Code.GDLeftBoundaryObjects4[i].hide();
}
for(var i = 0, len = gdjs.Level3Code.GDRightBoundaryObjects4.length ;i < len;++i) {
    gdjs.Level3Code.GDRightBoundaryObjects4[i].hide();
}
for(var i = 0, len = gdjs.Level3Code.GDBoundaryJumpThroughObjects4.length ;i < len;++i) {
    gdjs.Level3Code.GDBoundaryJumpThroughObjects4[i].hide();
}
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("BottomBoundary"), gdjs.Level3Code.GDBottomBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("LeftBoundary"), gdjs.Level3Code.GDLeftBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("RightBoundary"), gdjs.Level3Code.GDRightBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("TopBoundary"), gdjs.Level3Code.GDTopBoundaryObjects3);
{gdjs.evtTools.camera.clampCamera(runtimeScene, (( gdjs.Level3Code.GDLeftBoundaryObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDLeftBoundaryObjects3[0].getPointX("")) + (( gdjs.Level3Code.GDLeftBoundaryObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDLeftBoundaryObjects3[0].getWidth()), (( gdjs.Level3Code.GDTopBoundaryObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDTopBoundaryObjects3[0].getPointY("")) + (( gdjs.Level3Code.GDTopBoundaryObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDTopBoundaryObjects3[0].getHeight()), (( gdjs.Level3Code.GDRightBoundaryObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDRightBoundaryObjects3[0].getPointX("")), (( gdjs.Level3Code.GDBottomBoundaryObjects3.length === 0 ) ? 0 :gdjs.Level3Code.GDBottomBoundaryObjects3[0].getPointY("")), "", 0);
}}

}


};gdjs.Level3Code.eventsList36 = function(runtimeScene) {

{



}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "RandomNoiseTimer");
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("BackgroundPlants"), gdjs.Level3Code.GDBackgroundPlantsObjects3);
{for(var i = 0, len = gdjs.Level3Code.GDBackgroundPlantsObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDBackgroundPlantsObjects3[i].setWidth(gdjs.evtTools.camera.getCameraWidth(runtimeScene, "", 0));
}
}{for(var i = 0, len = gdjs.Level3Code.GDBackgroundPlantsObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDBackgroundPlantsObjects3[i].setXOffset(gdjs.evtTools.camera.getCameraBorderLeft(runtimeScene, "", 0) / 3 + 780);
}
}}

}


};gdjs.Level3Code.eventsList37 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{/* Unknown object - skipped. */}}

}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPortalObjects3Objects = Hashtable.newFrom({"Portal": gdjs.Level3Code.GDPortalObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPortalObjects2Objects = Hashtable.newFrom({"Portal": gdjs.Level3Code.GDPortalObjects2});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level3Code.GDPlayerObjects2});
gdjs.Level3Code.eventsList38 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "door.aac", 0, true, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.Level3Code.GDPortalObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects3Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPortalObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26948276);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Portal/PortalInteract.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects2);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.Level3Code.GDPortalObjects2);
{gdjs.evtsExt__VolumeFalloff__SetVolumeFalloff.func(runtimeScene, 0, "Sound", gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPortalObjects2Objects, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDPlayerObjects2Objects, 0, 100, 750, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level3Code.eventsList39 = function(runtimeScene) {

{


gdjs.Level3Code.eventsList35(runtimeScene);
}


{


gdjs.Level3Code.eventsList36(runtimeScene);
}


{


gdjs.Level3Code.eventsList37(runtimeScene);
}


{


gdjs.Level3Code.eventsList38(runtimeScene);
}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDBoundaryJumpThroughObjects2Objects = Hashtable.newFrom({"BoundaryJumpThrough": gdjs.Level3Code.GDBoundaryJumpThroughObjects2});
gdjs.Level3Code.eventsList40 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("BoundaryJumpThrough"), gdjs.Level3Code.GDBoundaryJumpThroughObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDBoundaryJumpThroughObjects2Objects);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26950020);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level3Code.GDBoundaryJumpThroughObjects2 */
{for(var i = 0, len = gdjs.Level3Code.GDBoundaryJumpThroughObjects2.length ;i < len;++i) {
    gdjs.Level3Code.GDBoundaryJumpThroughObjects2[i].hide();
}
}}

}


};gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level3Code.GDFlyingDemonObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.Level3Code.GDFireDemonObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level3Code.GDHorizontalDemonObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level3Code.GDSpikeDemon_9595BaseObjects3});
gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects3});
gdjs.Level3Code.eventsList41 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Tilde");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26951252);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level3Code.GDFireDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.Level3Code.GDFlyingDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.Level3Code.GDHorizontalDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level3Code.GDSpikeDemon_9595BaseObjects3);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects3);
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFlyingDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDFireDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDHorizontalDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDSpikeDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level3Code.mapOfGDgdjs_9546Level3Code_9546GDStalagtiteDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "i");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26952164);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects3);
{for(var i = 0, len = gdjs.Level3Code.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.Level3Code.GDPlayerObjects3[i].returnVariable(gdjs.Level3Code.GDPlayerObjects3[i].getVariables().getFromIndex(5)).setNumber(1 - gdjs.Level3Code.GDPlayerObjects3[i].getVariables().getFromIndex(5).getAsNumber());
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Escape");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26953716);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, gdjs.evtTools.runtimeScene.getSceneName(runtimeScene), false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "l");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26953788);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "TestingLevel", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "m");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26955156);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Mindscape", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num1");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26955916);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Tutorial", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num2");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26955596);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level1", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num3");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26957276);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level2", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num4");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26957940);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level3", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num5");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26958700);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level4", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num6");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26958380);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level5", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num7");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(26960060);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level6", false);
}}

}


};gdjs.Level3Code.eventsList42 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LifeForceBar"), gdjs.Level3Code.GDLifeForceBarObjects1);
gdjs.copyArray(runtimeScene.getObjects("LivesBar"), gdjs.Level3Code.GDLivesBarObjects1);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects1);
{for(var i = 0, len = gdjs.Level3Code.GDLivesBarObjects1.length ;i < len;++i) {
    gdjs.Level3Code.GDLivesBarObjects1[i].SetValue((gdjs.RuntimeObject.getVariableNumber(((gdjs.Level3Code.GDPlayerObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level3Code.GDPlayerObjects1[0].getVariables()).getFromIndex(1))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.Level3Code.GDLifeForceBarObjects1.length ;i < len;++i) {
    gdjs.Level3Code.GDLifeForceBarObjects1[i].SetValue((gdjs.RuntimeObject.getVariableNumber(((gdjs.Level3Code.GDPlayerObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level3Code.GDPlayerObjects1[0].getVariables()).getFromIndex(3))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level3Code.eventsList43 = function(runtimeScene) {

{


gdjs.Level3Code.eventsList2(runtimeScene);
}


{


gdjs.Level3Code.eventsList4(runtimeScene);
}


{


gdjs.Level3Code.eventsList12(runtimeScene);
}


{


gdjs.Level3Code.eventsList14(runtimeScene);
}


{


gdjs.Level3Code.eventsList16(runtimeScene);
}


{


gdjs.Level3Code.eventsList18(runtimeScene);
}


{


gdjs.Level3Code.eventsList23(runtimeScene);
}


{


gdjs.Level3Code.eventsList28(runtimeScene);
}


{


gdjs.Level3Code.eventsList34(runtimeScene);
}


{


gdjs.Level3Code.eventsList39(runtimeScene);
}


{


gdjs.Level3Code.eventsList40(runtimeScene);
}


{


gdjs.Level3Code.eventsList41(runtimeScene);
}


{


gdjs.Level3Code.eventsList42(runtimeScene);
}


};gdjs.Level3Code.eventsList44 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level3Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Level3Code.GDPlayerObjects2[i].getVariableNumber(gdjs.Level3Code.GDPlayerObjects2[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level3Code.GDPlayerObjects2[k] = gdjs.Level3Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Level3Code.GDPlayerObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Mindscape", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level3Code.GDPlayerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level3Code.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.Level3Code.GDPlayerObjects1[i].getVariableNumber(gdjs.Level3Code.GDPlayerObjects1[i].getVariables().getFromIndex(1)) <= 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level3Code.GDPlayerObjects1[k] = gdjs.Level3Code.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.Level3Code.GDPlayerObjects1.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level3", false);
}}

}


};gdjs.Level3Code.eventsList45 = function(runtimeScene) {

{

gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "EndScreen");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_RetryButton"), gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects2);
for (var i = 0, k = 0, l = gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects2.length;i<l;++i) {
    if ( gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects2[k] = gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects2[i];
        ++k;
    }
}
gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects2.length; j < jLen ; ++j) {
        if ( gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final.indexOf(gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects2[j]) === -1 )
            gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final.push(gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects2[j]);
    }
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "Space");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
gdjs.copyArray(gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final, gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects1);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level3Code.eventsList44(runtimeScene);} //End of subevents
}

}


};gdjs.Level3Code.eventsList46 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 1;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance1.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance2.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 3;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance3.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


};gdjs.Level3Code.eventsList47 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Levels/3/AmbientLoop.ogg", true, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "RandomNoiseTimer") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("TimeBeforeNextRandomSFX"));
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("RandomSFXIndex").setNumber(gdjs.randomInRange(1, 3));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "RandomNoiseTimer");
}{runtimeScene.getScene().getVariables().get("TimeBeforeNextRandomSFX").setNumber(gdjs.randomFloatInRange(30, 240));
}
{ //Subevents
gdjs.Level3Code.eventsList46(runtimeScene);} //End of subevents
}

}


};gdjs.Level3Code.eventsList48 = function(runtimeScene) {

{



}


{


gdjs.Level3Code.eventsList43(runtimeScene);
}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "LevelUI", 0, 0, 0);
}{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "LevelEndScreen", 0, 0, 0);
}}

}


{


gdjs.Level3Code.eventsList45(runtimeScene);
}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Memory"), gdjs.Level3Code.GDMemoryObjects1);
{for(var i = 0, len = gdjs.Level3Code.GDMemoryObjects1.length ;i < len;++i) {
    gdjs.Level3Code.GDMemoryObjects1[i].getBehavior("Animation").setAnimationName("LivingRoom");
}
}}

}


{


gdjs.Level3Code.eventsList47(runtimeScene);
}


};

gdjs.Level3Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Level3Code.GDBackgroundPlantsObjects1.length = 0;
gdjs.Level3Code.GDBackgroundPlantsObjects2.length = 0;
gdjs.Level3Code.GDBackgroundPlantsObjects3.length = 0;
gdjs.Level3Code.GDBackgroundPlantsObjects4.length = 0;
gdjs.Level3Code.GDBackgroundPlantsObjects5.length = 0;
gdjs.Level3Code.GDBackgroundPlantsObjects6.length = 0;
gdjs.Level3Code.GDLeftBoundaryObjects1.length = 0;
gdjs.Level3Code.GDLeftBoundaryObjects2.length = 0;
gdjs.Level3Code.GDLeftBoundaryObjects3.length = 0;
gdjs.Level3Code.GDLeftBoundaryObjects4.length = 0;
gdjs.Level3Code.GDLeftBoundaryObjects5.length = 0;
gdjs.Level3Code.GDLeftBoundaryObjects6.length = 0;
gdjs.Level3Code.GDRightBoundaryObjects1.length = 0;
gdjs.Level3Code.GDRightBoundaryObjects2.length = 0;
gdjs.Level3Code.GDRightBoundaryObjects3.length = 0;
gdjs.Level3Code.GDRightBoundaryObjects4.length = 0;
gdjs.Level3Code.GDRightBoundaryObjects5.length = 0;
gdjs.Level3Code.GDRightBoundaryObjects6.length = 0;
gdjs.Level3Code.GDTopBoundaryObjects1.length = 0;
gdjs.Level3Code.GDTopBoundaryObjects2.length = 0;
gdjs.Level3Code.GDTopBoundaryObjects3.length = 0;
gdjs.Level3Code.GDTopBoundaryObjects4.length = 0;
gdjs.Level3Code.GDTopBoundaryObjects5.length = 0;
gdjs.Level3Code.GDTopBoundaryObjects6.length = 0;
gdjs.Level3Code.GDBottomBoundaryObjects1.length = 0;
gdjs.Level3Code.GDBottomBoundaryObjects2.length = 0;
gdjs.Level3Code.GDBottomBoundaryObjects3.length = 0;
gdjs.Level3Code.GDBottomBoundaryObjects4.length = 0;
gdjs.Level3Code.GDBottomBoundaryObjects5.length = 0;
gdjs.Level3Code.GDBottomBoundaryObjects6.length = 0;
gdjs.Level3Code.GDBoundaryJumpThroughObjects1.length = 0;
gdjs.Level3Code.GDBoundaryJumpThroughObjects2.length = 0;
gdjs.Level3Code.GDBoundaryJumpThroughObjects3.length = 0;
gdjs.Level3Code.GDBoundaryJumpThroughObjects4.length = 0;
gdjs.Level3Code.GDBoundaryJumpThroughObjects5.length = 0;
gdjs.Level3Code.GDBoundaryJumpThroughObjects6.length = 0;
gdjs.Level3Code.GDPlayerObjects1.length = 0;
gdjs.Level3Code.GDPlayerObjects2.length = 0;
gdjs.Level3Code.GDPlayerObjects3.length = 0;
gdjs.Level3Code.GDPlayerObjects4.length = 0;
gdjs.Level3Code.GDPlayerObjects5.length = 0;
gdjs.Level3Code.GDPlayerObjects6.length = 0;
gdjs.Level3Code.GDFlyingDemonObjects1.length = 0;
gdjs.Level3Code.GDFlyingDemonObjects2.length = 0;
gdjs.Level3Code.GDFlyingDemonObjects3.length = 0;
gdjs.Level3Code.GDFlyingDemonObjects4.length = 0;
gdjs.Level3Code.GDFlyingDemonObjects5.length = 0;
gdjs.Level3Code.GDFlyingDemonObjects6.length = 0;
gdjs.Level3Code.GDFireDemonObjects1.length = 0;
gdjs.Level3Code.GDFireDemonObjects2.length = 0;
gdjs.Level3Code.GDFireDemonObjects3.length = 0;
gdjs.Level3Code.GDFireDemonObjects4.length = 0;
gdjs.Level3Code.GDFireDemonObjects5.length = 0;
gdjs.Level3Code.GDFireDemonObjects6.length = 0;
gdjs.Level3Code.GDCheckpointObjects1.length = 0;
gdjs.Level3Code.GDCheckpointObjects2.length = 0;
gdjs.Level3Code.GDCheckpointObjects3.length = 0;
gdjs.Level3Code.GDCheckpointObjects4.length = 0;
gdjs.Level3Code.GDCheckpointObjects5.length = 0;
gdjs.Level3Code.GDCheckpointObjects6.length = 0;
gdjs.Level3Code.GDStaticPlatform3Objects1.length = 0;
gdjs.Level3Code.GDStaticPlatform3Objects2.length = 0;
gdjs.Level3Code.GDStaticPlatform3Objects3.length = 0;
gdjs.Level3Code.GDStaticPlatform3Objects4.length = 0;
gdjs.Level3Code.GDStaticPlatform3Objects5.length = 0;
gdjs.Level3Code.GDStaticPlatform3Objects6.length = 0;
gdjs.Level3Code.GDStaticPlatform2Objects1.length = 0;
gdjs.Level3Code.GDStaticPlatform2Objects2.length = 0;
gdjs.Level3Code.GDStaticPlatform2Objects3.length = 0;
gdjs.Level3Code.GDStaticPlatform2Objects4.length = 0;
gdjs.Level3Code.GDStaticPlatform2Objects5.length = 0;
gdjs.Level3Code.GDStaticPlatform2Objects6.length = 0;
gdjs.Level3Code.GDHorizontalMovingPlatformObjects1.length = 0;
gdjs.Level3Code.GDHorizontalMovingPlatformObjects2.length = 0;
gdjs.Level3Code.GDHorizontalMovingPlatformObjects3.length = 0;
gdjs.Level3Code.GDHorizontalMovingPlatformObjects4.length = 0;
gdjs.Level3Code.GDHorizontalMovingPlatformObjects5.length = 0;
gdjs.Level3Code.GDHorizontalMovingPlatformObjects6.length = 0;
gdjs.Level3Code.GDStaticPlatform1Objects1.length = 0;
gdjs.Level3Code.GDStaticPlatform1Objects2.length = 0;
gdjs.Level3Code.GDStaticPlatform1Objects3.length = 0;
gdjs.Level3Code.GDStaticPlatform1Objects4.length = 0;
gdjs.Level3Code.GDStaticPlatform1Objects5.length = 0;
gdjs.Level3Code.GDStaticPlatform1Objects6.length = 0;
gdjs.Level3Code.GDPortalObjects1.length = 0;
gdjs.Level3Code.GDPortalObjects2.length = 0;
gdjs.Level3Code.GDPortalObjects3.length = 0;
gdjs.Level3Code.GDPortalObjects4.length = 0;
gdjs.Level3Code.GDPortalObjects5.length = 0;
gdjs.Level3Code.GDPortalObjects6.length = 0;
gdjs.Level3Code.GDLadderObjects1.length = 0;
gdjs.Level3Code.GDLadderObjects2.length = 0;
gdjs.Level3Code.GDLadderObjects3.length = 0;
gdjs.Level3Code.GDLadderObjects4.length = 0;
gdjs.Level3Code.GDLadderObjects5.length = 0;
gdjs.Level3Code.GDLadderObjects6.length = 0;
gdjs.Level3Code.GDHeartObjects1.length = 0;
gdjs.Level3Code.GDHeartObjects2.length = 0;
gdjs.Level3Code.GDHeartObjects3.length = 0;
gdjs.Level3Code.GDHeartObjects4.length = 0;
gdjs.Level3Code.GDHeartObjects5.length = 0;
gdjs.Level3Code.GDHeartObjects6.length = 0;
gdjs.Level3Code.GDMonsterParticlesObjects1.length = 0;
gdjs.Level3Code.GDMonsterParticlesObjects2.length = 0;
gdjs.Level3Code.GDMonsterParticlesObjects3.length = 0;
gdjs.Level3Code.GDMonsterParticlesObjects4.length = 0;
gdjs.Level3Code.GDMonsterParticlesObjects5.length = 0;
gdjs.Level3Code.GDMonsterParticlesObjects6.length = 0;
gdjs.Level3Code.GDSpikeParticlesObjects1.length = 0;
gdjs.Level3Code.GDSpikeParticlesObjects2.length = 0;
gdjs.Level3Code.GDSpikeParticlesObjects3.length = 0;
gdjs.Level3Code.GDSpikeParticlesObjects4.length = 0;
gdjs.Level3Code.GDSpikeParticlesObjects5.length = 0;
gdjs.Level3Code.GDSpikeParticlesObjects6.length = 0;
gdjs.Level3Code.GDDoorParticlesObjects1.length = 0;
gdjs.Level3Code.GDDoorParticlesObjects2.length = 0;
gdjs.Level3Code.GDDoorParticlesObjects3.length = 0;
gdjs.Level3Code.GDDoorParticlesObjects4.length = 0;
gdjs.Level3Code.GDDoorParticlesObjects5.length = 0;
gdjs.Level3Code.GDDoorParticlesObjects6.length = 0;
gdjs.Level3Code.GDDustParticleObjects1.length = 0;
gdjs.Level3Code.GDDustParticleObjects2.length = 0;
gdjs.Level3Code.GDDustParticleObjects3.length = 0;
gdjs.Level3Code.GDDustParticleObjects4.length = 0;
gdjs.Level3Code.GDDustParticleObjects5.length = 0;
gdjs.Level3Code.GDDustParticleObjects6.length = 0;
gdjs.Level3Code.GDLivesBarObjects1.length = 0;
gdjs.Level3Code.GDLivesBarObjects2.length = 0;
gdjs.Level3Code.GDLivesBarObjects3.length = 0;
gdjs.Level3Code.GDLivesBarObjects4.length = 0;
gdjs.Level3Code.GDLivesBarObjects5.length = 0;
gdjs.Level3Code.GDLivesBarObjects6.length = 0;
gdjs.Level3Code.GDLifeForceBarObjects1.length = 0;
gdjs.Level3Code.GDLifeForceBarObjects2.length = 0;
gdjs.Level3Code.GDLifeForceBarObjects3.length = 0;
gdjs.Level3Code.GDLifeForceBarObjects4.length = 0;
gdjs.Level3Code.GDLifeForceBarObjects5.length = 0;
gdjs.Level3Code.GDLifeForceBarObjects6.length = 0;
gdjs.Level3Code.GDMemoryObjects1.length = 0;
gdjs.Level3Code.GDMemoryObjects2.length = 0;
gdjs.Level3Code.GDMemoryObjects3.length = 0;
gdjs.Level3Code.GDMemoryObjects4.length = 0;
gdjs.Level3Code.GDMemoryObjects5.length = 0;
gdjs.Level3Code.GDMemoryObjects6.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects1.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects2.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects3.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects4.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects5.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595DeathTextObjects6.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects1.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects2.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects4.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects5.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595MemoryAcquiredObjects6.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595LivesObjects1.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595LivesObjects2.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595LivesObjects3.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595LivesObjects4.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595LivesObjects5.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595LivesObjects6.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595LifeForceObjects1.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595LifeForceObjects2.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595LifeForceObjects3.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595LifeForceObjects4.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595LifeForceObjects5.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595LifeForceObjects6.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects1.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects2.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects3.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects4.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects5.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595RetryButtonObjects6.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects1.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects2.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects3.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects4.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects5.length = 0;
gdjs.Level3Code.GDUI_9595Sinage_9595BackgroundObjects6.length = 0;
gdjs.Level3Code.GDHorizontalDemonObjects1.length = 0;
gdjs.Level3Code.GDHorizontalDemonObjects2.length = 0;
gdjs.Level3Code.GDHorizontalDemonObjects3.length = 0;
gdjs.Level3Code.GDHorizontalDemonObjects4.length = 0;
gdjs.Level3Code.GDHorizontalDemonObjects5.length = 0;
gdjs.Level3Code.GDHorizontalDemonObjects6.length = 0;
gdjs.Level3Code.GDSpikeDemon_9595BaseObjects1.length = 0;
gdjs.Level3Code.GDSpikeDemon_9595BaseObjects2.length = 0;
gdjs.Level3Code.GDSpikeDemon_9595BaseObjects3.length = 0;
gdjs.Level3Code.GDSpikeDemon_9595BaseObjects4.length = 0;
gdjs.Level3Code.GDSpikeDemon_9595BaseObjects5.length = 0;
gdjs.Level3Code.GDSpikeDemon_9595BaseObjects6.length = 0;
gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects1.length = 0;
gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects2.length = 0;
gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects3.length = 0;
gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects4.length = 0;
gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects5.length = 0;
gdjs.Level3Code.GDSpikeDemon_9595SpikeObjects6.length = 0;
gdjs.Level3Code.GDVerticalMovingPlatformObjects1.length = 0;
gdjs.Level3Code.GDVerticalMovingPlatformObjects2.length = 0;
gdjs.Level3Code.GDVerticalMovingPlatformObjects3.length = 0;
gdjs.Level3Code.GDVerticalMovingPlatformObjects4.length = 0;
gdjs.Level3Code.GDVerticalMovingPlatformObjects5.length = 0;
gdjs.Level3Code.GDVerticalMovingPlatformObjects6.length = 0;
gdjs.Level3Code.GDSpinningMovingPlatformObjects1.length = 0;
gdjs.Level3Code.GDSpinningMovingPlatformObjects2.length = 0;
gdjs.Level3Code.GDSpinningMovingPlatformObjects3.length = 0;
gdjs.Level3Code.GDSpinningMovingPlatformObjects4.length = 0;
gdjs.Level3Code.GDSpinningMovingPlatformObjects5.length = 0;
gdjs.Level3Code.GDSpinningMovingPlatformObjects6.length = 0;
gdjs.Level3Code.GDFlippingPlatformObjects1.length = 0;
gdjs.Level3Code.GDFlippingPlatformObjects2.length = 0;
gdjs.Level3Code.GDFlippingPlatformObjects3.length = 0;
gdjs.Level3Code.GDFlippingPlatformObjects4.length = 0;
gdjs.Level3Code.GDFlippingPlatformObjects5.length = 0;
gdjs.Level3Code.GDFlippingPlatformObjects6.length = 0;
gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects1.length = 0;
gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects2.length = 0;
gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects3.length = 0;
gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects4.length = 0;
gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects5.length = 0;
gdjs.Level3Code.GDStalagtiteDemon_9595BaseObjects6.length = 0;
gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects1.length = 0;
gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects2.length = 0;
gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects3.length = 0;
gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects4.length = 0;
gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects5.length = 0;
gdjs.Level3Code.GDStalagtiteDemon_9595SpikeObjects6.length = 0;

gdjs.Level3Code.eventsList48(runtimeScene);

return;

}

gdjs['Level3Code'] = gdjs.Level3Code;
