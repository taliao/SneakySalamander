
if (typeof gdjs.evtsExt__Player__HealPlayer !== "undefined") {
  gdjs.evtsExt__Player__HealPlayer.registeredGdjsCallbacks.forEach(callback =>
    gdjs._unregisterCallback(callback)
  );
}

gdjs.evtsExt__Player__HealPlayer = {};
gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1= [];
gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects2= [];
gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects3= [];


gdjs.evtsExt__Player__HealPlayer.eventsList0 = function(runtimeScene, eventsFunctionContext) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1, gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects2);

{for(var i = 0, len = gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects2[i].returnVariable(gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects2[i].getVariables().get("LifeForce")).sub(6);
}
}}

}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1 */
{for(var i = 0, len = gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1[i].returnVariable(gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1[i].getVariables().get("Lives")).add(1);
}
}}

}


};gdjs.evtsExt__Player__HealPlayer.eventsList1 = function(runtimeScene, eventsFunctionContext) {

{

gdjs.copyArray(eventsFunctionContext.getObjects("Player"), gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1[i].getVariableNumber(gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1[i].getVariables().get("LifeForce")) > 6 ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1[k] = gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1[i].getVariableNumber(gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1[i].getVariables().get("Lives")) < 3 ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1[k] = gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1.length = k;
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.evtsExt__Player__HealPlayer.eventsList0(runtimeScene, eventsFunctionContext);} //End of subevents
}

}


};

gdjs.evtsExt__Player__HealPlayer.func = function(runtimeScene, Player, parentEventsFunctionContext) {
var eventsFunctionContext = {
  _objectsMap: {
"Player": Player
},
  _objectArraysMap: {
"Player": gdjs.objectsListsToArray(Player)
},
  _behaviorNamesMap: {
},
  getObjects: function(objectName) {
    return eventsFunctionContext._objectArraysMap[objectName] || [];
  },
  getObjectsLists: function(objectName) {
    return eventsFunctionContext._objectsMap[objectName] || null;
  },
  getBehaviorName: function(behaviorName) {
    return eventsFunctionContext._behaviorNamesMap[behaviorName] || behaviorName;
  },
  createObject: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    if (objectsList) {
      const object = parentEventsFunctionContext ?
        parentEventsFunctionContext.createObject(objectsList.firstKey()) :
        runtimeScene.createObject(objectsList.firstKey());
      if (object) {
        objectsList.get(objectsList.firstKey()).push(object);
        eventsFunctionContext._objectArraysMap[objectName].push(object);
      }
      return object;    }
    return null;
  },
  getInstancesCountOnScene: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    let count = 0;
    if (objectsList) {
      for(const objectName in objectsList.items)
        count += parentEventsFunctionContext ?
parentEventsFunctionContext.getInstancesCountOnScene(objectName) :
        runtimeScene.getInstancesCountOnScene(objectName);
    }
    return count;
  },
  getLayer: function(layerName) {
    return runtimeScene.getLayer(layerName);
  },
  getArgument: function(argName) {
    return "";
  },
  getOnceTriggers: function() { return runtimeScene.getOnceTriggers(); }
};

gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects1.length = 0;
gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects2.length = 0;
gdjs.evtsExt__Player__HealPlayer.GDPlayerObjects3.length = 0;

gdjs.evtsExt__Player__HealPlayer.eventsList1(runtimeScene, eventsFunctionContext);

return;
}

gdjs.evtsExt__Player__HealPlayer.registeredGdjsCallbacks = [];