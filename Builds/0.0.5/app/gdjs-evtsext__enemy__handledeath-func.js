
if (typeof gdjs.evtsExt__Enemy__HandleDeath !== "undefined") {
  gdjs.evtsExt__Enemy__HandleDeath.registeredGdjsCallbacks.forEach(callback =>
    gdjs._unregisterCallback(callback)
  );
}

gdjs.evtsExt__Enemy__HandleDeath = {};
gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1= [];
gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects2= [];
gdjs.evtsExt__Enemy__HandleDeath.GDDeathParticlesObjects1= [];
gdjs.evtsExt__Enemy__HandleDeath.GDDeathParticlesObjects2= [];


gdjs.evtsExt__Enemy__HandleDeath.mapOfGDgdjs_9546evtsExt_9595_9595Enemy_9595_9595HandleDeath_9546GDDeathParticlesObjects1Objects = Hashtable.newFrom({"DeathParticles": gdjs.evtsExt__Enemy__HandleDeath.GDDeathParticlesObjects1});
gdjs.evtsExt__Enemy__HandleDeath.eventsList0 = function(runtimeScene, eventsFunctionContext) {

{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(eventsFunctionContext.getObjects("Enemy"), gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1);
gdjs.evtsExt__Enemy__HandleDeath.GDDeathParticlesObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.evtsExt__Enemy__HandleDeath.mapOfGDgdjs_9546evtsExt_9595_9595Enemy_9595_9595HandleDeath_9546GDDeathParticlesObjects1Objects, (( gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1.length === 0 ) ? 0 :gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1[0].getX()) + (( gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1.length === 0 ) ? 0 :gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1[0].getWidth()) / 2, (( gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1.length === 0 ) ? 0 :gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1[0].getY()) + (( gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1.length === 0 ) ? 0 :gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1[i].setPosition(0,4000);
}
}{for(var i = 0, len = gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1[i].setVariableBoolean(gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1[i].getVariables().get("hasBeenReaped"), true);
}
}}

}


};

gdjs.evtsExt__Enemy__HandleDeath.func = function(runtimeScene, Enemy, DeathParticles, parentEventsFunctionContext) {
var eventsFunctionContext = {
  _objectsMap: {
"Enemy": Enemy
, "DeathParticles": DeathParticles
},
  _objectArraysMap: {
"Enemy": gdjs.objectsListsToArray(Enemy)
, "DeathParticles": gdjs.objectsListsToArray(DeathParticles)
},
  _behaviorNamesMap: {
},
  getObjects: function(objectName) {
    return eventsFunctionContext._objectArraysMap[objectName] || [];
  },
  getObjectsLists: function(objectName) {
    return eventsFunctionContext._objectsMap[objectName] || null;
  },
  getBehaviorName: function(behaviorName) {
    return eventsFunctionContext._behaviorNamesMap[behaviorName] || behaviorName;
  },
  createObject: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    if (objectsList) {
      const object = parentEventsFunctionContext ?
        parentEventsFunctionContext.createObject(objectsList.firstKey()) :
        runtimeScene.createObject(objectsList.firstKey());
      if (object) {
        objectsList.get(objectsList.firstKey()).push(object);
        eventsFunctionContext._objectArraysMap[objectName].push(object);
      }
      return object;    }
    return null;
  },
  getInstancesCountOnScene: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    let count = 0;
    if (objectsList) {
      for(const objectName in objectsList.items)
        count += parentEventsFunctionContext ?
parentEventsFunctionContext.getInstancesCountOnScene(objectName) :
        runtimeScene.getInstancesCountOnScene(objectName);
    }
    return count;
  },
  getLayer: function(layerName) {
    return runtimeScene.getLayer(layerName);
  },
  getArgument: function(argName) {
    return "";
  },
  getOnceTriggers: function() { return runtimeScene.getOnceTriggers(); }
};

gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects1.length = 0;
gdjs.evtsExt__Enemy__HandleDeath.GDEnemyObjects2.length = 0;
gdjs.evtsExt__Enemy__HandleDeath.GDDeathParticlesObjects1.length = 0;
gdjs.evtsExt__Enemy__HandleDeath.GDDeathParticlesObjects2.length = 0;

gdjs.evtsExt__Enemy__HandleDeath.eventsList0(runtimeScene, eventsFunctionContext);

return;
}

gdjs.evtsExt__Enemy__HandleDeath.registeredGdjsCallbacks = [];