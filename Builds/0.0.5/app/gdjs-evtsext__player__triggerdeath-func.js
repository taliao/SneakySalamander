
if (typeof gdjs.evtsExt__Player__TriggerDeath !== "undefined") {
  gdjs.evtsExt__Player__TriggerDeath.registeredGdjsCallbacks.forEach(callback =>
    gdjs._unregisterCallback(callback)
  );
}

gdjs.evtsExt__Player__TriggerDeath = {};
gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1= [];
gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects2= [];


gdjs.evtsExt__Player__TriggerDeath.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595TriggerDeath_9546GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1});
gdjs.evtsExt__Player__TriggerDeath.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595TriggerDeath_9546GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1});
gdjs.evtsExt__Player__TriggerDeath.eventsList0 = function(runtimeScene, eventsFunctionContext) {

{

/* Reuse gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i].getVariableNumber(gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i].getVariables().get("IsImmortal")) == 0 ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[k] = gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = eventsFunctionContext.getOnceTriggers().triggerOnce(21970076);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1 */
{for(var i = 0, len = gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i].returnVariable(gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i].getVariables().get("Lives")).sub(1);
}
}}

}


};gdjs.evtsExt__Player__TriggerDeath.eventsList1 = function(runtimeScene, eventsFunctionContext) {

{



}


{

gdjs.copyArray(eventsFunctionContext.getObjects("Player"), gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i].getVariableNumber(gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i].getVariables().get("Lives")) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[k] = gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = eventsFunctionContext.getOnceTriggers().triggerOnce(21968356);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1 */
{gdjs.evtsExt__Checkpoints__LoadCheckpoint.func(runtimeScene, gdjs.evtsExt__Player__TriggerDeath.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595TriggerDeath_9546GDPlayerObjects1Objects, gdjs.evtsExt__Player__TriggerDeath.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595TriggerDeath_9546GDPlayerObjects1Objects, "Checkpoint", false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtTools.sound.playSound(runtimeScene, "AssetDev/Audio/DeathSound.wav", false, 50, 1);
}{for(var i = 0, len = gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i].setVariableBoolean(gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i].getVariables().get("NeedsEnemiesReset"), true);
}
}
{ //Subevents
gdjs.evtsExt__Player__TriggerDeath.eventsList0(runtimeScene, eventsFunctionContext);} //End of subevents
}

}


{

gdjs.copyArray(eventsFunctionContext.getObjects("Player"), gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i].getVariableNumber(gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i].getVariables().get("Lives")) <= 0 ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[k] = gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1 */
{for(var i = 0, len = gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i].setVariableBoolean(gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1[i].getVariables().get("IsTrulyDead"), true);
}
}}

}


};

gdjs.evtsExt__Player__TriggerDeath.func = function(runtimeScene, Player, parentEventsFunctionContext) {
var eventsFunctionContext = {
  _objectsMap: {
"Player": Player
},
  _objectArraysMap: {
"Player": gdjs.objectsListsToArray(Player)
},
  _behaviorNamesMap: {
},
  getObjects: function(objectName) {
    return eventsFunctionContext._objectArraysMap[objectName] || [];
  },
  getObjectsLists: function(objectName) {
    return eventsFunctionContext._objectsMap[objectName] || null;
  },
  getBehaviorName: function(behaviorName) {
    return eventsFunctionContext._behaviorNamesMap[behaviorName] || behaviorName;
  },
  createObject: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    if (objectsList) {
      const object = parentEventsFunctionContext ?
        parentEventsFunctionContext.createObject(objectsList.firstKey()) :
        runtimeScene.createObject(objectsList.firstKey());
      if (object) {
        objectsList.get(objectsList.firstKey()).push(object);
        eventsFunctionContext._objectArraysMap[objectName].push(object);
      }
      return object;    }
    return null;
  },
  getInstancesCountOnScene: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    let count = 0;
    if (objectsList) {
      for(const objectName in objectsList.items)
        count += parentEventsFunctionContext ?
parentEventsFunctionContext.getInstancesCountOnScene(objectName) :
        runtimeScene.getInstancesCountOnScene(objectName);
    }
    return count;
  },
  getLayer: function(layerName) {
    return runtimeScene.getLayer(layerName);
  },
  getArgument: function(argName) {
    return "";
  },
  getOnceTriggers: function() { return runtimeScene.getOnceTriggers(); }
};

gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects1.length = 0;
gdjs.evtsExt__Player__TriggerDeath.GDPlayerObjects2.length = 0;

gdjs.evtsExt__Player__TriggerDeath.eventsList1(runtimeScene, eventsFunctionContext);

return;
}

gdjs.evtsExt__Player__TriggerDeath.registeredGdjsCallbacks = [];