
if (typeof gdjs.evtsExt__Player__UpdateLifeForce !== "undefined") {
  gdjs.evtsExt__Player__UpdateLifeForce.registeredGdjsCallbacks.forEach(callback =>
    gdjs._unregisterCallback(callback)
  );
}

gdjs.evtsExt__Player__UpdateLifeForce = {};
gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1= [];
gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects2= [];


gdjs.evtsExt__Player__UpdateLifeForce.eventsList0 = function(runtimeScene, eventsFunctionContext) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(eventsFunctionContext.getObjects("Player"), gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1);
{for(var i = 0, len = gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1[i].returnVariable(gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1[i].getVariables().get("LifeForce")).add(eventsFunctionContext.getArgument("LifeForceToAward"));
}
}}

}


{

gdjs.copyArray(eventsFunctionContext.getObjects("Player"), gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1[i].getVariableNumber(gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1[i].getVariables().get("LifeForce")) > 21 ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1[k] = gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1 */
{for(var i = 0, len = gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1[i].returnVariable(gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1[i].getVariables().get("LifeForce")).setNumber(21);
}
}}

}


};

gdjs.evtsExt__Player__UpdateLifeForce.func = function(runtimeScene, Player, LifeForceToAward, parentEventsFunctionContext) {
var eventsFunctionContext = {
  _objectsMap: {
"Player": Player
},
  _objectArraysMap: {
"Player": gdjs.objectsListsToArray(Player)
},
  _behaviorNamesMap: {
},
  getObjects: function(objectName) {
    return eventsFunctionContext._objectArraysMap[objectName] || [];
  },
  getObjectsLists: function(objectName) {
    return eventsFunctionContext._objectsMap[objectName] || null;
  },
  getBehaviorName: function(behaviorName) {
    return eventsFunctionContext._behaviorNamesMap[behaviorName] || behaviorName;
  },
  createObject: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    if (objectsList) {
      const object = parentEventsFunctionContext ?
        parentEventsFunctionContext.createObject(objectsList.firstKey()) :
        runtimeScene.createObject(objectsList.firstKey());
      if (object) {
        objectsList.get(objectsList.firstKey()).push(object);
        eventsFunctionContext._objectArraysMap[objectName].push(object);
      }
      return object;    }
    return null;
  },
  getInstancesCountOnScene: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    let count = 0;
    if (objectsList) {
      for(const objectName in objectsList.items)
        count += parentEventsFunctionContext ?
parentEventsFunctionContext.getInstancesCountOnScene(objectName) :
        runtimeScene.getInstancesCountOnScene(objectName);
    }
    return count;
  },
  getLayer: function(layerName) {
    return runtimeScene.getLayer(layerName);
  },
  getArgument: function(argName) {
if (argName === "LifeForceToAward") return LifeForceToAward;
    return "";
  },
  getOnceTriggers: function() { return runtimeScene.getOnceTriggers(); }
};

gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects1.length = 0;
gdjs.evtsExt__Player__UpdateLifeForce.GDPlayerObjects2.length = 0;

gdjs.evtsExt__Player__UpdateLifeForce.eventsList0(runtimeScene, eventsFunctionContext);

return;
}

gdjs.evtsExt__Player__UpdateLifeForce.registeredGdjsCallbacks = [];