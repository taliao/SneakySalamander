gdjs.MindscapeCode = {};
gdjs.MindscapeCode.GDPlayerObjects3_1final = [];

gdjs.MindscapeCode.GDPortalObjects3_1final = [];

gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects1_1final = [];

gdjs.MindscapeCode.GDBackgroundPlantsObjects1= [];
gdjs.MindscapeCode.GDBackgroundPlantsObjects2= [];
gdjs.MindscapeCode.GDBackgroundPlantsObjects3= [];
gdjs.MindscapeCode.GDBackgroundPlantsObjects4= [];
gdjs.MindscapeCode.GDBackgroundPlantsObjects5= [];
gdjs.MindscapeCode.GDBackgroundPlantsObjects6= [];
gdjs.MindscapeCode.GDLeftBoundaryObjects1= [];
gdjs.MindscapeCode.GDLeftBoundaryObjects2= [];
gdjs.MindscapeCode.GDLeftBoundaryObjects3= [];
gdjs.MindscapeCode.GDLeftBoundaryObjects4= [];
gdjs.MindscapeCode.GDLeftBoundaryObjects5= [];
gdjs.MindscapeCode.GDLeftBoundaryObjects6= [];
gdjs.MindscapeCode.GDRightBoundaryObjects1= [];
gdjs.MindscapeCode.GDRightBoundaryObjects2= [];
gdjs.MindscapeCode.GDRightBoundaryObjects3= [];
gdjs.MindscapeCode.GDRightBoundaryObjects4= [];
gdjs.MindscapeCode.GDRightBoundaryObjects5= [];
gdjs.MindscapeCode.GDRightBoundaryObjects6= [];
gdjs.MindscapeCode.GDTopBoundaryObjects1= [];
gdjs.MindscapeCode.GDTopBoundaryObjects2= [];
gdjs.MindscapeCode.GDTopBoundaryObjects3= [];
gdjs.MindscapeCode.GDTopBoundaryObjects4= [];
gdjs.MindscapeCode.GDTopBoundaryObjects5= [];
gdjs.MindscapeCode.GDTopBoundaryObjects6= [];
gdjs.MindscapeCode.GDBottomBoundaryObjects1= [];
gdjs.MindscapeCode.GDBottomBoundaryObjects2= [];
gdjs.MindscapeCode.GDBottomBoundaryObjects3= [];
gdjs.MindscapeCode.GDBottomBoundaryObjects4= [];
gdjs.MindscapeCode.GDBottomBoundaryObjects5= [];
gdjs.MindscapeCode.GDBottomBoundaryObjects6= [];
gdjs.MindscapeCode.GDBoundaryJumpThroughObjects1= [];
gdjs.MindscapeCode.GDBoundaryJumpThroughObjects2= [];
gdjs.MindscapeCode.GDBoundaryJumpThroughObjects3= [];
gdjs.MindscapeCode.GDBoundaryJumpThroughObjects4= [];
gdjs.MindscapeCode.GDBoundaryJumpThroughObjects5= [];
gdjs.MindscapeCode.GDBoundaryJumpThroughObjects6= [];
gdjs.MindscapeCode.GDServerRackObjects1= [];
gdjs.MindscapeCode.GDServerRackObjects2= [];
gdjs.MindscapeCode.GDServerRackObjects3= [];
gdjs.MindscapeCode.GDServerRackObjects4= [];
gdjs.MindscapeCode.GDServerRackObjects5= [];
gdjs.MindscapeCode.GDServerRackObjects6= [];
gdjs.MindscapeCode.GDServerObjects1= [];
gdjs.MindscapeCode.GDServerObjects2= [];
gdjs.MindscapeCode.GDServerObjects3= [];
gdjs.MindscapeCode.GDServerObjects4= [];
gdjs.MindscapeCode.GDServerObjects5= [];
gdjs.MindscapeCode.GDServerObjects6= [];
gdjs.MindscapeCode.GDMemoryHolderObjects1= [];
gdjs.MindscapeCode.GDMemoryHolderObjects2= [];
gdjs.MindscapeCode.GDMemoryHolderObjects3= [];
gdjs.MindscapeCode.GDMemoryHolderObjects4= [];
gdjs.MindscapeCode.GDMemoryHolderObjects5= [];
gdjs.MindscapeCode.GDMemoryHolderObjects6= [];
gdjs.MindscapeCode.GDPortalToMountainsObjects1= [];
gdjs.MindscapeCode.GDPortalToMountainsObjects2= [];
gdjs.MindscapeCode.GDPortalToMountainsObjects3= [];
gdjs.MindscapeCode.GDPortalToMountainsObjects4= [];
gdjs.MindscapeCode.GDPortalToMountainsObjects5= [];
gdjs.MindscapeCode.GDPortalToMountainsObjects6= [];
gdjs.MindscapeCode.GDMindscapeFloorObjects1= [];
gdjs.MindscapeCode.GDMindscapeFloorObjects2= [];
gdjs.MindscapeCode.GDMindscapeFloorObjects3= [];
gdjs.MindscapeCode.GDMindscapeFloorObjects4= [];
gdjs.MindscapeCode.GDMindscapeFloorObjects5= [];
gdjs.MindscapeCode.GDMindscapeFloorObjects6= [];
gdjs.MindscapeCode.GDInteractPromptObjects1= [];
gdjs.MindscapeCode.GDInteractPromptObjects2= [];
gdjs.MindscapeCode.GDInteractPromptObjects3= [];
gdjs.MindscapeCode.GDInteractPromptObjects4= [];
gdjs.MindscapeCode.GDInteractPromptObjects5= [];
gdjs.MindscapeCode.GDInteractPromptObjects6= [];
gdjs.MindscapeCode.GDPortalToForestObjects1= [];
gdjs.MindscapeCode.GDPortalToForestObjects2= [];
gdjs.MindscapeCode.GDPortalToForestObjects3= [];
gdjs.MindscapeCode.GDPortalToForestObjects4= [];
gdjs.MindscapeCode.GDPortalToForestObjects5= [];
gdjs.MindscapeCode.GDPortalToForestObjects6= [];
gdjs.MindscapeCode.GDPortalToBeachObjects1= [];
gdjs.MindscapeCode.GDPortalToBeachObjects2= [];
gdjs.MindscapeCode.GDPortalToBeachObjects3= [];
gdjs.MindscapeCode.GDPortalToBeachObjects4= [];
gdjs.MindscapeCode.GDPortalToBeachObjects5= [];
gdjs.MindscapeCode.GDPortalToBeachObjects6= [];
gdjs.MindscapeCode.GDPortalToLivingRoomObjects1= [];
gdjs.MindscapeCode.GDPortalToLivingRoomObjects2= [];
gdjs.MindscapeCode.GDPortalToLivingRoomObjects3= [];
gdjs.MindscapeCode.GDPortalToLivingRoomObjects4= [];
gdjs.MindscapeCode.GDPortalToLivingRoomObjects5= [];
gdjs.MindscapeCode.GDPortalToLivingRoomObjects6= [];
gdjs.MindscapeCode.GDPortalToSchoolObjects1= [];
gdjs.MindscapeCode.GDPortalToSchoolObjects2= [];
gdjs.MindscapeCode.GDPortalToSchoolObjects3= [];
gdjs.MindscapeCode.GDPortalToSchoolObjects4= [];
gdjs.MindscapeCode.GDPortalToSchoolObjects5= [];
gdjs.MindscapeCode.GDPortalToSchoolObjects6= [];
gdjs.MindscapeCode.GDPortalToOceanObjects1= [];
gdjs.MindscapeCode.GDPortalToOceanObjects2= [];
gdjs.MindscapeCode.GDPortalToOceanObjects3= [];
gdjs.MindscapeCode.GDPortalToOceanObjects4= [];
gdjs.MindscapeCode.GDPortalToOceanObjects5= [];
gdjs.MindscapeCode.GDPortalToOceanObjects6= [];
gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects1= [];
gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects2= [];
gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects3= [];
gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects4= [];
gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects5= [];
gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects6= [];
gdjs.MindscapeCode.GDPlayerObjects1= [];
gdjs.MindscapeCode.GDPlayerObjects2= [];
gdjs.MindscapeCode.GDPlayerObjects3= [];
gdjs.MindscapeCode.GDPlayerObjects4= [];
gdjs.MindscapeCode.GDPlayerObjects5= [];
gdjs.MindscapeCode.GDPlayerObjects6= [];
gdjs.MindscapeCode.GDFlyingDemonObjects1= [];
gdjs.MindscapeCode.GDFlyingDemonObjects2= [];
gdjs.MindscapeCode.GDFlyingDemonObjects3= [];
gdjs.MindscapeCode.GDFlyingDemonObjects4= [];
gdjs.MindscapeCode.GDFlyingDemonObjects5= [];
gdjs.MindscapeCode.GDFlyingDemonObjects6= [];
gdjs.MindscapeCode.GDFireDemonObjects1= [];
gdjs.MindscapeCode.GDFireDemonObjects2= [];
gdjs.MindscapeCode.GDFireDemonObjects3= [];
gdjs.MindscapeCode.GDFireDemonObjects4= [];
gdjs.MindscapeCode.GDFireDemonObjects5= [];
gdjs.MindscapeCode.GDFireDemonObjects6= [];
gdjs.MindscapeCode.GDCheckpointObjects1= [];
gdjs.MindscapeCode.GDCheckpointObjects2= [];
gdjs.MindscapeCode.GDCheckpointObjects3= [];
gdjs.MindscapeCode.GDCheckpointObjects4= [];
gdjs.MindscapeCode.GDCheckpointObjects5= [];
gdjs.MindscapeCode.GDCheckpointObjects6= [];
gdjs.MindscapeCode.GDStaticPlatform3Objects1= [];
gdjs.MindscapeCode.GDStaticPlatform3Objects2= [];
gdjs.MindscapeCode.GDStaticPlatform3Objects3= [];
gdjs.MindscapeCode.GDStaticPlatform3Objects4= [];
gdjs.MindscapeCode.GDStaticPlatform3Objects5= [];
gdjs.MindscapeCode.GDStaticPlatform3Objects6= [];
gdjs.MindscapeCode.GDStaticPlatform2Objects1= [];
gdjs.MindscapeCode.GDStaticPlatform2Objects2= [];
gdjs.MindscapeCode.GDStaticPlatform2Objects3= [];
gdjs.MindscapeCode.GDStaticPlatform2Objects4= [];
gdjs.MindscapeCode.GDStaticPlatform2Objects5= [];
gdjs.MindscapeCode.GDStaticPlatform2Objects6= [];
gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects1= [];
gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects2= [];
gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects3= [];
gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects4= [];
gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects5= [];
gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects6= [];
gdjs.MindscapeCode.GDStaticPlatform1Objects1= [];
gdjs.MindscapeCode.GDStaticPlatform1Objects2= [];
gdjs.MindscapeCode.GDStaticPlatform1Objects3= [];
gdjs.MindscapeCode.GDStaticPlatform1Objects4= [];
gdjs.MindscapeCode.GDStaticPlatform1Objects5= [];
gdjs.MindscapeCode.GDStaticPlatform1Objects6= [];
gdjs.MindscapeCode.GDPortalObjects1= [];
gdjs.MindscapeCode.GDPortalObjects2= [];
gdjs.MindscapeCode.GDPortalObjects3= [];
gdjs.MindscapeCode.GDPortalObjects4= [];
gdjs.MindscapeCode.GDPortalObjects5= [];
gdjs.MindscapeCode.GDPortalObjects6= [];
gdjs.MindscapeCode.GDLadderObjects1= [];
gdjs.MindscapeCode.GDLadderObjects2= [];
gdjs.MindscapeCode.GDLadderObjects3= [];
gdjs.MindscapeCode.GDLadderObjects4= [];
gdjs.MindscapeCode.GDLadderObjects5= [];
gdjs.MindscapeCode.GDLadderObjects6= [];
gdjs.MindscapeCode.GDMonsterParticlesObjects1= [];
gdjs.MindscapeCode.GDMonsterParticlesObjects2= [];
gdjs.MindscapeCode.GDMonsterParticlesObjects3= [];
gdjs.MindscapeCode.GDMonsterParticlesObjects4= [];
gdjs.MindscapeCode.GDMonsterParticlesObjects5= [];
gdjs.MindscapeCode.GDMonsterParticlesObjects6= [];
gdjs.MindscapeCode.GDSpikeParticlesObjects1= [];
gdjs.MindscapeCode.GDSpikeParticlesObjects2= [];
gdjs.MindscapeCode.GDSpikeParticlesObjects3= [];
gdjs.MindscapeCode.GDSpikeParticlesObjects4= [];
gdjs.MindscapeCode.GDSpikeParticlesObjects5= [];
gdjs.MindscapeCode.GDSpikeParticlesObjects6= [];
gdjs.MindscapeCode.GDDoorParticlesObjects1= [];
gdjs.MindscapeCode.GDDoorParticlesObjects2= [];
gdjs.MindscapeCode.GDDoorParticlesObjects3= [];
gdjs.MindscapeCode.GDDoorParticlesObjects4= [];
gdjs.MindscapeCode.GDDoorParticlesObjects5= [];
gdjs.MindscapeCode.GDDoorParticlesObjects6= [];
gdjs.MindscapeCode.GDDustParticleObjects1= [];
gdjs.MindscapeCode.GDDustParticleObjects2= [];
gdjs.MindscapeCode.GDDustParticleObjects3= [];
gdjs.MindscapeCode.GDDustParticleObjects4= [];
gdjs.MindscapeCode.GDDustParticleObjects5= [];
gdjs.MindscapeCode.GDDustParticleObjects6= [];
gdjs.MindscapeCode.GDLivesBarObjects1= [];
gdjs.MindscapeCode.GDLivesBarObjects2= [];
gdjs.MindscapeCode.GDLivesBarObjects3= [];
gdjs.MindscapeCode.GDLivesBarObjects4= [];
gdjs.MindscapeCode.GDLivesBarObjects5= [];
gdjs.MindscapeCode.GDLivesBarObjects6= [];
gdjs.MindscapeCode.GDHopeBarObjects1= [];
gdjs.MindscapeCode.GDHopeBarObjects2= [];
gdjs.MindscapeCode.GDHopeBarObjects3= [];
gdjs.MindscapeCode.GDHopeBarObjects4= [];
gdjs.MindscapeCode.GDHopeBarObjects5= [];
gdjs.MindscapeCode.GDHopeBarObjects6= [];
gdjs.MindscapeCode.GDMemoryObjects1= [];
gdjs.MindscapeCode.GDMemoryObjects2= [];
gdjs.MindscapeCode.GDMemoryObjects3= [];
gdjs.MindscapeCode.GDMemoryObjects4= [];
gdjs.MindscapeCode.GDMemoryObjects5= [];
gdjs.MindscapeCode.GDMemoryObjects6= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects1= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects2= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects3= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects4= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects5= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects6= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects1= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects2= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects4= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects5= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects6= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595LivesObjects1= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595LivesObjects2= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595LivesObjects3= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595LivesObjects4= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595LivesObjects5= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595LivesObjects6= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595HopeObjects1= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595HopeObjects2= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595HopeObjects3= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595HopeObjects4= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595HopeObjects5= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595HopeObjects6= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects1= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects3= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects4= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects5= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects6= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects1= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects2= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects3= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects4= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects5= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects6= [];
gdjs.MindscapeCode.GDHorizontalDemonObjects1= [];
gdjs.MindscapeCode.GDHorizontalDemonObjects2= [];
gdjs.MindscapeCode.GDHorizontalDemonObjects3= [];
gdjs.MindscapeCode.GDHorizontalDemonObjects4= [];
gdjs.MindscapeCode.GDHorizontalDemonObjects5= [];
gdjs.MindscapeCode.GDHorizontalDemonObjects6= [];
gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects1= [];
gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects2= [];
gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects3= [];
gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects4= [];
gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5= [];
gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects6= [];
gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects1= [];
gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects2= [];
gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects3= [];
gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects4= [];
gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects5= [];
gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects6= [];
gdjs.MindscapeCode.GDVerticalMovingPlatformObjects1= [];
gdjs.MindscapeCode.GDVerticalMovingPlatformObjects2= [];
gdjs.MindscapeCode.GDVerticalMovingPlatformObjects3= [];
gdjs.MindscapeCode.GDVerticalMovingPlatformObjects4= [];
gdjs.MindscapeCode.GDVerticalMovingPlatformObjects5= [];
gdjs.MindscapeCode.GDVerticalMovingPlatformObjects6= [];
gdjs.MindscapeCode.GDSpinningMovingPlatformObjects1= [];
gdjs.MindscapeCode.GDSpinningMovingPlatformObjects2= [];
gdjs.MindscapeCode.GDSpinningMovingPlatformObjects3= [];
gdjs.MindscapeCode.GDSpinningMovingPlatformObjects4= [];
gdjs.MindscapeCode.GDSpinningMovingPlatformObjects5= [];
gdjs.MindscapeCode.GDSpinningMovingPlatformObjects6= [];
gdjs.MindscapeCode.GDFlippingPlatformObjects1= [];
gdjs.MindscapeCode.GDFlippingPlatformObjects2= [];
gdjs.MindscapeCode.GDFlippingPlatformObjects3= [];
gdjs.MindscapeCode.GDFlippingPlatformObjects4= [];
gdjs.MindscapeCode.GDFlippingPlatformObjects5= [];
gdjs.MindscapeCode.GDFlippingPlatformObjects6= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects1= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects2= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects3= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects4= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects5= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects6= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects1= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects2= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects3= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects5= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects6= [];
gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects1= [];
gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects2= [];
gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects3= [];
gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects4= [];
gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5= [];
gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects6= [];
gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects1= [];
gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects2= [];
gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects3= [];
gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects4= [];
gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects5= [];
gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects6= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects1= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects2= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects3= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects4= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects5= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects6= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects1= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects2= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects3= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects4= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects5= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects6= [];
gdjs.MindscapeCode.GDFlippingPlatform2Objects1= [];
gdjs.MindscapeCode.GDFlippingPlatform2Objects2= [];
gdjs.MindscapeCode.GDFlippingPlatform2Objects3= [];
gdjs.MindscapeCode.GDFlippingPlatform2Objects4= [];
gdjs.MindscapeCode.GDFlippingPlatform2Objects5= [];
gdjs.MindscapeCode.GDFlippingPlatform2Objects6= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects1= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects2= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects3= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects4= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects5= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects6= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects1= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects2= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects3= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects5= [];
gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects6= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects1= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects2= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects3= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects5= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects6= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects1= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects2= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects3= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects5= [];
gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects6= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects1= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects2= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects3= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects4= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects5= [];
gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects6= [];
gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects1= [];
gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects2= [];
gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3= [];
gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4= [];
gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects5= [];
gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects6= [];
gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects1= [];
gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects2= [];
gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3= [];
gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4= [];
gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects5= [];
gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects6= [];
gdjs.MindscapeCode.GDTestObjects1= [];
gdjs.MindscapeCode.GDTestObjects2= [];
gdjs.MindscapeCode.GDTestObjects3= [];
gdjs.MindscapeCode.GDTestObjects4= [];
gdjs.MindscapeCode.GDTestObjects5= [];
gdjs.MindscapeCode.GDTestObjects6= [];


gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects4Objects = Hashtable.newFrom({"HorizontalMovingPlatform": gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDVerticalMovingPlatformObjects4Objects = Hashtable.newFrom({"VerticalMovingPlatform": gdjs.MindscapeCode.GDVerticalMovingPlatformObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpinningMovingPlatformObjects3Objects = Hashtable.newFrom({"SpinningMovingPlatform": gdjs.MindscapeCode.GDSpinningMovingPlatformObjects3});
gdjs.MindscapeCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.MindscapeCode.GDVerticalMovingPlatformObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDVerticalMovingPlatformObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.MindscapeCode.GDSpinningMovingPlatformObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpinningMovingPlatformObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.MindscapeCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.MindscapeCode.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.MindscapeCode.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDFlippingPlatformObjects4[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.MindscapeCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.MindscapeCode.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.MindscapeCode.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDFlippingPlatformObjects4[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.MindscapeCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") <= 3;
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList1(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 3;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.MindscapeCode.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.MindscapeCode.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDFlippingPlatformObjects4[i].rotateTowardAngle(-(80), 0, runtimeScene);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList2(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 6;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.MindscapeCode.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.MindscapeCode.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDFlippingPlatformObjects4[i].rotateTowardAngle(0, 0, runtimeScene);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


};gdjs.MindscapeCode.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.MindscapeCode.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDFlippingPlatform2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.MindscapeCode.eventsList5 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.MindscapeCode.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDFlippingPlatform2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.MindscapeCode.asyncCallback23166852 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}}
gdjs.MindscapeCode.eventsList6 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23166852(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.eventsList7 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") <= 3;
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList4(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 3;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.MindscapeCode.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDFlippingPlatform2Objects3[i].rotateTowardAngle(-(80), 0, runtimeScene);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList5(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 6;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.MindscapeCode.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDFlippingPlatform2Objects3[i].rotateTowardAngle(0, 0, runtimeScene);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList6(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.eventsList8 = function(runtimeScene) {

{


gdjs.MindscapeCode.eventsList0(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList3(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList7(runtimeScene);
}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.eventsList9 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "w");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);
{for(var i = 0, len = gdjs.MindscapeCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Up");
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Ladder");
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "a");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDPlayerObjects4[i].getX() >= 0 ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDPlayerObjects4[k] = gdjs.MindscapeCode.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDPlayerObjects4.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects4 */
{for(var i = 0, len = gdjs.MindscapeCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Left");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "d");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);
{for(var i = 0, len = gdjs.MindscapeCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Right");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);
{for(var i = 0, len = gdjs.MindscapeCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Jump");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "s");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);
{for(var i = 0, len = gdjs.MindscapeCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Down");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "LShift");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "RShift");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23174796);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);
{gdjs.evtsExt__Player__HealPlayer.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDCheckpointObjects3Objects = Hashtable.newFrom({"Checkpoint": gdjs.MindscapeCode.GDCheckpointObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.eventsList10 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.MindscapeCode.GDCheckpointObjects3 */
/* Reuse gdjs.MindscapeCode.GDPlayerObjects3 */
{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, (( gdjs.MindscapeCode.GDCheckpointObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDCheckpointObjects3[0].getPointX("")), (( gdjs.MindscapeCode.GDCheckpointObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDCheckpointObjects3[0].getPointY("")), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.MindscapeCode.GDCheckpointObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDCheckpointObjects3[i].getBehavior("Animation").setAnimationName("Activate");
}
}}

}


};gdjs.MindscapeCode.eventsList11 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);
{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, (( gdjs.MindscapeCode.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPlayerObjects4[0].getPointX("")), (( gdjs.MindscapeCode.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPlayerObjects4[0].getPointY("")), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Checkpoint"), gdjs.MindscapeCode.GDCheckpointObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDCheckpointObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDCheckpointObjects3.length;i<l;++i) {
    if ( !(gdjs.MindscapeCode.GDCheckpointObjects3[i].isCurrentAnimationName("Activate")) ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDCheckpointObjects3[k] = gdjs.MindscapeCode.GDCheckpointObjects3[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDCheckpointObjects3.length = k;
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Checkpoint/Activate.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}
{ //Subevents
gdjs.MindscapeCode.eventsList10(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.eventsList12 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDPlayerObjects3[i].getY() > gdjs.evtTools.camera.getCameraBorderBottom(runtimeScene, "", 0) ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDPlayerObjects3[k] = gdjs.MindscapeCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects3 */
{gdjs.evtsExt__Player__TriggerDeath.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.MindscapeCode.eventsList13 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "AssetDev/Audio/Heartbeat_Amplified.wav", 2, true, 100, 1);
}{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDPlayerObjects4[i].getVariableNumber(gdjs.MindscapeCode.GDPlayerObjects4[i].getVariables().getFromIndex(1)) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDPlayerObjects4[k] = gdjs.MindscapeCode.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 60);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.getSoundOnChannelVolume(runtimeScene, 2) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDPlayerObjects3[i].getVariableNumber(gdjs.MindscapeCode.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 1 ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDPlayerObjects3[k] = gdjs.MindscapeCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDPlayerObjects3.length = k;
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 0);
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects2});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDDustParticleObjects2Objects = Hashtable.newFrom({"DustParticle": gdjs.MindscapeCode.GDDustParticleObjects2});
gdjs.MindscapeCode.eventsList14 = function(runtimeScene) {

{


gdjs.MindscapeCode.eventsList13(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDPlayerObjects3[i].getBehavior("PlatformerObject").isJumping() ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDPlayerObjects3[k] = gdjs.MindscapeCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23183324);
}
}
if (isConditionTrue_0) {
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtsExt__Player__IsSteppingOnFloor.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects, "PlatformerObject", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23183804);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects2 */
gdjs.MindscapeCode.GDDustParticleObjects2.length = 0;

{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "grass.mp3", 1, false, 20, gdjs.randomFloatInRange(0.7, 1.2));
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDDustParticleObjects2Objects, (( gdjs.MindscapeCode.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPlayerObjects2[0].getAABBCenterX()), (( gdjs.MindscapeCode.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPlayerObjects2[0].getAABBBottom()), "");
}{for(var i = 0, len = gdjs.MindscapeCode.GDDustParticleObjects2.length ;i < len;++i) {
    gdjs.MindscapeCode.GDDustParticleObjects2[i].setZOrder(-(1));
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDDustParticleObjects2.length ;i < len;++i) {
    gdjs.MindscapeCode.GDDustParticleObjects2[i].setAngle(270);
}
}}

}


};gdjs.MindscapeCode.eventsList15 = function(runtimeScene) {

{


gdjs.MindscapeCode.eventsList9(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList11(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList12(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList14(runtimeScene);
}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlippingPlatformObjects5Objects = Hashtable.newFrom({"FlippingPlatform": gdjs.MindscapeCode.GDFlippingPlatformObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlippingPlatform2Objects5Objects = Hashtable.newFrom({"FlippingPlatform2": gdjs.MindscapeCode.GDFlippingPlatform2Objects5});
gdjs.MindscapeCode.asyncCallback23187724 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}}
gdjs.MindscapeCode.eventsList16 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23187724(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects5Objects = Hashtable.newFrom({"HorizontalMovingPlatform": gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects5Objects = Hashtable.newFrom({"HorizontalMovingPlatform": gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDVerticalMovingPlatformObjects5Objects = Hashtable.newFrom({"VerticalMovingPlatform": gdjs.MindscapeCode.GDVerticalMovingPlatformObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDVerticalMovingPlatformObjects5Objects = Hashtable.newFrom({"VerticalMovingPlatform": gdjs.MindscapeCode.GDVerticalMovingPlatformObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpinningMovingPlatformObjects4Objects = Hashtable.newFrom({"SpinningMovingPlatform": gdjs.MindscapeCode.GDSpinningMovingPlatformObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpinningMovingPlatformObjects4Objects = Hashtable.newFrom({"SpinningMovingPlatform": gdjs.MindscapeCode.GDSpinningMovingPlatformObjects4});
gdjs.MindscapeCode.eventsList17 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.MindscapeCode.GDFlippingPlatformObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlippingPlatformObjects5Objects);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.MindscapeCode.GDFlippingPlatform2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlippingPlatform2Objects5Objects);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}
{ //Subevents
gdjs.MindscapeCode.eventsList16(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects5 */
{gdjs.evtsExt__Enemy__ResetPlatform.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.MindscapeCode.GDVerticalMovingPlatformObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDVerticalMovingPlatformObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDVerticalMovingPlatformObjects5 */
{gdjs.evtsExt__Enemy__ResetPlatform.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDVerticalMovingPlatformObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.MindscapeCode.GDSpinningMovingPlatformObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpinningMovingPlatformObjects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDSpinningMovingPlatformObjects4 */
{gdjs.evtsExt__Enemy__ResetPlatform.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpinningMovingPlatformObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects5Objects = Hashtable.newFrom({"FlyingDemon": gdjs.MindscapeCode.GDFlyingDemonObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects5Objects = Hashtable.newFrom({"FlyingDemon": gdjs.MindscapeCode.GDFlyingDemonObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects5Objects = Hashtable.newFrom({"FireDemon": gdjs.MindscapeCode.GDFireDemonObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects5Objects = Hashtable.newFrom({"FireDemon": gdjs.MindscapeCode.GDFireDemonObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects5Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.MindscapeCode.GDHorizontalDemonObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects5Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.MindscapeCode.GDHorizontalDemonObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects4});
gdjs.MindscapeCode.eventsList18 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.MindscapeCode.GDFlyingDemonObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDFlyingDemonObjects5 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.MindscapeCode.GDFireDemonObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDFireDemonObjects5 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.MindscapeCode.GDHorizontalDemonObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDHorizontalDemonObjects5 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Base2Objects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects4 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Base2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595SpikeObjects5Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Spike2Objects5Objects = Hashtable.newFrom({"SpikeDemon_Spike2": gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595SpikeObjects5Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Spike2Objects5Objects = Hashtable.newFrom({"StalagtiteDemon_Spike2": gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595SpikeObjects5Objects = Hashtable.newFrom({"StalagmiteDemon_Spike": gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike2": gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4});
gdjs.MindscapeCode.eventsList19 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595SpikeObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects5 */
{for(var i = 0, len = gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike2"), gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Spike2Objects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects5 */
{for(var i = 0, len = gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects5.length ;i < len;++i) {
    gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike"), gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595SpikeObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects5 */
{for(var i = 0, len = gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike2"), gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Spike2Objects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects5 */
{for(var i = 0, len = gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects5.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike"), gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595SpikeObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects5 */
{for(var i = 0, len = gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike2"), gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4 */
{for(var i = 0, len = gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.MindscapeCode.asyncCallback23199180 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy2");
}}
gdjs.MindscapeCode.eventsList20 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23199180(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.asyncCallback23199940 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}}
gdjs.MindscapeCode.eventsList21 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23199940(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.asyncCallback23200748 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy2");
}}
gdjs.MindscapeCode.eventsList22 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(4.25), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23200748(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.asyncCallback23201476 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy");
}}
gdjs.MindscapeCode.eventsList23 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23201476(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.asyncCallback23202252 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy2");
}}
gdjs.MindscapeCode.eventsList24 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(4.25), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23202252(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.eventsList25 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4);
{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4[i].setPosition(0,6000);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.25);
}
}}

}


};gdjs.MindscapeCode.eventsList26 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy2");
}
{ //Subevents
gdjs.MindscapeCode.eventsList20(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}
{ //Subevents
gdjs.MindscapeCode.eventsList21(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy2");
}
{ //Subevents
gdjs.MindscapeCode.eventsList22(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy");
}
{ //Subevents
gdjs.MindscapeCode.eventsList23(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy2");
}
{ //Subevents
gdjs.MindscapeCode.eventsList24(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "LaserEnemy");
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4);
{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4[i].getBehavior("Opacity").setOpacity(40);
}
}
{ //Subevents
gdjs.MindscapeCode.eventsList25(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.eventsList27 = function(runtimeScene) {

{


gdjs.MindscapeCode.eventsList17(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList18(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList19(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList26(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.MindscapeCode.GDPlayerObjects3 */
{for(var i = 0, len = gdjs.MindscapeCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDPlayerObjects3[i].setVariableBoolean(gdjs.MindscapeCode.GDPlayerObjects3[i].getVariables().getFromIndex(4), false);
}
}}

}


};gdjs.MindscapeCode.eventsList28 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDPlayerObjects3[i].getVariableBoolean(gdjs.MindscapeCode.GDPlayerObjects3[i].getVariables().getFromIndex(4), true) ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDPlayerObjects3[k] = gdjs.MindscapeCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList27(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects4Objects = Hashtable.newFrom({"FlyingDemon": gdjs.MindscapeCode.GDFlyingDemonObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.MindscapeCode.GDFlyingDemonObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.MindscapeCode.GDFlyingDemonObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.MindscapeCode.GDFlyingDemonObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.MindscapeCode.GDMonsterParticlesObjects3});
gdjs.MindscapeCode.eventsList29 = function(runtimeScene) {

{

/* Reuse gdjs.MindscapeCode.GDFlyingDemonObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDFlyingDemonObjects3.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDFlyingDemonObjects3[i].getVariableBoolean(gdjs.MindscapeCode.GDFlyingDemonObjects3[i].getVariables().getFromIndex(1), true) ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDFlyingDemonObjects3[k] = gdjs.MindscapeCode.GDFlyingDemonObjects3[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDFlyingDemonObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDFlyingDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.MindscapeCode.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects3Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.MindscapeCode.eventsList30 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.MindscapeCode.GDFlyingDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDFlyingDemonObjects3 */
/* Reuse gdjs.MindscapeCode.GDPlayerObjects3 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.MindscapeCode.eventsList29(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.eventsList31 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.MindscapeCode.GDFlyingDemonObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.MindscapeCode.eventsList30(runtimeScene);
}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects4Objects = Hashtable.newFrom({"FireDemon": gdjs.MindscapeCode.GDFireDemonObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.MindscapeCode.GDFireDemonObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.MindscapeCode.GDFireDemonObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.MindscapeCode.GDFireDemonObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.MindscapeCode.GDMonsterParticlesObjects3});
gdjs.MindscapeCode.eventsList32 = function(runtimeScene) {

{

/* Reuse gdjs.MindscapeCode.GDFireDemonObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDFireDemonObjects3.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDFireDemonObjects3[i].getVariableBoolean(gdjs.MindscapeCode.GDFireDemonObjects3[i].getVariables().getFromIndex(2), true) ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDFireDemonObjects3[k] = gdjs.MindscapeCode.GDFireDemonObjects3[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDFireDemonObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDFireDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.MindscapeCode.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects3Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.MindscapeCode.eventsList33 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.MindscapeCode.GDFireDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDFireDemonObjects3 */
/* Reuse gdjs.MindscapeCode.GDPlayerObjects3 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.MindscapeCode.eventsList32(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.eventsList34 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.MindscapeCode.GDFireDemonObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.MindscapeCode.GDFireDemonObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDFireDemonObjects4.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDFireDemonObjects4[i].getBehavior("Animation").getAnimationName() == "Fire" ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDFireDemonObjects4[k] = gdjs.MindscapeCode.GDFireDemonObjects4[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDFireDemonObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDFireDemonObjects4 */
{for(var i = 0, len = gdjs.MindscapeCode.GDFireDemonObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDFireDemonObjects4[i].returnVariable(gdjs.MindscapeCode.GDFireDemonObjects4[i].getVariables().getFromIndex(1)).setNumber(0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.MindscapeCode.GDFireDemonObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDFireDemonObjects4.length;i<l;++i) {
    if ( !(gdjs.MindscapeCode.GDFireDemonObjects4[i].getBehavior("Animation").getAnimationName() == "Fire") ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDFireDemonObjects4[k] = gdjs.MindscapeCode.GDFireDemonObjects4[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDFireDemonObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDFireDemonObjects4 */
{for(var i = 0, len = gdjs.MindscapeCode.GDFireDemonObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDFireDemonObjects4[i].returnVariable(gdjs.MindscapeCode.GDFireDemonObjects4[i].getVariables().getFromIndex(1)).setNumber(1);
}
}}

}


{


gdjs.MindscapeCode.eventsList33(runtimeScene);
}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects4Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.MindscapeCode.GDHorizontalDemonObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.MindscapeCode.GDHorizontalDemonObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.MindscapeCode.GDHorizontalDemonObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.MindscapeCode.GDHorizontalDemonObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.MindscapeCode.GDMonsterParticlesObjects3});
gdjs.MindscapeCode.eventsList35 = function(runtimeScene) {

{

/* Reuse gdjs.MindscapeCode.GDHorizontalDemonObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDHorizontalDemonObjects3.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDHorizontalDemonObjects3[i].getVariableBoolean(gdjs.MindscapeCode.GDHorizontalDemonObjects3[i].getVariables().getFromIndex(1), true) ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDHorizontalDemonObjects3[k] = gdjs.MindscapeCode.GDHorizontalDemonObjects3[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDHorizontalDemonObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDHorizontalDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.MindscapeCode.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects3Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.MindscapeCode.eventsList36 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.MindscapeCode.GDHorizontalDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDHorizontalDemonObjects3 */
/* Reuse gdjs.MindscapeCode.GDPlayerObjects3 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.MindscapeCode.eventsList35(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.eventsList37 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.MindscapeCode.GDHorizontalDemonObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.MindscapeCode.eventsList36(runtimeScene);
}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDMonsterParticlesObjects5Objects = Hashtable.newFrom({"MonsterParticles": gdjs.MindscapeCode.GDMonsterParticlesObjects5});
gdjs.MindscapeCode.eventsList38 = function(runtimeScene) {

{

/* Reuse gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5[i].getVariableBoolean(gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5[i].getVariables().getFromIndex(1), true) ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5[k] = gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.MindscapeCode.GDMonsterParticlesObjects5);
/* Reuse gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5 */
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects5Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDMonsterParticlesObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects4});
gdjs.MindscapeCode.eventsList39 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects5 */
/* Reuse gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.MindscapeCode.eventsList38(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595SpikeObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects4 */
/* Reuse gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595SpikeObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.MindscapeCode.userFunc0x11c5cb0 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
var NumSpikes = 6;
var SpikeScale = 64;

for (var x = 0; x < objects.length; x++) {
    var SpikeDemonBaseInstance = objects[x];

    for (var i = 0; i < NumSpikes; i++) {
        var SpikeAngle = (360/NumSpikes)*i;

        const Spike = runtimeScene.createObject("SpikeDemon_Spike");
        Spike.setWidth(SpikeScale);
        Spike.setHeight(SpikeScale);
        console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
        var CenterX = SpikeDemonBaseInstance.x + SpikeDemonBaseInstance.getWidth()/3.6;
        var CenterY = SpikeDemonBaseInstance.y + SpikeDemonBaseInstance.getHeight()/3.2;
        Spike.setPosition(CenterX, CenterY);
        Spike.setAngle(SpikeAngle);
        
        Spike.setLayer("Base Layer");


    }
}
};
gdjs.MindscapeCode.eventsList40 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects4);

var objects = [];
objects.push.apply(objects,gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects4);
gdjs.MindscapeCode.userFunc0x11c5cb0(runtimeScene, objects);

}


};gdjs.MindscapeCode.eventsList41 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 4, 4, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.MindscapeCode.eventsList42 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy") >= 6;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy");
}
{ //Subevents
gdjs.MindscapeCode.eventsList40(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy") >= 4.5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList41(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.eventsList43 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.MindscapeCode.eventsList39(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList42(runtimeScene);
}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDMonsterParticlesObjects5Objects = Hashtable.newFrom({"MonsterParticles": gdjs.MindscapeCode.GDMonsterParticlesObjects5});
gdjs.MindscapeCode.eventsList44 = function(runtimeScene) {

{

/* Reuse gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5[i].getVariableBoolean(gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5[i].getVariables().getFromIndex(1), true) ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5[k] = gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.MindscapeCode.GDMonsterParticlesObjects5);
/* Reuse gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5 */
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Base2Objects5Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDMonsterParticlesObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Spike2": gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Spike2": gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects4});
gdjs.MindscapeCode.eventsList45 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Base2Objects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects5 */
/* Reuse gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Base2Objects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.MindscapeCode.eventsList44(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike2"), gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Spike2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects4 */
/* Reuse gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Spike2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.MindscapeCode.asyncCallback23227740 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy2");
}}
gdjs.MindscapeCode.eventsList46 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23227740(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.userFunc0x10a8668 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
var NumSpikes = 6;
var SpikeScale = 64;

for (var x = 0; x < objects.length; x++) {
    var SpikeDemonBaseInstance = objects[x];

    for (var i = 0; i < NumSpikes; i++) {
        var SpikeAngle = (360/NumSpikes)*i;

        const Spike = runtimeScene.createObject("SpikeDemon_Spike2");
        Spike.setWidth(SpikeScale);
        Spike.setHeight(SpikeScale);
        console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
        var CenterX = SpikeDemonBaseInstance.x + SpikeDemonBaseInstance.getWidth()/3.6;
        var CenterY = SpikeDemonBaseInstance.y + SpikeDemonBaseInstance.getHeight()/3.2;
        Spike.setPosition(CenterX, CenterY);
        Spike.setAngle(SpikeAngle);
        
        Spike.setLayer("Base Layer");


    }
}
};
gdjs.MindscapeCode.eventsList47 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects4);

var objects = [];
objects.push.apply(objects,gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects4);
gdjs.MindscapeCode.userFunc0x10a8668(runtimeScene, objects);

}


};gdjs.MindscapeCode.eventsList48 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 4, 4, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.MindscapeCode.eventsList49 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList46(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy2") >= 6;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy2");
}
{ //Subevents
gdjs.MindscapeCode.eventsList47(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy2") >= 4.5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList48(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.eventsList50 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595Base2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.MindscapeCode.eventsList45(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList49(runtimeScene);
}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4});
gdjs.MindscapeCode.eventsList51 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595BaseObjects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects5 */
/* Reuse gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595BaseObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike"), gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595SpikeObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects4 */
/* Reuse gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595SpikeObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform3Objects4Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.MindscapeCode.GDStaticPlatform1Objects4, "HorizontalMovingPlatform": gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects4, "StaticPlatform2": gdjs.MindscapeCode.GDStaticPlatform2Objects4, "StaticPlatform3": gdjs.MindscapeCode.GDStaticPlatform3Objects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeParticlesObjects4Objects = Hashtable.newFrom({"SpikeParticles": gdjs.MindscapeCode.GDSpikeParticlesObjects4});
gdjs.MindscapeCode.eventsList52 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike"), gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.MindscapeCode.GDStaticPlatform1Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.MindscapeCode.GDStaticPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.MindscapeCode.GDStaticPlatform3Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595SpikeObjects4Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform3Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4 */
gdjs.MindscapeCode.GDSpikeParticlesObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeParticlesObjects4Objects, (( gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4[0].getPointX("")) + (( gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4[0].getWidth()) / 2, (( gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4[0].getPointY("")) + (( gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.MindscapeCode.asyncCallback23236220 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}}
gdjs.MindscapeCode.eventsList53 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23236220(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.userFunc0x7ad538 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagtite = objects[x];
    const Spike = runtimeScene.createObject("StalagtiteDemon_Spike");
    Spike.setWidth(96);
    Spike.setHeight(48);
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
    var CenterX = Stalagtite.x + Stalagtite.getWidth()/3;
    var CenterY = Stalagtite.y + Stalagtite.getHeight() - Stalagtite.getHeight()/3;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(90);
    Spike.setLayer("Base Layer");
}
};
gdjs.MindscapeCode.eventsList54 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects4);

var objects = [];
objects.push.apply(objects,gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects4);
gdjs.MindscapeCode.userFunc0x7ad538(runtimeScene, objects);

}


};gdjs.MindscapeCode.eventsList55 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList53(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy") >= 6.5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}
{ //Subevents
gdjs.MindscapeCode.eventsList54(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy") >= 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.MindscapeCode.eventsList56 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.MindscapeCode.eventsList51(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList52(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList55(runtimeScene);
}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"StalagtiteDemon_Base2": gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"StalagtiteDemon_Base2": gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"StalagtiteDemon_Base2": gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike2": gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike2": gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4});
gdjs.MindscapeCode.eventsList57 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base2"), gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Base2Objects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects5 */
/* Reuse gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Base2Objects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike2"), gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Spike2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects4 */
/* Reuse gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Spike2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike2": gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform3Objects4Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.MindscapeCode.GDStaticPlatform1Objects4, "HorizontalMovingPlatform": gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects4, "StaticPlatform2": gdjs.MindscapeCode.GDStaticPlatform2Objects4, "StaticPlatform3": gdjs.MindscapeCode.GDStaticPlatform3Objects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeParticlesObjects4Objects = Hashtable.newFrom({"SpikeParticles": gdjs.MindscapeCode.GDSpikeParticlesObjects4});
gdjs.MindscapeCode.eventsList58 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike2"), gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.MindscapeCode.GDStaticPlatform1Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.MindscapeCode.GDStaticPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.MindscapeCode.GDStaticPlatform3Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Spike2Objects4Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform3Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4 */
gdjs.MindscapeCode.GDSpikeParticlesObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeParticlesObjects4Objects, (( gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4[0].getPointX("")) + (( gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4[0].getWidth()) / 2, (( gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4[0].getPointY("")) + (( gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.MindscapeCode.asyncCallback23244284 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy2");
}}
gdjs.MindscapeCode.eventsList59 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(4.25), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23244284(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.userFunc0x1222900 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagtite = objects[x];
    const Spike = runtimeScene.createObject("StalagtiteDemon_Spike2");
    Spike.setWidth(96);
    Spike.setHeight(48);
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
    var CenterX = Stalagtite.x + Stalagtite.getWidth()/3;
    var CenterY = Stalagtite.y + Stalagtite.getHeight() - Stalagtite.getHeight()/3;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(90);
    Spike.setLayer("Base Layer");
}
};
gdjs.MindscapeCode.eventsList60 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base2"), gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects4);

var objects = [];
objects.push.apply(objects,gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects4);
gdjs.MindscapeCode.userFunc0x1222900(runtimeScene, objects);

}


};gdjs.MindscapeCode.eventsList61 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList59(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy2") >= 6.5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy2");
}
{ //Subevents
gdjs.MindscapeCode.eventsList60(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy2") >= 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base2"), gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.MindscapeCode.eventsList62 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base2"), gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595Base2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.MindscapeCode.eventsList57(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList58(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList61(runtimeScene);
}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"StalagmiteDemon_Base": gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"StalagmiteDemon_Base": gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"StalagmiteDemon_Base": gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike": gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike": gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4});
gdjs.MindscapeCode.eventsList63 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595BaseObjects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects5 */
/* Reuse gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595BaseObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike"), gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595SpikeObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects4 */
/* Reuse gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595SpikeObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike": gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform3Objects4Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.MindscapeCode.GDStaticPlatform1Objects4, "HorizontalMovingPlatform": gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects4, "StaticPlatform2": gdjs.MindscapeCode.GDStaticPlatform2Objects4, "StaticPlatform3": gdjs.MindscapeCode.GDStaticPlatform3Objects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeParticlesObjects4Objects = Hashtable.newFrom({"SpikeParticles": gdjs.MindscapeCode.GDSpikeParticlesObjects4});
gdjs.MindscapeCode.eventsList64 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike"), gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.MindscapeCode.GDStaticPlatform1Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.MindscapeCode.GDStaticPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.MindscapeCode.GDStaticPlatform3Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595SpikeObjects4Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform3Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4 */
gdjs.MindscapeCode.GDSpikeParticlesObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeParticlesObjects4Objects, (( gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4[0].getPointX("")) + (( gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4[0].getWidth()) / 2, (( gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4[0].getPointY("")) + (( gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.MindscapeCode.asyncCallback23252316 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy");
}}
gdjs.MindscapeCode.eventsList65 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23252316(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.userFunc0xb7e518 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagmite = objects[x];
    const Spike = runtimeScene.createObject("StalagmiteDemon_Spike");
    Spike.setWidth(96);
    Spike.setHeight(48);
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
    var CenterX = Stalagmite.x + Stalagmite.getWidth()/3;
    var CenterY = Stalagmite.y;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(270);
    Spike.setLayer("Base Layer");
}
};
gdjs.MindscapeCode.eventsList66 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects4);

var objects = [];
objects.push.apply(objects,gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects4);
gdjs.MindscapeCode.userFunc0xb7e518(runtimeScene, objects);

}


};gdjs.MindscapeCode.eventsList67 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList65(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagmiteEnemy") >= 6.5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy");
}
{ //Subevents
gdjs.MindscapeCode.eventsList66(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagmiteEnemy") >= 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.MindscapeCode.eventsList68 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.MindscapeCode.eventsList63(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList64(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList67(runtimeScene);
}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Base2": gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"StalagmiteDemon_Base2": gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"StalagmiteDemon_Base2": gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects5});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike2": gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike2": gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4});
gdjs.MindscapeCode.eventsList69 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base2"), gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Base2Objects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects5 */
/* Reuse gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Base2Objects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike2"), gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects4 */
/* Reuse gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike2": gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform3Objects4Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.MindscapeCode.GDStaticPlatform1Objects4, "HorizontalMovingPlatform": gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects4, "StaticPlatform2": gdjs.MindscapeCode.GDStaticPlatform2Objects4, "StaticPlatform3": gdjs.MindscapeCode.GDStaticPlatform3Objects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeParticlesObjects4Objects = Hashtable.newFrom({"SpikeParticles": gdjs.MindscapeCode.GDSpikeParticlesObjects4});
gdjs.MindscapeCode.eventsList70 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike2"), gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.MindscapeCode.GDStaticPlatform1Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.MindscapeCode.GDStaticPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.MindscapeCode.GDStaticPlatform3Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546MindscapeCode_9546GDStaticPlatform3Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4 */
gdjs.MindscapeCode.GDSpikeParticlesObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeParticlesObjects4Objects, (( gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4[0].getPointX("")) + (( gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4[0].getWidth()) / 2, (( gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4[0].getPointY("")) + (( gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.MindscapeCode.asyncCallback23260420 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy2");
}}
gdjs.MindscapeCode.eventsList71 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(4.25), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23260420(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.userFunc0x121f120 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagmite = objects[x];
    const Spike = runtimeScene.createObject("StalagmiteDemon_Spike2");
    Spike.setWidth(96);
    Spike.setHeight(48);
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
    var CenterX = Stalagmite.x + Stalagmite.getWidth()/3;
    var CenterY = Stalagmite.y;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(270);
    Spike.setLayer("Base Layer");
}
};
gdjs.MindscapeCode.eventsList72 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base2"), gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects4);

var objects = [];
objects.push.apply(objects,gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects4);
gdjs.MindscapeCode.userFunc0x121f120(runtimeScene, objects);

}


};gdjs.MindscapeCode.eventsList73 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList71(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagmiteEnemy2") >= 6.5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy2");
}
{ //Subevents
gdjs.MindscapeCode.eventsList72(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagmiteEnemy2") >= 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base2"), gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.MindscapeCode.eventsList74 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base2"), gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagmiteDemon_95959595Base2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.MindscapeCode.eventsList69(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList70(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList73(runtimeScene);
}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"LaserDemon_Base": gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BeamObjects3Objects = Hashtable.newFrom({"LaserDemon_Beam": gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3});
gdjs.MindscapeCode.eventsList75 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BeamObjects3Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3 */
{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3[i].returnVariable(gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3[i].getVariables().getFromIndex(1)).setNumber((gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3[i].getPointX("")));
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3[i].returnVariable(gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3[i].getVariables().getFromIndex(2)).setNumber((gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3[i].getPointY("")));
}
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"LaserDemon_Base": gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"LaserDemon_Base": gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"LaserDemon_Base": gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDMonsterParticlesObjects4Objects = Hashtable.newFrom({"MonsterParticles": gdjs.MindscapeCode.GDMonsterParticlesObjects4});
gdjs.MindscapeCode.eventsList76 = function(runtimeScene) {

{

/* Reuse gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4[k] = gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.MindscapeCode.GDMonsterParticlesObjects4);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BaseObjects4Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDMonsterParticlesObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BeamObjects3Objects = Hashtable.newFrom({"LaserDemon_Beam": gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BeamObjects3Objects = Hashtable.newFrom({"LaserDemon_Beam": gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3});
gdjs.MindscapeCode.eventsList77 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BaseObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4 */
/* Reuse gdjs.MindscapeCode.GDPlayerObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.MindscapeCode.eventsList76(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BeamObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3 */
/* Reuse gdjs.MindscapeCode.GDPlayerObjects3 */
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects3);
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BeamObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects3[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.MindscapeCode.eventsList78 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3[i].setPosition(0,6000);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.25);
}
}}

}


};gdjs.MindscapeCode.asyncCallback23276348 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects6);

{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects6.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects6[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 2);
}
}}
gdjs.MindscapeCode.eventsList79 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects5) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23276348(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.asyncCallback23276180 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects5);

{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects5.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects5[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 1);
}
}
{ //Subevents
gdjs.MindscapeCode.eventsList79(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.MindscapeCode.eventsList80 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23276180(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.asyncCallback23275644 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4);

{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.5);
}
}
{ //Subevents
gdjs.MindscapeCode.eventsList80(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.MindscapeCode.eventsList81 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
for (const obj of gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23275644(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.eventsList82 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3[i].setPosition(gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3[i].getVariables().getFromIndex(1).getAsNumber(),gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3[i].getVariables().getFromIndex(2).getAsNumber());
}
}
{ //Subevents
gdjs.MindscapeCode.eventsList81(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.asyncCallback23281572 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects5);

{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects5.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects5[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0);
}
}}
gdjs.MindscapeCode.eventsList83 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.1), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23281572(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.asyncCallback23280628 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4);

{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.3);
}
}
{ //Subevents
gdjs.MindscapeCode.eventsList83(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.MindscapeCode.eventsList84 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.1), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23280628(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.asyncCallback23280172 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3);

{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0.5);
}
}
{ //Subevents
gdjs.MindscapeCode.eventsList84(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.MindscapeCode.eventsList85 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
for (const obj of gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects2) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.1), (runtimeScene) => (gdjs.MindscapeCode.asyncCallback23280172(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MindscapeCode.eventsList86 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects2);
{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects2.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects2[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 1.25);
}
}
{ //Subevents
gdjs.MindscapeCode.eventsList85(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.eventsList87 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "LaserEnemy");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 10;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "LaserEnemy");
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 4;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23270692);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3[i].getBehavior("Opacity").setOpacity(40);
}
}
{ //Subevents
gdjs.MindscapeCode.eventsList78(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 2.75;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 4;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23273532);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "easeInQuad", 1.25, false);
}
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 4;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 10;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23275188);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList82(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 9;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 10;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23277988);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3[i].getBehavior("Tween").addObjectOpacityTween2("ChargeDown", 40, "easeInQuad", 1, false);
}
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 9.7;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 10;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23279652);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList86(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.eventsList88 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDLaserDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.MindscapeCode.eventsList75(runtimeScene);} //End of subevents
}

}


{


gdjs.MindscapeCode.eventsList77(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList87(runtimeScene);
}


};gdjs.MindscapeCode.eventsList89 = function(runtimeScene) {

{


gdjs.MindscapeCode.eventsList28(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList31(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList34(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList37(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList43(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList50(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList56(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList62(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList68(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList74(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList88(runtimeScene);
}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalObjects4Objects = Hashtable.newFrom({"Portal": gdjs.MindscapeCode.GDPortalObjects4});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalObjects3Objects = Hashtable.newFrom({"Portal": gdjs.MindscapeCode.GDPortalObjects3});
gdjs.MindscapeCode.eventsList90 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.MindscapeCode.GDPlayerObjects3, gdjs.MindscapeCode.GDPlayerObjects4);

{for(var i = 0, len = gdjs.MindscapeCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDPlayerObjects4[i].activateBehavior("PlatformerObject", false);
}
}}

}


{

/* Reuse gdjs.MindscapeCode.GDPlayerObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDPlayerObjects3[i].getVariableNumber(gdjs.MindscapeCode.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDPlayerObjects3[k] = gdjs.MindscapeCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDPlayerObjects3 */
/* Reuse gdjs.MindscapeCode.GDPortalObjects3 */
{gdjs.evtsExt__Player__AnimateFallingIntoPortal.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, "Tween", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.MindscapeCode.eventsList91 = function(runtimeScene) {

{


gdjs.MindscapeCode.eventsList90(runtimeScene);
}


};gdjs.MindscapeCode.eventsList92 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDPlayerObjects4[i].getVariableNumber(gdjs.MindscapeCode.GDPlayerObjects4[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDPlayerObjects4[k] = gdjs.MindscapeCode.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_DeathText"), gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects4);
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_RetryButton"), gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects4);
{for(var i = 0, len = gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects4[i].hide();
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDPlayerObjects3.length;i<l;++i) {
    if ( !(gdjs.MindscapeCode.GDPlayerObjects3[i].getVariableNumber(gdjs.MindscapeCode.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 0) ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDPlayerObjects3[k] = gdjs.MindscapeCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_MemoryAcquired"), gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3);
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_ProceedButton"), gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3[i].hide();
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects3[i].hide();
}
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDUI_95959595Sinage_95959595BackgroundObjects2Objects = Hashtable.newFrom({"UI_Sinage_Background": gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects2});
gdjs.MindscapeCode.eventsList93 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23287540);
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList92(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_Background"), gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects2);
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_RetryButton"), gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2);
{gdjs.evtsExt__UserInterface__StretchToFillScreen.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDUI_95959595Sinage_95959595BackgroundObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects2.length ;i < len;++i) {
    gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects2[i].setCenterXInScene(gdjs.evtTools.camera.getCameraX(runtimeScene, "EndScreen", 0));
}
for(var i = 0, len = gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2.length ;i < len;++i) {
    gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2[i].setCenterXInScene(gdjs.evtTools.camera.getCameraX(runtimeScene, "EndScreen", 0));
}
}}

}


};gdjs.MindscapeCode.eventsList94 = function(runtimeScene) {

{


gdjs.MindscapeCode.eventsList93(runtimeScene);
}


};gdjs.MindscapeCode.eventsList95 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_Background"), gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects3);
{gdjs.evtTools.camera.hideLayer(runtimeScene, "EndScreen");
}{for(var i = 0, len = gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects3[i].setOpacity(180);
}
}}

}


{



}


{

gdjs.MindscapeCode.GDPlayerObjects3.length = 0;

gdjs.MindscapeCode.GDPortalObjects3.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.MindscapeCode.GDPlayerObjects3_1final.length = 0;
gdjs.MindscapeCode.GDPortalObjects3_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.MindscapeCode.GDPortalObjects4);
isConditionTrue_1 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects4Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalObjects4Objects, false, runtimeScene, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.MindscapeCode.GDPlayerObjects4.length; j < jLen ; ++j) {
        if ( gdjs.MindscapeCode.GDPlayerObjects3_1final.indexOf(gdjs.MindscapeCode.GDPlayerObjects4[j]) === -1 )
            gdjs.MindscapeCode.GDPlayerObjects3_1final.push(gdjs.MindscapeCode.GDPlayerObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.MindscapeCode.GDPortalObjects4.length; j < jLen ; ++j) {
        if ( gdjs.MindscapeCode.GDPortalObjects3_1final.indexOf(gdjs.MindscapeCode.GDPortalObjects4[j]) === -1 )
            gdjs.MindscapeCode.GDPortalObjects3_1final.push(gdjs.MindscapeCode.GDPortalObjects4[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects4);
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDPlayerObjects4[i].getVariableNumber(gdjs.MindscapeCode.GDPlayerObjects4[i].getVariables().getFromIndex(1)) <= 0 ) {
        isConditionTrue_1 = true;
        gdjs.MindscapeCode.GDPlayerObjects4[k] = gdjs.MindscapeCode.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDPlayerObjects4.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.MindscapeCode.GDPlayerObjects4.length; j < jLen ; ++j) {
        if ( gdjs.MindscapeCode.GDPlayerObjects3_1final.indexOf(gdjs.MindscapeCode.GDPlayerObjects4[j]) === -1 )
            gdjs.MindscapeCode.GDPlayerObjects3_1final.push(gdjs.MindscapeCode.GDPlayerObjects4[j]);
    }
}
}
{
gdjs.copyArray(gdjs.MindscapeCode.GDPlayerObjects3_1final, gdjs.MindscapeCode.GDPlayerObjects3);
gdjs.copyArray(gdjs.MindscapeCode.GDPortalObjects3_1final, gdjs.MindscapeCode.GDPortalObjects3);
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23283420);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.camera.showLayer(runtimeScene, "EndScreen");
}{gdjs.evtTools.sound.stopSoundOnChannel(runtimeScene, 2);
}
{ //Subevents
gdjs.MindscapeCode.eventsList91(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "EndScreen");
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList94(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.eventsList96 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("BoundaryJumpThrough"), gdjs.MindscapeCode.GDBoundaryJumpThroughObjects4);
gdjs.copyArray(runtimeScene.getObjects("LeftBoundary"), gdjs.MindscapeCode.GDLeftBoundaryObjects4);
gdjs.copyArray(runtimeScene.getObjects("RightBoundary"), gdjs.MindscapeCode.GDRightBoundaryObjects4);
{for(var i = 0, len = gdjs.MindscapeCode.GDLeftBoundaryObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLeftBoundaryObjects4[i].hide();
}
for(var i = 0, len = gdjs.MindscapeCode.GDRightBoundaryObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDRightBoundaryObjects4[i].hide();
}
for(var i = 0, len = gdjs.MindscapeCode.GDBoundaryJumpThroughObjects4.length ;i < len;++i) {
    gdjs.MindscapeCode.GDBoundaryJumpThroughObjects4[i].hide();
}
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("BottomBoundary"), gdjs.MindscapeCode.GDBottomBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("LeftBoundary"), gdjs.MindscapeCode.GDLeftBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("RightBoundary"), gdjs.MindscapeCode.GDRightBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("TopBoundary"), gdjs.MindscapeCode.GDTopBoundaryObjects3);
{gdjs.evtTools.camera.clampCamera(runtimeScene, (( gdjs.MindscapeCode.GDLeftBoundaryObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDLeftBoundaryObjects3[0].getPointX("")) + (( gdjs.MindscapeCode.GDLeftBoundaryObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDLeftBoundaryObjects3[0].getWidth()), (( gdjs.MindscapeCode.GDTopBoundaryObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDTopBoundaryObjects3[0].getPointY("")) + (( gdjs.MindscapeCode.GDTopBoundaryObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDTopBoundaryObjects3[0].getHeight()), (( gdjs.MindscapeCode.GDRightBoundaryObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDRightBoundaryObjects3[0].getPointX("")), (( gdjs.MindscapeCode.GDBottomBoundaryObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDBottomBoundaryObjects3[0].getPointY("")), "", 0);
}}

}


};gdjs.MindscapeCode.eventsList97 = function(runtimeScene) {

{



}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "RandomNoiseTimer");
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("BackgroundPlants"), gdjs.MindscapeCode.GDBackgroundPlantsObjects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDBackgroundPlantsObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDBackgroundPlantsObjects3[i].setWidth(gdjs.evtTools.camera.getCameraWidth(runtimeScene, "", 0));
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDBackgroundPlantsObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDBackgroundPlantsObjects3[i].setXOffset(gdjs.evtTools.camera.getCameraBorderLeft(runtimeScene, "", 0) / 3 + 780);
}
}}

}


};gdjs.MindscapeCode.eventsList98 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{/* Unknown object - skipped. */}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalObjects3Objects = Hashtable.newFrom({"Portal": gdjs.MindscapeCode.GDPortalObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalObjects2Objects = Hashtable.newFrom({"Portal": gdjs.MindscapeCode.GDPortalObjects2});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects2});
gdjs.MindscapeCode.eventsList99 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "door.aac", 0, true, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.MindscapeCode.GDPortalObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23297804);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Portal/PortalInteract.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects2);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.MindscapeCode.GDPortalObjects2);
{gdjs.evtsExt__VolumeFalloff__SetVolumeFalloff.func(runtimeScene, 0, "Sound", gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalObjects2Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects, 0, 100, 750, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.MindscapeCode.eventsList100 = function(runtimeScene) {

{


gdjs.MindscapeCode.eventsList96(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList97(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList98(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList99(runtimeScene);
}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDBoundaryJumpThroughObjects2Objects = Hashtable.newFrom({"BoundaryJumpThrough": gdjs.MindscapeCode.GDBoundaryJumpThroughObjects2});
gdjs.MindscapeCode.eventsList101 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("BoundaryJumpThrough"), gdjs.MindscapeCode.GDBoundaryJumpThroughObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDBoundaryJumpThroughObjects2Objects);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23299580);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.MindscapeCode.GDBoundaryJumpThroughObjects2 */
{for(var i = 0, len = gdjs.MindscapeCode.GDBoundaryJumpThroughObjects2.length ;i < len;++i) {
    gdjs.MindscapeCode.GDBoundaryJumpThroughObjects2[i].hide();
}
}}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.MindscapeCode.GDFlyingDemonObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.MindscapeCode.GDFireDemonObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.MindscapeCode.GDHorizontalDemonObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects3});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects3});
gdjs.MindscapeCode.eventsList102 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "t");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23300812);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDPlayerObjects3[i].returnVariable(gdjs.MindscapeCode.GDPlayerObjects3[i].getVariables().getFromIndex(5)).setNumber(1);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDPlayerObjects3[i].setPosition(gdjs.MindscapeCode.GDPlayerObjects3[i].getVariables().getFromIndex(6).getAsNumber(),gdjs.MindscapeCode.GDPlayerObjects3[i].getVariables().getFromIndex(7).getAsNumber());
}
}{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects3Objects, ((gdjs.MindscapeCode.GDPlayerObjects3.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.MindscapeCode.GDPlayerObjects3[0].getVariables()).getFromIndex(6).getAsNumber(), ((gdjs.MindscapeCode.GDPlayerObjects3.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.MindscapeCode.GDPlayerObjects3[0].getVariables()).getFromIndex(7).getAsNumber(), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Tilde");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23302108);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.MindscapeCode.GDFireDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.MindscapeCode.GDFlyingDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.MindscapeCode.GDHorizontalDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects3);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects3);
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFlyingDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDFireDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDHorizontalDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDSpikeDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDStalagtiteDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "i");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23303020);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects3);
{for(var i = 0, len = gdjs.MindscapeCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDPlayerObjects3[i].returnVariable(gdjs.MindscapeCode.GDPlayerObjects3[i].getVariables().getFromIndex(5)).setNumber(1 - gdjs.MindscapeCode.GDPlayerObjects3[i].getVariables().getFromIndex(5).getAsNumber());
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Escape");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23303940);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, gdjs.evtTools.runtimeScene.getSceneName(runtimeScene), false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "l");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23304796);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "TestingLevel", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "m");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23306020);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Mindscape", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num0");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23306732);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Tutorial", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num1");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23306460);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level1", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num2");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23308092);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level2", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num3");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23308812);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level3", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num4");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23308492);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level4", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num5");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23309636);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level5", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num6");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23310300);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level6", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num7");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23310964);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level7", false);
}}

}


};gdjs.MindscapeCode.eventsList103 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("HopeBar"), gdjs.MindscapeCode.GDHopeBarObjects2);
gdjs.copyArray(runtimeScene.getObjects("LivesBar"), gdjs.MindscapeCode.GDLivesBarObjects2);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects2);
{for(var i = 0, len = gdjs.MindscapeCode.GDLivesBarObjects2.length ;i < len;++i) {
    gdjs.MindscapeCode.GDLivesBarObjects2[i].SetValue((gdjs.RuntimeObject.getVariableNumber(((gdjs.MindscapeCode.GDPlayerObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.MindscapeCode.GDPlayerObjects2[0].getVariables()).getFromIndex(1))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDHopeBarObjects2.length ;i < len;++i) {
    gdjs.MindscapeCode.GDHopeBarObjects2[i].SetValue((gdjs.RuntimeObject.getVariableNumber(((gdjs.MindscapeCode.GDPlayerObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.MindscapeCode.GDPlayerObjects2[0].getVariables()).getFromIndex(3))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "LevelUI", 0, 0, 0);
}{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "LevelEndScreen", 0, 0, 0);
}}

}


};gdjs.MindscapeCode.eventsList104 = function(runtimeScene) {

{


gdjs.MindscapeCode.eventsList8(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList15(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList89(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList95(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList100(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList101(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList102(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList103(runtimeScene);
}


};gdjs.MindscapeCode.eventsList105 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDPlayerObjects2[i].getVariableNumber(gdjs.MindscapeCode.GDPlayerObjects2[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDPlayerObjects2[k] = gdjs.MindscapeCode.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDPlayerObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level1", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDPlayerObjects1[i].getVariableNumber(gdjs.MindscapeCode.GDPlayerObjects1[i].getVariables().getFromIndex(1)) <= 0 ) {
        isConditionTrue_0 = true;
        gdjs.MindscapeCode.GDPlayerObjects1[k] = gdjs.MindscapeCode.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDPlayerObjects1.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Tutorial", false);
}}

}


};gdjs.MindscapeCode.eventsList106 = function(runtimeScene) {

{

gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "EndScreen");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_RetryButton"), gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2);
for (var i = 0, k = 0, l = gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2.length;i<l;++i) {
    if ( gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2[k] = gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2[i];
        ++k;
    }
}
gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2.length; j < jLen ; ++j) {
        if ( gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects1_1final.indexOf(gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2[j]) === -1 )
            gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects1_1final.push(gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2[j]);
    }
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
gdjs.copyArray(gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects1_1final, gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects1);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList105(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects2});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToMountainsObjects2Objects = Hashtable.newFrom({"PortalToMountains": gdjs.MindscapeCode.GDPortalToMountainsObjects2});
gdjs.MindscapeCode.eventsList107 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("InteractPrompt"), gdjs.MindscapeCode.GDInteractPromptObjects3);
gdjs.copyArray(gdjs.MindscapeCode.GDPortalToMountainsObjects2, gdjs.MindscapeCode.GDPortalToMountainsObjects3);

{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects3[i].hide(false);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects3[i].setPosition((( gdjs.MindscapeCode.GDPortalToMountainsObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToMountainsObjects3[0].getPointX("")) + (( gdjs.MindscapeCode.GDPortalToMountainsObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToMountainsObjects3[0].getWidth()) / 4,(( gdjs.MindscapeCode.GDPortalToMountainsObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToMountainsObjects3[0].getPointY("")) - ((( gdjs.MindscapeCode.GDPortalToMountainsObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToMountainsObjects3[0].getHeight()) / 3) * 2);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "e");
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Tutorial", false);
}}

}


};gdjs.MindscapeCode.eventsList108 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects2);
gdjs.copyArray(runtimeScene.getObjects("PortalToMountains"), gdjs.MindscapeCode.GDPortalToMountainsObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.distanceTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToMountainsObjects2Objects, 350, false);
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList107(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects2});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToForestObjects2Objects = Hashtable.newFrom({"PortalToForest": gdjs.MindscapeCode.GDPortalToForestObjects2});
gdjs.MindscapeCode.eventsList109 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("InteractPrompt"), gdjs.MindscapeCode.GDInteractPromptObjects3);
gdjs.copyArray(gdjs.MindscapeCode.GDPortalToForestObjects2, gdjs.MindscapeCode.GDPortalToForestObjects3);

{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects3[i].hide(false);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects3[i].setPosition((( gdjs.MindscapeCode.GDPortalToForestObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToForestObjects3[0].getPointX("")) + (( gdjs.MindscapeCode.GDPortalToForestObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToForestObjects3[0].getWidth()) / 4,(( gdjs.MindscapeCode.GDPortalToForestObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToForestObjects3[0].getPointY("")) - ((( gdjs.MindscapeCode.GDPortalToForestObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToForestObjects3[0].getHeight()) / 3) * 2);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "e");
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level1", false);
}}

}


};gdjs.MindscapeCode.eventsList110 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects2);
gdjs.copyArray(runtimeScene.getObjects("PortalToForest"), gdjs.MindscapeCode.GDPortalToForestObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.distanceTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToForestObjects2Objects, 350, false);
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList109(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects2});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToBeachObjects2Objects = Hashtable.newFrom({"PortalToBeach": gdjs.MindscapeCode.GDPortalToBeachObjects2});
gdjs.MindscapeCode.eventsList111 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("InteractPrompt"), gdjs.MindscapeCode.GDInteractPromptObjects3);
gdjs.copyArray(gdjs.MindscapeCode.GDPortalToBeachObjects2, gdjs.MindscapeCode.GDPortalToBeachObjects3);

{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects3[i].hide(false);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects3[i].setPosition((( gdjs.MindscapeCode.GDPortalToBeachObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToBeachObjects3[0].getPointX("")) + (( gdjs.MindscapeCode.GDPortalToBeachObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToBeachObjects3[0].getWidth()) / 4,(( gdjs.MindscapeCode.GDPortalToBeachObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToBeachObjects3[0].getPointY("")) - ((( gdjs.MindscapeCode.GDPortalToBeachObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToBeachObjects3[0].getHeight()) / 3) * 2);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "e");
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level2", false);
}}

}


};gdjs.MindscapeCode.eventsList112 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects2);
gdjs.copyArray(runtimeScene.getObjects("PortalToBeach"), gdjs.MindscapeCode.GDPortalToBeachObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.distanceTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToBeachObjects2Objects, 350, false);
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList111(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects2});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToLivingRoomObjects2Objects = Hashtable.newFrom({"PortalToLivingRoom": gdjs.MindscapeCode.GDPortalToLivingRoomObjects2});
gdjs.MindscapeCode.eventsList113 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("InteractPrompt"), gdjs.MindscapeCode.GDInteractPromptObjects3);
gdjs.copyArray(gdjs.MindscapeCode.GDPortalToLivingRoomObjects2, gdjs.MindscapeCode.GDPortalToLivingRoomObjects3);

{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects3[i].hide(false);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects3[i].setPosition((( gdjs.MindscapeCode.GDPortalToLivingRoomObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToLivingRoomObjects3[0].getPointX("")) + (( gdjs.MindscapeCode.GDPortalToLivingRoomObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToLivingRoomObjects3[0].getWidth()) / 4,(( gdjs.MindscapeCode.GDPortalToLivingRoomObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToLivingRoomObjects3[0].getPointY("")) - ((( gdjs.MindscapeCode.GDPortalToLivingRoomObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToLivingRoomObjects3[0].getHeight()) / 3) * 2);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "e");
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level3", false);
}}

}


};gdjs.MindscapeCode.eventsList114 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects2);
gdjs.copyArray(runtimeScene.getObjects("PortalToLivingRoom"), gdjs.MindscapeCode.GDPortalToLivingRoomObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.distanceTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToLivingRoomObjects2Objects, 350, false);
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList113(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects2});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToSchoolObjects2Objects = Hashtable.newFrom({"PortalToSchool": gdjs.MindscapeCode.GDPortalToSchoolObjects2});
gdjs.MindscapeCode.eventsList115 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("InteractPrompt"), gdjs.MindscapeCode.GDInteractPromptObjects3);
gdjs.copyArray(gdjs.MindscapeCode.GDPortalToSchoolObjects2, gdjs.MindscapeCode.GDPortalToSchoolObjects3);

{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects3[i].hide(false);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects3[i].setPosition((( gdjs.MindscapeCode.GDPortalToSchoolObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToSchoolObjects3[0].getPointX("")) + (( gdjs.MindscapeCode.GDPortalToSchoolObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToSchoolObjects3[0].getWidth()) / 4,(( gdjs.MindscapeCode.GDPortalToSchoolObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToSchoolObjects3[0].getPointY("")) - ((( gdjs.MindscapeCode.GDPortalToSchoolObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToSchoolObjects3[0].getHeight()) / 3) * 2);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "e");
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level5", false);
}}

}


};gdjs.MindscapeCode.eventsList116 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects2);
gdjs.copyArray(runtimeScene.getObjects("PortalToSchool"), gdjs.MindscapeCode.GDPortalToSchoolObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.distanceTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToSchoolObjects2Objects, 350, false);
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList115(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects2});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToOceanObjects2Objects = Hashtable.newFrom({"PortalToOcean": gdjs.MindscapeCode.GDPortalToOceanObjects2});
gdjs.MindscapeCode.eventsList117 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("InteractPrompt"), gdjs.MindscapeCode.GDInteractPromptObjects3);
gdjs.copyArray(gdjs.MindscapeCode.GDPortalToOceanObjects2, gdjs.MindscapeCode.GDPortalToOceanObjects3);

{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects3[i].hide(false);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects3.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects3[i].setPosition((( gdjs.MindscapeCode.GDPortalToOceanObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToOceanObjects3[0].getPointX("")) + (( gdjs.MindscapeCode.GDPortalToOceanObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToOceanObjects3[0].getWidth()) / 4,(( gdjs.MindscapeCode.GDPortalToOceanObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToOceanObjects3[0].getPointY("")) - ((( gdjs.MindscapeCode.GDPortalToOceanObjects3.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToOceanObjects3[0].getHeight()) / 3) * 2);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "e");
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level4", false);
}}

}


};gdjs.MindscapeCode.eventsList118 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects2);
gdjs.copyArray(runtimeScene.getObjects("PortalToOcean"), gdjs.MindscapeCode.GDPortalToOceanObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.distanceTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects2Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToOceanObjects2Objects, 350, false);
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList117(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.MindscapeCode.GDPlayerObjects1});
gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToWhimsicalTreeObjects1Objects = Hashtable.newFrom({"PortalToWhimsicalTree": gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects1});
gdjs.MindscapeCode.eventsList119 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("InteractPrompt"), gdjs.MindscapeCode.GDInteractPromptObjects2);
gdjs.copyArray(gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects1, gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects2);

{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects2.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects2.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects2[i].setPosition((( gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects2.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects2[0].getPointX("")) + (( gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects2.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects2[0].getWidth()) / 4,(( gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects2.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects2[0].getPointY("")) - ((( gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects2.length === 0 ) ? 0 :gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects2[0].getHeight()) / 3) * 2);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "e");
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level6", false);
}}

}


};gdjs.MindscapeCode.eventsList120 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MindscapeCode.GDPlayerObjects1);
gdjs.copyArray(runtimeScene.getObjects("PortalToWhimsicalTree"), gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.distanceTest(gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPlayerObjects1Objects, gdjs.MindscapeCode.mapOfGDgdjs_9546MindscapeCode_9546GDPortalToWhimsicalTreeObjects1Objects, 350, false);
if (isConditionTrue_0) {

{ //Subevents
gdjs.MindscapeCode.eventsList119(runtimeScene);} //End of subevents
}

}


};gdjs.MindscapeCode.eventsList121 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("InteractPrompt"), gdjs.MindscapeCode.GDInteractPromptObjects2);
{for(var i = 0, len = gdjs.MindscapeCode.GDInteractPromptObjects2.length ;i < len;++i) {
    gdjs.MindscapeCode.GDInteractPromptObjects2[i].hide();
}
}}

}


{


gdjs.MindscapeCode.eventsList108(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList110(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList112(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList114(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList116(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList118(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList120(runtimeScene);
}


};gdjs.MindscapeCode.eventsList122 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.camera.hideLayer(runtimeScene, "UI");
}}

}


};gdjs.MindscapeCode.eventsList123 = function(runtimeScene) {

{



}


{


gdjs.MindscapeCode.eventsList104(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList106(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList121(runtimeScene);
}


{


gdjs.MindscapeCode.eventsList122(runtimeScene);
}


};

gdjs.MindscapeCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.MindscapeCode.GDBackgroundPlantsObjects1.length = 0;
gdjs.MindscapeCode.GDBackgroundPlantsObjects2.length = 0;
gdjs.MindscapeCode.GDBackgroundPlantsObjects3.length = 0;
gdjs.MindscapeCode.GDBackgroundPlantsObjects4.length = 0;
gdjs.MindscapeCode.GDBackgroundPlantsObjects5.length = 0;
gdjs.MindscapeCode.GDBackgroundPlantsObjects6.length = 0;
gdjs.MindscapeCode.GDLeftBoundaryObjects1.length = 0;
gdjs.MindscapeCode.GDLeftBoundaryObjects2.length = 0;
gdjs.MindscapeCode.GDLeftBoundaryObjects3.length = 0;
gdjs.MindscapeCode.GDLeftBoundaryObjects4.length = 0;
gdjs.MindscapeCode.GDLeftBoundaryObjects5.length = 0;
gdjs.MindscapeCode.GDLeftBoundaryObjects6.length = 0;
gdjs.MindscapeCode.GDRightBoundaryObjects1.length = 0;
gdjs.MindscapeCode.GDRightBoundaryObjects2.length = 0;
gdjs.MindscapeCode.GDRightBoundaryObjects3.length = 0;
gdjs.MindscapeCode.GDRightBoundaryObjects4.length = 0;
gdjs.MindscapeCode.GDRightBoundaryObjects5.length = 0;
gdjs.MindscapeCode.GDRightBoundaryObjects6.length = 0;
gdjs.MindscapeCode.GDTopBoundaryObjects1.length = 0;
gdjs.MindscapeCode.GDTopBoundaryObjects2.length = 0;
gdjs.MindscapeCode.GDTopBoundaryObjects3.length = 0;
gdjs.MindscapeCode.GDTopBoundaryObjects4.length = 0;
gdjs.MindscapeCode.GDTopBoundaryObjects5.length = 0;
gdjs.MindscapeCode.GDTopBoundaryObjects6.length = 0;
gdjs.MindscapeCode.GDBottomBoundaryObjects1.length = 0;
gdjs.MindscapeCode.GDBottomBoundaryObjects2.length = 0;
gdjs.MindscapeCode.GDBottomBoundaryObjects3.length = 0;
gdjs.MindscapeCode.GDBottomBoundaryObjects4.length = 0;
gdjs.MindscapeCode.GDBottomBoundaryObjects5.length = 0;
gdjs.MindscapeCode.GDBottomBoundaryObjects6.length = 0;
gdjs.MindscapeCode.GDBoundaryJumpThroughObjects1.length = 0;
gdjs.MindscapeCode.GDBoundaryJumpThroughObjects2.length = 0;
gdjs.MindscapeCode.GDBoundaryJumpThroughObjects3.length = 0;
gdjs.MindscapeCode.GDBoundaryJumpThroughObjects4.length = 0;
gdjs.MindscapeCode.GDBoundaryJumpThroughObjects5.length = 0;
gdjs.MindscapeCode.GDBoundaryJumpThroughObjects6.length = 0;
gdjs.MindscapeCode.GDServerRackObjects1.length = 0;
gdjs.MindscapeCode.GDServerRackObjects2.length = 0;
gdjs.MindscapeCode.GDServerRackObjects3.length = 0;
gdjs.MindscapeCode.GDServerRackObjects4.length = 0;
gdjs.MindscapeCode.GDServerRackObjects5.length = 0;
gdjs.MindscapeCode.GDServerRackObjects6.length = 0;
gdjs.MindscapeCode.GDServerObjects1.length = 0;
gdjs.MindscapeCode.GDServerObjects2.length = 0;
gdjs.MindscapeCode.GDServerObjects3.length = 0;
gdjs.MindscapeCode.GDServerObjects4.length = 0;
gdjs.MindscapeCode.GDServerObjects5.length = 0;
gdjs.MindscapeCode.GDServerObjects6.length = 0;
gdjs.MindscapeCode.GDMemoryHolderObjects1.length = 0;
gdjs.MindscapeCode.GDMemoryHolderObjects2.length = 0;
gdjs.MindscapeCode.GDMemoryHolderObjects3.length = 0;
gdjs.MindscapeCode.GDMemoryHolderObjects4.length = 0;
gdjs.MindscapeCode.GDMemoryHolderObjects5.length = 0;
gdjs.MindscapeCode.GDMemoryHolderObjects6.length = 0;
gdjs.MindscapeCode.GDPortalToMountainsObjects1.length = 0;
gdjs.MindscapeCode.GDPortalToMountainsObjects2.length = 0;
gdjs.MindscapeCode.GDPortalToMountainsObjects3.length = 0;
gdjs.MindscapeCode.GDPortalToMountainsObjects4.length = 0;
gdjs.MindscapeCode.GDPortalToMountainsObjects5.length = 0;
gdjs.MindscapeCode.GDPortalToMountainsObjects6.length = 0;
gdjs.MindscapeCode.GDMindscapeFloorObjects1.length = 0;
gdjs.MindscapeCode.GDMindscapeFloorObjects2.length = 0;
gdjs.MindscapeCode.GDMindscapeFloorObjects3.length = 0;
gdjs.MindscapeCode.GDMindscapeFloorObjects4.length = 0;
gdjs.MindscapeCode.GDMindscapeFloorObjects5.length = 0;
gdjs.MindscapeCode.GDMindscapeFloorObjects6.length = 0;
gdjs.MindscapeCode.GDInteractPromptObjects1.length = 0;
gdjs.MindscapeCode.GDInteractPromptObjects2.length = 0;
gdjs.MindscapeCode.GDInteractPromptObjects3.length = 0;
gdjs.MindscapeCode.GDInteractPromptObjects4.length = 0;
gdjs.MindscapeCode.GDInteractPromptObjects5.length = 0;
gdjs.MindscapeCode.GDInteractPromptObjects6.length = 0;
gdjs.MindscapeCode.GDPortalToForestObjects1.length = 0;
gdjs.MindscapeCode.GDPortalToForestObjects2.length = 0;
gdjs.MindscapeCode.GDPortalToForestObjects3.length = 0;
gdjs.MindscapeCode.GDPortalToForestObjects4.length = 0;
gdjs.MindscapeCode.GDPortalToForestObjects5.length = 0;
gdjs.MindscapeCode.GDPortalToForestObjects6.length = 0;
gdjs.MindscapeCode.GDPortalToBeachObjects1.length = 0;
gdjs.MindscapeCode.GDPortalToBeachObjects2.length = 0;
gdjs.MindscapeCode.GDPortalToBeachObjects3.length = 0;
gdjs.MindscapeCode.GDPortalToBeachObjects4.length = 0;
gdjs.MindscapeCode.GDPortalToBeachObjects5.length = 0;
gdjs.MindscapeCode.GDPortalToBeachObjects6.length = 0;
gdjs.MindscapeCode.GDPortalToLivingRoomObjects1.length = 0;
gdjs.MindscapeCode.GDPortalToLivingRoomObjects2.length = 0;
gdjs.MindscapeCode.GDPortalToLivingRoomObjects3.length = 0;
gdjs.MindscapeCode.GDPortalToLivingRoomObjects4.length = 0;
gdjs.MindscapeCode.GDPortalToLivingRoomObjects5.length = 0;
gdjs.MindscapeCode.GDPortalToLivingRoomObjects6.length = 0;
gdjs.MindscapeCode.GDPortalToSchoolObjects1.length = 0;
gdjs.MindscapeCode.GDPortalToSchoolObjects2.length = 0;
gdjs.MindscapeCode.GDPortalToSchoolObjects3.length = 0;
gdjs.MindscapeCode.GDPortalToSchoolObjects4.length = 0;
gdjs.MindscapeCode.GDPortalToSchoolObjects5.length = 0;
gdjs.MindscapeCode.GDPortalToSchoolObjects6.length = 0;
gdjs.MindscapeCode.GDPortalToOceanObjects1.length = 0;
gdjs.MindscapeCode.GDPortalToOceanObjects2.length = 0;
gdjs.MindscapeCode.GDPortalToOceanObjects3.length = 0;
gdjs.MindscapeCode.GDPortalToOceanObjects4.length = 0;
gdjs.MindscapeCode.GDPortalToOceanObjects5.length = 0;
gdjs.MindscapeCode.GDPortalToOceanObjects6.length = 0;
gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects1.length = 0;
gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects2.length = 0;
gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects3.length = 0;
gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects4.length = 0;
gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects5.length = 0;
gdjs.MindscapeCode.GDPortalToWhimsicalTreeObjects6.length = 0;
gdjs.MindscapeCode.GDPlayerObjects1.length = 0;
gdjs.MindscapeCode.GDPlayerObjects2.length = 0;
gdjs.MindscapeCode.GDPlayerObjects3.length = 0;
gdjs.MindscapeCode.GDPlayerObjects4.length = 0;
gdjs.MindscapeCode.GDPlayerObjects5.length = 0;
gdjs.MindscapeCode.GDPlayerObjects6.length = 0;
gdjs.MindscapeCode.GDFlyingDemonObjects1.length = 0;
gdjs.MindscapeCode.GDFlyingDemonObjects2.length = 0;
gdjs.MindscapeCode.GDFlyingDemonObjects3.length = 0;
gdjs.MindscapeCode.GDFlyingDemonObjects4.length = 0;
gdjs.MindscapeCode.GDFlyingDemonObjects5.length = 0;
gdjs.MindscapeCode.GDFlyingDemonObjects6.length = 0;
gdjs.MindscapeCode.GDFireDemonObjects1.length = 0;
gdjs.MindscapeCode.GDFireDemonObjects2.length = 0;
gdjs.MindscapeCode.GDFireDemonObjects3.length = 0;
gdjs.MindscapeCode.GDFireDemonObjects4.length = 0;
gdjs.MindscapeCode.GDFireDemonObjects5.length = 0;
gdjs.MindscapeCode.GDFireDemonObjects6.length = 0;
gdjs.MindscapeCode.GDCheckpointObjects1.length = 0;
gdjs.MindscapeCode.GDCheckpointObjects2.length = 0;
gdjs.MindscapeCode.GDCheckpointObjects3.length = 0;
gdjs.MindscapeCode.GDCheckpointObjects4.length = 0;
gdjs.MindscapeCode.GDCheckpointObjects5.length = 0;
gdjs.MindscapeCode.GDCheckpointObjects6.length = 0;
gdjs.MindscapeCode.GDStaticPlatform3Objects1.length = 0;
gdjs.MindscapeCode.GDStaticPlatform3Objects2.length = 0;
gdjs.MindscapeCode.GDStaticPlatform3Objects3.length = 0;
gdjs.MindscapeCode.GDStaticPlatform3Objects4.length = 0;
gdjs.MindscapeCode.GDStaticPlatform3Objects5.length = 0;
gdjs.MindscapeCode.GDStaticPlatform3Objects6.length = 0;
gdjs.MindscapeCode.GDStaticPlatform2Objects1.length = 0;
gdjs.MindscapeCode.GDStaticPlatform2Objects2.length = 0;
gdjs.MindscapeCode.GDStaticPlatform2Objects3.length = 0;
gdjs.MindscapeCode.GDStaticPlatform2Objects4.length = 0;
gdjs.MindscapeCode.GDStaticPlatform2Objects5.length = 0;
gdjs.MindscapeCode.GDStaticPlatform2Objects6.length = 0;
gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects1.length = 0;
gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects2.length = 0;
gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects3.length = 0;
gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects4.length = 0;
gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects5.length = 0;
gdjs.MindscapeCode.GDHorizontalMovingPlatformObjects6.length = 0;
gdjs.MindscapeCode.GDStaticPlatform1Objects1.length = 0;
gdjs.MindscapeCode.GDStaticPlatform1Objects2.length = 0;
gdjs.MindscapeCode.GDStaticPlatform1Objects3.length = 0;
gdjs.MindscapeCode.GDStaticPlatform1Objects4.length = 0;
gdjs.MindscapeCode.GDStaticPlatform1Objects5.length = 0;
gdjs.MindscapeCode.GDStaticPlatform1Objects6.length = 0;
gdjs.MindscapeCode.GDPortalObjects1.length = 0;
gdjs.MindscapeCode.GDPortalObjects2.length = 0;
gdjs.MindscapeCode.GDPortalObjects3.length = 0;
gdjs.MindscapeCode.GDPortalObjects4.length = 0;
gdjs.MindscapeCode.GDPortalObjects5.length = 0;
gdjs.MindscapeCode.GDPortalObjects6.length = 0;
gdjs.MindscapeCode.GDLadderObjects1.length = 0;
gdjs.MindscapeCode.GDLadderObjects2.length = 0;
gdjs.MindscapeCode.GDLadderObjects3.length = 0;
gdjs.MindscapeCode.GDLadderObjects4.length = 0;
gdjs.MindscapeCode.GDLadderObjects5.length = 0;
gdjs.MindscapeCode.GDLadderObjects6.length = 0;
gdjs.MindscapeCode.GDMonsterParticlesObjects1.length = 0;
gdjs.MindscapeCode.GDMonsterParticlesObjects2.length = 0;
gdjs.MindscapeCode.GDMonsterParticlesObjects3.length = 0;
gdjs.MindscapeCode.GDMonsterParticlesObjects4.length = 0;
gdjs.MindscapeCode.GDMonsterParticlesObjects5.length = 0;
gdjs.MindscapeCode.GDMonsterParticlesObjects6.length = 0;
gdjs.MindscapeCode.GDSpikeParticlesObjects1.length = 0;
gdjs.MindscapeCode.GDSpikeParticlesObjects2.length = 0;
gdjs.MindscapeCode.GDSpikeParticlesObjects3.length = 0;
gdjs.MindscapeCode.GDSpikeParticlesObjects4.length = 0;
gdjs.MindscapeCode.GDSpikeParticlesObjects5.length = 0;
gdjs.MindscapeCode.GDSpikeParticlesObjects6.length = 0;
gdjs.MindscapeCode.GDDoorParticlesObjects1.length = 0;
gdjs.MindscapeCode.GDDoorParticlesObjects2.length = 0;
gdjs.MindscapeCode.GDDoorParticlesObjects3.length = 0;
gdjs.MindscapeCode.GDDoorParticlesObjects4.length = 0;
gdjs.MindscapeCode.GDDoorParticlesObjects5.length = 0;
gdjs.MindscapeCode.GDDoorParticlesObjects6.length = 0;
gdjs.MindscapeCode.GDDustParticleObjects1.length = 0;
gdjs.MindscapeCode.GDDustParticleObjects2.length = 0;
gdjs.MindscapeCode.GDDustParticleObjects3.length = 0;
gdjs.MindscapeCode.GDDustParticleObjects4.length = 0;
gdjs.MindscapeCode.GDDustParticleObjects5.length = 0;
gdjs.MindscapeCode.GDDustParticleObjects6.length = 0;
gdjs.MindscapeCode.GDLivesBarObjects1.length = 0;
gdjs.MindscapeCode.GDLivesBarObjects2.length = 0;
gdjs.MindscapeCode.GDLivesBarObjects3.length = 0;
gdjs.MindscapeCode.GDLivesBarObjects4.length = 0;
gdjs.MindscapeCode.GDLivesBarObjects5.length = 0;
gdjs.MindscapeCode.GDLivesBarObjects6.length = 0;
gdjs.MindscapeCode.GDHopeBarObjects1.length = 0;
gdjs.MindscapeCode.GDHopeBarObjects2.length = 0;
gdjs.MindscapeCode.GDHopeBarObjects3.length = 0;
gdjs.MindscapeCode.GDHopeBarObjects4.length = 0;
gdjs.MindscapeCode.GDHopeBarObjects5.length = 0;
gdjs.MindscapeCode.GDHopeBarObjects6.length = 0;
gdjs.MindscapeCode.GDMemoryObjects1.length = 0;
gdjs.MindscapeCode.GDMemoryObjects2.length = 0;
gdjs.MindscapeCode.GDMemoryObjects3.length = 0;
gdjs.MindscapeCode.GDMemoryObjects4.length = 0;
gdjs.MindscapeCode.GDMemoryObjects5.length = 0;
gdjs.MindscapeCode.GDMemoryObjects6.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects1.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects2.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects3.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects4.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects5.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595DeathTextObjects6.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects1.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects2.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects4.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects5.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595MemoryAcquiredObjects6.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595LivesObjects1.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595LivesObjects2.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595LivesObjects3.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595LivesObjects4.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595LivesObjects5.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595LivesObjects6.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595HopeObjects1.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595HopeObjects2.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595HopeObjects3.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595HopeObjects4.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595HopeObjects5.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595HopeObjects6.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects1.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects2.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects3.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects4.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects5.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595RetryButtonObjects6.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects1.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects2.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects3.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects4.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects5.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595BackgroundObjects6.length = 0;
gdjs.MindscapeCode.GDHorizontalDemonObjects1.length = 0;
gdjs.MindscapeCode.GDHorizontalDemonObjects2.length = 0;
gdjs.MindscapeCode.GDHorizontalDemonObjects3.length = 0;
gdjs.MindscapeCode.GDHorizontalDemonObjects4.length = 0;
gdjs.MindscapeCode.GDHorizontalDemonObjects5.length = 0;
gdjs.MindscapeCode.GDHorizontalDemonObjects6.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects1.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects2.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects3.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects4.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects5.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595BaseObjects6.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects1.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects2.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects3.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects4.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects5.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595SpikeObjects6.length = 0;
gdjs.MindscapeCode.GDVerticalMovingPlatformObjects1.length = 0;
gdjs.MindscapeCode.GDVerticalMovingPlatformObjects2.length = 0;
gdjs.MindscapeCode.GDVerticalMovingPlatformObjects3.length = 0;
gdjs.MindscapeCode.GDVerticalMovingPlatformObjects4.length = 0;
gdjs.MindscapeCode.GDVerticalMovingPlatformObjects5.length = 0;
gdjs.MindscapeCode.GDVerticalMovingPlatformObjects6.length = 0;
gdjs.MindscapeCode.GDSpinningMovingPlatformObjects1.length = 0;
gdjs.MindscapeCode.GDSpinningMovingPlatformObjects2.length = 0;
gdjs.MindscapeCode.GDSpinningMovingPlatformObjects3.length = 0;
gdjs.MindscapeCode.GDSpinningMovingPlatformObjects4.length = 0;
gdjs.MindscapeCode.GDSpinningMovingPlatformObjects5.length = 0;
gdjs.MindscapeCode.GDSpinningMovingPlatformObjects6.length = 0;
gdjs.MindscapeCode.GDFlippingPlatformObjects1.length = 0;
gdjs.MindscapeCode.GDFlippingPlatformObjects2.length = 0;
gdjs.MindscapeCode.GDFlippingPlatformObjects3.length = 0;
gdjs.MindscapeCode.GDFlippingPlatformObjects4.length = 0;
gdjs.MindscapeCode.GDFlippingPlatformObjects5.length = 0;
gdjs.MindscapeCode.GDFlippingPlatformObjects6.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects1.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects2.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects3.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects4.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects5.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595BaseObjects6.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects1.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects2.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects3.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects4.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects5.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595SpikeObjects6.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects1.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects2.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects3.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects4.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects5.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595Base2Objects6.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects1.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects2.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects3.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects4.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects5.length = 0;
gdjs.MindscapeCode.GDSpikeDemon_9595Spike2Objects6.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects1.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects2.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects3.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects4.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects5.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595Base2Objects6.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects1.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects2.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects3.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects4.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects5.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595BaseObjects6.length = 0;
gdjs.MindscapeCode.GDFlippingPlatform2Objects1.length = 0;
gdjs.MindscapeCode.GDFlippingPlatform2Objects2.length = 0;
gdjs.MindscapeCode.GDFlippingPlatform2Objects3.length = 0;
gdjs.MindscapeCode.GDFlippingPlatform2Objects4.length = 0;
gdjs.MindscapeCode.GDFlippingPlatform2Objects5.length = 0;
gdjs.MindscapeCode.GDFlippingPlatform2Objects6.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects1.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects2.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects3.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects4.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects5.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595Base2Objects6.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects1.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects2.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects3.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects4.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects5.length = 0;
gdjs.MindscapeCode.GDStalagtiteDemon_9595Spike2Objects6.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects1.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects2.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects3.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects4.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects5.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595SpikeObjects6.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects1.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects2.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects3.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects4.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects5.length = 0;
gdjs.MindscapeCode.GDStalagmiteDemon_9595Spike2Objects6.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects1.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects2.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects3.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects4.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects5.length = 0;
gdjs.MindscapeCode.GDUI_9595Sinage_9595ProceedButtonObjects6.length = 0;
gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects1.length = 0;
gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects2.length = 0;
gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects3.length = 0;
gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects4.length = 0;
gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects5.length = 0;
gdjs.MindscapeCode.GDLaserDemon_9595BeamObjects6.length = 0;
gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects1.length = 0;
gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects2.length = 0;
gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects3.length = 0;
gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects4.length = 0;
gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects5.length = 0;
gdjs.MindscapeCode.GDLaserDemon_9595BaseObjects6.length = 0;
gdjs.MindscapeCode.GDTestObjects1.length = 0;
gdjs.MindscapeCode.GDTestObjects2.length = 0;
gdjs.MindscapeCode.GDTestObjects3.length = 0;
gdjs.MindscapeCode.GDTestObjects4.length = 0;
gdjs.MindscapeCode.GDTestObjects5.length = 0;
gdjs.MindscapeCode.GDTestObjects6.length = 0;

gdjs.MindscapeCode.eventsList123(runtimeScene);

return;

}

gdjs['MindscapeCode'] = gdjs.MindscapeCode;
