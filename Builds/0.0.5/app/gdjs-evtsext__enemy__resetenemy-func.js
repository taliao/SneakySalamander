
if (typeof gdjs.evtsExt__Enemy__ResetEnemy !== "undefined") {
  gdjs.evtsExt__Enemy__ResetEnemy.registeredGdjsCallbacks.forEach(callback =>
    gdjs._unregisterCallback(callback)
  );
}

gdjs.evtsExt__Enemy__ResetEnemy = {};
gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1= [];


gdjs.evtsExt__Enemy__ResetEnemy.userFunc0x8bc298 = function GDJSInlineCode(runtimeScene, objects, eventsFunctionContext) {
"use strict";
for (var i = 0; i < objects.length; i++) {
    const currEnemy = objects[i];
    const newEnemy = runtimeScene.createObject(currEnemy.getName());

    newEnemy.setWidth(currEnemy.getWidth());
    newEnemy.setHeight(currEnemy.getHeight());
    newEnemy.setVariableNumber(newEnemy.getVariables().get("OldPosition_X"), currEnemy.getVariables().get("OldPosition_X").getAsNumber());
    newEnemy.setVariableNumber(newEnemy.getVariables().get("OldPosition_Y"), currEnemy.getVariables().get("OldPosition_Y").getAsNumber());
    newEnemy.setVariableBoolean(newEnemy.getVariables().get("hasBeenReaped"), currEnemy.getVariables().get("hasBeenReaped").getAsBoolean());
    newEnemy.setPosition(newEnemy.getVariables().get("OldPosition_X").getAsNumber(), newEnemy.getVariables().get("OldPosition_Y").getAsNumber());
    currEnemy.deleteFromScene(currEnemy.getInstanceContainer());
}
};
gdjs.evtsExt__Enemy__ResetEnemy.eventsList0 = function(runtimeScene, eventsFunctionContext) {

{



}


{

gdjs.copyArray(eventsFunctionContext.getObjects("Enemy"), gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1);

var objects = [];
objects.push.apply(objects,gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1);
gdjs.evtsExt__Enemy__ResetEnemy.userFunc0x8bc298(runtimeScene, objects, typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined);

}


};

gdjs.evtsExt__Enemy__ResetEnemy.func = function(runtimeScene, Enemy, parentEventsFunctionContext) {
var eventsFunctionContext = {
  _objectsMap: {
"Enemy": Enemy
},
  _objectArraysMap: {
"Enemy": gdjs.objectsListsToArray(Enemy)
},
  _behaviorNamesMap: {
},
  getObjects: function(objectName) {
    return eventsFunctionContext._objectArraysMap[objectName] || [];
  },
  getObjectsLists: function(objectName) {
    return eventsFunctionContext._objectsMap[objectName] || null;
  },
  getBehaviorName: function(behaviorName) {
    return eventsFunctionContext._behaviorNamesMap[behaviorName] || behaviorName;
  },
  createObject: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    if (objectsList) {
      const object = parentEventsFunctionContext ?
        parentEventsFunctionContext.createObject(objectsList.firstKey()) :
        runtimeScene.createObject(objectsList.firstKey());
      if (object) {
        objectsList.get(objectsList.firstKey()).push(object);
        eventsFunctionContext._objectArraysMap[objectName].push(object);
      }
      return object;    }
    return null;
  },
  getInstancesCountOnScene: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    let count = 0;
    if (objectsList) {
      for(const objectName in objectsList.items)
        count += parentEventsFunctionContext ?
parentEventsFunctionContext.getInstancesCountOnScene(objectName) :
        runtimeScene.getInstancesCountOnScene(objectName);
    }
    return count;
  },
  getLayer: function(layerName) {
    return runtimeScene.getLayer(layerName);
  },
  getArgument: function(argName) {
    return "";
  },
  getOnceTriggers: function() { return runtimeScene.getOnceTriggers(); }
};

gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1.length = 0;

gdjs.evtsExt__Enemy__ResetEnemy.eventsList0(runtimeScene, eventsFunctionContext);

return;
}

gdjs.evtsExt__Enemy__ResetEnemy.registeredGdjsCallbacks = [];