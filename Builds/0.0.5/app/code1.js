gdjs.Level6Code = {};
gdjs.Level6Code.GDPlayerObjects3_1final = [];

gdjs.Level6Code.GDPortalObjects3_1final = [];

gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final = [];

gdjs.Level6Code.GDBackgroundPlantsObjects1= [];
gdjs.Level6Code.GDBackgroundPlantsObjects2= [];
gdjs.Level6Code.GDBackgroundPlantsObjects3= [];
gdjs.Level6Code.GDBackgroundPlantsObjects4= [];
gdjs.Level6Code.GDBackgroundPlantsObjects5= [];
gdjs.Level6Code.GDBackgroundPlantsObjects6= [];
gdjs.Level6Code.GDLeftBoundaryObjects1= [];
gdjs.Level6Code.GDLeftBoundaryObjects2= [];
gdjs.Level6Code.GDLeftBoundaryObjects3= [];
gdjs.Level6Code.GDLeftBoundaryObjects4= [];
gdjs.Level6Code.GDLeftBoundaryObjects5= [];
gdjs.Level6Code.GDLeftBoundaryObjects6= [];
gdjs.Level6Code.GDRightBoundaryObjects1= [];
gdjs.Level6Code.GDRightBoundaryObjects2= [];
gdjs.Level6Code.GDRightBoundaryObjects3= [];
gdjs.Level6Code.GDRightBoundaryObjects4= [];
gdjs.Level6Code.GDRightBoundaryObjects5= [];
gdjs.Level6Code.GDRightBoundaryObjects6= [];
gdjs.Level6Code.GDTopBoundaryObjects1= [];
gdjs.Level6Code.GDTopBoundaryObjects2= [];
gdjs.Level6Code.GDTopBoundaryObjects3= [];
gdjs.Level6Code.GDTopBoundaryObjects4= [];
gdjs.Level6Code.GDTopBoundaryObjects5= [];
gdjs.Level6Code.GDTopBoundaryObjects6= [];
gdjs.Level6Code.GDBottomBoundaryObjects1= [];
gdjs.Level6Code.GDBottomBoundaryObjects2= [];
gdjs.Level6Code.GDBottomBoundaryObjects3= [];
gdjs.Level6Code.GDBottomBoundaryObjects4= [];
gdjs.Level6Code.GDBottomBoundaryObjects5= [];
gdjs.Level6Code.GDBottomBoundaryObjects6= [];
gdjs.Level6Code.GDBoundaryJumpThroughObjects1= [];
gdjs.Level6Code.GDBoundaryJumpThroughObjects2= [];
gdjs.Level6Code.GDBoundaryJumpThroughObjects3= [];
gdjs.Level6Code.GDBoundaryJumpThroughObjects4= [];
gdjs.Level6Code.GDBoundaryJumpThroughObjects5= [];
gdjs.Level6Code.GDBoundaryJumpThroughObjects6= [];
gdjs.Level6Code.GDPlayerObjects1= [];
gdjs.Level6Code.GDPlayerObjects2= [];
gdjs.Level6Code.GDPlayerObjects3= [];
gdjs.Level6Code.GDPlayerObjects4= [];
gdjs.Level6Code.GDPlayerObjects5= [];
gdjs.Level6Code.GDPlayerObjects6= [];
gdjs.Level6Code.GDFlyingDemonObjects1= [];
gdjs.Level6Code.GDFlyingDemonObjects2= [];
gdjs.Level6Code.GDFlyingDemonObjects3= [];
gdjs.Level6Code.GDFlyingDemonObjects4= [];
gdjs.Level6Code.GDFlyingDemonObjects5= [];
gdjs.Level6Code.GDFlyingDemonObjects6= [];
gdjs.Level6Code.GDFireDemonObjects1= [];
gdjs.Level6Code.GDFireDemonObjects2= [];
gdjs.Level6Code.GDFireDemonObjects3= [];
gdjs.Level6Code.GDFireDemonObjects4= [];
gdjs.Level6Code.GDFireDemonObjects5= [];
gdjs.Level6Code.GDFireDemonObjects6= [];
gdjs.Level6Code.GDCheckpointObjects1= [];
gdjs.Level6Code.GDCheckpointObjects2= [];
gdjs.Level6Code.GDCheckpointObjects3= [];
gdjs.Level6Code.GDCheckpointObjects4= [];
gdjs.Level6Code.GDCheckpointObjects5= [];
gdjs.Level6Code.GDCheckpointObjects6= [];
gdjs.Level6Code.GDStaticPlatform3Objects1= [];
gdjs.Level6Code.GDStaticPlatform3Objects2= [];
gdjs.Level6Code.GDStaticPlatform3Objects3= [];
gdjs.Level6Code.GDStaticPlatform3Objects4= [];
gdjs.Level6Code.GDStaticPlatform3Objects5= [];
gdjs.Level6Code.GDStaticPlatform3Objects6= [];
gdjs.Level6Code.GDStaticPlatform2Objects1= [];
gdjs.Level6Code.GDStaticPlatform2Objects2= [];
gdjs.Level6Code.GDStaticPlatform2Objects3= [];
gdjs.Level6Code.GDStaticPlatform2Objects4= [];
gdjs.Level6Code.GDStaticPlatform2Objects5= [];
gdjs.Level6Code.GDStaticPlatform2Objects6= [];
gdjs.Level6Code.GDHorizontalMovingPlatformObjects1= [];
gdjs.Level6Code.GDHorizontalMovingPlatformObjects2= [];
gdjs.Level6Code.GDHorizontalMovingPlatformObjects3= [];
gdjs.Level6Code.GDHorizontalMovingPlatformObjects4= [];
gdjs.Level6Code.GDHorizontalMovingPlatformObjects5= [];
gdjs.Level6Code.GDHorizontalMovingPlatformObjects6= [];
gdjs.Level6Code.GDStaticPlatform1Objects1= [];
gdjs.Level6Code.GDStaticPlatform1Objects2= [];
gdjs.Level6Code.GDStaticPlatform1Objects3= [];
gdjs.Level6Code.GDStaticPlatform1Objects4= [];
gdjs.Level6Code.GDStaticPlatform1Objects5= [];
gdjs.Level6Code.GDStaticPlatform1Objects6= [];
gdjs.Level6Code.GDPortalObjects1= [];
gdjs.Level6Code.GDPortalObjects2= [];
gdjs.Level6Code.GDPortalObjects3= [];
gdjs.Level6Code.GDPortalObjects4= [];
gdjs.Level6Code.GDPortalObjects5= [];
gdjs.Level6Code.GDPortalObjects6= [];
gdjs.Level6Code.GDLadderObjects1= [];
gdjs.Level6Code.GDLadderObjects2= [];
gdjs.Level6Code.GDLadderObjects3= [];
gdjs.Level6Code.GDLadderObjects4= [];
gdjs.Level6Code.GDLadderObjects5= [];
gdjs.Level6Code.GDLadderObjects6= [];
gdjs.Level6Code.GDMonsterParticlesObjects1= [];
gdjs.Level6Code.GDMonsterParticlesObjects2= [];
gdjs.Level6Code.GDMonsterParticlesObjects3= [];
gdjs.Level6Code.GDMonsterParticlesObjects4= [];
gdjs.Level6Code.GDMonsterParticlesObjects5= [];
gdjs.Level6Code.GDMonsterParticlesObjects6= [];
gdjs.Level6Code.GDSpikeParticlesObjects1= [];
gdjs.Level6Code.GDSpikeParticlesObjects2= [];
gdjs.Level6Code.GDSpikeParticlesObjects3= [];
gdjs.Level6Code.GDSpikeParticlesObjects4= [];
gdjs.Level6Code.GDSpikeParticlesObjects5= [];
gdjs.Level6Code.GDSpikeParticlesObjects6= [];
gdjs.Level6Code.GDDoorParticlesObjects1= [];
gdjs.Level6Code.GDDoorParticlesObjects2= [];
gdjs.Level6Code.GDDoorParticlesObjects3= [];
gdjs.Level6Code.GDDoorParticlesObjects4= [];
gdjs.Level6Code.GDDoorParticlesObjects5= [];
gdjs.Level6Code.GDDoorParticlesObjects6= [];
gdjs.Level6Code.GDDustParticleObjects1= [];
gdjs.Level6Code.GDDustParticleObjects2= [];
gdjs.Level6Code.GDDustParticleObjects3= [];
gdjs.Level6Code.GDDustParticleObjects4= [];
gdjs.Level6Code.GDDustParticleObjects5= [];
gdjs.Level6Code.GDDustParticleObjects6= [];
gdjs.Level6Code.GDLivesBarObjects1= [];
gdjs.Level6Code.GDLivesBarObjects2= [];
gdjs.Level6Code.GDLivesBarObjects3= [];
gdjs.Level6Code.GDLivesBarObjects4= [];
gdjs.Level6Code.GDLivesBarObjects5= [];
gdjs.Level6Code.GDLivesBarObjects6= [];
gdjs.Level6Code.GDHopeBarObjects1= [];
gdjs.Level6Code.GDHopeBarObjects2= [];
gdjs.Level6Code.GDHopeBarObjects3= [];
gdjs.Level6Code.GDHopeBarObjects4= [];
gdjs.Level6Code.GDHopeBarObjects5= [];
gdjs.Level6Code.GDHopeBarObjects6= [];
gdjs.Level6Code.GDMemoryObjects1= [];
gdjs.Level6Code.GDMemoryObjects2= [];
gdjs.Level6Code.GDMemoryObjects3= [];
gdjs.Level6Code.GDMemoryObjects4= [];
gdjs.Level6Code.GDMemoryObjects5= [];
gdjs.Level6Code.GDMemoryObjects6= [];
gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects1= [];
gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects2= [];
gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects3= [];
gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects4= [];
gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects5= [];
gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects6= [];
gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects1= [];
gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects2= [];
gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3= [];
gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects4= [];
gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects5= [];
gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects6= [];
gdjs.Level6Code.GDUI_9595Sinage_9595LivesObjects1= [];
gdjs.Level6Code.GDUI_9595Sinage_9595LivesObjects2= [];
gdjs.Level6Code.GDUI_9595Sinage_9595LivesObjects3= [];
gdjs.Level6Code.GDUI_9595Sinage_9595LivesObjects4= [];
gdjs.Level6Code.GDUI_9595Sinage_9595LivesObjects5= [];
gdjs.Level6Code.GDUI_9595Sinage_9595LivesObjects6= [];
gdjs.Level6Code.GDUI_9595Sinage_9595HopeObjects1= [];
gdjs.Level6Code.GDUI_9595Sinage_9595HopeObjects2= [];
gdjs.Level6Code.GDUI_9595Sinage_9595HopeObjects3= [];
gdjs.Level6Code.GDUI_9595Sinage_9595HopeObjects4= [];
gdjs.Level6Code.GDUI_9595Sinage_9595HopeObjects5= [];
gdjs.Level6Code.GDUI_9595Sinage_9595HopeObjects6= [];
gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects1= [];
gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects2= [];
gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects3= [];
gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects4= [];
gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects5= [];
gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects6= [];
gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects1= [];
gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects2= [];
gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects3= [];
gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects4= [];
gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects5= [];
gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects6= [];
gdjs.Level6Code.GDHorizontalDemonObjects1= [];
gdjs.Level6Code.GDHorizontalDemonObjects2= [];
gdjs.Level6Code.GDHorizontalDemonObjects3= [];
gdjs.Level6Code.GDHorizontalDemonObjects4= [];
gdjs.Level6Code.GDHorizontalDemonObjects5= [];
gdjs.Level6Code.GDHorizontalDemonObjects6= [];
gdjs.Level6Code.GDSpikeDemon_9595BaseObjects1= [];
gdjs.Level6Code.GDSpikeDemon_9595BaseObjects2= [];
gdjs.Level6Code.GDSpikeDemon_9595BaseObjects3= [];
gdjs.Level6Code.GDSpikeDemon_9595BaseObjects4= [];
gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5= [];
gdjs.Level6Code.GDSpikeDemon_9595BaseObjects6= [];
gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects1= [];
gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects2= [];
gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects3= [];
gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects4= [];
gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects5= [];
gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects6= [];
gdjs.Level6Code.GDVerticalMovingPlatformObjects1= [];
gdjs.Level6Code.GDVerticalMovingPlatformObjects2= [];
gdjs.Level6Code.GDVerticalMovingPlatformObjects3= [];
gdjs.Level6Code.GDVerticalMovingPlatformObjects4= [];
gdjs.Level6Code.GDVerticalMovingPlatformObjects5= [];
gdjs.Level6Code.GDVerticalMovingPlatformObjects6= [];
gdjs.Level6Code.GDSpinningMovingPlatformObjects1= [];
gdjs.Level6Code.GDSpinningMovingPlatformObjects2= [];
gdjs.Level6Code.GDSpinningMovingPlatformObjects3= [];
gdjs.Level6Code.GDSpinningMovingPlatformObjects4= [];
gdjs.Level6Code.GDSpinningMovingPlatformObjects5= [];
gdjs.Level6Code.GDSpinningMovingPlatformObjects6= [];
gdjs.Level6Code.GDFlippingPlatformObjects1= [];
gdjs.Level6Code.GDFlippingPlatformObjects2= [];
gdjs.Level6Code.GDFlippingPlatformObjects3= [];
gdjs.Level6Code.GDFlippingPlatformObjects4= [];
gdjs.Level6Code.GDFlippingPlatformObjects5= [];
gdjs.Level6Code.GDFlippingPlatformObjects6= [];
gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects1= [];
gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects2= [];
gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects3= [];
gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects4= [];
gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects5= [];
gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects6= [];
gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects1= [];
gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects2= [];
gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects3= [];
gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4= [];
gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects5= [];
gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects6= [];
gdjs.Level6Code.GDSpikeDemon_9595Base2Objects1= [];
gdjs.Level6Code.GDSpikeDemon_9595Base2Objects2= [];
gdjs.Level6Code.GDSpikeDemon_9595Base2Objects3= [];
gdjs.Level6Code.GDSpikeDemon_9595Base2Objects4= [];
gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5= [];
gdjs.Level6Code.GDSpikeDemon_9595Base2Objects6= [];
gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects1= [];
gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects2= [];
gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects3= [];
gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects4= [];
gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects5= [];
gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects6= [];
gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects1= [];
gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects2= [];
gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects3= [];
gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects4= [];
gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects5= [];
gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects6= [];
gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects1= [];
gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects2= [];
gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects3= [];
gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects4= [];
gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects5= [];
gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects6= [];
gdjs.Level6Code.GDFlippingPlatform2Objects1= [];
gdjs.Level6Code.GDFlippingPlatform2Objects2= [];
gdjs.Level6Code.GDFlippingPlatform2Objects3= [];
gdjs.Level6Code.GDFlippingPlatform2Objects4= [];
gdjs.Level6Code.GDFlippingPlatform2Objects5= [];
gdjs.Level6Code.GDFlippingPlatform2Objects6= [];
gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects1= [];
gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects2= [];
gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects3= [];
gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects4= [];
gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects5= [];
gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects6= [];
gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects1= [];
gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects2= [];
gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects3= [];
gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4= [];
gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects5= [];
gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects6= [];
gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects1= [];
gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects2= [];
gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects3= [];
gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4= [];
gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects5= [];
gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects6= [];
gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects1= [];
gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects2= [];
gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects3= [];
gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4= [];
gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects5= [];
gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects6= [];
gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects1= [];
gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects2= [];
gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects3= [];
gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects4= [];
gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects5= [];
gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects6= [];
gdjs.Level6Code.GDLaserDemon_9595BeamObjects1= [];
gdjs.Level6Code.GDLaserDemon_9595BeamObjects2= [];
gdjs.Level6Code.GDLaserDemon_9595BeamObjects3= [];
gdjs.Level6Code.GDLaserDemon_9595BeamObjects4= [];
gdjs.Level6Code.GDLaserDemon_9595BeamObjects5= [];
gdjs.Level6Code.GDLaserDemon_9595BeamObjects6= [];
gdjs.Level6Code.GDLaserDemon_9595BaseObjects1= [];
gdjs.Level6Code.GDLaserDemon_9595BaseObjects2= [];
gdjs.Level6Code.GDLaserDemon_9595BaseObjects3= [];
gdjs.Level6Code.GDLaserDemon_9595BaseObjects4= [];
gdjs.Level6Code.GDLaserDemon_9595BaseObjects5= [];
gdjs.Level6Code.GDLaserDemon_9595BaseObjects6= [];
gdjs.Level6Code.GDTestObjects1= [];
gdjs.Level6Code.GDTestObjects2= [];
gdjs.Level6Code.GDTestObjects3= [];
gdjs.Level6Code.GDTestObjects4= [];
gdjs.Level6Code.GDTestObjects5= [];
gdjs.Level6Code.GDTestObjects6= [];


gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects4Objects = Hashtable.newFrom({"HorizontalMovingPlatform": gdjs.Level6Code.GDHorizontalMovingPlatformObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects4Objects = Hashtable.newFrom({"VerticalMovingPlatform": gdjs.Level6Code.GDVerticalMovingPlatformObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects3Objects = Hashtable.newFrom({"SpinningMovingPlatform": gdjs.Level6Code.GDSpinningMovingPlatformObjects3});
gdjs.Level6Code.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.Level6Code.GDHorizontalMovingPlatformObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.Level6Code.GDVerticalMovingPlatformObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.Level6Code.GDSpinningMovingPlatformObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level6Code.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level6Code.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.Level6Code.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDFlippingPlatformObjects4[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level6Code.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level6Code.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.Level6Code.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDFlippingPlatformObjects4[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level6Code.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") <= 3;
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList1(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 3;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level6Code.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.Level6Code.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDFlippingPlatformObjects4[i].rotateTowardAngle(-(80), 0, runtimeScene);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList2(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 6;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level6Code.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.Level6Code.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDFlippingPlatformObjects4[i].rotateTowardAngle(0, 0, runtimeScene);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


};gdjs.Level6Code.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level6Code.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.Level6Code.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.Level6Code.GDFlippingPlatform2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level6Code.eventsList5 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level6Code.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.Level6Code.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.Level6Code.GDFlippingPlatform2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level6Code.asyncCallback23166852 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}}
gdjs.Level6Code.eventsList6 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.Level6Code.asyncCallback23166852(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.eventsList7 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") <= 3;
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList4(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 3;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level6Code.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.Level6Code.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.Level6Code.GDFlippingPlatform2Objects3[i].rotateTowardAngle(-(80), 0, runtimeScene);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList5(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 6;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level6Code.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.Level6Code.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.Level6Code.GDFlippingPlatform2Objects3[i].rotateTowardAngle(0, 0, runtimeScene);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList6(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.eventsList8 = function(runtimeScene) {

{


gdjs.Level6Code.eventsList0(runtimeScene);
}


{


gdjs.Level6Code.eventsList3(runtimeScene);
}


{


gdjs.Level6Code.eventsList7(runtimeScene);
}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.eventsList9 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "w");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);
{for(var i = 0, len = gdjs.Level6Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Up");
}
}{for(var i = 0, len = gdjs.Level6Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Ladder");
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "a");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level6Code.GDPlayerObjects4[i].getX() >= 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDPlayerObjects4[k] = gdjs.Level6Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level6Code.GDPlayerObjects4.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects4 */
{for(var i = 0, len = gdjs.Level6Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Left");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "d");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);
{for(var i = 0, len = gdjs.Level6Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Right");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);
{for(var i = 0, len = gdjs.Level6Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Jump");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "s");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);
{for(var i = 0, len = gdjs.Level6Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Down");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "LShift");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "RShift");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23174796);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);
{gdjs.evtsExt__Player__HealPlayer.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDCheckpointObjects3Objects = Hashtable.newFrom({"Checkpoint": gdjs.Level6Code.GDCheckpointObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.eventsList10 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.Level6Code.GDCheckpointObjects3 */
/* Reuse gdjs.Level6Code.GDPlayerObjects3 */
{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, (( gdjs.Level6Code.GDCheckpointObjects3.length === 0 ) ? 0 :gdjs.Level6Code.GDCheckpointObjects3[0].getPointX("")), (( gdjs.Level6Code.GDCheckpointObjects3.length === 0 ) ? 0 :gdjs.Level6Code.GDCheckpointObjects3[0].getPointY("")), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.Level6Code.GDCheckpointObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDCheckpointObjects3[i].getBehavior("Animation").setAnimationName("Activate");
}
}}

}


};gdjs.Level6Code.eventsList11 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);
{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, (( gdjs.Level6Code.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.Level6Code.GDPlayerObjects4[0].getPointX("")), (( gdjs.Level6Code.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.Level6Code.GDPlayerObjects4[0].getPointY("")), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Checkpoint"), gdjs.Level6Code.GDCheckpointObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDCheckpointObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDCheckpointObjects3.length;i<l;++i) {
    if ( !(gdjs.Level6Code.GDCheckpointObjects3[i].isCurrentAnimationName("Activate")) ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDCheckpointObjects3[k] = gdjs.Level6Code.GDCheckpointObjects3[i];
        ++k;
    }
}
gdjs.Level6Code.GDCheckpointObjects3.length = k;
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Checkpoint/Activate.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}
{ //Subevents
gdjs.Level6Code.eventsList10(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.eventsList12 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.Level6Code.GDPlayerObjects3[i].getY() > gdjs.evtTools.camera.getCameraBorderBottom(runtimeScene, "", 0) ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDPlayerObjects3[k] = gdjs.Level6Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level6Code.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects3 */
{gdjs.evtsExt__Player__TriggerDeath.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level6Code.eventsList13 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "AssetDev/Audio/Heartbeat_Amplified.wav", 2, true, 100, 1);
}{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level6Code.GDPlayerObjects4[i].getVariableNumber(gdjs.Level6Code.GDPlayerObjects4[i].getVariables().getFromIndex(1)) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDPlayerObjects4[k] = gdjs.Level6Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level6Code.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 60);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.getSoundOnChannelVolume(runtimeScene, 2) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.Level6Code.GDPlayerObjects3[i].getVariableNumber(gdjs.Level6Code.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDPlayerObjects3[k] = gdjs.Level6Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level6Code.GDPlayerObjects3.length = k;
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 0);
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects2});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDDustParticleObjects2Objects = Hashtable.newFrom({"DustParticle": gdjs.Level6Code.GDDustParticleObjects2});
gdjs.Level6Code.eventsList14 = function(runtimeScene) {

{


gdjs.Level6Code.eventsList13(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.Level6Code.GDPlayerObjects3[i].getBehavior("PlatformerObject").isJumping() ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDPlayerObjects3[k] = gdjs.Level6Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level6Code.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23183324);
}
}
if (isConditionTrue_0) {
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtsExt__Player__IsSteppingOnFloor.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects2Objects, "PlatformerObject", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23183804);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects2 */
gdjs.Level6Code.GDDustParticleObjects2.length = 0;

{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "grass.mp3", 1, false, 20, gdjs.randomFloatInRange(0.7, 1.2));
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDDustParticleObjects2Objects, (( gdjs.Level6Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Level6Code.GDPlayerObjects2[0].getAABBCenterX()), (( gdjs.Level6Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Level6Code.GDPlayerObjects2[0].getAABBBottom()), "");
}{for(var i = 0, len = gdjs.Level6Code.GDDustParticleObjects2.length ;i < len;++i) {
    gdjs.Level6Code.GDDustParticleObjects2[i].setZOrder(-(1));
}
}{for(var i = 0, len = gdjs.Level6Code.GDDustParticleObjects2.length ;i < len;++i) {
    gdjs.Level6Code.GDDustParticleObjects2[i].setAngle(270);
}
}}

}


};gdjs.Level6Code.eventsList15 = function(runtimeScene) {

{


gdjs.Level6Code.eventsList9(runtimeScene);
}


{


gdjs.Level6Code.eventsList11(runtimeScene);
}


{


gdjs.Level6Code.eventsList12(runtimeScene);
}


{


gdjs.Level6Code.eventsList14(runtimeScene);
}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlippingPlatformObjects5Objects = Hashtable.newFrom({"FlippingPlatform": gdjs.Level6Code.GDFlippingPlatformObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlippingPlatform2Objects5Objects = Hashtable.newFrom({"FlippingPlatform2": gdjs.Level6Code.GDFlippingPlatform2Objects5});
gdjs.Level6Code.asyncCallback23187724 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}}
gdjs.Level6Code.eventsList16 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.Level6Code.asyncCallback23187724(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects5Objects = Hashtable.newFrom({"HorizontalMovingPlatform": gdjs.Level6Code.GDHorizontalMovingPlatformObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects5Objects = Hashtable.newFrom({"HorizontalMovingPlatform": gdjs.Level6Code.GDHorizontalMovingPlatformObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects5Objects = Hashtable.newFrom({"VerticalMovingPlatform": gdjs.Level6Code.GDVerticalMovingPlatformObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects5Objects = Hashtable.newFrom({"VerticalMovingPlatform": gdjs.Level6Code.GDVerticalMovingPlatformObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects4Objects = Hashtable.newFrom({"SpinningMovingPlatform": gdjs.Level6Code.GDSpinningMovingPlatformObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects4Objects = Hashtable.newFrom({"SpinningMovingPlatform": gdjs.Level6Code.GDSpinningMovingPlatformObjects4});
gdjs.Level6Code.eventsList17 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level6Code.GDFlippingPlatformObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlippingPlatformObjects5Objects);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level6Code.GDFlippingPlatform2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlippingPlatform2Objects5Objects);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}
{ //Subevents
gdjs.Level6Code.eventsList16(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.Level6Code.GDHorizontalMovingPlatformObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDHorizontalMovingPlatformObjects5 */
{gdjs.evtsExt__Enemy__ResetPlatform.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.Level6Code.GDVerticalMovingPlatformObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDVerticalMovingPlatformObjects5 */
{gdjs.evtsExt__Enemy__ResetPlatform.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.Level6Code.GDSpinningMovingPlatformObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDSpinningMovingPlatformObjects4 */
{gdjs.evtsExt__Enemy__ResetPlatform.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects5Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level6Code.GDFlyingDemonObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects5Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level6Code.GDFlyingDemonObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects5Objects = Hashtable.newFrom({"FireDemon": gdjs.Level6Code.GDFireDemonObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects5Objects = Hashtable.newFrom({"FireDemon": gdjs.Level6Code.GDFireDemonObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects5Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level6Code.GDHorizontalDemonObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects5Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level6Code.GDHorizontalDemonObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.Level6Code.GDSpikeDemon_9595Base2Objects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.Level6Code.GDSpikeDemon_9595Base2Objects4});
gdjs.Level6Code.eventsList18 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.Level6Code.GDFlyingDemonObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDFlyingDemonObjects5 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level6Code.GDFireDemonObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDFireDemonObjects5 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.Level6Code.GDHorizontalDemonObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDHorizontalDemonObjects5 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.Level6Code.GDSpikeDemon_9595Base2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Base2Objects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDSpikeDemon_9595Base2Objects4 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Base2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595SpikeObjects5Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Spike2Objects5Objects = Hashtable.newFrom({"SpikeDemon_Spike2": gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595SpikeObjects5Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Spike2Objects5Objects = Hashtable.newFrom({"StalagtiteDemon_Spike2": gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595SpikeObjects5Objects = Hashtable.newFrom({"StalagmiteDemon_Spike": gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike2": gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4});
gdjs.Level6Code.eventsList19 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595SpikeObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects5 */
{for(var i = 0, len = gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike2"), gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Spike2Objects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects5 */
{for(var i = 0, len = gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects5.length ;i < len;++i) {
    gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike"), gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595SpikeObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects5 */
{for(var i = 0, len = gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike2"), gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Spike2Objects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects5 */
{for(var i = 0, len = gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects5.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike"), gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595SpikeObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects5 */
{for(var i = 0, len = gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike2"), gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Spike2Objects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4 */
{for(var i = 0, len = gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level6Code.asyncCallback23199180 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy2");
}}
gdjs.Level6Code.eventsList20 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.Level6Code.asyncCallback23199180(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.asyncCallback23199940 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}}
gdjs.Level6Code.eventsList21 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.Level6Code.asyncCallback23199940(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.asyncCallback23200748 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy2");
}}
gdjs.Level6Code.eventsList22 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(4.25), (runtimeScene) => (gdjs.Level6Code.asyncCallback23200748(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.asyncCallback23201476 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy");
}}
gdjs.Level6Code.eventsList23 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.Level6Code.asyncCallback23201476(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.asyncCallback23202252 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy2");
}}
gdjs.Level6Code.eventsList24 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(4.25), (runtimeScene) => (gdjs.Level6Code.asyncCallback23202252(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.eventsList25 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.Level6Code.GDLaserDemon_9595BeamObjects4);
{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects4[i].setPosition(0,6000);
}
}{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0);
}
}{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.25);
}
}}

}


};gdjs.Level6Code.eventsList26 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy2");
}
{ //Subevents
gdjs.Level6Code.eventsList20(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}
{ //Subevents
gdjs.Level6Code.eventsList21(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy2");
}
{ //Subevents
gdjs.Level6Code.eventsList22(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy");
}
{ //Subevents
gdjs.Level6Code.eventsList23(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy2");
}
{ //Subevents
gdjs.Level6Code.eventsList24(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "LaserEnemy");
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level6Code.GDLaserDemon_9595BaseObjects4);
{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BaseObjects4[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
}{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BaseObjects4[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
}{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BaseObjects4[i].getBehavior("Opacity").setOpacity(40);
}
}
{ //Subevents
gdjs.Level6Code.eventsList25(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.eventsList27 = function(runtimeScene) {

{


gdjs.Level6Code.eventsList17(runtimeScene);
}


{


gdjs.Level6Code.eventsList18(runtimeScene);
}


{


gdjs.Level6Code.eventsList19(runtimeScene);
}


{


gdjs.Level6Code.eventsList26(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.Level6Code.GDPlayerObjects3 */
{for(var i = 0, len = gdjs.Level6Code.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDPlayerObjects3[i].setVariableBoolean(gdjs.Level6Code.GDPlayerObjects3[i].getVariables().getFromIndex(4), false);
}
}}

}


};gdjs.Level6Code.eventsList28 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.Level6Code.GDPlayerObjects3[i].getVariableBoolean(gdjs.Level6Code.GDPlayerObjects3[i].getVariables().getFromIndex(4), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDPlayerObjects3[k] = gdjs.Level6Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level6Code.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList27(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects4Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level6Code.GDFlyingDemonObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level6Code.GDFlyingDemonObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level6Code.GDFlyingDemonObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level6Code.GDFlyingDemonObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.Level6Code.GDMonsterParticlesObjects3});
gdjs.Level6Code.eventsList29 = function(runtimeScene) {

{

/* Reuse gdjs.Level6Code.GDFlyingDemonObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDFlyingDemonObjects3.length;i<l;++i) {
    if ( gdjs.Level6Code.GDFlyingDemonObjects3[i].getVariableBoolean(gdjs.Level6Code.GDFlyingDemonObjects3[i].getVariables().getFromIndex(1), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDFlyingDemonObjects3[k] = gdjs.Level6Code.GDFlyingDemonObjects3[i];
        ++k;
    }
}
gdjs.Level6Code.GDFlyingDemonObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDFlyingDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.Level6Code.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects3Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level6Code.eventsList30 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.Level6Code.GDFlyingDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDFlyingDemonObjects3 */
/* Reuse gdjs.Level6Code.GDPlayerObjects3 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.Level6Code.eventsList29(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.eventsList31 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.Level6Code.GDFlyingDemonObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Level6Code.eventsList30(runtimeScene);
}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects4Objects = Hashtable.newFrom({"FireDemon": gdjs.Level6Code.GDFireDemonObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.Level6Code.GDFireDemonObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.Level6Code.GDFireDemonObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.Level6Code.GDFireDemonObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.Level6Code.GDMonsterParticlesObjects3});
gdjs.Level6Code.eventsList32 = function(runtimeScene) {

{

/* Reuse gdjs.Level6Code.GDFireDemonObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDFireDemonObjects3.length;i<l;++i) {
    if ( gdjs.Level6Code.GDFireDemonObjects3[i].getVariableBoolean(gdjs.Level6Code.GDFireDemonObjects3[i].getVariables().getFromIndex(2), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDFireDemonObjects3[k] = gdjs.Level6Code.GDFireDemonObjects3[i];
        ++k;
    }
}
gdjs.Level6Code.GDFireDemonObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDFireDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.Level6Code.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects3Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level6Code.eventsList33 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level6Code.GDFireDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDFireDemonObjects3 */
/* Reuse gdjs.Level6Code.GDPlayerObjects3 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.Level6Code.eventsList32(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.eventsList34 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level6Code.GDFireDemonObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level6Code.GDFireDemonObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDFireDemonObjects4.length;i<l;++i) {
    if ( gdjs.Level6Code.GDFireDemonObjects4[i].getBehavior("Animation").getAnimationName() == "Fire" ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDFireDemonObjects4[k] = gdjs.Level6Code.GDFireDemonObjects4[i];
        ++k;
    }
}
gdjs.Level6Code.GDFireDemonObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDFireDemonObjects4 */
{for(var i = 0, len = gdjs.Level6Code.GDFireDemonObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDFireDemonObjects4[i].returnVariable(gdjs.Level6Code.GDFireDemonObjects4[i].getVariables().getFromIndex(1)).setNumber(0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level6Code.GDFireDemonObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDFireDemonObjects4.length;i<l;++i) {
    if ( !(gdjs.Level6Code.GDFireDemonObjects4[i].getBehavior("Animation").getAnimationName() == "Fire") ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDFireDemonObjects4[k] = gdjs.Level6Code.GDFireDemonObjects4[i];
        ++k;
    }
}
gdjs.Level6Code.GDFireDemonObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDFireDemonObjects4 */
{for(var i = 0, len = gdjs.Level6Code.GDFireDemonObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDFireDemonObjects4[i].returnVariable(gdjs.Level6Code.GDFireDemonObjects4[i].getVariables().getFromIndex(1)).setNumber(1);
}
}}

}


{


gdjs.Level6Code.eventsList33(runtimeScene);
}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects4Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level6Code.GDHorizontalDemonObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level6Code.GDHorizontalDemonObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level6Code.GDHorizontalDemonObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level6Code.GDHorizontalDemonObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.Level6Code.GDMonsterParticlesObjects3});
gdjs.Level6Code.eventsList35 = function(runtimeScene) {

{

/* Reuse gdjs.Level6Code.GDHorizontalDemonObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDHorizontalDemonObjects3.length;i<l;++i) {
    if ( gdjs.Level6Code.GDHorizontalDemonObjects3[i].getVariableBoolean(gdjs.Level6Code.GDHorizontalDemonObjects3[i].getVariables().getFromIndex(1), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDHorizontalDemonObjects3[k] = gdjs.Level6Code.GDHorizontalDemonObjects3[i];
        ++k;
    }
}
gdjs.Level6Code.GDHorizontalDemonObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDHorizontalDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.Level6Code.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects3Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level6Code.eventsList36 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.Level6Code.GDHorizontalDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDHorizontalDemonObjects3 */
/* Reuse gdjs.Level6Code.GDPlayerObjects3 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.Level6Code.eventsList35(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.eventsList37 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.Level6Code.GDHorizontalDemonObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Level6Code.eventsList36(runtimeScene);
}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level6Code.GDSpikeDemon_9595BaseObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDMonsterParticlesObjects5Objects = Hashtable.newFrom({"MonsterParticles": gdjs.Level6Code.GDMonsterParticlesObjects5});
gdjs.Level6Code.eventsList38 = function(runtimeScene) {

{

/* Reuse gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5[i].getVariableBoolean(gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5[i].getVariables().getFromIndex(1), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5[k] = gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.Level6Code.GDMonsterParticlesObjects5);
/* Reuse gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5 */
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects5Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDMonsterParticlesObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects4});
gdjs.Level6Code.eventsList39 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects5 */
/* Reuse gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.Level6Code.eventsList38(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595SpikeObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects4 */
/* Reuse gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595SpikeObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level6Code.userFunc0x990258 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
var NumSpikes = 6;
var SpikeScale = 64;

for (var x = 0; x < objects.length; x++) {
    var SpikeDemonBaseInstance = objects[x];

    for (var i = 0; i < NumSpikes; i++) {
        var SpikeAngle = (360/NumSpikes)*i;

        const Spike = runtimeScene.createObject("SpikeDemon_Spike");
        Spike.setWidth(SpikeScale);
        Spike.setHeight(SpikeScale);
        console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
        var CenterX = SpikeDemonBaseInstance.x + SpikeDemonBaseInstance.getWidth()/3.6;
        var CenterY = SpikeDemonBaseInstance.y + SpikeDemonBaseInstance.getHeight()/3.2;
        Spike.setPosition(CenterX, CenterY);
        Spike.setAngle(SpikeAngle);
        
        Spike.setLayer("Base Layer");


    }
}
};
gdjs.Level6Code.eventsList40 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level6Code.GDSpikeDemon_9595BaseObjects4);

var objects = [];
objects.push.apply(objects,gdjs.Level6Code.GDSpikeDemon_9595BaseObjects4);
gdjs.Level6Code.userFunc0x990258(runtimeScene, objects);

}


};gdjs.Level6Code.eventsList41 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level6Code.GDSpikeDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.Level6Code.GDSpikeDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDSpikeDemon_9595BaseObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 4, 4, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level6Code.eventsList42 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy") >= 6;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy");
}
{ //Subevents
gdjs.Level6Code.eventsList40(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy") >= 4.5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList41(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.eventsList43 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level6Code.GDSpikeDemon_9595BaseObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Level6Code.eventsList39(runtimeScene);
}


{


gdjs.Level6Code.eventsList42(runtimeScene);
}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.Level6Code.GDSpikeDemon_9595Base2Objects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDMonsterParticlesObjects5Objects = Hashtable.newFrom({"MonsterParticles": gdjs.Level6Code.GDMonsterParticlesObjects5});
gdjs.Level6Code.eventsList44 = function(runtimeScene) {

{

/* Reuse gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5.length;i<l;++i) {
    if ( gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5[i].getVariableBoolean(gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5[i].getVariables().getFromIndex(1), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5[k] = gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5[i];
        ++k;
    }
}
gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.Level6Code.GDMonsterParticlesObjects5);
/* Reuse gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5 */
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Base2Objects5Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDMonsterParticlesObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Spike2": gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Spike2": gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects4});
gdjs.Level6Code.eventsList45 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Base2Objects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects5 */
/* Reuse gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Base2Objects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.Level6Code.eventsList44(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike2"), gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Spike2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects4 */
/* Reuse gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Spike2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level6Code.asyncCallback23227740 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy2");
}}
gdjs.Level6Code.eventsList46 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.Level6Code.asyncCallback23227740(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.userFunc0x13a2c30 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
var NumSpikes = 6;
var SpikeScale = 64;

for (var x = 0; x < objects.length; x++) {
    var SpikeDemonBaseInstance = objects[x];

    for (var i = 0; i < NumSpikes; i++) {
        var SpikeAngle = (360/NumSpikes)*i;

        const Spike = runtimeScene.createObject("SpikeDemon_Spike2");
        Spike.setWidth(SpikeScale);
        Spike.setHeight(SpikeScale);
        console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
        var CenterX = SpikeDemonBaseInstance.x + SpikeDemonBaseInstance.getWidth()/3.6;
        var CenterY = SpikeDemonBaseInstance.y + SpikeDemonBaseInstance.getHeight()/3.2;
        Spike.setPosition(CenterX, CenterY);
        Spike.setAngle(SpikeAngle);
        
        Spike.setLayer("Base Layer");


    }
}
};
gdjs.Level6Code.eventsList47 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.Level6Code.GDSpikeDemon_9595Base2Objects4);

var objects = [];
objects.push.apply(objects,gdjs.Level6Code.GDSpikeDemon_9595Base2Objects4);
gdjs.Level6Code.userFunc0x13a2c30(runtimeScene, objects);

}


};gdjs.Level6Code.eventsList48 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.Level6Code.GDSpikeDemon_9595Base2Objects3);
{for(var i = 0, len = gdjs.Level6Code.GDSpikeDemon_9595Base2Objects3.length ;i < len;++i) {
    gdjs.Level6Code.GDSpikeDemon_9595Base2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 4, 4, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level6Code.eventsList49 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList46(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy2") >= 6;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy2");
}
{ //Subevents
gdjs.Level6Code.eventsList47(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy2") >= 4.5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList48(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.eventsList50 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.Level6Code.GDSpikeDemon_9595Base2Objects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595Base2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Level6Code.eventsList45(runtimeScene);
}


{


gdjs.Level6Code.eventsList49(runtimeScene);
}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4});
gdjs.Level6Code.eventsList51 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595BaseObjects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects5 */
/* Reuse gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595BaseObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike"), gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595SpikeObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects4 */
/* Reuse gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595SpikeObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatform2Objects4Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.Level6Code.GDStaticPlatform1Objects4, "HorizontalMovingPlatform": gdjs.Level6Code.GDHorizontalMovingPlatformObjects4, "StaticPlatform2": gdjs.Level6Code.GDStaticPlatform2Objects4, "StaticPlatform3": gdjs.Level6Code.GDStaticPlatform3Objects4, "VerticalMovingPlatform": gdjs.Level6Code.GDVerticalMovingPlatformObjects4, "SpinningMovingPlatform": gdjs.Level6Code.GDSpinningMovingPlatformObjects4, "FlippingPlatform": gdjs.Level6Code.GDFlippingPlatformObjects4, "FlippingPlatform2": gdjs.Level6Code.GDFlippingPlatform2Objects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeParticlesObjects4Objects = Hashtable.newFrom({"SpikeParticles": gdjs.Level6Code.GDSpikeParticlesObjects4});
gdjs.Level6Code.eventsList52 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level6Code.GDFlippingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level6Code.GDFlippingPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.Level6Code.GDHorizontalMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.Level6Code.GDSpinningMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike"), gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.Level6Code.GDStaticPlatform1Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.Level6Code.GDStaticPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.Level6Code.GDStaticPlatform3Objects4);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.Level6Code.GDVerticalMovingPlatformObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595SpikeObjects4Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatform2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4 */
gdjs.Level6Code.GDSpikeParticlesObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeParticlesObjects4Objects, (( gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4[0].getPointX("")) + (( gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4[0].getWidth()) / 2, (( gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4[0].getPointY("")) + (( gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level6Code.asyncCallback23236220 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}}
gdjs.Level6Code.eventsList53 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.Level6Code.asyncCallback23236220(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.userFunc0x10a7d98 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagtite = objects[x];
    const Spike = runtimeScene.createObject("StalagtiteDemon_Spike");
    Spike.setWidth(96);
    Spike.setHeight(48);
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
    var CenterX = Stalagtite.x + Stalagtite.getWidth()/3;
    var CenterY = Stalagtite.y + Stalagtite.getHeight() - Stalagtite.getHeight()/3;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(90);
    Spike.setLayer("Base Layer");
}
};
gdjs.Level6Code.eventsList54 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects4);

var objects = [];
objects.push.apply(objects,gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects4);
gdjs.Level6Code.userFunc0x10a7d98(runtimeScene, objects);

}


};gdjs.Level6Code.eventsList55 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList53(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy") >= 6.5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}
{ //Subevents
gdjs.Level6Code.eventsList54(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy") >= 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level6Code.eventsList56 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Level6Code.eventsList51(runtimeScene);
}


{


gdjs.Level6Code.eventsList52(runtimeScene);
}


{


gdjs.Level6Code.eventsList55(runtimeScene);
}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"StalagtiteDemon_Base2": gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"StalagtiteDemon_Base2": gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"StalagtiteDemon_Base2": gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike2": gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike2": gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4});
gdjs.Level6Code.eventsList57 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base2"), gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Base2Objects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects5 */
/* Reuse gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Base2Objects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike2"), gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Spike2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects4 */
/* Reuse gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Spike2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike2": gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatform2Objects4Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.Level6Code.GDStaticPlatform1Objects4, "HorizontalMovingPlatform": gdjs.Level6Code.GDHorizontalMovingPlatformObjects4, "StaticPlatform2": gdjs.Level6Code.GDStaticPlatform2Objects4, "StaticPlatform3": gdjs.Level6Code.GDStaticPlatform3Objects4, "VerticalMovingPlatform": gdjs.Level6Code.GDVerticalMovingPlatformObjects4, "SpinningMovingPlatform": gdjs.Level6Code.GDSpinningMovingPlatformObjects4, "FlippingPlatform": gdjs.Level6Code.GDFlippingPlatformObjects4, "FlippingPlatform2": gdjs.Level6Code.GDFlippingPlatform2Objects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeParticlesObjects4Objects = Hashtable.newFrom({"SpikeParticles": gdjs.Level6Code.GDSpikeParticlesObjects4});
gdjs.Level6Code.eventsList58 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level6Code.GDFlippingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level6Code.GDFlippingPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.Level6Code.GDHorizontalMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.Level6Code.GDSpinningMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike2"), gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.Level6Code.GDStaticPlatform1Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.Level6Code.GDStaticPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.Level6Code.GDStaticPlatform3Objects4);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.Level6Code.GDVerticalMovingPlatformObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Spike2Objects4Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatform2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4 */
gdjs.Level6Code.GDSpikeParticlesObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeParticlesObjects4Objects, (( gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4[0].getPointX("")) + (( gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4[0].getWidth()) / 2, (( gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4[0].getPointY("")) + (( gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level6Code.asyncCallback23244284 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy2");
}}
gdjs.Level6Code.eventsList59 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(4.25), (runtimeScene) => (gdjs.Level6Code.asyncCallback23244284(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.userFunc0x10c13e8 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagtite = objects[x];
    const Spike = runtimeScene.createObject("StalagtiteDemon_Spike2");
    Spike.setWidth(96);
    Spike.setHeight(48);
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
    var CenterX = Stalagtite.x + Stalagtite.getWidth()/3;
    var CenterY = Stalagtite.y + Stalagtite.getHeight() - Stalagtite.getHeight()/3;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(90);
    Spike.setLayer("Base Layer");
}
};
gdjs.Level6Code.eventsList60 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base2"), gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects4);

var objects = [];
objects.push.apply(objects,gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects4);
gdjs.Level6Code.userFunc0x10c13e8(runtimeScene, objects);

}


};gdjs.Level6Code.eventsList61 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList59(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy2") >= 6.5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy2");
}
{ //Subevents
gdjs.Level6Code.eventsList60(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy2") >= 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base2"), gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects3);
{for(var i = 0, len = gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects3.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level6Code.eventsList62 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base2"), gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595Base2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Level6Code.eventsList57(runtimeScene);
}


{


gdjs.Level6Code.eventsList58(runtimeScene);
}


{


gdjs.Level6Code.eventsList61(runtimeScene);
}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"StalagmiteDemon_Base": gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"StalagmiteDemon_Base": gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"StalagmiteDemon_Base": gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike": gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike": gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4});
gdjs.Level6Code.eventsList63 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595BaseObjects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects5 */
/* Reuse gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595BaseObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike"), gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595SpikeObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects4 */
/* Reuse gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595SpikeObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike": gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatform2Objects4Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.Level6Code.GDStaticPlatform1Objects4, "HorizontalMovingPlatform": gdjs.Level6Code.GDHorizontalMovingPlatformObjects4, "StaticPlatform2": gdjs.Level6Code.GDStaticPlatform2Objects4, "StaticPlatform3": gdjs.Level6Code.GDStaticPlatform3Objects4, "VerticalMovingPlatform": gdjs.Level6Code.GDVerticalMovingPlatformObjects4, "SpinningMovingPlatform": gdjs.Level6Code.GDSpinningMovingPlatformObjects4, "FlippingPlatform": gdjs.Level6Code.GDFlippingPlatformObjects4, "FlippingPlatform2": gdjs.Level6Code.GDFlippingPlatform2Objects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeParticlesObjects4Objects = Hashtable.newFrom({"SpikeParticles": gdjs.Level6Code.GDSpikeParticlesObjects4});
gdjs.Level6Code.eventsList64 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level6Code.GDFlippingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level6Code.GDFlippingPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.Level6Code.GDHorizontalMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.Level6Code.GDSpinningMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike"), gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.Level6Code.GDStaticPlatform1Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.Level6Code.GDStaticPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.Level6Code.GDStaticPlatform3Objects4);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.Level6Code.GDVerticalMovingPlatformObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595SpikeObjects4Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatform2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4 */
gdjs.Level6Code.GDSpikeParticlesObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeParticlesObjects4Objects, (( gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4[0].getPointX("")) + (( gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4[0].getWidth()) / 2, (( gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4[0].getPointY("")) + (( gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level6Code.asyncCallback23252316 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy");
}}
gdjs.Level6Code.eventsList65 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.Level6Code.asyncCallback23252316(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.userFunc0x107e708 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagmite = objects[x];
    const Spike = runtimeScene.createObject("StalagmiteDemon_Spike");
    Spike.setWidth(96);
    Spike.setHeight(48);
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
    var CenterX = Stalagmite.x + Stalagmite.getWidth()/3;
    var CenterY = Stalagmite.y;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(270);
    Spike.setLayer("Base Layer");
}
};
gdjs.Level6Code.eventsList66 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects4);

var objects = [];
objects.push.apply(objects,gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects4);
gdjs.Level6Code.userFunc0x107e708(runtimeScene, objects);

}


};gdjs.Level6Code.eventsList67 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList65(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagmiteEnemy") >= 6.5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy");
}
{ //Subevents
gdjs.Level6Code.eventsList66(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagmiteEnemy") >= 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level6Code.eventsList68 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Level6Code.eventsList63(runtimeScene);
}


{


gdjs.Level6Code.eventsList64(runtimeScene);
}


{


gdjs.Level6Code.eventsList67(runtimeScene);
}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Base2": gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"StalagmiteDemon_Base2": gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"StalagmiteDemon_Base2": gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects5});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike2": gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike2": gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4});
gdjs.Level6Code.eventsList69 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base2"), gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Base2Objects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects5 */
/* Reuse gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Base2Objects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike2"), gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Spike2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects4 */
/* Reuse gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Spike2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike2": gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatform2Objects4Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.Level6Code.GDStaticPlatform1Objects4, "HorizontalMovingPlatform": gdjs.Level6Code.GDHorizontalMovingPlatformObjects4, "StaticPlatform2": gdjs.Level6Code.GDStaticPlatform2Objects4, "StaticPlatform3": gdjs.Level6Code.GDStaticPlatform3Objects4, "VerticalMovingPlatform": gdjs.Level6Code.GDVerticalMovingPlatformObjects4, "SpinningMovingPlatform": gdjs.Level6Code.GDSpinningMovingPlatformObjects4, "FlippingPlatform": gdjs.Level6Code.GDFlippingPlatformObjects4, "FlippingPlatform2": gdjs.Level6Code.GDFlippingPlatform2Objects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeParticlesObjects4Objects = Hashtable.newFrom({"SpikeParticles": gdjs.Level6Code.GDSpikeParticlesObjects4});
gdjs.Level6Code.eventsList70 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level6Code.GDFlippingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level6Code.GDFlippingPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.Level6Code.GDHorizontalMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.Level6Code.GDSpinningMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike2"), gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.Level6Code.GDStaticPlatform1Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.Level6Code.GDStaticPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.Level6Code.GDStaticPlatform3Objects4);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.Level6Code.GDVerticalMovingPlatformObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Spike2Objects4Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546Level6Code_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546Level6Code_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546Level6Code_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546Level6Code_9546GDFlippingPlatform2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4 */
gdjs.Level6Code.GDSpikeParticlesObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeParticlesObjects4Objects, (( gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4[0].getPointX("")) + (( gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4[0].getWidth()) / 2, (( gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4[0].getPointY("")) + (( gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level6Code.asyncCallback23260420 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy2");
}}
gdjs.Level6Code.eventsList71 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(4.25), (runtimeScene) => (gdjs.Level6Code.asyncCallback23260420(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.userFunc0x1222008 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagmite = objects[x];
    const Spike = runtimeScene.createObject("StalagmiteDemon_Spike2");
    Spike.setWidth(96);
    Spike.setHeight(48);
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
    var CenterX = Stalagmite.x + Stalagmite.getWidth()/3;
    var CenterY = Stalagmite.y;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(270);
    Spike.setLayer("Base Layer");
}
};
gdjs.Level6Code.eventsList72 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base2"), gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects4);

var objects = [];
objects.push.apply(objects,gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects4);
gdjs.Level6Code.userFunc0x1222008(runtimeScene, objects);

}


};gdjs.Level6Code.eventsList73 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList71(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagmiteEnemy2") >= 6.5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy2");
}
{ //Subevents
gdjs.Level6Code.eventsList72(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagmiteEnemy2") >= 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base2"), gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects3);
{for(var i = 0, len = gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects3.length ;i < len;++i) {
    gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level6Code.eventsList74 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base2"), gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagmiteDemon_95959595Base2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Level6Code.eventsList69(runtimeScene);
}


{


gdjs.Level6Code.eventsList70(runtimeScene);
}


{


gdjs.Level6Code.eventsList73(runtimeScene);
}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"LaserDemon_Base": gdjs.Level6Code.GDLaserDemon_9595BaseObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BeamObjects3Objects = Hashtable.newFrom({"LaserDemon_Beam": gdjs.Level6Code.GDLaserDemon_9595BeamObjects3});
gdjs.Level6Code.eventsList75 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.Level6Code.GDLaserDemon_9595BeamObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BeamObjects3Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDLaserDemon_9595BeamObjects3 */
{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects3[i].returnVariable(gdjs.Level6Code.GDLaserDemon_9595BeamObjects3[i].getVariables().getFromIndex(1)).setNumber((gdjs.Level6Code.GDLaserDemon_9595BeamObjects3[i].getPointX("")));
}
}{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects3[i].returnVariable(gdjs.Level6Code.GDLaserDemon_9595BeamObjects3[i].getVariables().getFromIndex(2)).setNumber((gdjs.Level6Code.GDLaserDemon_9595BeamObjects3[i].getPointY("")));
}
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"LaserDemon_Base": gdjs.Level6Code.GDLaserDemon_9595BaseObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"LaserDemon_Base": gdjs.Level6Code.GDLaserDemon_9595BaseObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"LaserDemon_Base": gdjs.Level6Code.GDLaserDemon_9595BaseObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDMonsterParticlesObjects4Objects = Hashtable.newFrom({"MonsterParticles": gdjs.Level6Code.GDMonsterParticlesObjects4});
gdjs.Level6Code.eventsList76 = function(runtimeScene) {

{

/* Reuse gdjs.Level6Code.GDLaserDemon_9595BaseObjects4 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDLaserDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level6Code.GDLaserDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level6Code.GDLaserDemon_9595BaseObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDLaserDemon_9595BaseObjects4[k] = gdjs.Level6Code.GDLaserDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level6Code.GDLaserDemon_9595BaseObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDLaserDemon_9595BaseObjects4 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.Level6Code.GDMonsterParticlesObjects4);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BaseObjects4Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDMonsterParticlesObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BeamObjects3Objects = Hashtable.newFrom({"LaserDemon_Beam": gdjs.Level6Code.GDLaserDemon_9595BeamObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BeamObjects3Objects = Hashtable.newFrom({"LaserDemon_Beam": gdjs.Level6Code.GDLaserDemon_9595BeamObjects3});
gdjs.Level6Code.eventsList77 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level6Code.GDLaserDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BaseObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDLaserDemon_9595BaseObjects4 */
/* Reuse gdjs.Level6Code.GDPlayerObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.Level6Code.eventsList76(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.Level6Code.GDLaserDemon_9595BeamObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BeamObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDLaserDemon_9595BeamObjects3 */
/* Reuse gdjs.Level6Code.GDPlayerObjects3 */
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects3);
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BeamObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects3[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level6Code.eventsList78 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.Level6Code.GDLaserDemon_9595BeamObjects3);
{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects3[i].setPosition(0,6000);
}
}{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects3[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0);
}
}{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects3[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.25);
}
}}

}


};gdjs.Level6Code.asyncCallback23276348 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.Level6Code.GDLaserDemon_9595BeamObjects6);

{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects6.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects6[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 2);
}
}}
gdjs.Level6Code.eventsList79 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.Level6Code.GDLaserDemon_9595BeamObjects5) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.Level6Code.asyncCallback23276348(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.asyncCallback23276180 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.Level6Code.GDLaserDemon_9595BeamObjects5);

{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects5.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects5[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 1);
}
}
{ //Subevents
gdjs.Level6Code.eventsList79(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.Level6Code.eventsList80 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.Level6Code.GDLaserDemon_9595BeamObjects4) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.Level6Code.asyncCallback23276180(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.asyncCallback23275644 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.Level6Code.GDLaserDemon_9595BeamObjects4);

{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.5);
}
}
{ //Subevents
gdjs.Level6Code.eventsList80(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.Level6Code.eventsList81 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
for (const obj of gdjs.Level6Code.GDLaserDemon_9595BeamObjects3) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.Level6Code.asyncCallback23275644(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.eventsList82 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.Level6Code.GDLaserDemon_9595BeamObjects3);
{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects3[i].setPosition(gdjs.Level6Code.GDLaserDemon_9595BeamObjects3[i].getVariables().getFromIndex(1).getAsNumber(),gdjs.Level6Code.GDLaserDemon_9595BeamObjects3[i].getVariables().getFromIndex(2).getAsNumber());
}
}
{ //Subevents
gdjs.Level6Code.eventsList81(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.asyncCallback23281572 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.Level6Code.GDLaserDemon_9595BeamObjects5);

{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects5.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects5[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0);
}
}}
gdjs.Level6Code.eventsList83 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.Level6Code.GDLaserDemon_9595BeamObjects4) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.1), (runtimeScene) => (gdjs.Level6Code.asyncCallback23281572(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.asyncCallback23280628 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.Level6Code.GDLaserDemon_9595BeamObjects4);

{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0);
}
}{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.3);
}
}
{ //Subevents
gdjs.Level6Code.eventsList83(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.Level6Code.eventsList84 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.Level6Code.GDLaserDemon_9595BeamObjects3) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.1), (runtimeScene) => (gdjs.Level6Code.asyncCallback23280628(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.asyncCallback23280172 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.Level6Code.GDLaserDemon_9595BeamObjects3);

{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects3[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0.5);
}
}
{ //Subevents
gdjs.Level6Code.eventsList84(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.Level6Code.eventsList85 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
for (const obj of gdjs.Level6Code.GDLaserDemon_9595BeamObjects2) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.1), (runtimeScene) => (gdjs.Level6Code.asyncCallback23280172(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level6Code.eventsList86 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.Level6Code.GDLaserDemon_9595BeamObjects2);
{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BeamObjects2.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BeamObjects2[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 1.25);
}
}
{ //Subevents
gdjs.Level6Code.eventsList85(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.eventsList87 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "LaserEnemy");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 10;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "LaserEnemy");
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 4;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23270692);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level6Code.GDLaserDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BaseObjects3[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
}{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BaseObjects3[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
}{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BaseObjects3[i].getBehavior("Opacity").setOpacity(40);
}
}
{ //Subevents
gdjs.Level6Code.eventsList78(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 2.75;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 4;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23273532);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level6Code.GDLaserDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BaseObjects3[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "easeInQuad", 1.25, false);
}
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 4;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 10;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23275188);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList82(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 9;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 10;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23277988);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level6Code.GDLaserDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.Level6Code.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDLaserDemon_9595BaseObjects3[i].getBehavior("Tween").addObjectOpacityTween2("ChargeDown", 40, "easeInQuad", 1, false);
}
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 9.7;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 10;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23279652);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList86(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.eventsList88 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level6Code.GDLaserDemon_9595BaseObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDLaserDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.Level6Code.eventsList75(runtimeScene);} //End of subevents
}

}


{


gdjs.Level6Code.eventsList77(runtimeScene);
}


{


gdjs.Level6Code.eventsList87(runtimeScene);
}


};gdjs.Level6Code.eventsList89 = function(runtimeScene) {

{


gdjs.Level6Code.eventsList28(runtimeScene);
}


{


gdjs.Level6Code.eventsList31(runtimeScene);
}


{


gdjs.Level6Code.eventsList34(runtimeScene);
}


{


gdjs.Level6Code.eventsList37(runtimeScene);
}


{


gdjs.Level6Code.eventsList43(runtimeScene);
}


{


gdjs.Level6Code.eventsList50(runtimeScene);
}


{


gdjs.Level6Code.eventsList56(runtimeScene);
}


{


gdjs.Level6Code.eventsList62(runtimeScene);
}


{


gdjs.Level6Code.eventsList68(runtimeScene);
}


{


gdjs.Level6Code.eventsList74(runtimeScene);
}


{


gdjs.Level6Code.eventsList88(runtimeScene);
}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPortalObjects4Objects = Hashtable.newFrom({"Portal": gdjs.Level6Code.GDPortalObjects4});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPortalObjects3Objects = Hashtable.newFrom({"Portal": gdjs.Level6Code.GDPortalObjects3});
gdjs.Level6Code.eventsList90 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.Level6Code.GDPlayerObjects3, gdjs.Level6Code.GDPlayerObjects4);

{for(var i = 0, len = gdjs.Level6Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDPlayerObjects4[i].activateBehavior("PlatformerObject", false);
}
}}

}


{

/* Reuse gdjs.Level6Code.GDPlayerObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.Level6Code.GDPlayerObjects3[i].getVariableNumber(gdjs.Level6Code.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDPlayerObjects3[k] = gdjs.Level6Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level6Code.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDPlayerObjects3 */
/* Reuse gdjs.Level6Code.GDPortalObjects3 */
{gdjs.evtsExt__Player__AnimateFallingIntoPortal.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, "Tween", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPortalObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level6Code.eventsList91 = function(runtimeScene) {

{


gdjs.Level6Code.eventsList90(runtimeScene);
}


};gdjs.Level6Code.eventsList92 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level6Code.GDPlayerObjects4[i].getVariableNumber(gdjs.Level6Code.GDPlayerObjects4[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDPlayerObjects4[k] = gdjs.Level6Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level6Code.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_DeathText"), gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects4);
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_RetryButton"), gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects4);
{for(var i = 0, len = gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects4[i].hide();
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDPlayerObjects3.length;i<l;++i) {
    if ( !(gdjs.Level6Code.GDPlayerObjects3[i].getVariableNumber(gdjs.Level6Code.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 0) ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDPlayerObjects3[k] = gdjs.Level6Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level6Code.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_MemoryAcquired"), gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3);
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_ProceedButton"), gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects3);
{for(var i = 0, len = gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3[i].hide();
}
}{for(var i = 0, len = gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects3[i].hide();
}
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDUI_95959595Sinage_95959595BackgroundObjects2Objects = Hashtable.newFrom({"UI_Sinage_Background": gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects2});
gdjs.Level6Code.eventsList93 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23287540);
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList92(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_Background"), gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects2);
{gdjs.evtsExt__UserInterface__StretchToFillScreen.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDUI_95959595Sinage_95959595BackgroundObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{}}

}


};gdjs.Level6Code.eventsList94 = function(runtimeScene) {

{


gdjs.Level6Code.eventsList93(runtimeScene);
}


};gdjs.Level6Code.eventsList95 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_Background"), gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects3);
{gdjs.evtTools.camera.hideLayer(runtimeScene, "EndScreen");
}{for(var i = 0, len = gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects3[i].setOpacity(180);
}
}}

}


{



}


{

gdjs.Level6Code.GDPlayerObjects3.length = 0;

gdjs.Level6Code.GDPortalObjects3.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.Level6Code.GDPlayerObjects3_1final.length = 0;
gdjs.Level6Code.GDPortalObjects3_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.Level6Code.GDPortalObjects4);
isConditionTrue_1 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects4Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPortalObjects4Objects, false, runtimeScene, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level6Code.GDPlayerObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level6Code.GDPlayerObjects3_1final.indexOf(gdjs.Level6Code.GDPlayerObjects4[j]) === -1 )
            gdjs.Level6Code.GDPlayerObjects3_1final.push(gdjs.Level6Code.GDPlayerObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level6Code.GDPortalObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level6Code.GDPortalObjects3_1final.indexOf(gdjs.Level6Code.GDPortalObjects4[j]) === -1 )
            gdjs.Level6Code.GDPortalObjects3_1final.push(gdjs.Level6Code.GDPortalObjects4[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects4);
for (var i = 0, k = 0, l = gdjs.Level6Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level6Code.GDPlayerObjects4[i].getVariableNumber(gdjs.Level6Code.GDPlayerObjects4[i].getVariables().getFromIndex(1)) <= 0 ) {
        isConditionTrue_1 = true;
        gdjs.Level6Code.GDPlayerObjects4[k] = gdjs.Level6Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level6Code.GDPlayerObjects4.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level6Code.GDPlayerObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level6Code.GDPlayerObjects3_1final.indexOf(gdjs.Level6Code.GDPlayerObjects4[j]) === -1 )
            gdjs.Level6Code.GDPlayerObjects3_1final.push(gdjs.Level6Code.GDPlayerObjects4[j]);
    }
}
}
{
gdjs.copyArray(gdjs.Level6Code.GDPlayerObjects3_1final, gdjs.Level6Code.GDPlayerObjects3);
gdjs.copyArray(gdjs.Level6Code.GDPortalObjects3_1final, gdjs.Level6Code.GDPortalObjects3);
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23283420);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.camera.showLayer(runtimeScene, "EndScreen");
}{gdjs.evtTools.sound.stopSoundOnChannel(runtimeScene, 2);
}
{ //Subevents
gdjs.Level6Code.eventsList91(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "EndScreen");
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList94(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.eventsList96 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("BoundaryJumpThrough"), gdjs.Level6Code.GDBoundaryJumpThroughObjects4);
gdjs.copyArray(runtimeScene.getObjects("LeftBoundary"), gdjs.Level6Code.GDLeftBoundaryObjects4);
gdjs.copyArray(runtimeScene.getObjects("RightBoundary"), gdjs.Level6Code.GDRightBoundaryObjects4);
{for(var i = 0, len = gdjs.Level6Code.GDLeftBoundaryObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDLeftBoundaryObjects4[i].hide();
}
for(var i = 0, len = gdjs.Level6Code.GDRightBoundaryObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDRightBoundaryObjects4[i].hide();
}
for(var i = 0, len = gdjs.Level6Code.GDBoundaryJumpThroughObjects4.length ;i < len;++i) {
    gdjs.Level6Code.GDBoundaryJumpThroughObjects4[i].hide();
}
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("BottomBoundary"), gdjs.Level6Code.GDBottomBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("LeftBoundary"), gdjs.Level6Code.GDLeftBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("RightBoundary"), gdjs.Level6Code.GDRightBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("TopBoundary"), gdjs.Level6Code.GDTopBoundaryObjects3);
{gdjs.evtTools.camera.clampCamera(runtimeScene, (( gdjs.Level6Code.GDLeftBoundaryObjects3.length === 0 ) ? 0 :gdjs.Level6Code.GDLeftBoundaryObjects3[0].getPointX("")) + (( gdjs.Level6Code.GDLeftBoundaryObjects3.length === 0 ) ? 0 :gdjs.Level6Code.GDLeftBoundaryObjects3[0].getWidth()), (( gdjs.Level6Code.GDTopBoundaryObjects3.length === 0 ) ? 0 :gdjs.Level6Code.GDTopBoundaryObjects3[0].getPointY("")) + (( gdjs.Level6Code.GDTopBoundaryObjects3.length === 0 ) ? 0 :gdjs.Level6Code.GDTopBoundaryObjects3[0].getHeight()), (( gdjs.Level6Code.GDRightBoundaryObjects3.length === 0 ) ? 0 :gdjs.Level6Code.GDRightBoundaryObjects3[0].getPointX("")), (( gdjs.Level6Code.GDBottomBoundaryObjects3.length === 0 ) ? 0 :gdjs.Level6Code.GDBottomBoundaryObjects3[0].getPointY("")), "", 0);
}}

}


};gdjs.Level6Code.eventsList97 = function(runtimeScene) {

{



}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "RandomNoiseTimer");
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("BackgroundPlants"), gdjs.Level6Code.GDBackgroundPlantsObjects3);
{for(var i = 0, len = gdjs.Level6Code.GDBackgroundPlantsObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDBackgroundPlantsObjects3[i].setWidth(gdjs.evtTools.camera.getCameraWidth(runtimeScene, "", 0));
}
}{for(var i = 0, len = gdjs.Level6Code.GDBackgroundPlantsObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDBackgroundPlantsObjects3[i].setXOffset(gdjs.evtTools.camera.getCameraBorderLeft(runtimeScene, "", 0) / 3 + 780);
}
}}

}


};gdjs.Level6Code.eventsList98 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{/* Unknown object - skipped. */}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPortalObjects3Objects = Hashtable.newFrom({"Portal": gdjs.Level6Code.GDPortalObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPortalObjects2Objects = Hashtable.newFrom({"Portal": gdjs.Level6Code.GDPortalObjects2});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects2});
gdjs.Level6Code.eventsList99 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "door.aac", 0, true, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.Level6Code.GDPortalObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPortalObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23297804);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Portal/PortalInteract.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects2);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.Level6Code.GDPortalObjects2);
{gdjs.evtsExt__VolumeFalloff__SetVolumeFalloff.func(runtimeScene, 0, "Sound", gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPortalObjects2Objects, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects2Objects, 0, 100, 750, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level6Code.eventsList100 = function(runtimeScene) {

{


gdjs.Level6Code.eventsList96(runtimeScene);
}


{


gdjs.Level6Code.eventsList97(runtimeScene);
}


{


gdjs.Level6Code.eventsList98(runtimeScene);
}


{


gdjs.Level6Code.eventsList99(runtimeScene);
}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDBoundaryJumpThroughObjects2Objects = Hashtable.newFrom({"BoundaryJumpThrough": gdjs.Level6Code.GDBoundaryJumpThroughObjects2});
gdjs.Level6Code.eventsList101 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("BoundaryJumpThrough"), gdjs.Level6Code.GDBoundaryJumpThroughObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDBoundaryJumpThroughObjects2Objects);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23299580);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level6Code.GDBoundaryJumpThroughObjects2 */
{for(var i = 0, len = gdjs.Level6Code.GDBoundaryJumpThroughObjects2.length ;i < len;++i) {
    gdjs.Level6Code.GDBoundaryJumpThroughObjects2[i].hide();
}
}}

}


};gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level6Code.GDPlayerObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level6Code.GDFlyingDemonObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.Level6Code.GDFireDemonObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.Level6Code.GDHorizontalDemonObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level6Code.GDSpikeDemon_9595BaseObjects3});
gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects3});
gdjs.Level6Code.eventsList102 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "t");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23300812);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);
{for(var i = 0, len = gdjs.Level6Code.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDPlayerObjects3[i].returnVariable(gdjs.Level6Code.GDPlayerObjects3[i].getVariables().getFromIndex(5)).setNumber(1);
}
}{for(var i = 0, len = gdjs.Level6Code.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDPlayerObjects3[i].setPosition(gdjs.Level6Code.GDPlayerObjects3[i].getVariables().getFromIndex(6).getAsNumber(),gdjs.Level6Code.GDPlayerObjects3[i].getVariables().getFromIndex(7).getAsNumber());
}
}{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDPlayerObjects3Objects, ((gdjs.Level6Code.GDPlayerObjects3.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level6Code.GDPlayerObjects3[0].getVariables()).getFromIndex(6).getAsNumber(), ((gdjs.Level6Code.GDPlayerObjects3.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level6Code.GDPlayerObjects3[0].getVariables()).getFromIndex(7).getAsNumber(), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Tilde");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23302108);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level6Code.GDFireDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.Level6Code.GDFlyingDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.Level6Code.GDHorizontalDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level6Code.GDSpikeDemon_9595BaseObjects3);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects3);
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFlyingDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDFireDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDHorizontalDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDSpikeDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.Level6Code.mapOfGDgdjs_9546Level6Code_9546GDStalagtiteDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "i");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23303020);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects3);
{for(var i = 0, len = gdjs.Level6Code.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.Level6Code.GDPlayerObjects3[i].returnVariable(gdjs.Level6Code.GDPlayerObjects3[i].getVariables().getFromIndex(5)).setNumber(1 - gdjs.Level6Code.GDPlayerObjects3[i].getVariables().getFromIndex(5).getAsNumber());
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Escape");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23303940);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, gdjs.evtTools.runtimeScene.getSceneName(runtimeScene), false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "l");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23304796);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "TestingLevel", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "m");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23306020);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Mindscape", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num0");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23306732);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Tutorial", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num1");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23306460);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level1", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num2");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23308092);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level2", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num3");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23308812);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level3", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num4");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23308492);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level4", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num5");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23309636);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level5", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num6");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23310300);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level6", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num7");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23310964);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level7", false);
}}

}


};gdjs.Level6Code.eventsList103 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("HopeBar"), gdjs.Level6Code.GDHopeBarObjects2);
gdjs.copyArray(runtimeScene.getObjects("LivesBar"), gdjs.Level6Code.GDLivesBarObjects2);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects2);
{for(var i = 0, len = gdjs.Level6Code.GDLivesBarObjects2.length ;i < len;++i) {
    gdjs.Level6Code.GDLivesBarObjects2[i].SetValue((gdjs.RuntimeObject.getVariableNumber(((gdjs.Level6Code.GDPlayerObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level6Code.GDPlayerObjects2[0].getVariables()).getFromIndex(1))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.Level6Code.GDHopeBarObjects2.length ;i < len;++i) {
    gdjs.Level6Code.GDHopeBarObjects2[i].SetValue((gdjs.RuntimeObject.getVariableNumber(((gdjs.Level6Code.GDPlayerObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level6Code.GDPlayerObjects2[0].getVariables()).getFromIndex(3))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "LevelUI", 0, 0, 0);
}{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "LevelEndScreen", 0, 0, 0);
}}

}


};gdjs.Level6Code.eventsList104 = function(runtimeScene) {

{


gdjs.Level6Code.eventsList8(runtimeScene);
}


{


gdjs.Level6Code.eventsList15(runtimeScene);
}


{


gdjs.Level6Code.eventsList89(runtimeScene);
}


{


gdjs.Level6Code.eventsList95(runtimeScene);
}


{


gdjs.Level6Code.eventsList100(runtimeScene);
}


{


gdjs.Level6Code.eventsList101(runtimeScene);
}


{


gdjs.Level6Code.eventsList102(runtimeScene);
}


{


gdjs.Level6Code.eventsList103(runtimeScene);
}


};gdjs.Level6Code.eventsList105 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Level6Code.GDPlayerObjects2[i].getVariableNumber(gdjs.Level6Code.GDPlayerObjects2[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDPlayerObjects2[k] = gdjs.Level6Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Level6Code.GDPlayerObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Mindscape", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level6Code.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.Level6Code.GDPlayerObjects1[i].getVariableNumber(gdjs.Level6Code.GDPlayerObjects1[i].getVariables().getFromIndex(1)) <= 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level6Code.GDPlayerObjects1[k] = gdjs.Level6Code.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.Level6Code.GDPlayerObjects1.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level6", false);
}}

}


};gdjs.Level6Code.eventsList106 = function(runtimeScene) {

{

gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "EndScreen");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_RetryButton"), gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects2);
for (var i = 0, k = 0, l = gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects2.length;i<l;++i) {
    if ( gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects2[k] = gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects2[i];
        ++k;
    }
}
gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects2.length; j < jLen ; ++j) {
        if ( gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final.indexOf(gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects2[j]) === -1 )
            gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final.push(gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects2[j]);
    }
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "Space");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
gdjs.copyArray(gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final, gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects1);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level6Code.eventsList105(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.eventsList107 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 1;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance1.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance2.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 3;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance3.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


};gdjs.Level6Code.eventsList108 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Levels/6/AmbientLoop.ogg", true, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "RandomNoiseTimer") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("TimeBeforeNextRandomSFX"));
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("RandomSFXIndex").setNumber(gdjs.randomInRange(1, 3));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "RandomNoiseTimer");
}{runtimeScene.getScene().getVariables().get("TimeBeforeNextRandomSFX").setNumber(gdjs.randomFloatInRange(30, 240));
}
{ //Subevents
gdjs.Level6Code.eventsList107(runtimeScene);} //End of subevents
}

}


};gdjs.Level6Code.eventsList109 = function(runtimeScene) {

{



}


{


gdjs.Level6Code.eventsList104(runtimeScene);
}


{


gdjs.Level6Code.eventsList106(runtimeScene);
}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Memory"), gdjs.Level6Code.GDMemoryObjects1);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level6Code.GDPlayerObjects1);
{for(var i = 0, len = gdjs.Level6Code.GDMemoryObjects1.length ;i < len;++i) {
    gdjs.Level6Code.GDMemoryObjects1[i].getBehavior("Animation").setAnimationName("WhimsicalTree");
}
}{for(var i = 0, len = gdjs.Level6Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Level6Code.GDPlayerObjects1[i].returnVariable(gdjs.Level6Code.GDPlayerObjects1[i].getVariables().getFromIndex(3)).setNumber(12);
}
}}

}


{


gdjs.Level6Code.eventsList108(runtimeScene);
}


};

gdjs.Level6Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Level6Code.GDBackgroundPlantsObjects1.length = 0;
gdjs.Level6Code.GDBackgroundPlantsObjects2.length = 0;
gdjs.Level6Code.GDBackgroundPlantsObjects3.length = 0;
gdjs.Level6Code.GDBackgroundPlantsObjects4.length = 0;
gdjs.Level6Code.GDBackgroundPlantsObjects5.length = 0;
gdjs.Level6Code.GDBackgroundPlantsObjects6.length = 0;
gdjs.Level6Code.GDLeftBoundaryObjects1.length = 0;
gdjs.Level6Code.GDLeftBoundaryObjects2.length = 0;
gdjs.Level6Code.GDLeftBoundaryObjects3.length = 0;
gdjs.Level6Code.GDLeftBoundaryObjects4.length = 0;
gdjs.Level6Code.GDLeftBoundaryObjects5.length = 0;
gdjs.Level6Code.GDLeftBoundaryObjects6.length = 0;
gdjs.Level6Code.GDRightBoundaryObjects1.length = 0;
gdjs.Level6Code.GDRightBoundaryObjects2.length = 0;
gdjs.Level6Code.GDRightBoundaryObjects3.length = 0;
gdjs.Level6Code.GDRightBoundaryObjects4.length = 0;
gdjs.Level6Code.GDRightBoundaryObjects5.length = 0;
gdjs.Level6Code.GDRightBoundaryObjects6.length = 0;
gdjs.Level6Code.GDTopBoundaryObjects1.length = 0;
gdjs.Level6Code.GDTopBoundaryObjects2.length = 0;
gdjs.Level6Code.GDTopBoundaryObjects3.length = 0;
gdjs.Level6Code.GDTopBoundaryObjects4.length = 0;
gdjs.Level6Code.GDTopBoundaryObjects5.length = 0;
gdjs.Level6Code.GDTopBoundaryObjects6.length = 0;
gdjs.Level6Code.GDBottomBoundaryObjects1.length = 0;
gdjs.Level6Code.GDBottomBoundaryObjects2.length = 0;
gdjs.Level6Code.GDBottomBoundaryObjects3.length = 0;
gdjs.Level6Code.GDBottomBoundaryObjects4.length = 0;
gdjs.Level6Code.GDBottomBoundaryObjects5.length = 0;
gdjs.Level6Code.GDBottomBoundaryObjects6.length = 0;
gdjs.Level6Code.GDBoundaryJumpThroughObjects1.length = 0;
gdjs.Level6Code.GDBoundaryJumpThroughObjects2.length = 0;
gdjs.Level6Code.GDBoundaryJumpThroughObjects3.length = 0;
gdjs.Level6Code.GDBoundaryJumpThroughObjects4.length = 0;
gdjs.Level6Code.GDBoundaryJumpThroughObjects5.length = 0;
gdjs.Level6Code.GDBoundaryJumpThroughObjects6.length = 0;
gdjs.Level6Code.GDPlayerObjects1.length = 0;
gdjs.Level6Code.GDPlayerObjects2.length = 0;
gdjs.Level6Code.GDPlayerObjects3.length = 0;
gdjs.Level6Code.GDPlayerObjects4.length = 0;
gdjs.Level6Code.GDPlayerObjects5.length = 0;
gdjs.Level6Code.GDPlayerObjects6.length = 0;
gdjs.Level6Code.GDFlyingDemonObjects1.length = 0;
gdjs.Level6Code.GDFlyingDemonObjects2.length = 0;
gdjs.Level6Code.GDFlyingDemonObjects3.length = 0;
gdjs.Level6Code.GDFlyingDemonObjects4.length = 0;
gdjs.Level6Code.GDFlyingDemonObjects5.length = 0;
gdjs.Level6Code.GDFlyingDemonObjects6.length = 0;
gdjs.Level6Code.GDFireDemonObjects1.length = 0;
gdjs.Level6Code.GDFireDemonObjects2.length = 0;
gdjs.Level6Code.GDFireDemonObjects3.length = 0;
gdjs.Level6Code.GDFireDemonObjects4.length = 0;
gdjs.Level6Code.GDFireDemonObjects5.length = 0;
gdjs.Level6Code.GDFireDemonObjects6.length = 0;
gdjs.Level6Code.GDCheckpointObjects1.length = 0;
gdjs.Level6Code.GDCheckpointObjects2.length = 0;
gdjs.Level6Code.GDCheckpointObjects3.length = 0;
gdjs.Level6Code.GDCheckpointObjects4.length = 0;
gdjs.Level6Code.GDCheckpointObjects5.length = 0;
gdjs.Level6Code.GDCheckpointObjects6.length = 0;
gdjs.Level6Code.GDStaticPlatform3Objects1.length = 0;
gdjs.Level6Code.GDStaticPlatform3Objects2.length = 0;
gdjs.Level6Code.GDStaticPlatform3Objects3.length = 0;
gdjs.Level6Code.GDStaticPlatform3Objects4.length = 0;
gdjs.Level6Code.GDStaticPlatform3Objects5.length = 0;
gdjs.Level6Code.GDStaticPlatform3Objects6.length = 0;
gdjs.Level6Code.GDStaticPlatform2Objects1.length = 0;
gdjs.Level6Code.GDStaticPlatform2Objects2.length = 0;
gdjs.Level6Code.GDStaticPlatform2Objects3.length = 0;
gdjs.Level6Code.GDStaticPlatform2Objects4.length = 0;
gdjs.Level6Code.GDStaticPlatform2Objects5.length = 0;
gdjs.Level6Code.GDStaticPlatform2Objects6.length = 0;
gdjs.Level6Code.GDHorizontalMovingPlatformObjects1.length = 0;
gdjs.Level6Code.GDHorizontalMovingPlatformObjects2.length = 0;
gdjs.Level6Code.GDHorizontalMovingPlatformObjects3.length = 0;
gdjs.Level6Code.GDHorizontalMovingPlatformObjects4.length = 0;
gdjs.Level6Code.GDHorizontalMovingPlatformObjects5.length = 0;
gdjs.Level6Code.GDHorizontalMovingPlatformObjects6.length = 0;
gdjs.Level6Code.GDStaticPlatform1Objects1.length = 0;
gdjs.Level6Code.GDStaticPlatform1Objects2.length = 0;
gdjs.Level6Code.GDStaticPlatform1Objects3.length = 0;
gdjs.Level6Code.GDStaticPlatform1Objects4.length = 0;
gdjs.Level6Code.GDStaticPlatform1Objects5.length = 0;
gdjs.Level6Code.GDStaticPlatform1Objects6.length = 0;
gdjs.Level6Code.GDPortalObjects1.length = 0;
gdjs.Level6Code.GDPortalObjects2.length = 0;
gdjs.Level6Code.GDPortalObjects3.length = 0;
gdjs.Level6Code.GDPortalObjects4.length = 0;
gdjs.Level6Code.GDPortalObjects5.length = 0;
gdjs.Level6Code.GDPortalObjects6.length = 0;
gdjs.Level6Code.GDLadderObjects1.length = 0;
gdjs.Level6Code.GDLadderObjects2.length = 0;
gdjs.Level6Code.GDLadderObjects3.length = 0;
gdjs.Level6Code.GDLadderObjects4.length = 0;
gdjs.Level6Code.GDLadderObjects5.length = 0;
gdjs.Level6Code.GDLadderObjects6.length = 0;
gdjs.Level6Code.GDMonsterParticlesObjects1.length = 0;
gdjs.Level6Code.GDMonsterParticlesObjects2.length = 0;
gdjs.Level6Code.GDMonsterParticlesObjects3.length = 0;
gdjs.Level6Code.GDMonsterParticlesObjects4.length = 0;
gdjs.Level6Code.GDMonsterParticlesObjects5.length = 0;
gdjs.Level6Code.GDMonsterParticlesObjects6.length = 0;
gdjs.Level6Code.GDSpikeParticlesObjects1.length = 0;
gdjs.Level6Code.GDSpikeParticlesObjects2.length = 0;
gdjs.Level6Code.GDSpikeParticlesObjects3.length = 0;
gdjs.Level6Code.GDSpikeParticlesObjects4.length = 0;
gdjs.Level6Code.GDSpikeParticlesObjects5.length = 0;
gdjs.Level6Code.GDSpikeParticlesObjects6.length = 0;
gdjs.Level6Code.GDDoorParticlesObjects1.length = 0;
gdjs.Level6Code.GDDoorParticlesObjects2.length = 0;
gdjs.Level6Code.GDDoorParticlesObjects3.length = 0;
gdjs.Level6Code.GDDoorParticlesObjects4.length = 0;
gdjs.Level6Code.GDDoorParticlesObjects5.length = 0;
gdjs.Level6Code.GDDoorParticlesObjects6.length = 0;
gdjs.Level6Code.GDDustParticleObjects1.length = 0;
gdjs.Level6Code.GDDustParticleObjects2.length = 0;
gdjs.Level6Code.GDDustParticleObjects3.length = 0;
gdjs.Level6Code.GDDustParticleObjects4.length = 0;
gdjs.Level6Code.GDDustParticleObjects5.length = 0;
gdjs.Level6Code.GDDustParticleObjects6.length = 0;
gdjs.Level6Code.GDLivesBarObjects1.length = 0;
gdjs.Level6Code.GDLivesBarObjects2.length = 0;
gdjs.Level6Code.GDLivesBarObjects3.length = 0;
gdjs.Level6Code.GDLivesBarObjects4.length = 0;
gdjs.Level6Code.GDLivesBarObjects5.length = 0;
gdjs.Level6Code.GDLivesBarObjects6.length = 0;
gdjs.Level6Code.GDHopeBarObjects1.length = 0;
gdjs.Level6Code.GDHopeBarObjects2.length = 0;
gdjs.Level6Code.GDHopeBarObjects3.length = 0;
gdjs.Level6Code.GDHopeBarObjects4.length = 0;
gdjs.Level6Code.GDHopeBarObjects5.length = 0;
gdjs.Level6Code.GDHopeBarObjects6.length = 0;
gdjs.Level6Code.GDMemoryObjects1.length = 0;
gdjs.Level6Code.GDMemoryObjects2.length = 0;
gdjs.Level6Code.GDMemoryObjects3.length = 0;
gdjs.Level6Code.GDMemoryObjects4.length = 0;
gdjs.Level6Code.GDMemoryObjects5.length = 0;
gdjs.Level6Code.GDMemoryObjects6.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects1.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects2.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects3.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects4.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects5.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595DeathTextObjects6.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects1.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects2.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects4.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects5.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595MemoryAcquiredObjects6.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595LivesObjects1.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595LivesObjects2.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595LivesObjects3.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595LivesObjects4.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595LivesObjects5.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595LivesObjects6.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595HopeObjects1.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595HopeObjects2.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595HopeObjects3.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595HopeObjects4.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595HopeObjects5.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595HopeObjects6.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects1.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects2.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects3.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects4.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects5.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595RetryButtonObjects6.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects1.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects2.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects3.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects4.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects5.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595BackgroundObjects6.length = 0;
gdjs.Level6Code.GDHorizontalDemonObjects1.length = 0;
gdjs.Level6Code.GDHorizontalDemonObjects2.length = 0;
gdjs.Level6Code.GDHorizontalDemonObjects3.length = 0;
gdjs.Level6Code.GDHorizontalDemonObjects4.length = 0;
gdjs.Level6Code.GDHorizontalDemonObjects5.length = 0;
gdjs.Level6Code.GDHorizontalDemonObjects6.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595BaseObjects1.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595BaseObjects2.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595BaseObjects3.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595BaseObjects4.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595BaseObjects5.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595BaseObjects6.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects1.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects2.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects3.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects4.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects5.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595SpikeObjects6.length = 0;
gdjs.Level6Code.GDVerticalMovingPlatformObjects1.length = 0;
gdjs.Level6Code.GDVerticalMovingPlatformObjects2.length = 0;
gdjs.Level6Code.GDVerticalMovingPlatformObjects3.length = 0;
gdjs.Level6Code.GDVerticalMovingPlatformObjects4.length = 0;
gdjs.Level6Code.GDVerticalMovingPlatformObjects5.length = 0;
gdjs.Level6Code.GDVerticalMovingPlatformObjects6.length = 0;
gdjs.Level6Code.GDSpinningMovingPlatformObjects1.length = 0;
gdjs.Level6Code.GDSpinningMovingPlatformObjects2.length = 0;
gdjs.Level6Code.GDSpinningMovingPlatformObjects3.length = 0;
gdjs.Level6Code.GDSpinningMovingPlatformObjects4.length = 0;
gdjs.Level6Code.GDSpinningMovingPlatformObjects5.length = 0;
gdjs.Level6Code.GDSpinningMovingPlatformObjects6.length = 0;
gdjs.Level6Code.GDFlippingPlatformObjects1.length = 0;
gdjs.Level6Code.GDFlippingPlatformObjects2.length = 0;
gdjs.Level6Code.GDFlippingPlatformObjects3.length = 0;
gdjs.Level6Code.GDFlippingPlatformObjects4.length = 0;
gdjs.Level6Code.GDFlippingPlatformObjects5.length = 0;
gdjs.Level6Code.GDFlippingPlatformObjects6.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects1.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects2.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects3.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects4.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects5.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595BaseObjects6.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects1.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects2.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects3.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects4.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects5.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595SpikeObjects6.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595Base2Objects1.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595Base2Objects2.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595Base2Objects3.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595Base2Objects4.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595Base2Objects5.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595Base2Objects6.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects1.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects2.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects3.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects4.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects5.length = 0;
gdjs.Level6Code.GDSpikeDemon_9595Spike2Objects6.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects1.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects2.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects3.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects4.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects5.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595Base2Objects6.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects1.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects2.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects3.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects4.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects5.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595BaseObjects6.length = 0;
gdjs.Level6Code.GDFlippingPlatform2Objects1.length = 0;
gdjs.Level6Code.GDFlippingPlatform2Objects2.length = 0;
gdjs.Level6Code.GDFlippingPlatform2Objects3.length = 0;
gdjs.Level6Code.GDFlippingPlatform2Objects4.length = 0;
gdjs.Level6Code.GDFlippingPlatform2Objects5.length = 0;
gdjs.Level6Code.GDFlippingPlatform2Objects6.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects1.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects2.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects3.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects4.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects5.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595Base2Objects6.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects1.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects2.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects3.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects4.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects5.length = 0;
gdjs.Level6Code.GDStalagtiteDemon_9595Spike2Objects6.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects1.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects2.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects3.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects4.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects5.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595SpikeObjects6.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects1.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects2.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects3.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects4.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects5.length = 0;
gdjs.Level6Code.GDStalagmiteDemon_9595Spike2Objects6.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects1.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects2.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects3.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects4.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects5.length = 0;
gdjs.Level6Code.GDUI_9595Sinage_9595ProceedButtonObjects6.length = 0;
gdjs.Level6Code.GDLaserDemon_9595BeamObjects1.length = 0;
gdjs.Level6Code.GDLaserDemon_9595BeamObjects2.length = 0;
gdjs.Level6Code.GDLaserDemon_9595BeamObjects3.length = 0;
gdjs.Level6Code.GDLaserDemon_9595BeamObjects4.length = 0;
gdjs.Level6Code.GDLaserDemon_9595BeamObjects5.length = 0;
gdjs.Level6Code.GDLaserDemon_9595BeamObjects6.length = 0;
gdjs.Level6Code.GDLaserDemon_9595BaseObjects1.length = 0;
gdjs.Level6Code.GDLaserDemon_9595BaseObjects2.length = 0;
gdjs.Level6Code.GDLaserDemon_9595BaseObjects3.length = 0;
gdjs.Level6Code.GDLaserDemon_9595BaseObjects4.length = 0;
gdjs.Level6Code.GDLaserDemon_9595BaseObjects5.length = 0;
gdjs.Level6Code.GDLaserDemon_9595BaseObjects6.length = 0;
gdjs.Level6Code.GDTestObjects1.length = 0;
gdjs.Level6Code.GDTestObjects2.length = 0;
gdjs.Level6Code.GDTestObjects3.length = 0;
gdjs.Level6Code.GDTestObjects4.length = 0;
gdjs.Level6Code.GDTestObjects5.length = 0;
gdjs.Level6Code.GDTestObjects6.length = 0;

gdjs.Level6Code.eventsList109(runtimeScene);

return;

}

gdjs['Level6Code'] = gdjs.Level6Code;
