gdjs.TutorialCode = {};
gdjs.TutorialCode.GDPlayerObjects3_1final = [];

gdjs.TutorialCode.GDPortalObjects3_1final = [];

gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2_1final = [];

gdjs.TutorialCode.GDBackgroundPlantsObjects1= [];
gdjs.TutorialCode.GDBackgroundPlantsObjects2= [];
gdjs.TutorialCode.GDBackgroundPlantsObjects3= [];
gdjs.TutorialCode.GDBackgroundPlantsObjects4= [];
gdjs.TutorialCode.GDBackgroundPlantsObjects5= [];
gdjs.TutorialCode.GDBackgroundPlantsObjects6= [];
gdjs.TutorialCode.GDLeftBoundaryObjects1= [];
gdjs.TutorialCode.GDLeftBoundaryObjects2= [];
gdjs.TutorialCode.GDLeftBoundaryObjects3= [];
gdjs.TutorialCode.GDLeftBoundaryObjects4= [];
gdjs.TutorialCode.GDLeftBoundaryObjects5= [];
gdjs.TutorialCode.GDLeftBoundaryObjects6= [];
gdjs.TutorialCode.GDRightBoundaryObjects1= [];
gdjs.TutorialCode.GDRightBoundaryObjects2= [];
gdjs.TutorialCode.GDRightBoundaryObjects3= [];
gdjs.TutorialCode.GDRightBoundaryObjects4= [];
gdjs.TutorialCode.GDRightBoundaryObjects5= [];
gdjs.TutorialCode.GDRightBoundaryObjects6= [];
gdjs.TutorialCode.GDTopBoundaryObjects1= [];
gdjs.TutorialCode.GDTopBoundaryObjects2= [];
gdjs.TutorialCode.GDTopBoundaryObjects3= [];
gdjs.TutorialCode.GDTopBoundaryObjects4= [];
gdjs.TutorialCode.GDTopBoundaryObjects5= [];
gdjs.TutorialCode.GDTopBoundaryObjects6= [];
gdjs.TutorialCode.GDBottomBoundaryObjects1= [];
gdjs.TutorialCode.GDBottomBoundaryObjects2= [];
gdjs.TutorialCode.GDBottomBoundaryObjects3= [];
gdjs.TutorialCode.GDBottomBoundaryObjects4= [];
gdjs.TutorialCode.GDBottomBoundaryObjects5= [];
gdjs.TutorialCode.GDBottomBoundaryObjects6= [];
gdjs.TutorialCode.GDBoundaryJumpThroughObjects1= [];
gdjs.TutorialCode.GDBoundaryJumpThroughObjects2= [];
gdjs.TutorialCode.GDBoundaryJumpThroughObjects3= [];
gdjs.TutorialCode.GDBoundaryJumpThroughObjects4= [];
gdjs.TutorialCode.GDBoundaryJumpThroughObjects5= [];
gdjs.TutorialCode.GDBoundaryJumpThroughObjects6= [];
gdjs.TutorialCode.GDSinage_9595ArrowObjects1= [];
gdjs.TutorialCode.GDSinage_9595ArrowObjects2= [];
gdjs.TutorialCode.GDSinage_9595ArrowObjects3= [];
gdjs.TutorialCode.GDSinage_9595ArrowObjects4= [];
gdjs.TutorialCode.GDSinage_9595ArrowObjects5= [];
gdjs.TutorialCode.GDSinage_9595ArrowObjects6= [];
gdjs.TutorialCode.GDSinage_9595JumpObjects1= [];
gdjs.TutorialCode.GDSinage_9595JumpObjects2= [];
gdjs.TutorialCode.GDSinage_9595JumpObjects3= [];
gdjs.TutorialCode.GDSinage_9595JumpObjects4= [];
gdjs.TutorialCode.GDSinage_9595JumpObjects5= [];
gdjs.TutorialCode.GDSinage_9595JumpObjects6= [];
gdjs.TutorialCode.GDSinage_9595CollectObjects1= [];
gdjs.TutorialCode.GDSinage_9595CollectObjects2= [];
gdjs.TutorialCode.GDSinage_9595CollectObjects3= [];
gdjs.TutorialCode.GDSinage_9595CollectObjects4= [];
gdjs.TutorialCode.GDSinage_9595CollectObjects5= [];
gdjs.TutorialCode.GDSinage_9595CollectObjects6= [];
gdjs.TutorialCode.GDSinage_9595KillObjects1= [];
gdjs.TutorialCode.GDSinage_9595KillObjects2= [];
gdjs.TutorialCode.GDSinage_9595KillObjects3= [];
gdjs.TutorialCode.GDSinage_9595KillObjects4= [];
gdjs.TutorialCode.GDSinage_9595KillObjects5= [];
gdjs.TutorialCode.GDSinage_9595KillObjects6= [];
gdjs.TutorialCode.GDSinage_9595CheckpointObjects1= [];
gdjs.TutorialCode.GDSinage_9595CheckpointObjects2= [];
gdjs.TutorialCode.GDSinage_9595CheckpointObjects3= [];
gdjs.TutorialCode.GDSinage_9595CheckpointObjects4= [];
gdjs.TutorialCode.GDSinage_9595CheckpointObjects5= [];
gdjs.TutorialCode.GDSinage_9595CheckpointObjects6= [];
gdjs.TutorialCode.GDSinage_9595DownArrowObjects1= [];
gdjs.TutorialCode.GDSinage_9595DownArrowObjects2= [];
gdjs.TutorialCode.GDSinage_9595DownArrowObjects3= [];
gdjs.TutorialCode.GDSinage_9595DownArrowObjects4= [];
gdjs.TutorialCode.GDSinage_9595DownArrowObjects5= [];
gdjs.TutorialCode.GDSinage_9595DownArrowObjects6= [];
gdjs.TutorialCode.GDSinage_9595DeathObjects1= [];
gdjs.TutorialCode.GDSinage_9595DeathObjects2= [];
gdjs.TutorialCode.GDSinage_9595DeathObjects3= [];
gdjs.TutorialCode.GDSinage_9595DeathObjects4= [];
gdjs.TutorialCode.GDSinage_9595DeathObjects5= [];
gdjs.TutorialCode.GDSinage_9595DeathObjects6= [];
gdjs.TutorialCode.GDSinage_9595ProceedObjects1= [];
gdjs.TutorialCode.GDSinage_9595ProceedObjects2= [];
gdjs.TutorialCode.GDSinage_9595ProceedObjects3= [];
gdjs.TutorialCode.GDSinage_9595ProceedObjects4= [];
gdjs.TutorialCode.GDSinage_9595ProceedObjects5= [];
gdjs.TutorialCode.GDSinage_9595ProceedObjects6= [];
gdjs.TutorialCode.GDSinage_9595SpaceObjects1= [];
gdjs.TutorialCode.GDSinage_9595SpaceObjects2= [];
gdjs.TutorialCode.GDSinage_9595SpaceObjects3= [];
gdjs.TutorialCode.GDSinage_9595SpaceObjects4= [];
gdjs.TutorialCode.GDSinage_9595SpaceObjects5= [];
gdjs.TutorialCode.GDSinage_9595SpaceObjects6= [];
gdjs.TutorialCode.GDSinage_9595WObjects1= [];
gdjs.TutorialCode.GDSinage_9595WObjects2= [];
gdjs.TutorialCode.GDSinage_9595WObjects3= [];
gdjs.TutorialCode.GDSinage_9595WObjects4= [];
gdjs.TutorialCode.GDSinage_9595WObjects5= [];
gdjs.TutorialCode.GDSinage_9595WObjects6= [];
gdjs.TutorialCode.GDSinage_9595AObjects1= [];
gdjs.TutorialCode.GDSinage_9595AObjects2= [];
gdjs.TutorialCode.GDSinage_9595AObjects3= [];
gdjs.TutorialCode.GDSinage_9595AObjects4= [];
gdjs.TutorialCode.GDSinage_9595AObjects5= [];
gdjs.TutorialCode.GDSinage_9595AObjects6= [];
gdjs.TutorialCode.GDSinage_9595DObjects1= [];
gdjs.TutorialCode.GDSinage_9595DObjects2= [];
gdjs.TutorialCode.GDSinage_9595DObjects3= [];
gdjs.TutorialCode.GDSinage_9595DObjects4= [];
gdjs.TutorialCode.GDSinage_9595DObjects5= [];
gdjs.TutorialCode.GDSinage_9595DObjects6= [];
gdjs.TutorialCode.GDSinage_9595SObjects1= [];
gdjs.TutorialCode.GDSinage_9595SObjects2= [];
gdjs.TutorialCode.GDSinage_9595SObjects3= [];
gdjs.TutorialCode.GDSinage_9595SObjects4= [];
gdjs.TutorialCode.GDSinage_9595SObjects5= [];
gdjs.TutorialCode.GDSinage_9595SObjects6= [];
gdjs.TutorialCode.GDPlayerObjects1= [];
gdjs.TutorialCode.GDPlayerObjects2= [];
gdjs.TutorialCode.GDPlayerObjects3= [];
gdjs.TutorialCode.GDPlayerObjects4= [];
gdjs.TutorialCode.GDPlayerObjects5= [];
gdjs.TutorialCode.GDPlayerObjects6= [];
gdjs.TutorialCode.GDFlyingDemonObjects1= [];
gdjs.TutorialCode.GDFlyingDemonObjects2= [];
gdjs.TutorialCode.GDFlyingDemonObjects3= [];
gdjs.TutorialCode.GDFlyingDemonObjects4= [];
gdjs.TutorialCode.GDFlyingDemonObjects5= [];
gdjs.TutorialCode.GDFlyingDemonObjects6= [];
gdjs.TutorialCode.GDFireDemonObjects1= [];
gdjs.TutorialCode.GDFireDemonObjects2= [];
gdjs.TutorialCode.GDFireDemonObjects3= [];
gdjs.TutorialCode.GDFireDemonObjects4= [];
gdjs.TutorialCode.GDFireDemonObjects5= [];
gdjs.TutorialCode.GDFireDemonObjects6= [];
gdjs.TutorialCode.GDCheckpointObjects1= [];
gdjs.TutorialCode.GDCheckpointObjects2= [];
gdjs.TutorialCode.GDCheckpointObjects3= [];
gdjs.TutorialCode.GDCheckpointObjects4= [];
gdjs.TutorialCode.GDCheckpointObjects5= [];
gdjs.TutorialCode.GDCheckpointObjects6= [];
gdjs.TutorialCode.GDStaticPlatform3Objects1= [];
gdjs.TutorialCode.GDStaticPlatform3Objects2= [];
gdjs.TutorialCode.GDStaticPlatform3Objects3= [];
gdjs.TutorialCode.GDStaticPlatform3Objects4= [];
gdjs.TutorialCode.GDStaticPlatform3Objects5= [];
gdjs.TutorialCode.GDStaticPlatform3Objects6= [];
gdjs.TutorialCode.GDStaticPlatform2Objects1= [];
gdjs.TutorialCode.GDStaticPlatform2Objects2= [];
gdjs.TutorialCode.GDStaticPlatform2Objects3= [];
gdjs.TutorialCode.GDStaticPlatform2Objects4= [];
gdjs.TutorialCode.GDStaticPlatform2Objects5= [];
gdjs.TutorialCode.GDStaticPlatform2Objects6= [];
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects1= [];
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects2= [];
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects3= [];
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4= [];
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects5= [];
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects6= [];
gdjs.TutorialCode.GDStaticPlatform1Objects1= [];
gdjs.TutorialCode.GDStaticPlatform1Objects2= [];
gdjs.TutorialCode.GDStaticPlatform1Objects3= [];
gdjs.TutorialCode.GDStaticPlatform1Objects4= [];
gdjs.TutorialCode.GDStaticPlatform1Objects5= [];
gdjs.TutorialCode.GDStaticPlatform1Objects6= [];
gdjs.TutorialCode.GDPortalObjects1= [];
gdjs.TutorialCode.GDPortalObjects2= [];
gdjs.TutorialCode.GDPortalObjects3= [];
gdjs.TutorialCode.GDPortalObjects4= [];
gdjs.TutorialCode.GDPortalObjects5= [];
gdjs.TutorialCode.GDPortalObjects6= [];
gdjs.TutorialCode.GDLadderObjects1= [];
gdjs.TutorialCode.GDLadderObjects2= [];
gdjs.TutorialCode.GDLadderObjects3= [];
gdjs.TutorialCode.GDLadderObjects4= [];
gdjs.TutorialCode.GDLadderObjects5= [];
gdjs.TutorialCode.GDLadderObjects6= [];
gdjs.TutorialCode.GDMonsterParticlesObjects1= [];
gdjs.TutorialCode.GDMonsterParticlesObjects2= [];
gdjs.TutorialCode.GDMonsterParticlesObjects3= [];
gdjs.TutorialCode.GDMonsterParticlesObjects4= [];
gdjs.TutorialCode.GDMonsterParticlesObjects5= [];
gdjs.TutorialCode.GDMonsterParticlesObjects6= [];
gdjs.TutorialCode.GDSpikeParticlesObjects1= [];
gdjs.TutorialCode.GDSpikeParticlesObjects2= [];
gdjs.TutorialCode.GDSpikeParticlesObjects3= [];
gdjs.TutorialCode.GDSpikeParticlesObjects4= [];
gdjs.TutorialCode.GDSpikeParticlesObjects5= [];
gdjs.TutorialCode.GDSpikeParticlesObjects6= [];
gdjs.TutorialCode.GDDoorParticlesObjects1= [];
gdjs.TutorialCode.GDDoorParticlesObjects2= [];
gdjs.TutorialCode.GDDoorParticlesObjects3= [];
gdjs.TutorialCode.GDDoorParticlesObjects4= [];
gdjs.TutorialCode.GDDoorParticlesObjects5= [];
gdjs.TutorialCode.GDDoorParticlesObjects6= [];
gdjs.TutorialCode.GDDustParticleObjects1= [];
gdjs.TutorialCode.GDDustParticleObjects2= [];
gdjs.TutorialCode.GDDustParticleObjects3= [];
gdjs.TutorialCode.GDDustParticleObjects4= [];
gdjs.TutorialCode.GDDustParticleObjects5= [];
gdjs.TutorialCode.GDDustParticleObjects6= [];
gdjs.TutorialCode.GDLivesBarObjects1= [];
gdjs.TutorialCode.GDLivesBarObjects2= [];
gdjs.TutorialCode.GDLivesBarObjects3= [];
gdjs.TutorialCode.GDLivesBarObjects4= [];
gdjs.TutorialCode.GDLivesBarObjects5= [];
gdjs.TutorialCode.GDLivesBarObjects6= [];
gdjs.TutorialCode.GDHopeBarObjects1= [];
gdjs.TutorialCode.GDHopeBarObjects2= [];
gdjs.TutorialCode.GDHopeBarObjects3= [];
gdjs.TutorialCode.GDHopeBarObjects4= [];
gdjs.TutorialCode.GDHopeBarObjects5= [];
gdjs.TutorialCode.GDHopeBarObjects6= [];
gdjs.TutorialCode.GDMemoryObjects1= [];
gdjs.TutorialCode.GDMemoryObjects2= [];
gdjs.TutorialCode.GDMemoryObjects3= [];
gdjs.TutorialCode.GDMemoryObjects4= [];
gdjs.TutorialCode.GDMemoryObjects5= [];
gdjs.TutorialCode.GDMemoryObjects6= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects1= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects2= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects3= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects4= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects5= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects6= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects1= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects2= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects4= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects5= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects6= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects1= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects2= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects3= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects4= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects5= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects6= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595HopeObjects1= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595HopeObjects2= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595HopeObjects3= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595HopeObjects4= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595HopeObjects5= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595HopeObjects6= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects1= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects3= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects4= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects5= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects6= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects1= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects2= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects3= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects4= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects5= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects6= [];
gdjs.TutorialCode.GDHorizontalDemonObjects1= [];
gdjs.TutorialCode.GDHorizontalDemonObjects2= [];
gdjs.TutorialCode.GDHorizontalDemonObjects3= [];
gdjs.TutorialCode.GDHorizontalDemonObjects4= [];
gdjs.TutorialCode.GDHorizontalDemonObjects5= [];
gdjs.TutorialCode.GDHorizontalDemonObjects6= [];
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects1= [];
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects2= [];
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3= [];
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4= [];
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5= [];
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects6= [];
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects1= [];
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects2= [];
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3= [];
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects4= [];
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects5= [];
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects6= [];
gdjs.TutorialCode.GDVerticalMovingPlatformObjects1= [];
gdjs.TutorialCode.GDVerticalMovingPlatformObjects2= [];
gdjs.TutorialCode.GDVerticalMovingPlatformObjects3= [];
gdjs.TutorialCode.GDVerticalMovingPlatformObjects4= [];
gdjs.TutorialCode.GDVerticalMovingPlatformObjects5= [];
gdjs.TutorialCode.GDVerticalMovingPlatformObjects6= [];
gdjs.TutorialCode.GDSpinningMovingPlatformObjects1= [];
gdjs.TutorialCode.GDSpinningMovingPlatformObjects2= [];
gdjs.TutorialCode.GDSpinningMovingPlatformObjects3= [];
gdjs.TutorialCode.GDSpinningMovingPlatformObjects4= [];
gdjs.TutorialCode.GDSpinningMovingPlatformObjects5= [];
gdjs.TutorialCode.GDSpinningMovingPlatformObjects6= [];
gdjs.TutorialCode.GDFlippingPlatformObjects1= [];
gdjs.TutorialCode.GDFlippingPlatformObjects2= [];
gdjs.TutorialCode.GDFlippingPlatformObjects3= [];
gdjs.TutorialCode.GDFlippingPlatformObjects4= [];
gdjs.TutorialCode.GDFlippingPlatformObjects5= [];
gdjs.TutorialCode.GDFlippingPlatformObjects6= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects1= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects2= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects5= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects6= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects1= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects2= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects5= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects6= [];
gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects1= [];
gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects2= [];
gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects3= [];
gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects4= [];
gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5= [];
gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects6= [];
gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects1= [];
gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects2= [];
gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects3= [];
gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects4= [];
gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects5= [];
gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects6= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects1= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects2= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects3= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects4= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects5= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects6= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects1= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects2= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects3= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects4= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects5= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects6= [];
gdjs.TutorialCode.GDFlippingPlatform2Objects1= [];
gdjs.TutorialCode.GDFlippingPlatform2Objects2= [];
gdjs.TutorialCode.GDFlippingPlatform2Objects3= [];
gdjs.TutorialCode.GDFlippingPlatform2Objects4= [];
gdjs.TutorialCode.GDFlippingPlatform2Objects5= [];
gdjs.TutorialCode.GDFlippingPlatform2Objects6= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects1= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects2= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects3= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects4= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects5= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects6= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects1= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects2= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects3= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects5= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects6= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects1= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects2= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects3= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects5= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects6= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects1= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects2= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects3= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects5= [];
gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects6= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects1= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects2= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects3= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects4= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects5= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects6= [];
gdjs.TutorialCode.GDLaserDemon_9595BeamObjects1= [];
gdjs.TutorialCode.GDLaserDemon_9595BeamObjects2= [];
gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3= [];
gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4= [];
gdjs.TutorialCode.GDLaserDemon_9595BeamObjects5= [];
gdjs.TutorialCode.GDLaserDemon_9595BeamObjects6= [];
gdjs.TutorialCode.GDLaserDemon_9595BaseObjects1= [];
gdjs.TutorialCode.GDLaserDemon_9595BaseObjects2= [];
gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3= [];
gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4= [];
gdjs.TutorialCode.GDLaserDemon_9595BaseObjects5= [];
gdjs.TutorialCode.GDLaserDemon_9595BaseObjects6= [];
gdjs.TutorialCode.GDTestObjects1= [];
gdjs.TutorialCode.GDTestObjects2= [];
gdjs.TutorialCode.GDTestObjects3= [];
gdjs.TutorialCode.GDTestObjects4= [];
gdjs.TutorialCode.GDTestObjects5= [];
gdjs.TutorialCode.GDTestObjects6= [];


gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects4Objects = Hashtable.newFrom({"HorizontalMovingPlatform": gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects4Objects = Hashtable.newFrom({"VerticalMovingPlatform": gdjs.TutorialCode.GDVerticalMovingPlatformObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects3Objects = Hashtable.newFrom({"SpinningMovingPlatform": gdjs.TutorialCode.GDSpinningMovingPlatformObjects3});
gdjs.TutorialCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.TutorialCode.GDVerticalMovingPlatformObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.TutorialCode.GDSpinningMovingPlatformObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.TutorialCode.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDFlippingPlatformObjects4[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.TutorialCode.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDFlippingPlatformObjects4[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") <= 3;
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList1(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 3;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.TutorialCode.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDFlippingPlatformObjects4[i].rotateTowardAngle(-(80), 0, runtimeScene);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList2(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 6;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.TutorialCode.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDFlippingPlatformObjects4[i].rotateTowardAngle(0, 0, runtimeScene);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


};gdjs.TutorialCode.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.TutorialCode.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.TutorialCode.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDFlippingPlatform2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.eventsList5 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.TutorialCode.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.TutorialCode.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDFlippingPlatform2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.asyncCallback23166852 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}}
gdjs.TutorialCode.eventsList6 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23166852(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.eventsList7 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") <= 3;
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList4(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 3;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.TutorialCode.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.TutorialCode.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDFlippingPlatform2Objects3[i].rotateTowardAngle(-(80), 0, runtimeScene);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList5(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 6;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.TutorialCode.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.TutorialCode.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDFlippingPlatform2Objects3[i].rotateTowardAngle(0, 0, runtimeScene);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList6(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList8 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList0(runtimeScene);
}


{


gdjs.TutorialCode.eventsList3(runtimeScene);
}


{


gdjs.TutorialCode.eventsList7(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.eventsList9 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "w");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Up");
}
}{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Ladder");
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "a");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects4[i].getX() >= 0 ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects4[k] = gdjs.TutorialCode.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects4.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects4 */
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Left");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "d");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Right");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Jump");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "s");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Down");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "LShift");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "RShift");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23174796);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);
{gdjs.evtsExt__Player__HealPlayer.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDCheckpointObjects3Objects = Hashtable.newFrom({"Checkpoint": gdjs.TutorialCode.GDCheckpointObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.eventsList10 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.TutorialCode.GDCheckpointObjects3 */
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, (( gdjs.TutorialCode.GDCheckpointObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDCheckpointObjects3[0].getPointX("")), (( gdjs.TutorialCode.GDCheckpointObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDCheckpointObjects3[0].getPointY("")), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.TutorialCode.GDCheckpointObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDCheckpointObjects3[i].getBehavior("Animation").setAnimationName("Activate");
}
}}

}


};gdjs.TutorialCode.eventsList11 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, (( gdjs.TutorialCode.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDPlayerObjects4[0].getPointX("")), (( gdjs.TutorialCode.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDPlayerObjects4[0].getPointY("")), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Checkpoint"), gdjs.TutorialCode.GDCheckpointObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDCheckpointObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDCheckpointObjects3.length;i<l;++i) {
    if ( !(gdjs.TutorialCode.GDCheckpointObjects3[i].isCurrentAnimationName("Activate")) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDCheckpointObjects3[k] = gdjs.TutorialCode.GDCheckpointObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDCheckpointObjects3.length = k;
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Checkpoint/Activate.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}
{ //Subevents
gdjs.TutorialCode.eventsList10(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.eventsList12 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects3[i].getY() > gdjs.evtTools.camera.getCameraBorderBottom(runtimeScene, "", 0) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects3[k] = gdjs.TutorialCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
{gdjs.evtsExt__Player__TriggerDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList13 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "AssetDev/Audio/Heartbeat_Amplified.wav", 2, true, 100, 1);
}{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects4[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects4[i].getVariables().getFromIndex(1)) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects4[k] = gdjs.TutorialCode.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 60);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.getSoundOnChannelVolume(runtimeScene, 2) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects3[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 1 ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects3[k] = gdjs.TutorialCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects3.length = k;
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 0);
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects2});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDDustParticleObjects2Objects = Hashtable.newFrom({"DustParticle": gdjs.TutorialCode.GDDustParticleObjects2});
gdjs.TutorialCode.eventsList14 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList13(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects3[i].getBehavior("PlatformerObject").isJumping() ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects3[k] = gdjs.TutorialCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23183324);
}
}
if (isConditionTrue_0) {
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtsExt__Player__IsSteppingOnFloor.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects, "PlatformerObject", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23183804);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects2 */
gdjs.TutorialCode.GDDustParticleObjects2.length = 0;

{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "grass.mp3", 1, false, 20, gdjs.randomFloatInRange(0.7, 1.2));
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDDustParticleObjects2Objects, (( gdjs.TutorialCode.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.TutorialCode.GDPlayerObjects2[0].getAABBCenterX()), (( gdjs.TutorialCode.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.TutorialCode.GDPlayerObjects2[0].getAABBBottom()), "");
}{for(var i = 0, len = gdjs.TutorialCode.GDDustParticleObjects2.length ;i < len;++i) {
    gdjs.TutorialCode.GDDustParticleObjects2[i].setZOrder(-(1));
}
}{for(var i = 0, len = gdjs.TutorialCode.GDDustParticleObjects2.length ;i < len;++i) {
    gdjs.TutorialCode.GDDustParticleObjects2[i].setAngle(270);
}
}}

}


};gdjs.TutorialCode.eventsList15 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList9(runtimeScene);
}


{


gdjs.TutorialCode.eventsList11(runtimeScene);
}


{


gdjs.TutorialCode.eventsList12(runtimeScene);
}


{


gdjs.TutorialCode.eventsList14(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlippingPlatformObjects5Objects = Hashtable.newFrom({"FlippingPlatform": gdjs.TutorialCode.GDFlippingPlatformObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlippingPlatform2Objects5Objects = Hashtable.newFrom({"FlippingPlatform2": gdjs.TutorialCode.GDFlippingPlatform2Objects5});
gdjs.TutorialCode.asyncCallback23187724 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}}
gdjs.TutorialCode.eventsList16 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23187724(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects5Objects = Hashtable.newFrom({"HorizontalMovingPlatform": gdjs.TutorialCode.GDHorizontalMovingPlatformObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects5Objects = Hashtable.newFrom({"HorizontalMovingPlatform": gdjs.TutorialCode.GDHorizontalMovingPlatformObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects5Objects = Hashtable.newFrom({"VerticalMovingPlatform": gdjs.TutorialCode.GDVerticalMovingPlatformObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects5Objects = Hashtable.newFrom({"VerticalMovingPlatform": gdjs.TutorialCode.GDVerticalMovingPlatformObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects4Objects = Hashtable.newFrom({"SpinningMovingPlatform": gdjs.TutorialCode.GDSpinningMovingPlatformObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects4Objects = Hashtable.newFrom({"SpinningMovingPlatform": gdjs.TutorialCode.GDSpinningMovingPlatformObjects4});
gdjs.TutorialCode.eventsList17 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.TutorialCode.GDFlippingPlatformObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlippingPlatformObjects5Objects);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.TutorialCode.GDFlippingPlatform2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlippingPlatform2Objects5Objects);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}
{ //Subevents
gdjs.TutorialCode.eventsList16(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.TutorialCode.GDHorizontalMovingPlatformObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDHorizontalMovingPlatformObjects5 */
{gdjs.evtsExt__Enemy__ResetPlatform.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.TutorialCode.GDVerticalMovingPlatformObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDVerticalMovingPlatformObjects5 */
{gdjs.evtsExt__Enemy__ResetPlatform.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.TutorialCode.GDSpinningMovingPlatformObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDSpinningMovingPlatformObjects4 */
{gdjs.evtsExt__Enemy__ResetPlatform.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects5Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects5Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects5Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects5Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects5Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects5Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects4});
gdjs.TutorialCode.eventsList18 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.TutorialCode.GDFlyingDemonObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFlyingDemonObjects5 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.TutorialCode.GDFireDemonObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFireDemonObjects5 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.TutorialCode.GDHorizontalDemonObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDHorizontalDemonObjects5 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Base2Objects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects4 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Base2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects5Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Spike2Objects5Objects = Hashtable.newFrom({"SpikeDemon_Spike2": gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects5Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Spike2Objects5Objects = Hashtable.newFrom({"StalagtiteDemon_Spike2": gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595SpikeObjects5Objects = Hashtable.newFrom({"StalagmiteDemon_Spike": gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike2": gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4});
gdjs.TutorialCode.eventsList19 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects5 */
{for(var i = 0, len = gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike2"), gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Spike2Objects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects5 */
{for(var i = 0, len = gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects5.length ;i < len;++i) {
    gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike"), gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects5 */
{for(var i = 0, len = gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike2"), gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Spike2Objects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects5 */
{for(var i = 0, len = gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects5.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike"), gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595SpikeObjects5Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects5 */
{for(var i = 0, len = gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike2"), gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4 */
{for(var i = 0, len = gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.TutorialCode.asyncCallback23199180 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy2");
}}
gdjs.TutorialCode.eventsList20 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23199180(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.asyncCallback23199940 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}}
gdjs.TutorialCode.eventsList21 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23199940(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.asyncCallback23200748 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy2");
}}
gdjs.TutorialCode.eventsList22 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(4.25), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23200748(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.asyncCallback23201476 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy");
}}
gdjs.TutorialCode.eventsList23 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23201476(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.asyncCallback23202252 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy2");
}}
gdjs.TutorialCode.eventsList24 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(4.25), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23202252(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.eventsList25 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4[i].setPosition(0,6000);
}
}{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0);
}
}{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.25);
}
}}

}


};gdjs.TutorialCode.eventsList26 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy2");
}
{ //Subevents
gdjs.TutorialCode.eventsList20(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}
{ //Subevents
gdjs.TutorialCode.eventsList21(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy2");
}
{ //Subevents
gdjs.TutorialCode.eventsList22(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy");
}
{ //Subevents
gdjs.TutorialCode.eventsList23(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy2");
}
{ //Subevents
gdjs.TutorialCode.eventsList24(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "LaserEnemy");
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
}{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
}{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4[i].getBehavior("Opacity").setOpacity(40);
}
}
{ //Subevents
gdjs.TutorialCode.eventsList25(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList27 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList17(runtimeScene);
}


{


gdjs.TutorialCode.eventsList18(runtimeScene);
}


{


gdjs.TutorialCode.eventsList19(runtimeScene);
}


{


gdjs.TutorialCode.eventsList26(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects3[i].setVariableBoolean(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(4), false);
}
}}

}


};gdjs.TutorialCode.eventsList28 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects3[i].getVariableBoolean(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(4), true) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects3[k] = gdjs.TutorialCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList27(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects4Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.TutorialCode.GDMonsterParticlesObjects3});
gdjs.TutorialCode.eventsList29 = function(runtimeScene) {

{

/* Reuse gdjs.TutorialCode.GDFlyingDemonObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDFlyingDemonObjects3.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDFlyingDemonObjects3[i].getVariableBoolean(gdjs.TutorialCode.GDFlyingDemonObjects3[i].getVariables().getFromIndex(1), true) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDFlyingDemonObjects3[k] = gdjs.TutorialCode.GDFlyingDemonObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDFlyingDemonObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFlyingDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.TutorialCode.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList30 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.TutorialCode.GDFlyingDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFlyingDemonObjects3 */
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.TutorialCode.eventsList29(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList31 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.TutorialCode.GDFlyingDemonObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.TutorialCode.eventsList30(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects4Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.TutorialCode.GDMonsterParticlesObjects3});
gdjs.TutorialCode.eventsList32 = function(runtimeScene) {

{

/* Reuse gdjs.TutorialCode.GDFireDemonObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDFireDemonObjects3.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDFireDemonObjects3[i].getVariableBoolean(gdjs.TutorialCode.GDFireDemonObjects3[i].getVariables().getFromIndex(2), true) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDFireDemonObjects3[k] = gdjs.TutorialCode.GDFireDemonObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDFireDemonObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFireDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.TutorialCode.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList33 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.TutorialCode.GDFireDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFireDemonObjects3 */
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.TutorialCode.eventsList32(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList34 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.TutorialCode.GDFireDemonObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.TutorialCode.GDFireDemonObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDFireDemonObjects4.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDFireDemonObjects4[i].getBehavior("Animation").getAnimationName() == "Fire" ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDFireDemonObjects4[k] = gdjs.TutorialCode.GDFireDemonObjects4[i];
        ++k;
    }
}
gdjs.TutorialCode.GDFireDemonObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFireDemonObjects4 */
{for(var i = 0, len = gdjs.TutorialCode.GDFireDemonObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDFireDemonObjects4[i].returnVariable(gdjs.TutorialCode.GDFireDemonObjects4[i].getVariables().getFromIndex(1)).setNumber(0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.TutorialCode.GDFireDemonObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDFireDemonObjects4.length;i<l;++i) {
    if ( !(gdjs.TutorialCode.GDFireDemonObjects4[i].getBehavior("Animation").getAnimationName() == "Fire") ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDFireDemonObjects4[k] = gdjs.TutorialCode.GDFireDemonObjects4[i];
        ++k;
    }
}
gdjs.TutorialCode.GDFireDemonObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFireDemonObjects4 */
{for(var i = 0, len = gdjs.TutorialCode.GDFireDemonObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDFireDemonObjects4[i].returnVariable(gdjs.TutorialCode.GDFireDemonObjects4[i].getVariables().getFromIndex(1)).setNumber(1);
}
}}

}


{


gdjs.TutorialCode.eventsList33(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects4Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.TutorialCode.GDMonsterParticlesObjects3});
gdjs.TutorialCode.eventsList35 = function(runtimeScene) {

{

/* Reuse gdjs.TutorialCode.GDHorizontalDemonObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDHorizontalDemonObjects3.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDHorizontalDemonObjects3[i].getVariableBoolean(gdjs.TutorialCode.GDHorizontalDemonObjects3[i].getVariables().getFromIndex(1), true) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDHorizontalDemonObjects3[k] = gdjs.TutorialCode.GDHorizontalDemonObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDHorizontalDemonObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDHorizontalDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.TutorialCode.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList36 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.TutorialCode.GDHorizontalDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDHorizontalDemonObjects3 */
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.TutorialCode.eventsList35(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList37 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.TutorialCode.GDHorizontalDemonObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.TutorialCode.eventsList36(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects5Objects = Hashtable.newFrom({"MonsterParticles": gdjs.TutorialCode.GDMonsterParticlesObjects5});
gdjs.TutorialCode.eventsList38 = function(runtimeScene) {

{

/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5[i].getVariableBoolean(gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5[i].getVariables().getFromIndex(1), true) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5[k] = gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.TutorialCode.GDMonsterParticlesObjects5);
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5 */
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects5Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects4});
gdjs.TutorialCode.eventsList39 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects5 */
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.TutorialCode.eventsList38(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects4 */
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.TutorialCode.userFunc0x13a2b10 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
var NumSpikes = 6;
var SpikeScale = 64;

for (var x = 0; x < objects.length; x++) {
    var SpikeDemonBaseInstance = objects[x];

    for (var i = 0; i < NumSpikes; i++) {
        var SpikeAngle = (360/NumSpikes)*i;

        const Spike = runtimeScene.createObject("SpikeDemon_Spike");
        Spike.setWidth(SpikeScale);
        Spike.setHeight(SpikeScale);
        console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
        var CenterX = SpikeDemonBaseInstance.x + SpikeDemonBaseInstance.getWidth()/3.6;
        var CenterY = SpikeDemonBaseInstance.y + SpikeDemonBaseInstance.getHeight()/3.2;
        Spike.setPosition(CenterX, CenterY);
        Spike.setAngle(SpikeAngle);
        
        Spike.setLayer("Base Layer");


    }
}
};
gdjs.TutorialCode.eventsList40 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4);

var objects = [];
objects.push.apply(objects,gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4);
gdjs.TutorialCode.userFunc0x13a2b10(runtimeScene, objects);

}


};gdjs.TutorialCode.eventsList41 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 4, 4, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.eventsList42 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy") >= 6;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy");
}
{ //Subevents
gdjs.TutorialCode.eventsList40(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy") >= 4.5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList41(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList43 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.TutorialCode.eventsList39(runtimeScene);
}


{


gdjs.TutorialCode.eventsList42(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"SpikeDemon_Base2": gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects5Objects = Hashtable.newFrom({"MonsterParticles": gdjs.TutorialCode.GDMonsterParticlesObjects5});
gdjs.TutorialCode.eventsList44 = function(runtimeScene) {

{

/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5[i].getVariableBoolean(gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5[i].getVariables().getFromIndex(1), true) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5[k] = gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5[i];
        ++k;
    }
}
gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.TutorialCode.GDMonsterParticlesObjects5);
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5 */
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Base2Objects5Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Spike2": gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"SpikeDemon_Spike2": gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects4});
gdjs.TutorialCode.eventsList45 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Base2Objects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects5 */
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Base2Objects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.TutorialCode.eventsList44(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike2"), gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Spike2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects4 */
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Spike2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.TutorialCode.asyncCallback23227740 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy2");
}}
gdjs.TutorialCode.eventsList46 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23227740(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.userFunc0x11c6450 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
var NumSpikes = 6;
var SpikeScale = 64;

for (var x = 0; x < objects.length; x++) {
    var SpikeDemonBaseInstance = objects[x];

    for (var i = 0; i < NumSpikes; i++) {
        var SpikeAngle = (360/NumSpikes)*i;

        const Spike = runtimeScene.createObject("SpikeDemon_Spike2");
        Spike.setWidth(SpikeScale);
        Spike.setHeight(SpikeScale);
        console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
        var CenterX = SpikeDemonBaseInstance.x + SpikeDemonBaseInstance.getWidth()/3.6;
        var CenterY = SpikeDemonBaseInstance.y + SpikeDemonBaseInstance.getHeight()/3.2;
        Spike.setPosition(CenterX, CenterY);
        Spike.setAngle(SpikeAngle);
        
        Spike.setLayer("Base Layer");


    }
}
};
gdjs.TutorialCode.eventsList47 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects4);

var objects = [];
objects.push.apply(objects,gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects4);
gdjs.TutorialCode.userFunc0x11c6450(runtimeScene, objects);

}


};gdjs.TutorialCode.eventsList48 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects3);
{for(var i = 0, len = gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 4, 4, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.eventsList49 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList46(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy2") >= 6;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy2");
}
{ //Subevents
gdjs.TutorialCode.eventsList47(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy2") >= 4.5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList48(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList50 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base2"), gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595Base2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.TutorialCode.eventsList45(runtimeScene);
}


{


gdjs.TutorialCode.eventsList49(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4});
gdjs.TutorialCode.eventsList51 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects5 */
/* Reuse gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike"), gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects4 */
/* Reuse gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatform2Objects4Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.TutorialCode.GDStaticPlatform1Objects4, "HorizontalMovingPlatform": gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4, "StaticPlatform2": gdjs.TutorialCode.GDStaticPlatform2Objects4, "StaticPlatform3": gdjs.TutorialCode.GDStaticPlatform3Objects4, "VerticalMovingPlatform": gdjs.TutorialCode.GDVerticalMovingPlatformObjects4, "SpinningMovingPlatform": gdjs.TutorialCode.GDSpinningMovingPlatformObjects4, "FlippingPlatform": gdjs.TutorialCode.GDFlippingPlatformObjects4, "FlippingPlatform2": gdjs.TutorialCode.GDFlippingPlatform2Objects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeParticlesObjects4Objects = Hashtable.newFrom({"SpikeParticles": gdjs.TutorialCode.GDSpikeParticlesObjects4});
gdjs.TutorialCode.eventsList52 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.TutorialCode.GDFlippingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.TutorialCode.GDFlippingPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.TutorialCode.GDSpinningMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike"), gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.TutorialCode.GDStaticPlatform1Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.TutorialCode.GDStaticPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.TutorialCode.GDStaticPlatform3Objects4);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.TutorialCode.GDVerticalMovingPlatformObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatform2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4 */
gdjs.TutorialCode.GDSpikeParticlesObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeParticlesObjects4Objects, (( gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4[0].getPointX("")) + (( gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4[0].getWidth()) / 2, (( gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4[0].getPointY("")) + (( gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.TutorialCode.asyncCallback23236220 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}}
gdjs.TutorialCode.eventsList53 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23236220(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.userFunc0x10c11d0 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagtite = objects[x];
    const Spike = runtimeScene.createObject("StalagtiteDemon_Spike");
    Spike.setWidth(96);
    Spike.setHeight(48);
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
    var CenterX = Stalagtite.x + Stalagtite.getWidth()/3;
    var CenterY = Stalagtite.y + Stalagtite.getHeight() - Stalagtite.getHeight()/3;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(90);
    Spike.setLayer("Base Layer");
}
};
gdjs.TutorialCode.eventsList54 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4);

var objects = [];
objects.push.apply(objects,gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4);
gdjs.TutorialCode.userFunc0x10c11d0(runtimeScene, objects);

}


};gdjs.TutorialCode.eventsList55 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList53(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy") >= 6.5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}
{ //Subevents
gdjs.TutorialCode.eventsList54(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy") >= 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.eventsList56 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.TutorialCode.eventsList51(runtimeScene);
}


{


gdjs.TutorialCode.eventsList52(runtimeScene);
}


{


gdjs.TutorialCode.eventsList55(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"StalagtiteDemon_Base2": gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"StalagtiteDemon_Base2": gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"StalagtiteDemon_Base2": gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike2": gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike2": gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4});
gdjs.TutorialCode.eventsList57 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base2"), gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Base2Objects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects5 */
/* Reuse gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Base2Objects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike2"), gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Spike2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects4 */
/* Reuse gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Spike2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagtiteDemon_Spike2": gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatform2Objects4Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.TutorialCode.GDStaticPlatform1Objects4, "HorizontalMovingPlatform": gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4, "StaticPlatform2": gdjs.TutorialCode.GDStaticPlatform2Objects4, "StaticPlatform3": gdjs.TutorialCode.GDStaticPlatform3Objects4, "VerticalMovingPlatform": gdjs.TutorialCode.GDVerticalMovingPlatformObjects4, "SpinningMovingPlatform": gdjs.TutorialCode.GDSpinningMovingPlatformObjects4, "FlippingPlatform": gdjs.TutorialCode.GDFlippingPlatformObjects4, "FlippingPlatform2": gdjs.TutorialCode.GDFlippingPlatform2Objects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeParticlesObjects4Objects = Hashtable.newFrom({"SpikeParticles": gdjs.TutorialCode.GDSpikeParticlesObjects4});
gdjs.TutorialCode.eventsList58 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.TutorialCode.GDFlippingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.TutorialCode.GDFlippingPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.TutorialCode.GDSpinningMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike2"), gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.TutorialCode.GDStaticPlatform1Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.TutorialCode.GDStaticPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.TutorialCode.GDStaticPlatform3Objects4);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.TutorialCode.GDVerticalMovingPlatformObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Spike2Objects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatform2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4 */
gdjs.TutorialCode.GDSpikeParticlesObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeParticlesObjects4Objects, (( gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4[0].getPointX("")) + (( gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4[0].getWidth()) / 2, (( gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4[0].getPointY("")) + (( gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.TutorialCode.asyncCallback23244284 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy2");
}}
gdjs.TutorialCode.eventsList59 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(4.25), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23244284(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.userFunc0x107e500 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagtite = objects[x];
    const Spike = runtimeScene.createObject("StalagtiteDemon_Spike2");
    Spike.setWidth(96);
    Spike.setHeight(48);
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
    var CenterX = Stalagtite.x + Stalagtite.getWidth()/3;
    var CenterY = Stalagtite.y + Stalagtite.getHeight() - Stalagtite.getHeight()/3;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(90);
    Spike.setLayer("Base Layer");
}
};
gdjs.TutorialCode.eventsList60 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base2"), gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects4);

var objects = [];
objects.push.apply(objects,gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects4);
gdjs.TutorialCode.userFunc0x107e500(runtimeScene, objects);

}


};gdjs.TutorialCode.eventsList61 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList59(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy2") >= 6.5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy2");
}
{ //Subevents
gdjs.TutorialCode.eventsList60(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy2") >= 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base2"), gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects3);
{for(var i = 0, len = gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.eventsList62 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base2"), gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595Base2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.TutorialCode.eventsList57(runtimeScene);
}


{


gdjs.TutorialCode.eventsList58(runtimeScene);
}


{


gdjs.TutorialCode.eventsList61(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"StalagmiteDemon_Base": gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"StalagmiteDemon_Base": gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"StalagmiteDemon_Base": gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike": gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike": gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4});
gdjs.TutorialCode.eventsList63 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595BaseObjects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects5 */
/* Reuse gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595BaseObjects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike"), gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595SpikeObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects4 */
/* Reuse gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595SpikeObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike": gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatform2Objects4Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.TutorialCode.GDStaticPlatform1Objects4, "HorizontalMovingPlatform": gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4, "StaticPlatform2": gdjs.TutorialCode.GDStaticPlatform2Objects4, "StaticPlatform3": gdjs.TutorialCode.GDStaticPlatform3Objects4, "VerticalMovingPlatform": gdjs.TutorialCode.GDVerticalMovingPlatformObjects4, "SpinningMovingPlatform": gdjs.TutorialCode.GDSpinningMovingPlatformObjects4, "FlippingPlatform": gdjs.TutorialCode.GDFlippingPlatformObjects4, "FlippingPlatform2": gdjs.TutorialCode.GDFlippingPlatform2Objects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeParticlesObjects4Objects = Hashtable.newFrom({"SpikeParticles": gdjs.TutorialCode.GDSpikeParticlesObjects4});
gdjs.TutorialCode.eventsList64 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.TutorialCode.GDFlippingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.TutorialCode.GDFlippingPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.TutorialCode.GDSpinningMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike"), gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.TutorialCode.GDStaticPlatform1Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.TutorialCode.GDStaticPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.TutorialCode.GDStaticPlatform3Objects4);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.TutorialCode.GDVerticalMovingPlatformObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595SpikeObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatform2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4 */
gdjs.TutorialCode.GDSpikeParticlesObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeParticlesObjects4Objects, (( gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4[0].getPointX("")) + (( gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4[0].getWidth()) / 2, (( gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4[0].getPointY("")) + (( gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.TutorialCode.asyncCallback23252316 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy");
}}
gdjs.TutorialCode.eventsList65 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23252316(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.userFunc0xb54730 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagmite = objects[x];
    const Spike = runtimeScene.createObject("StalagmiteDemon_Spike");
    Spike.setWidth(96);
    Spike.setHeight(48);
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
    var CenterX = Stalagmite.x + Stalagmite.getWidth()/3;
    var CenterY = Stalagmite.y;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(270);
    Spike.setLayer("Base Layer");
}
};
gdjs.TutorialCode.eventsList66 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects4);

var objects = [];
objects.push.apply(objects,gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects4);
gdjs.TutorialCode.userFunc0xb54730(runtimeScene, objects);

}


};gdjs.TutorialCode.eventsList67 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList65(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagmiteEnemy") >= 6.5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy");
}
{ //Subevents
gdjs.TutorialCode.eventsList66(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagmiteEnemy") >= 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.eventsList68 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.TutorialCode.eventsList63(runtimeScene);
}


{


gdjs.TutorialCode.eventsList64(runtimeScene);
}


{


gdjs.TutorialCode.eventsList67(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Base2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Base2": gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"StalagmiteDemon_Base2": gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Base2Objects5Objects = Hashtable.newFrom({"StalagmiteDemon_Base2": gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects5});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike2": gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike2": gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4});
gdjs.TutorialCode.eventsList69 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base2"), gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Base2Objects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects5 */
/* Reuse gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects5 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects5Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Base2Objects5Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike2"), gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects4 */
/* Reuse gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects = Hashtable.newFrom({"StalagmiteDemon_Spike2": gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatform2Objects4Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.TutorialCode.GDStaticPlatform1Objects4, "HorizontalMovingPlatform": gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4, "StaticPlatform2": gdjs.TutorialCode.GDStaticPlatform2Objects4, "StaticPlatform3": gdjs.TutorialCode.GDStaticPlatform3Objects4, "VerticalMovingPlatform": gdjs.TutorialCode.GDVerticalMovingPlatformObjects4, "SpinningMovingPlatform": gdjs.TutorialCode.GDSpinningMovingPlatformObjects4, "FlippingPlatform": gdjs.TutorialCode.GDFlippingPlatformObjects4, "FlippingPlatform2": gdjs.TutorialCode.GDFlippingPlatform2Objects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeParticlesObjects4Objects = Hashtable.newFrom({"SpikeParticles": gdjs.TutorialCode.GDSpikeParticlesObjects4});
gdjs.TutorialCode.eventsList70 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.TutorialCode.GDFlippingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.TutorialCode.GDFlippingPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningMovingPlatform"), gdjs.TutorialCode.GDSpinningMovingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike2"), gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.TutorialCode.GDStaticPlatform1Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.TutorialCode.GDStaticPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.TutorialCode.GDStaticPlatform3Objects4);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatform"), gdjs.TutorialCode.GDVerticalMovingPlatformObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Spike2Objects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546TutorialCode_9546GDVerticalMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDSpinningMovingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546TutorialCode_9546GDFlippingPlatform2Objects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4 */
gdjs.TutorialCode.GDSpikeParticlesObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeParticlesObjects4Objects, (( gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4[0].getPointX("")) + (( gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4[0].getWidth()) / 2, (( gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4[0].getPointY("")) + (( gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.TutorialCode.asyncCallback23260420 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy2");
}}
gdjs.TutorialCode.eventsList71 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(4.25), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23260420(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.userFunc0xd4b8e0 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagmite = objects[x];
    const Spike = runtimeScene.createObject("StalagmiteDemon_Spike2");
    Spike.setWidth(96);
    Spike.setHeight(48);
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight()); // doesn't work without this print statement (gives time for renderer to update() maybe?)
    var CenterX = Stalagmite.x + Stalagmite.getWidth()/3;
    var CenterY = Stalagmite.y;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(270);
    Spike.setLayer("Base Layer");
}
};
gdjs.TutorialCode.eventsList72 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base2"), gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects4);

var objects = [];
objects.push.apply(objects,gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects4);
gdjs.TutorialCode.userFunc0xd4b8e0(runtimeScene, objects);

}


};gdjs.TutorialCode.eventsList73 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList71(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagmiteEnemy2") >= 6.5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagmiteEnemy2");
}
{ //Subevents
gdjs.TutorialCode.eventsList72(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagmiteEnemy2") >= 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base2"), gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects3);
{for(var i = 0, len = gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.04, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.eventsList74 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base2"), gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects4);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagmiteDemon_95959595Base2Objects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.TutorialCode.eventsList69(runtimeScene);
}


{


gdjs.TutorialCode.eventsList70(runtimeScene);
}


{


gdjs.TutorialCode.eventsList73(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"LaserDemon_Base": gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BeamObjects3Objects = Hashtable.newFrom({"LaserDemon_Beam": gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3});
gdjs.TutorialCode.eventsList75 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BeamObjects3Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3 */
{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3[i].returnVariable(gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3[i].getVariables().getFromIndex(1)).setNumber((gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3[i].getPointX("")));
}
}{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3[i].returnVariable(gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3[i].getVariables().getFromIndex(2)).setNumber((gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3[i].getPointY("")));
}
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"LaserDemon_Base": gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"LaserDemon_Base": gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"LaserDemon_Base": gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects4Objects = Hashtable.newFrom({"MonsterParticles": gdjs.TutorialCode.GDMonsterParticlesObjects4});
gdjs.TutorialCode.eventsList76 = function(runtimeScene) {

{

/* Reuse gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4[k] = gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.TutorialCode.GDMonsterParticlesObjects4);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BaseObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BeamObjects3Objects = Hashtable.newFrom({"LaserDemon_Beam": gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BeamObjects3Objects = Hashtable.newFrom({"LaserDemon_Beam": gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3});
gdjs.TutorialCode.eventsList77 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BaseObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4 */
/* Reuse gdjs.TutorialCode.GDPlayerObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.TutorialCode.eventsList76(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BeamObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3 */
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3);
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BeamObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.TutorialCode.eventsList78 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3[i].setPosition(0,6000);
}
}{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0);
}
}{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.25);
}
}}

}


};gdjs.TutorialCode.asyncCallback23276348 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.TutorialCode.GDLaserDemon_9595BeamObjects6);

{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects6.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects6[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 2);
}
}}
gdjs.TutorialCode.eventsList79 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TutorialCode.GDLaserDemon_9595BeamObjects5) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23276348(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.asyncCallback23276180 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.TutorialCode.GDLaserDemon_9595BeamObjects5);

{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects5.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects5[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 1);
}
}
{ //Subevents
gdjs.TutorialCode.eventsList79(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TutorialCode.eventsList80 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23276180(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.asyncCallback23275644 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4);

{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.5);
}
}
{ //Subevents
gdjs.TutorialCode.eventsList80(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TutorialCode.eventsList81 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
for (const obj of gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23275644(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.eventsList82 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3[i].setPosition(gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3[i].getVariables().getFromIndex(1).getAsNumber(),gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3[i].getVariables().getFromIndex(2).getAsNumber());
}
}
{ //Subevents
gdjs.TutorialCode.eventsList81(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.asyncCallback23281572 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.TutorialCode.GDLaserDemon_9595BeamObjects5);

{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects5.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects5[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0);
}
}}
gdjs.TutorialCode.eventsList83 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.1), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23281572(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.asyncCallback23280628 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4);

{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0);
}
}{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.3);
}
}
{ //Subevents
gdjs.TutorialCode.eventsList83(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TutorialCode.eventsList84 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.1), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23280628(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.asyncCallback23280172 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_Beam"), gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3);

{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0.5);
}
}
{ //Subevents
gdjs.TutorialCode.eventsList84(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TutorialCode.eventsList85 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
for (const obj of gdjs.TutorialCode.GDLaserDemon_9595BeamObjects2) asyncObjectsList.addObject("LaserDemon_Beam", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.1), (runtimeScene) => (gdjs.TutorialCode.asyncCallback23280172(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TutorialCode.eventsList86 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.TutorialCode.GDLaserDemon_9595BeamObjects2);
{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BeamObjects2.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BeamObjects2[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 1.25);
}
}
{ //Subevents
gdjs.TutorialCode.eventsList85(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList87 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "LaserEnemy");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 10;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "LaserEnemy");
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 4;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23270692);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
}{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
}{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3[i].getBehavior("Opacity").setOpacity(40);
}
}
{ //Subevents
gdjs.TutorialCode.eventsList78(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 2.75;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 4;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23273532);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "easeInQuad", 1.25, false);
}
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 4;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 10;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23275188);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList82(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 9;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 10;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23277988);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3[i].getBehavior("Tween").addObjectOpacityTween2("ChargeDown", 40, "easeInQuad", 1, false);
}
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") >= 9.7;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "LaserEnemy") < 10;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23279652);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList86(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList88 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDLaserDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.TutorialCode.eventsList75(runtimeScene);} //End of subevents
}

}


{


gdjs.TutorialCode.eventsList77(runtimeScene);
}


{


gdjs.TutorialCode.eventsList87(runtimeScene);
}


};gdjs.TutorialCode.eventsList89 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList28(runtimeScene);
}


{


gdjs.TutorialCode.eventsList31(runtimeScene);
}


{


gdjs.TutorialCode.eventsList34(runtimeScene);
}


{


gdjs.TutorialCode.eventsList37(runtimeScene);
}


{


gdjs.TutorialCode.eventsList43(runtimeScene);
}


{


gdjs.TutorialCode.eventsList50(runtimeScene);
}


{


gdjs.TutorialCode.eventsList56(runtimeScene);
}


{


gdjs.TutorialCode.eventsList62(runtimeScene);
}


{


gdjs.TutorialCode.eventsList68(runtimeScene);
}


{


gdjs.TutorialCode.eventsList74(runtimeScene);
}


{


gdjs.TutorialCode.eventsList88(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects4Objects = Hashtable.newFrom({"Portal": gdjs.TutorialCode.GDPortalObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects3Objects = Hashtable.newFrom({"Portal": gdjs.TutorialCode.GDPortalObjects3});
gdjs.TutorialCode.eventsList90 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.TutorialCode.GDPlayerObjects3, gdjs.TutorialCode.GDPlayerObjects4);

{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects4[i].activateBehavior("PlatformerObject", false);
}
}}

}


{

/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects3[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects3[k] = gdjs.TutorialCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
/* Reuse gdjs.TutorialCode.GDPortalObjects3 */
{gdjs.evtsExt__Player__AnimateFallingIntoPortal.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, "Tween", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList91 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList90(runtimeScene);
}


};gdjs.TutorialCode.eventsList92 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects4[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects4[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects4[k] = gdjs.TutorialCode.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_DeathText"), gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects4);
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_RetryButton"), gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects4[i].hide();
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects3.length;i<l;++i) {
    if ( !(gdjs.TutorialCode.GDPlayerObjects3[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 0) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects3[k] = gdjs.TutorialCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_MemoryAcquired"), gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3);
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_ProceedButton"), gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3[i].hide();
}
}{for(var i = 0, len = gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects3[i].hide();
}
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDUI_95959595Sinage_95959595BackgroundObjects2Objects = Hashtable.newFrom({"UI_Sinage_Background": gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects2});
gdjs.TutorialCode.eventsList93 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23287540);
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList92(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_Background"), gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects2);
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_RetryButton"), gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2);
{gdjs.evtsExt__UserInterface__StretchToFillScreen.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDUI_95959595Sinage_95959595BackgroundObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects2.length ;i < len;++i) {
    gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects2[i].setCenterXInScene(gdjs.evtTools.camera.getCameraX(runtimeScene, "EndScreen", 0));
}
for(var i = 0, len = gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2.length ;i < len;++i) {
    gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2[i].setCenterXInScene(gdjs.evtTools.camera.getCameraX(runtimeScene, "EndScreen", 0));
}
}}

}


};gdjs.TutorialCode.eventsList94 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList93(runtimeScene);
}


};gdjs.TutorialCode.eventsList95 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_Background"), gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects3);
{gdjs.evtTools.camera.hideLayer(runtimeScene, "EndScreen");
}{for(var i = 0, len = gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects3[i].setOpacity(180);
}
}}

}


{



}


{

gdjs.TutorialCode.GDPlayerObjects3.length = 0;

gdjs.TutorialCode.GDPortalObjects3.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.TutorialCode.GDPlayerObjects3_1final.length = 0;
gdjs.TutorialCode.GDPortalObjects3_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.TutorialCode.GDPortalObjects4);
isConditionTrue_1 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects4Objects, false, runtimeScene, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.TutorialCode.GDPlayerObjects4.length; j < jLen ; ++j) {
        if ( gdjs.TutorialCode.GDPlayerObjects3_1final.indexOf(gdjs.TutorialCode.GDPlayerObjects4[j]) === -1 )
            gdjs.TutorialCode.GDPlayerObjects3_1final.push(gdjs.TutorialCode.GDPlayerObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.TutorialCode.GDPortalObjects4.length; j < jLen ; ++j) {
        if ( gdjs.TutorialCode.GDPortalObjects3_1final.indexOf(gdjs.TutorialCode.GDPortalObjects4[j]) === -1 )
            gdjs.TutorialCode.GDPortalObjects3_1final.push(gdjs.TutorialCode.GDPortalObjects4[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects4[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects4[i].getVariables().getFromIndex(1)) <= 0 ) {
        isConditionTrue_1 = true;
        gdjs.TutorialCode.GDPlayerObjects4[k] = gdjs.TutorialCode.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects4.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.TutorialCode.GDPlayerObjects4.length; j < jLen ; ++j) {
        if ( gdjs.TutorialCode.GDPlayerObjects3_1final.indexOf(gdjs.TutorialCode.GDPlayerObjects4[j]) === -1 )
            gdjs.TutorialCode.GDPlayerObjects3_1final.push(gdjs.TutorialCode.GDPlayerObjects4[j]);
    }
}
}
{
gdjs.copyArray(gdjs.TutorialCode.GDPlayerObjects3_1final, gdjs.TutorialCode.GDPlayerObjects3);
gdjs.copyArray(gdjs.TutorialCode.GDPortalObjects3_1final, gdjs.TutorialCode.GDPortalObjects3);
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23283420);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.camera.showLayer(runtimeScene, "EndScreen");
}{gdjs.evtTools.sound.stopSoundOnChannel(runtimeScene, 2);
}
{ //Subevents
gdjs.TutorialCode.eventsList91(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "EndScreen");
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList94(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList96 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("BoundaryJumpThrough"), gdjs.TutorialCode.GDBoundaryJumpThroughObjects4);
gdjs.copyArray(runtimeScene.getObjects("LeftBoundary"), gdjs.TutorialCode.GDLeftBoundaryObjects4);
gdjs.copyArray(runtimeScene.getObjects("RightBoundary"), gdjs.TutorialCode.GDRightBoundaryObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDLeftBoundaryObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDLeftBoundaryObjects4[i].hide();
}
for(var i = 0, len = gdjs.TutorialCode.GDRightBoundaryObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDRightBoundaryObjects4[i].hide();
}
for(var i = 0, len = gdjs.TutorialCode.GDBoundaryJumpThroughObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDBoundaryJumpThroughObjects4[i].hide();
}
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("BottomBoundary"), gdjs.TutorialCode.GDBottomBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("LeftBoundary"), gdjs.TutorialCode.GDLeftBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("RightBoundary"), gdjs.TutorialCode.GDRightBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("TopBoundary"), gdjs.TutorialCode.GDTopBoundaryObjects3);
{gdjs.evtTools.camera.clampCamera(runtimeScene, (( gdjs.TutorialCode.GDLeftBoundaryObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDLeftBoundaryObjects3[0].getPointX("")) + (( gdjs.TutorialCode.GDLeftBoundaryObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDLeftBoundaryObjects3[0].getWidth()), (( gdjs.TutorialCode.GDTopBoundaryObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDTopBoundaryObjects3[0].getPointY("")) + (( gdjs.TutorialCode.GDTopBoundaryObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDTopBoundaryObjects3[0].getHeight()), (( gdjs.TutorialCode.GDRightBoundaryObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDRightBoundaryObjects3[0].getPointX("")), (( gdjs.TutorialCode.GDBottomBoundaryObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDBottomBoundaryObjects3[0].getPointY("")), "", 0);
}}

}


};gdjs.TutorialCode.eventsList97 = function(runtimeScene) {

{



}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "RandomNoiseTimer");
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("BackgroundPlants"), gdjs.TutorialCode.GDBackgroundPlantsObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDBackgroundPlantsObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDBackgroundPlantsObjects3[i].setWidth(gdjs.evtTools.camera.getCameraWidth(runtimeScene, "", 0));
}
}{for(var i = 0, len = gdjs.TutorialCode.GDBackgroundPlantsObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDBackgroundPlantsObjects3[i].setXOffset(gdjs.evtTools.camera.getCameraBorderLeft(runtimeScene, "", 0) / 3 + 780);
}
}}

}


};gdjs.TutorialCode.eventsList98 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{/* Unknown object - skipped. */}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects3Objects = Hashtable.newFrom({"Portal": gdjs.TutorialCode.GDPortalObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects2Objects = Hashtable.newFrom({"Portal": gdjs.TutorialCode.GDPortalObjects2});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects2});
gdjs.TutorialCode.eventsList99 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "door.aac", 0, true, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.TutorialCode.GDPortalObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23297804);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Portal/PortalInteract.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects2);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.TutorialCode.GDPortalObjects2);
{gdjs.evtsExt__VolumeFalloff__SetVolumeFalloff.func(runtimeScene, 0, "Sound", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects2Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects, 0, 100, 750, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList100 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList96(runtimeScene);
}


{


gdjs.TutorialCode.eventsList97(runtimeScene);
}


{


gdjs.TutorialCode.eventsList98(runtimeScene);
}


{


gdjs.TutorialCode.eventsList99(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDBoundaryJumpThroughObjects2Objects = Hashtable.newFrom({"BoundaryJumpThrough": gdjs.TutorialCode.GDBoundaryJumpThroughObjects2});
gdjs.TutorialCode.eventsList101 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("BoundaryJumpThrough"), gdjs.TutorialCode.GDBoundaryJumpThroughObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDBoundaryJumpThroughObjects2Objects);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23299580);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDBoundaryJumpThroughObjects2 */
{for(var i = 0, len = gdjs.TutorialCode.GDBoundaryJumpThroughObjects2.length ;i < len;++i) {
    gdjs.TutorialCode.GDBoundaryJumpThroughObjects2[i].hide();
}
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3});
gdjs.TutorialCode.eventsList102 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "t");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23300812);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects3[i].returnVariable(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(5)).setNumber(1);
}
}{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects3[i].setPosition(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(6).getAsNumber(),gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(7).getAsNumber());
}
}{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, ((gdjs.TutorialCode.GDPlayerObjects3.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.TutorialCode.GDPlayerObjects3[0].getVariables()).getFromIndex(6).getAsNumber(), ((gdjs.TutorialCode.GDPlayerObjects3.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.TutorialCode.GDPlayerObjects3[0].getVariables()).getFromIndex(7).getAsNumber(), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Tilde");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23302108);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.TutorialCode.GDFireDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.TutorialCode.GDFlyingDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.TutorialCode.GDHorizontalDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3);
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "i");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23303020);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects3[i].returnVariable(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(5)).setNumber(1 - gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(5).getAsNumber());
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Escape");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23303940);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, gdjs.evtTools.runtimeScene.getSceneName(runtimeScene), false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "l");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23304796);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "TestingLevel", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "m");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23306020);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Mindscape", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num0");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23306732);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Tutorial", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num1");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23306460);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level1", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num2");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23308092);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level2", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num3");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23308812);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level3", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num4");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23308492);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level4", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num5");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23309636);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level5", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num6");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23310300);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level6", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num7");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(23310964);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level7", false);
}}

}


};gdjs.TutorialCode.eventsList103 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("HopeBar"), gdjs.TutorialCode.GDHopeBarObjects2);
gdjs.copyArray(runtimeScene.getObjects("LivesBar"), gdjs.TutorialCode.GDLivesBarObjects2);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects2);
{for(var i = 0, len = gdjs.TutorialCode.GDLivesBarObjects2.length ;i < len;++i) {
    gdjs.TutorialCode.GDLivesBarObjects2[i].SetValue((gdjs.RuntimeObject.getVariableNumber(((gdjs.TutorialCode.GDPlayerObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.TutorialCode.GDPlayerObjects2[0].getVariables()).getFromIndex(1))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.TutorialCode.GDHopeBarObjects2.length ;i < len;++i) {
    gdjs.TutorialCode.GDHopeBarObjects2[i].SetValue((gdjs.RuntimeObject.getVariableNumber(((gdjs.TutorialCode.GDPlayerObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.TutorialCode.GDPlayerObjects2[0].getVariables()).getFromIndex(3))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "LevelUI", 0, 0, 0);
}{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "LevelEndScreen", 0, 0, 0);
}}

}


};gdjs.TutorialCode.eventsList104 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList8(runtimeScene);
}


{


gdjs.TutorialCode.eventsList15(runtimeScene);
}


{


gdjs.TutorialCode.eventsList89(runtimeScene);
}


{


gdjs.TutorialCode.eventsList95(runtimeScene);
}


{


gdjs.TutorialCode.eventsList100(runtimeScene);
}


{


gdjs.TutorialCode.eventsList101(runtimeScene);
}


{


gdjs.TutorialCode.eventsList102(runtimeScene);
}


{


gdjs.TutorialCode.eventsList103(runtimeScene);
}


};gdjs.TutorialCode.eventsList105 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects3[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects3[k] = gdjs.TutorialCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Mindscape", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects2[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects2[i].getVariables().getFromIndex(1)) <= 0 ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects2[k] = gdjs.TutorialCode.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Tutorial", false);
}}

}


};gdjs.TutorialCode.eventsList106 = function(runtimeScene) {

{

gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "EndScreen");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_RetryButton"), gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects3);
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects3.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects3[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects3[k] = gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects3.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects3.length; j < jLen ; ++j) {
        if ( gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2_1final.indexOf(gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects3[j]) === -1 )
            gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2_1final.push(gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects3[j]);
    }
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "Space");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
gdjs.copyArray(gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2_1final, gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList105(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects1);
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects1[i].returnVariable(gdjs.TutorialCode.GDPlayerObjects1[i].getVariables().getFromIndex(3)).setNumber(0);
}
}}

}


};gdjs.TutorialCode.eventsList107 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 1;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance1.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance2.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 3;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance3.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


};gdjs.TutorialCode.eventsList108 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Levels/0/AmbientLoop.ogg", true, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "RandomNoiseTimer") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("TimeBeforeNextRandomSFX"));
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("RandomSFXIndex").setNumber(gdjs.randomInRange(1, 3));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "RandomNoiseTimer");
}{runtimeScene.getScene().getVariables().get("TimeBeforeNextRandomSFX").setNumber(gdjs.randomFloatInRange(30, 240));
}
{ //Subevents
gdjs.TutorialCode.eventsList107(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList109 = function(runtimeScene) {

{



}


{


gdjs.TutorialCode.eventsList104(runtimeScene);
}


{


gdjs.TutorialCode.eventsList106(runtimeScene);
}


{


gdjs.TutorialCode.eventsList108(runtimeScene);
}


};

gdjs.TutorialCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.TutorialCode.GDBackgroundPlantsObjects1.length = 0;
gdjs.TutorialCode.GDBackgroundPlantsObjects2.length = 0;
gdjs.TutorialCode.GDBackgroundPlantsObjects3.length = 0;
gdjs.TutorialCode.GDBackgroundPlantsObjects4.length = 0;
gdjs.TutorialCode.GDBackgroundPlantsObjects5.length = 0;
gdjs.TutorialCode.GDBackgroundPlantsObjects6.length = 0;
gdjs.TutorialCode.GDLeftBoundaryObjects1.length = 0;
gdjs.TutorialCode.GDLeftBoundaryObjects2.length = 0;
gdjs.TutorialCode.GDLeftBoundaryObjects3.length = 0;
gdjs.TutorialCode.GDLeftBoundaryObjects4.length = 0;
gdjs.TutorialCode.GDLeftBoundaryObjects5.length = 0;
gdjs.TutorialCode.GDLeftBoundaryObjects6.length = 0;
gdjs.TutorialCode.GDRightBoundaryObjects1.length = 0;
gdjs.TutorialCode.GDRightBoundaryObjects2.length = 0;
gdjs.TutorialCode.GDRightBoundaryObjects3.length = 0;
gdjs.TutorialCode.GDRightBoundaryObjects4.length = 0;
gdjs.TutorialCode.GDRightBoundaryObjects5.length = 0;
gdjs.TutorialCode.GDRightBoundaryObjects6.length = 0;
gdjs.TutorialCode.GDTopBoundaryObjects1.length = 0;
gdjs.TutorialCode.GDTopBoundaryObjects2.length = 0;
gdjs.TutorialCode.GDTopBoundaryObjects3.length = 0;
gdjs.TutorialCode.GDTopBoundaryObjects4.length = 0;
gdjs.TutorialCode.GDTopBoundaryObjects5.length = 0;
gdjs.TutorialCode.GDTopBoundaryObjects6.length = 0;
gdjs.TutorialCode.GDBottomBoundaryObjects1.length = 0;
gdjs.TutorialCode.GDBottomBoundaryObjects2.length = 0;
gdjs.TutorialCode.GDBottomBoundaryObjects3.length = 0;
gdjs.TutorialCode.GDBottomBoundaryObjects4.length = 0;
gdjs.TutorialCode.GDBottomBoundaryObjects5.length = 0;
gdjs.TutorialCode.GDBottomBoundaryObjects6.length = 0;
gdjs.TutorialCode.GDBoundaryJumpThroughObjects1.length = 0;
gdjs.TutorialCode.GDBoundaryJumpThroughObjects2.length = 0;
gdjs.TutorialCode.GDBoundaryJumpThroughObjects3.length = 0;
gdjs.TutorialCode.GDBoundaryJumpThroughObjects4.length = 0;
gdjs.TutorialCode.GDBoundaryJumpThroughObjects5.length = 0;
gdjs.TutorialCode.GDBoundaryJumpThroughObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595ArrowObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595ArrowObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595ArrowObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595ArrowObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595ArrowObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595ArrowObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595JumpObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595JumpObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595JumpObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595JumpObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595JumpObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595JumpObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595CollectObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595CollectObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595CollectObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595CollectObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595CollectObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595CollectObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595KillObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595KillObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595KillObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595KillObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595KillObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595KillObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595CheckpointObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595CheckpointObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595CheckpointObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595CheckpointObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595CheckpointObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595CheckpointObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595DownArrowObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595DownArrowObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595DownArrowObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595DownArrowObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595DownArrowObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595DownArrowObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595DeathObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595DeathObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595DeathObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595DeathObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595DeathObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595DeathObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595ProceedObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595ProceedObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595ProceedObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595ProceedObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595ProceedObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595ProceedObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595SpaceObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595SpaceObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595SpaceObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595SpaceObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595SpaceObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595SpaceObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595WObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595WObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595WObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595WObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595WObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595WObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595AObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595AObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595AObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595AObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595AObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595AObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595DObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595DObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595DObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595DObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595DObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595DObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595SObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595SObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595SObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595SObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595SObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595SObjects6.length = 0;
gdjs.TutorialCode.GDPlayerObjects1.length = 0;
gdjs.TutorialCode.GDPlayerObjects2.length = 0;
gdjs.TutorialCode.GDPlayerObjects3.length = 0;
gdjs.TutorialCode.GDPlayerObjects4.length = 0;
gdjs.TutorialCode.GDPlayerObjects5.length = 0;
gdjs.TutorialCode.GDPlayerObjects6.length = 0;
gdjs.TutorialCode.GDFlyingDemonObjects1.length = 0;
gdjs.TutorialCode.GDFlyingDemonObjects2.length = 0;
gdjs.TutorialCode.GDFlyingDemonObjects3.length = 0;
gdjs.TutorialCode.GDFlyingDemonObjects4.length = 0;
gdjs.TutorialCode.GDFlyingDemonObjects5.length = 0;
gdjs.TutorialCode.GDFlyingDemonObjects6.length = 0;
gdjs.TutorialCode.GDFireDemonObjects1.length = 0;
gdjs.TutorialCode.GDFireDemonObjects2.length = 0;
gdjs.TutorialCode.GDFireDemonObjects3.length = 0;
gdjs.TutorialCode.GDFireDemonObjects4.length = 0;
gdjs.TutorialCode.GDFireDemonObjects5.length = 0;
gdjs.TutorialCode.GDFireDemonObjects6.length = 0;
gdjs.TutorialCode.GDCheckpointObjects1.length = 0;
gdjs.TutorialCode.GDCheckpointObjects2.length = 0;
gdjs.TutorialCode.GDCheckpointObjects3.length = 0;
gdjs.TutorialCode.GDCheckpointObjects4.length = 0;
gdjs.TutorialCode.GDCheckpointObjects5.length = 0;
gdjs.TutorialCode.GDCheckpointObjects6.length = 0;
gdjs.TutorialCode.GDStaticPlatform3Objects1.length = 0;
gdjs.TutorialCode.GDStaticPlatform3Objects2.length = 0;
gdjs.TutorialCode.GDStaticPlatform3Objects3.length = 0;
gdjs.TutorialCode.GDStaticPlatform3Objects4.length = 0;
gdjs.TutorialCode.GDStaticPlatform3Objects5.length = 0;
gdjs.TutorialCode.GDStaticPlatform3Objects6.length = 0;
gdjs.TutorialCode.GDStaticPlatform2Objects1.length = 0;
gdjs.TutorialCode.GDStaticPlatform2Objects2.length = 0;
gdjs.TutorialCode.GDStaticPlatform2Objects3.length = 0;
gdjs.TutorialCode.GDStaticPlatform2Objects4.length = 0;
gdjs.TutorialCode.GDStaticPlatform2Objects5.length = 0;
gdjs.TutorialCode.GDStaticPlatform2Objects6.length = 0;
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects1.length = 0;
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects2.length = 0;
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects3.length = 0;
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4.length = 0;
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects5.length = 0;
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects6.length = 0;
gdjs.TutorialCode.GDStaticPlatform1Objects1.length = 0;
gdjs.TutorialCode.GDStaticPlatform1Objects2.length = 0;
gdjs.TutorialCode.GDStaticPlatform1Objects3.length = 0;
gdjs.TutorialCode.GDStaticPlatform1Objects4.length = 0;
gdjs.TutorialCode.GDStaticPlatform1Objects5.length = 0;
gdjs.TutorialCode.GDStaticPlatform1Objects6.length = 0;
gdjs.TutorialCode.GDPortalObjects1.length = 0;
gdjs.TutorialCode.GDPortalObjects2.length = 0;
gdjs.TutorialCode.GDPortalObjects3.length = 0;
gdjs.TutorialCode.GDPortalObjects4.length = 0;
gdjs.TutorialCode.GDPortalObjects5.length = 0;
gdjs.TutorialCode.GDPortalObjects6.length = 0;
gdjs.TutorialCode.GDLadderObjects1.length = 0;
gdjs.TutorialCode.GDLadderObjects2.length = 0;
gdjs.TutorialCode.GDLadderObjects3.length = 0;
gdjs.TutorialCode.GDLadderObjects4.length = 0;
gdjs.TutorialCode.GDLadderObjects5.length = 0;
gdjs.TutorialCode.GDLadderObjects6.length = 0;
gdjs.TutorialCode.GDMonsterParticlesObjects1.length = 0;
gdjs.TutorialCode.GDMonsterParticlesObjects2.length = 0;
gdjs.TutorialCode.GDMonsterParticlesObjects3.length = 0;
gdjs.TutorialCode.GDMonsterParticlesObjects4.length = 0;
gdjs.TutorialCode.GDMonsterParticlesObjects5.length = 0;
gdjs.TutorialCode.GDMonsterParticlesObjects6.length = 0;
gdjs.TutorialCode.GDSpikeParticlesObjects1.length = 0;
gdjs.TutorialCode.GDSpikeParticlesObjects2.length = 0;
gdjs.TutorialCode.GDSpikeParticlesObjects3.length = 0;
gdjs.TutorialCode.GDSpikeParticlesObjects4.length = 0;
gdjs.TutorialCode.GDSpikeParticlesObjects5.length = 0;
gdjs.TutorialCode.GDSpikeParticlesObjects6.length = 0;
gdjs.TutorialCode.GDDoorParticlesObjects1.length = 0;
gdjs.TutorialCode.GDDoorParticlesObjects2.length = 0;
gdjs.TutorialCode.GDDoorParticlesObjects3.length = 0;
gdjs.TutorialCode.GDDoorParticlesObjects4.length = 0;
gdjs.TutorialCode.GDDoorParticlesObjects5.length = 0;
gdjs.TutorialCode.GDDoorParticlesObjects6.length = 0;
gdjs.TutorialCode.GDDustParticleObjects1.length = 0;
gdjs.TutorialCode.GDDustParticleObjects2.length = 0;
gdjs.TutorialCode.GDDustParticleObjects3.length = 0;
gdjs.TutorialCode.GDDustParticleObjects4.length = 0;
gdjs.TutorialCode.GDDustParticleObjects5.length = 0;
gdjs.TutorialCode.GDDustParticleObjects6.length = 0;
gdjs.TutorialCode.GDLivesBarObjects1.length = 0;
gdjs.TutorialCode.GDLivesBarObjects2.length = 0;
gdjs.TutorialCode.GDLivesBarObjects3.length = 0;
gdjs.TutorialCode.GDLivesBarObjects4.length = 0;
gdjs.TutorialCode.GDLivesBarObjects5.length = 0;
gdjs.TutorialCode.GDLivesBarObjects6.length = 0;
gdjs.TutorialCode.GDHopeBarObjects1.length = 0;
gdjs.TutorialCode.GDHopeBarObjects2.length = 0;
gdjs.TutorialCode.GDHopeBarObjects3.length = 0;
gdjs.TutorialCode.GDHopeBarObjects4.length = 0;
gdjs.TutorialCode.GDHopeBarObjects5.length = 0;
gdjs.TutorialCode.GDHopeBarObjects6.length = 0;
gdjs.TutorialCode.GDMemoryObjects1.length = 0;
gdjs.TutorialCode.GDMemoryObjects2.length = 0;
gdjs.TutorialCode.GDMemoryObjects3.length = 0;
gdjs.TutorialCode.GDMemoryObjects4.length = 0;
gdjs.TutorialCode.GDMemoryObjects5.length = 0;
gdjs.TutorialCode.GDMemoryObjects6.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects1.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects2.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects3.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects4.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects5.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects6.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects1.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects2.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects4.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects5.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects6.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects1.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects2.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects3.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects4.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects5.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects6.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595HopeObjects1.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595HopeObjects2.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595HopeObjects3.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595HopeObjects4.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595HopeObjects5.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595HopeObjects6.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects1.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects3.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects4.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects5.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects6.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects1.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects2.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects3.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects4.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects5.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects6.length = 0;
gdjs.TutorialCode.GDHorizontalDemonObjects1.length = 0;
gdjs.TutorialCode.GDHorizontalDemonObjects2.length = 0;
gdjs.TutorialCode.GDHorizontalDemonObjects3.length = 0;
gdjs.TutorialCode.GDHorizontalDemonObjects4.length = 0;
gdjs.TutorialCode.GDHorizontalDemonObjects5.length = 0;
gdjs.TutorialCode.GDHorizontalDemonObjects6.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects1.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects2.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects6.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects1.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects2.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects4.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects5.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects6.length = 0;
gdjs.TutorialCode.GDVerticalMovingPlatformObjects1.length = 0;
gdjs.TutorialCode.GDVerticalMovingPlatformObjects2.length = 0;
gdjs.TutorialCode.GDVerticalMovingPlatformObjects3.length = 0;
gdjs.TutorialCode.GDVerticalMovingPlatformObjects4.length = 0;
gdjs.TutorialCode.GDVerticalMovingPlatformObjects5.length = 0;
gdjs.TutorialCode.GDVerticalMovingPlatformObjects6.length = 0;
gdjs.TutorialCode.GDSpinningMovingPlatformObjects1.length = 0;
gdjs.TutorialCode.GDSpinningMovingPlatformObjects2.length = 0;
gdjs.TutorialCode.GDSpinningMovingPlatformObjects3.length = 0;
gdjs.TutorialCode.GDSpinningMovingPlatformObjects4.length = 0;
gdjs.TutorialCode.GDSpinningMovingPlatformObjects5.length = 0;
gdjs.TutorialCode.GDSpinningMovingPlatformObjects6.length = 0;
gdjs.TutorialCode.GDFlippingPlatformObjects1.length = 0;
gdjs.TutorialCode.GDFlippingPlatformObjects2.length = 0;
gdjs.TutorialCode.GDFlippingPlatformObjects3.length = 0;
gdjs.TutorialCode.GDFlippingPlatformObjects4.length = 0;
gdjs.TutorialCode.GDFlippingPlatformObjects5.length = 0;
gdjs.TutorialCode.GDFlippingPlatformObjects6.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects1.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects2.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects5.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects6.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects1.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects2.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects5.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects6.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects1.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects2.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects3.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects4.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects5.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595Base2Objects6.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects1.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects2.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects3.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects4.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects5.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595Spike2Objects6.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects1.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects2.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects3.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects4.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects5.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595Base2Objects6.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects1.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects2.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects3.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects4.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects5.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595BaseObjects6.length = 0;
gdjs.TutorialCode.GDFlippingPlatform2Objects1.length = 0;
gdjs.TutorialCode.GDFlippingPlatform2Objects2.length = 0;
gdjs.TutorialCode.GDFlippingPlatform2Objects3.length = 0;
gdjs.TutorialCode.GDFlippingPlatform2Objects4.length = 0;
gdjs.TutorialCode.GDFlippingPlatform2Objects5.length = 0;
gdjs.TutorialCode.GDFlippingPlatform2Objects6.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects1.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects2.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects3.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects4.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects5.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595Base2Objects6.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects1.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects2.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects3.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects4.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects5.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595Spike2Objects6.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects1.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects2.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects3.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects4.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects5.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595SpikeObjects6.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects1.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects2.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects3.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects4.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects5.length = 0;
gdjs.TutorialCode.GDStalagmiteDemon_9595Spike2Objects6.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects1.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects2.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects3.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects4.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects5.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595ProceedButtonObjects6.length = 0;
gdjs.TutorialCode.GDLaserDemon_9595BeamObjects1.length = 0;
gdjs.TutorialCode.GDLaserDemon_9595BeamObjects2.length = 0;
gdjs.TutorialCode.GDLaserDemon_9595BeamObjects3.length = 0;
gdjs.TutorialCode.GDLaserDemon_9595BeamObjects4.length = 0;
gdjs.TutorialCode.GDLaserDemon_9595BeamObjects5.length = 0;
gdjs.TutorialCode.GDLaserDemon_9595BeamObjects6.length = 0;
gdjs.TutorialCode.GDLaserDemon_9595BaseObjects1.length = 0;
gdjs.TutorialCode.GDLaserDemon_9595BaseObjects2.length = 0;
gdjs.TutorialCode.GDLaserDemon_9595BaseObjects3.length = 0;
gdjs.TutorialCode.GDLaserDemon_9595BaseObjects4.length = 0;
gdjs.TutorialCode.GDLaserDemon_9595BaseObjects5.length = 0;
gdjs.TutorialCode.GDLaserDemon_9595BaseObjects6.length = 0;
gdjs.TutorialCode.GDTestObjects1.length = 0;
gdjs.TutorialCode.GDTestObjects2.length = 0;
gdjs.TutorialCode.GDTestObjects3.length = 0;
gdjs.TutorialCode.GDTestObjects4.length = 0;
gdjs.TutorialCode.GDTestObjects5.length = 0;
gdjs.TutorialCode.GDTestObjects6.length = 0;

gdjs.TutorialCode.eventsList109(runtimeScene);

return;

}

gdjs['TutorialCode'] = gdjs.TutorialCode;
