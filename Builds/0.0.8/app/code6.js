gdjs.Level4Code = {};
gdjs.Level4Code.GDFlippingPlatform2Objects3_1final = [];

gdjs.Level4Code.GDFlippingPlatformObjects3_1final = [];

gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3_1final = [];

gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3_1final = [];

gdjs.Level4Code.GDLadderObjects3_1final = [];

gdjs.Level4Code.GDMiteDemon_9595BaseObjects3_1final = [];

gdjs.Level4Code.GDMiteDemon_9595MiteObjects3_1final = [];

gdjs.Level4Code.GDPlayerObjects3_1final = [];

gdjs.Level4Code.GDPlayerObjects5_1final = [];

gdjs.Level4Code.GDPortalObjects3_1final = [];

gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3_1final = [];

gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3_1final = [];

gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5_1final = [];

gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3_1final = [];

gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3_1final = [];

gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3_1final = [];

gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3_1final = [];

gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3_1final = [];

gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3_1final = [];

gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3_1final = [];

gdjs.Level4Code.GDStaticPlatform1Objects3_1final = [];

gdjs.Level4Code.GDStaticPlatform2Objects3_1final = [];

gdjs.Level4Code.GDStaticPlatform3Objects3_1final = [];

gdjs.Level4Code.GDTurretDemon_9595EyeObjects3_1final = [];

gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final = [];

gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3_1final = [];

gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3_1final = [];

gdjs.Level4Code.forEachCount0_4 = 0;

gdjs.Level4Code.forEachCount1_4 = 0;

gdjs.Level4Code.forEachCount2_4 = 0;

gdjs.Level4Code.forEachCount3_4 = 0;

gdjs.Level4Code.forEachIndex4 = 0;

gdjs.Level4Code.forEachObjects4 = [];

gdjs.Level4Code.forEachTemporary4 = null;

gdjs.Level4Code.forEachTotalCount4 = 0;

gdjs.Level4Code.GDBackgroundPlantsObjects1= [];
gdjs.Level4Code.GDBackgroundPlantsObjects2= [];
gdjs.Level4Code.GDBackgroundPlantsObjects3= [];
gdjs.Level4Code.GDBackgroundPlantsObjects4= [];
gdjs.Level4Code.GDBackgroundPlantsObjects5= [];
gdjs.Level4Code.GDBackgroundPlantsObjects6= [];
gdjs.Level4Code.GDBackgroundPlantsObjects7= [];
gdjs.Level4Code.GDBackgroundPlantsObjects8= [];
gdjs.Level4Code.GDPlayerObjects1= [];
gdjs.Level4Code.GDPlayerObjects2= [];
gdjs.Level4Code.GDPlayerObjects3= [];
gdjs.Level4Code.GDPlayerObjects4= [];
gdjs.Level4Code.GDPlayerObjects5= [];
gdjs.Level4Code.GDPlayerObjects6= [];
gdjs.Level4Code.GDPlayerObjects7= [];
gdjs.Level4Code.GDPlayerObjects8= [];
gdjs.Level4Code.GDFlyingDemonObjects1= [];
gdjs.Level4Code.GDFlyingDemonObjects2= [];
gdjs.Level4Code.GDFlyingDemonObjects3= [];
gdjs.Level4Code.GDFlyingDemonObjects4= [];
gdjs.Level4Code.GDFlyingDemonObjects5= [];
gdjs.Level4Code.GDFlyingDemonObjects6= [];
gdjs.Level4Code.GDFlyingDemonObjects7= [];
gdjs.Level4Code.GDFlyingDemonObjects8= [];
gdjs.Level4Code.GDFireDemonObjects1= [];
gdjs.Level4Code.GDFireDemonObjects2= [];
gdjs.Level4Code.GDFireDemonObjects3= [];
gdjs.Level4Code.GDFireDemonObjects4= [];
gdjs.Level4Code.GDFireDemonObjects5= [];
gdjs.Level4Code.GDFireDemonObjects6= [];
gdjs.Level4Code.GDFireDemonObjects7= [];
gdjs.Level4Code.GDFireDemonObjects8= [];
gdjs.Level4Code.GDCheckpointObjects1= [];
gdjs.Level4Code.GDCheckpointObjects2= [];
gdjs.Level4Code.GDCheckpointObjects3= [];
gdjs.Level4Code.GDCheckpointObjects4= [];
gdjs.Level4Code.GDCheckpointObjects5= [];
gdjs.Level4Code.GDCheckpointObjects6= [];
gdjs.Level4Code.GDCheckpointObjects7= [];
gdjs.Level4Code.GDCheckpointObjects8= [];
gdjs.Level4Code.GDStaticPlatform3Objects1= [];
gdjs.Level4Code.GDStaticPlatform3Objects2= [];
gdjs.Level4Code.GDStaticPlatform3Objects3= [];
gdjs.Level4Code.GDStaticPlatform3Objects4= [];
gdjs.Level4Code.GDStaticPlatform3Objects5= [];
gdjs.Level4Code.GDStaticPlatform3Objects6= [];
gdjs.Level4Code.GDStaticPlatform3Objects7= [];
gdjs.Level4Code.GDStaticPlatform3Objects8= [];
gdjs.Level4Code.GDStaticPlatform2Objects1= [];
gdjs.Level4Code.GDStaticPlatform2Objects2= [];
gdjs.Level4Code.GDStaticPlatform2Objects3= [];
gdjs.Level4Code.GDStaticPlatform2Objects4= [];
gdjs.Level4Code.GDStaticPlatform2Objects5= [];
gdjs.Level4Code.GDStaticPlatform2Objects6= [];
gdjs.Level4Code.GDStaticPlatform2Objects7= [];
gdjs.Level4Code.GDStaticPlatform2Objects8= [];
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects1= [];
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects2= [];
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3= [];
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects4= [];
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects5= [];
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects6= [];
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects7= [];
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects8= [];
gdjs.Level4Code.GDStaticPlatform1Objects1= [];
gdjs.Level4Code.GDStaticPlatform1Objects2= [];
gdjs.Level4Code.GDStaticPlatform1Objects3= [];
gdjs.Level4Code.GDStaticPlatform1Objects4= [];
gdjs.Level4Code.GDStaticPlatform1Objects5= [];
gdjs.Level4Code.GDStaticPlatform1Objects6= [];
gdjs.Level4Code.GDStaticPlatform1Objects7= [];
gdjs.Level4Code.GDStaticPlatform1Objects8= [];
gdjs.Level4Code.GDPortalObjects1= [];
gdjs.Level4Code.GDPortalObjects2= [];
gdjs.Level4Code.GDPortalObjects3= [];
gdjs.Level4Code.GDPortalObjects4= [];
gdjs.Level4Code.GDPortalObjects5= [];
gdjs.Level4Code.GDPortalObjects6= [];
gdjs.Level4Code.GDPortalObjects7= [];
gdjs.Level4Code.GDPortalObjects8= [];
gdjs.Level4Code.GDLadderObjects1= [];
gdjs.Level4Code.GDLadderObjects2= [];
gdjs.Level4Code.GDLadderObjects3= [];
gdjs.Level4Code.GDLadderObjects4= [];
gdjs.Level4Code.GDLadderObjects5= [];
gdjs.Level4Code.GDLadderObjects6= [];
gdjs.Level4Code.GDLadderObjects7= [];
gdjs.Level4Code.GDLadderObjects8= [];
gdjs.Level4Code.GDBloodParticlesObjects1= [];
gdjs.Level4Code.GDBloodParticlesObjects2= [];
gdjs.Level4Code.GDBloodParticlesObjects3= [];
gdjs.Level4Code.GDBloodParticlesObjects4= [];
gdjs.Level4Code.GDBloodParticlesObjects5= [];
gdjs.Level4Code.GDBloodParticlesObjects6= [];
gdjs.Level4Code.GDBloodParticlesObjects7= [];
gdjs.Level4Code.GDBloodParticlesObjects8= [];
gdjs.Level4Code.GDProjectileDeathParticlesObjects1= [];
gdjs.Level4Code.GDProjectileDeathParticlesObjects2= [];
gdjs.Level4Code.GDProjectileDeathParticlesObjects3= [];
gdjs.Level4Code.GDProjectileDeathParticlesObjects4= [];
gdjs.Level4Code.GDProjectileDeathParticlesObjects5= [];
gdjs.Level4Code.GDProjectileDeathParticlesObjects6= [];
gdjs.Level4Code.GDProjectileDeathParticlesObjects7= [];
gdjs.Level4Code.GDProjectileDeathParticlesObjects8= [];
gdjs.Level4Code.GDDoorParticlesObjects1= [];
gdjs.Level4Code.GDDoorParticlesObjects2= [];
gdjs.Level4Code.GDDoorParticlesObjects3= [];
gdjs.Level4Code.GDDoorParticlesObjects4= [];
gdjs.Level4Code.GDDoorParticlesObjects5= [];
gdjs.Level4Code.GDDoorParticlesObjects6= [];
gdjs.Level4Code.GDDoorParticlesObjects7= [];
gdjs.Level4Code.GDDoorParticlesObjects8= [];
gdjs.Level4Code.GDDustParticleObjects1= [];
gdjs.Level4Code.GDDustParticleObjects2= [];
gdjs.Level4Code.GDDustParticleObjects3= [];
gdjs.Level4Code.GDDustParticleObjects4= [];
gdjs.Level4Code.GDDustParticleObjects5= [];
gdjs.Level4Code.GDDustParticleObjects6= [];
gdjs.Level4Code.GDDustParticleObjects7= [];
gdjs.Level4Code.GDDustParticleObjects8= [];
gdjs.Level4Code.GDLivesBarObjects1= [];
gdjs.Level4Code.GDLivesBarObjects2= [];
gdjs.Level4Code.GDLivesBarObjects3= [];
gdjs.Level4Code.GDLivesBarObjects4= [];
gdjs.Level4Code.GDLivesBarObjects5= [];
gdjs.Level4Code.GDLivesBarObjects6= [];
gdjs.Level4Code.GDLivesBarObjects7= [];
gdjs.Level4Code.GDLivesBarObjects8= [];
gdjs.Level4Code.GDHopeBarObjects1= [];
gdjs.Level4Code.GDHopeBarObjects2= [];
gdjs.Level4Code.GDHopeBarObjects3= [];
gdjs.Level4Code.GDHopeBarObjects4= [];
gdjs.Level4Code.GDHopeBarObjects5= [];
gdjs.Level4Code.GDHopeBarObjects6= [];
gdjs.Level4Code.GDHopeBarObjects7= [];
gdjs.Level4Code.GDHopeBarObjects8= [];
gdjs.Level4Code.GDMemoryObjects1= [];
gdjs.Level4Code.GDMemoryObjects2= [];
gdjs.Level4Code.GDMemoryObjects3= [];
gdjs.Level4Code.GDMemoryObjects4= [];
gdjs.Level4Code.GDMemoryObjects5= [];
gdjs.Level4Code.GDMemoryObjects6= [];
gdjs.Level4Code.GDMemoryObjects7= [];
gdjs.Level4Code.GDMemoryObjects8= [];
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects1= [];
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects2= [];
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects3= [];
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects4= [];
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects5= [];
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects6= [];
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects7= [];
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects8= [];
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects1= [];
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects2= [];
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3= [];
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects4= [];
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects5= [];
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects6= [];
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects7= [];
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects8= [];
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects1= [];
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects2= [];
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects3= [];
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects4= [];
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects5= [];
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects6= [];
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects7= [];
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects8= [];
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects1= [];
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects2= [];
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects3= [];
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects4= [];
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects5= [];
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects6= [];
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects7= [];
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects8= [];
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects1= [];
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects2= [];
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects3= [];
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects4= [];
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects5= [];
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects6= [];
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects7= [];
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects8= [];
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects1= [];
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects2= [];
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects3= [];
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects4= [];
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects5= [];
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects6= [];
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects7= [];
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects8= [];
gdjs.Level4Code.GDHorizontalDemonObjects1= [];
gdjs.Level4Code.GDHorizontalDemonObjects2= [];
gdjs.Level4Code.GDHorizontalDemonObjects3= [];
gdjs.Level4Code.GDHorizontalDemonObjects4= [];
gdjs.Level4Code.GDHorizontalDemonObjects5= [];
gdjs.Level4Code.GDHorizontalDemonObjects6= [];
gdjs.Level4Code.GDHorizontalDemonObjects7= [];
gdjs.Level4Code.GDHorizontalDemonObjects8= [];
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects1= [];
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects2= [];
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3= [];
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4= [];
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5= [];
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects6= [];
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects7= [];
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects8= [];
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects1= [];
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects2= [];
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects3= [];
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4= [];
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5= [];
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects6= [];
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects7= [];
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects8= [];
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects1= [];
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects2= [];
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3= [];
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects4= [];
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects5= [];
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects6= [];
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects7= [];
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects8= [];
gdjs.Level4Code.GDFlippingPlatformObjects1= [];
gdjs.Level4Code.GDFlippingPlatformObjects2= [];
gdjs.Level4Code.GDFlippingPlatformObjects3= [];
gdjs.Level4Code.GDFlippingPlatformObjects4= [];
gdjs.Level4Code.GDFlippingPlatformObjects5= [];
gdjs.Level4Code.GDFlippingPlatformObjects6= [];
gdjs.Level4Code.GDFlippingPlatformObjects7= [];
gdjs.Level4Code.GDFlippingPlatformObjects8= [];
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects1= [];
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects2= [];
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3= [];
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4= [];
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5= [];
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects6= [];
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects7= [];
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects8= [];
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects1= [];
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects2= [];
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects3= [];
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4= [];
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5= [];
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects6= [];
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects7= [];
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects8= [];
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects1= [];
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects2= [];
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3= [];
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4= [];
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5= [];
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects6= [];
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects7= [];
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects8= [];
gdjs.Level4Code.GDFlippingPlatform2Objects1= [];
gdjs.Level4Code.GDFlippingPlatform2Objects2= [];
gdjs.Level4Code.GDFlippingPlatform2Objects3= [];
gdjs.Level4Code.GDFlippingPlatform2Objects4= [];
gdjs.Level4Code.GDFlippingPlatform2Objects5= [];
gdjs.Level4Code.GDFlippingPlatform2Objects6= [];
gdjs.Level4Code.GDFlippingPlatform2Objects7= [];
gdjs.Level4Code.GDFlippingPlatform2Objects8= [];
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects1= [];
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects2= [];
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects3= [];
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects4= [];
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects5= [];
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects6= [];
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects7= [];
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects8= [];
gdjs.Level4Code.GDLaserDemon_9595BeamObjects1= [];
gdjs.Level4Code.GDLaserDemon_9595BeamObjects2= [];
gdjs.Level4Code.GDLaserDemon_9595BeamObjects3= [];
gdjs.Level4Code.GDLaserDemon_9595BeamObjects4= [];
gdjs.Level4Code.GDLaserDemon_9595BeamObjects5= [];
gdjs.Level4Code.GDLaserDemon_9595BeamObjects6= [];
gdjs.Level4Code.GDLaserDemon_9595BeamObjects7= [];
gdjs.Level4Code.GDLaserDemon_9595BeamObjects8= [];
gdjs.Level4Code.GDLaserDemon_9595BaseObjects1= [];
gdjs.Level4Code.GDLaserDemon_9595BaseObjects2= [];
gdjs.Level4Code.GDLaserDemon_9595BaseObjects3= [];
gdjs.Level4Code.GDLaserDemon_9595BaseObjects4= [];
gdjs.Level4Code.GDLaserDemon_9595BaseObjects5= [];
gdjs.Level4Code.GDLaserDemon_9595BaseObjects6= [];
gdjs.Level4Code.GDLaserDemon_9595BaseObjects7= [];
gdjs.Level4Code.GDLaserDemon_9595BaseObjects8= [];
gdjs.Level4Code.GDTestObjects1= [];
gdjs.Level4Code.GDTestObjects2= [];
gdjs.Level4Code.GDTestObjects3= [];
gdjs.Level4Code.GDTestObjects4= [];
gdjs.Level4Code.GDTestObjects5= [];
gdjs.Level4Code.GDTestObjects6= [];
gdjs.Level4Code.GDTestObjects7= [];
gdjs.Level4Code.GDTestObjects8= [];
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects1= [];
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects2= [];
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3= [];
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects4= [];
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects5= [];
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects6= [];
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects7= [];
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects8= [];
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects1= [];
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects2= [];
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3= [];
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects4= [];
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects5= [];
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects6= [];
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects7= [];
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects8= [];
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects1= [];
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects2= [];
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3= [];
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects4= [];
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects5= [];
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects6= [];
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects7= [];
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects8= [];
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects1= [];
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects2= [];
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3= [];
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects4= [];
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects5= [];
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects6= [];
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects7= [];
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects8= [];
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects1= [];
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects2= [];
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3= [];
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects4= [];
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects5= [];
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects6= [];
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects7= [];
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects8= [];
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects1= [];
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects2= [];
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3= [];
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects4= [];
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects5= [];
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects6= [];
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects7= [];
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects8= [];
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects1= [];
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects2= [];
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects3= [];
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4= [];
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5= [];
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects6= [];
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects7= [];
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects8= [];
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects1= [];
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects2= [];
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects3= [];
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4= [];
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects5= [];
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects6= [];
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects7= [];
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects8= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects1= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects2= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects3= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects5= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects6= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects7= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects8= [];
gdjs.Level4Code.GDLaserRingObjects1= [];
gdjs.Level4Code.GDLaserRingObjects2= [];
gdjs.Level4Code.GDLaserRingObjects3= [];
gdjs.Level4Code.GDLaserRingObjects4= [];
gdjs.Level4Code.GDLaserRingObjects5= [];
gdjs.Level4Code.GDLaserRingObjects6= [];
gdjs.Level4Code.GDLaserRingObjects7= [];
gdjs.Level4Code.GDLaserRingObjects8= [];
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects1= [];
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects2= [];
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3= [];
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4= [];
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects5= [];
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects6= [];
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects7= [];
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects8= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects1= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects2= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects3= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects5= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects6= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects7= [];
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects8= [];
gdjs.Level4Code.GDTurretDemon_9595BaseObjects1= [];
gdjs.Level4Code.GDTurretDemon_9595BaseObjects2= [];
gdjs.Level4Code.GDTurretDemon_9595BaseObjects3= [];
gdjs.Level4Code.GDTurretDemon_9595BaseObjects4= [];
gdjs.Level4Code.GDTurretDemon_9595BaseObjects5= [];
gdjs.Level4Code.GDTurretDemon_9595BaseObjects6= [];
gdjs.Level4Code.GDTurretDemon_9595BaseObjects7= [];
gdjs.Level4Code.GDTurretDemon_9595BaseObjects8= [];
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects1= [];
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects2= [];
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects3= [];
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4= [];
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5= [];
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects6= [];
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects7= [];
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects8= [];
gdjs.Level4Code.GDTurretDemon_9595EyeObjects1= [];
gdjs.Level4Code.GDTurretDemon_9595EyeObjects2= [];
gdjs.Level4Code.GDTurretDemon_9595EyeObjects3= [];
gdjs.Level4Code.GDTurretDemon_9595EyeObjects4= [];
gdjs.Level4Code.GDTurretDemon_9595EyeObjects5= [];
gdjs.Level4Code.GDTurretDemon_9595EyeObjects6= [];
gdjs.Level4Code.GDTurretDemon_9595EyeObjects7= [];
gdjs.Level4Code.GDTurretDemon_9595EyeObjects8= [];
gdjs.Level4Code.GDCameraGlitchFixObjects1= [];
gdjs.Level4Code.GDCameraGlitchFixObjects2= [];
gdjs.Level4Code.GDCameraGlitchFixObjects3= [];
gdjs.Level4Code.GDCameraGlitchFixObjects4= [];
gdjs.Level4Code.GDCameraGlitchFixObjects5= [];
gdjs.Level4Code.GDCameraGlitchFixObjects6= [];
gdjs.Level4Code.GDCameraGlitchFixObjects7= [];
gdjs.Level4Code.GDCameraGlitchFixObjects8= [];
gdjs.Level4Code.GDLeftBoundaryObjects1= [];
gdjs.Level4Code.GDLeftBoundaryObjects2= [];
gdjs.Level4Code.GDLeftBoundaryObjects3= [];
gdjs.Level4Code.GDLeftBoundaryObjects4= [];
gdjs.Level4Code.GDLeftBoundaryObjects5= [];
gdjs.Level4Code.GDLeftBoundaryObjects6= [];
gdjs.Level4Code.GDLeftBoundaryObjects7= [];
gdjs.Level4Code.GDLeftBoundaryObjects8= [];
gdjs.Level4Code.GDRightBoundaryObjects1= [];
gdjs.Level4Code.GDRightBoundaryObjects2= [];
gdjs.Level4Code.GDRightBoundaryObjects3= [];
gdjs.Level4Code.GDRightBoundaryObjects4= [];
gdjs.Level4Code.GDRightBoundaryObjects5= [];
gdjs.Level4Code.GDRightBoundaryObjects6= [];
gdjs.Level4Code.GDRightBoundaryObjects7= [];
gdjs.Level4Code.GDRightBoundaryObjects8= [];
gdjs.Level4Code.GDTopBoundaryObjects1= [];
gdjs.Level4Code.GDTopBoundaryObjects2= [];
gdjs.Level4Code.GDTopBoundaryObjects3= [];
gdjs.Level4Code.GDTopBoundaryObjects4= [];
gdjs.Level4Code.GDTopBoundaryObjects5= [];
gdjs.Level4Code.GDTopBoundaryObjects6= [];
gdjs.Level4Code.GDTopBoundaryObjects7= [];
gdjs.Level4Code.GDTopBoundaryObjects8= [];
gdjs.Level4Code.GDBottomBoundaryObjects1= [];
gdjs.Level4Code.GDBottomBoundaryObjects2= [];
gdjs.Level4Code.GDBottomBoundaryObjects3= [];
gdjs.Level4Code.GDBottomBoundaryObjects4= [];
gdjs.Level4Code.GDBottomBoundaryObjects5= [];
gdjs.Level4Code.GDBottomBoundaryObjects6= [];
gdjs.Level4Code.GDBottomBoundaryObjects7= [];
gdjs.Level4Code.GDBottomBoundaryObjects8= [];
gdjs.Level4Code.GDBoundaryJumpThroughObjects1= [];
gdjs.Level4Code.GDBoundaryJumpThroughObjects2= [];
gdjs.Level4Code.GDBoundaryJumpThroughObjects3= [];
gdjs.Level4Code.GDBoundaryJumpThroughObjects4= [];
gdjs.Level4Code.GDBoundaryJumpThroughObjects5= [];
gdjs.Level4Code.GDBoundaryJumpThroughObjects6= [];
gdjs.Level4Code.GDBoundaryJumpThroughObjects7= [];
gdjs.Level4Code.GDBoundaryJumpThroughObjects8= [];
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects1= [];
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects2= [];
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3= [];
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4= [];
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5= [];
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects6= [];
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects7= [];
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects8= [];
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects1= [];
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects2= [];
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3= [];
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4= [];
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5= [];
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects6= [];
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects7= [];
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects8= [];
gdjs.Level4Code.GDMiteDemon_9595BaseObjects1= [];
gdjs.Level4Code.GDMiteDemon_9595BaseObjects2= [];
gdjs.Level4Code.GDMiteDemon_9595BaseObjects3= [];
gdjs.Level4Code.GDMiteDemon_9595BaseObjects4= [];
gdjs.Level4Code.GDMiteDemon_9595BaseObjects5= [];
gdjs.Level4Code.GDMiteDemon_9595BaseObjects6= [];
gdjs.Level4Code.GDMiteDemon_9595BaseObjects7= [];
gdjs.Level4Code.GDMiteDemon_9595BaseObjects8= [];
gdjs.Level4Code.GDMiteDemon_9595MiteObjects1= [];
gdjs.Level4Code.GDMiteDemon_9595MiteObjects2= [];
gdjs.Level4Code.GDMiteDemon_9595MiteObjects3= [];
gdjs.Level4Code.GDMiteDemon_9595MiteObjects4= [];
gdjs.Level4Code.GDMiteDemon_9595MiteObjects5= [];
gdjs.Level4Code.GDMiteDemon_9595MiteObjects6= [];
gdjs.Level4Code.GDMiteDemon_9595MiteObjects7= [];
gdjs.Level4Code.GDMiteDemon_9595MiteObjects8= [];
gdjs.Level4Code.GDSinage_9595WObjects1= [];
gdjs.Level4Code.GDSinage_9595WObjects2= [];
gdjs.Level4Code.GDSinage_9595WObjects3= [];
gdjs.Level4Code.GDSinage_9595WObjects4= [];
gdjs.Level4Code.GDSinage_9595WObjects5= [];
gdjs.Level4Code.GDSinage_9595WObjects6= [];
gdjs.Level4Code.GDSinage_9595WObjects7= [];
gdjs.Level4Code.GDSinage_9595WObjects8= [];
gdjs.Level4Code.GDSinage_9595AObjects1= [];
gdjs.Level4Code.GDSinage_9595AObjects2= [];
gdjs.Level4Code.GDSinage_9595AObjects3= [];
gdjs.Level4Code.GDSinage_9595AObjects4= [];
gdjs.Level4Code.GDSinage_9595AObjects5= [];
gdjs.Level4Code.GDSinage_9595AObjects6= [];
gdjs.Level4Code.GDSinage_9595AObjects7= [];
gdjs.Level4Code.GDSinage_9595AObjects8= [];
gdjs.Level4Code.GDSinage_9595SObjects1= [];
gdjs.Level4Code.GDSinage_9595SObjects2= [];
gdjs.Level4Code.GDSinage_9595SObjects3= [];
gdjs.Level4Code.GDSinage_9595SObjects4= [];
gdjs.Level4Code.GDSinage_9595SObjects5= [];
gdjs.Level4Code.GDSinage_9595SObjects6= [];
gdjs.Level4Code.GDSinage_9595SObjects7= [];
gdjs.Level4Code.GDSinage_9595SObjects8= [];
gdjs.Level4Code.GDSinage_9595DObjects1= [];
gdjs.Level4Code.GDSinage_9595DObjects2= [];
gdjs.Level4Code.GDSinage_9595DObjects3= [];
gdjs.Level4Code.GDSinage_9595DObjects4= [];
gdjs.Level4Code.GDSinage_9595DObjects5= [];
gdjs.Level4Code.GDSinage_9595DObjects6= [];
gdjs.Level4Code.GDSinage_9595DObjects7= [];
gdjs.Level4Code.GDSinage_9595DObjects8= [];
gdjs.Level4Code.GDSinage_9595SpaceObjects1= [];
gdjs.Level4Code.GDSinage_9595SpaceObjects2= [];
gdjs.Level4Code.GDSinage_9595SpaceObjects3= [];
gdjs.Level4Code.GDSinage_9595SpaceObjects4= [];
gdjs.Level4Code.GDSinage_9595SpaceObjects5= [];
gdjs.Level4Code.GDSinage_9595SpaceObjects6= [];
gdjs.Level4Code.GDSinage_9595SpaceObjects7= [];
gdjs.Level4Code.GDSinage_9595SpaceObjects8= [];
gdjs.Level4Code.GDSinage_9595HealObjects1= [];
gdjs.Level4Code.GDSinage_9595HealObjects2= [];
gdjs.Level4Code.GDSinage_9595HealObjects3= [];
gdjs.Level4Code.GDSinage_9595HealObjects4= [];
gdjs.Level4Code.GDSinage_9595HealObjects5= [];
gdjs.Level4Code.GDSinage_9595HealObjects6= [];
gdjs.Level4Code.GDSinage_9595HealObjects7= [];
gdjs.Level4Code.GDSinage_9595HealObjects8= [];
gdjs.Level4Code.GDSinage_9595ProceedObjects1= [];
gdjs.Level4Code.GDSinage_9595ProceedObjects2= [];
gdjs.Level4Code.GDSinage_9595ProceedObjects3= [];
gdjs.Level4Code.GDSinage_9595ProceedObjects4= [];
gdjs.Level4Code.GDSinage_9595ProceedObjects5= [];
gdjs.Level4Code.GDSinage_9595ProceedObjects6= [];
gdjs.Level4Code.GDSinage_9595ProceedObjects7= [];
gdjs.Level4Code.GDSinage_9595ProceedObjects8= [];
gdjs.Level4Code.GDSinage_9595DeathObjects1= [];
gdjs.Level4Code.GDSinage_9595DeathObjects2= [];
gdjs.Level4Code.GDSinage_9595DeathObjects3= [];
gdjs.Level4Code.GDSinage_9595DeathObjects4= [];
gdjs.Level4Code.GDSinage_9595DeathObjects5= [];
gdjs.Level4Code.GDSinage_9595DeathObjects6= [];
gdjs.Level4Code.GDSinage_9595DeathObjects7= [];
gdjs.Level4Code.GDSinage_9595DeathObjects8= [];
gdjs.Level4Code.GDSinage_9595DownArrowObjects1= [];
gdjs.Level4Code.GDSinage_9595DownArrowObjects2= [];
gdjs.Level4Code.GDSinage_9595DownArrowObjects3= [];
gdjs.Level4Code.GDSinage_9595DownArrowObjects4= [];
gdjs.Level4Code.GDSinage_9595DownArrowObjects5= [];
gdjs.Level4Code.GDSinage_9595DownArrowObjects6= [];
gdjs.Level4Code.GDSinage_9595DownArrowObjects7= [];
gdjs.Level4Code.GDSinage_9595DownArrowObjects8= [];
gdjs.Level4Code.GDSinage_9595CheckpointObjects1= [];
gdjs.Level4Code.GDSinage_9595CheckpointObjects2= [];
gdjs.Level4Code.GDSinage_9595CheckpointObjects3= [];
gdjs.Level4Code.GDSinage_9595CheckpointObjects4= [];
gdjs.Level4Code.GDSinage_9595CheckpointObjects5= [];
gdjs.Level4Code.GDSinage_9595CheckpointObjects6= [];
gdjs.Level4Code.GDSinage_9595CheckpointObjects7= [];
gdjs.Level4Code.GDSinage_9595CheckpointObjects8= [];
gdjs.Level4Code.GDSinage_9595KillObjects1= [];
gdjs.Level4Code.GDSinage_9595KillObjects2= [];
gdjs.Level4Code.GDSinage_9595KillObjects3= [];
gdjs.Level4Code.GDSinage_9595KillObjects4= [];
gdjs.Level4Code.GDSinage_9595KillObjects5= [];
gdjs.Level4Code.GDSinage_9595KillObjects6= [];
gdjs.Level4Code.GDSinage_9595KillObjects7= [];
gdjs.Level4Code.GDSinage_9595KillObjects8= [];
gdjs.Level4Code.GDSinage_9595CollectObjects1= [];
gdjs.Level4Code.GDSinage_9595CollectObjects2= [];
gdjs.Level4Code.GDSinage_9595CollectObjects3= [];
gdjs.Level4Code.GDSinage_9595CollectObjects4= [];
gdjs.Level4Code.GDSinage_9595CollectObjects5= [];
gdjs.Level4Code.GDSinage_9595CollectObjects6= [];
gdjs.Level4Code.GDSinage_9595CollectObjects7= [];
gdjs.Level4Code.GDSinage_9595CollectObjects8= [];
gdjs.Level4Code.GDSinage_9595ArrowObjects1= [];
gdjs.Level4Code.GDSinage_9595ArrowObjects2= [];
gdjs.Level4Code.GDSinage_9595ArrowObjects3= [];
gdjs.Level4Code.GDSinage_9595ArrowObjects4= [];
gdjs.Level4Code.GDSinage_9595ArrowObjects5= [];
gdjs.Level4Code.GDSinage_9595ArrowObjects6= [];
gdjs.Level4Code.GDSinage_9595ArrowObjects7= [];
gdjs.Level4Code.GDSinage_9595ArrowObjects8= [];
gdjs.Level4Code.GDSinage_9595JumpObjects1= [];
gdjs.Level4Code.GDSinage_9595JumpObjects2= [];
gdjs.Level4Code.GDSinage_9595JumpObjects3= [];
gdjs.Level4Code.GDSinage_9595JumpObjects4= [];
gdjs.Level4Code.GDSinage_9595JumpObjects5= [];
gdjs.Level4Code.GDSinage_9595JumpObjects6= [];
gdjs.Level4Code.GDSinage_9595JumpObjects7= [];
gdjs.Level4Code.GDSinage_9595JumpObjects8= [];
gdjs.Level4Code.GDSinage_9595HoldObjects1= [];
gdjs.Level4Code.GDSinage_9595HoldObjects2= [];
gdjs.Level4Code.GDSinage_9595HoldObjects3= [];
gdjs.Level4Code.GDSinage_9595HoldObjects4= [];
gdjs.Level4Code.GDSinage_9595HoldObjects5= [];
gdjs.Level4Code.GDSinage_9595HoldObjects6= [];
gdjs.Level4Code.GDSinage_9595HoldObjects7= [];
gdjs.Level4Code.GDSinage_9595HoldObjects8= [];
gdjs.Level4Code.GDSinage_9595LongerJumpObjects1= [];
gdjs.Level4Code.GDSinage_9595LongerJumpObjects2= [];
gdjs.Level4Code.GDSinage_9595LongerJumpObjects3= [];
gdjs.Level4Code.GDSinage_9595LongerJumpObjects4= [];
gdjs.Level4Code.GDSinage_9595LongerJumpObjects5= [];
gdjs.Level4Code.GDSinage_9595LongerJumpObjects6= [];
gdjs.Level4Code.GDSinage_9595LongerJumpObjects7= [];
gdjs.Level4Code.GDSinage_9595LongerJumpObjects8= [];
gdjs.Level4Code.GDSinage_9595RestartObjects1= [];
gdjs.Level4Code.GDSinage_9595RestartObjects2= [];
gdjs.Level4Code.GDSinage_9595RestartObjects3= [];
gdjs.Level4Code.GDSinage_9595RestartObjects4= [];
gdjs.Level4Code.GDSinage_9595RestartObjects5= [];
gdjs.Level4Code.GDSinage_9595RestartObjects6= [];
gdjs.Level4Code.GDSinage_9595RestartObjects7= [];
gdjs.Level4Code.GDSinage_9595RestartObjects8= [];
gdjs.Level4Code.GDSinage_9595PauseObjects1= [];
gdjs.Level4Code.GDSinage_9595PauseObjects2= [];
gdjs.Level4Code.GDSinage_9595PauseObjects3= [];
gdjs.Level4Code.GDSinage_9595PauseObjects4= [];
gdjs.Level4Code.GDSinage_9595PauseObjects5= [];
gdjs.Level4Code.GDSinage_9595PauseObjects6= [];
gdjs.Level4Code.GDSinage_9595PauseObjects7= [];
gdjs.Level4Code.GDSinage_9595PauseObjects8= [];
gdjs.Level4Code.GDTest2Objects1= [];
gdjs.Level4Code.GDTest2Objects2= [];
gdjs.Level4Code.GDTest2Objects3= [];
gdjs.Level4Code.GDTest2Objects4= [];
gdjs.Level4Code.GDTest2Objects5= [];
gdjs.Level4Code.GDTest2Objects6= [];
gdjs.Level4Code.GDTest2Objects7= [];
gdjs.Level4Code.GDTest2Objects8= [];
gdjs.Level4Code.GDFrameRateObjects1= [];
gdjs.Level4Code.GDFrameRateObjects2= [];
gdjs.Level4Code.GDFrameRateObjects3= [];
gdjs.Level4Code.GDFrameRateObjects4= [];
gdjs.Level4Code.GDFrameRateObjects5= [];
gdjs.Level4Code.GDFrameRateObjects6= [];
gdjs.Level4Code.GDFrameRateObjects7= [];
gdjs.Level4Code.GDFrameRateObjects8= [];


gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects3});
gdjs.Level4Code.eventsList0 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "w");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Up");
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Ladder");
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "a");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects4[i].getX() >= 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects4[k] = gdjs.Level4Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects4.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDPlayerObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Left");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "d");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Right");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Jump");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "s");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].getBehavior("PlatformerObject").simulateControl("Down");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "LShift");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "RShift");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33398148);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects3);
{gdjs.evtsExt__Player__HealPlayer.func(runtimeScene, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects3});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDCheckpointObjects3Objects = Hashtable.newFrom({"Checkpoint": gdjs.Level4Code.GDCheckpointObjects3});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects3});
gdjs.Level4Code.eventsList1 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.Level4Code.GDCheckpointObjects3 */
/* Reuse gdjs.Level4Code.GDPlayerObjects3 */
{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects3Objects, (( gdjs.Level4Code.GDCheckpointObjects3.length === 0 ) ? 0 :gdjs.Level4Code.GDCheckpointObjects3[0].getPointX("")), (( gdjs.Level4Code.GDCheckpointObjects3.length === 0 ) ? 0 :gdjs.Level4Code.GDCheckpointObjects3[0].getPointY("")), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.Level4Code.GDCheckpointObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDCheckpointObjects3[i].getBehavior("Animation").setAnimationName("Activate");
}
}}

}


};gdjs.Level4Code.eventsList2 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);
{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects4Objects, (( gdjs.Level4Code.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects4[0].getPointX("")), (( gdjs.Level4Code.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects4[0].getPointY("")), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Checkpoint"), gdjs.Level4Code.GDCheckpointObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects3Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDCheckpointObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDCheckpointObjects3.length;i<l;++i) {
    if ( !(gdjs.Level4Code.GDCheckpointObjects3[i].isCurrentAnimationName("Activate")) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDCheckpointObjects3[k] = gdjs.Level4Code.GDCheckpointObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDCheckpointObjects3.length = k;
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Checkpoint/Activate.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}
{ //Subevents
gdjs.Level4Code.eventsList1(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDFlyingDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDFireDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDHorizontalDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BasePermanentObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserRingObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamSingleObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentSingleObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level4Code.GDFlyingDemonObjects4, "FireDemon": gdjs.Level4Code.GDFireDemonObjects4, "HorizontalDemon": gdjs.Level4Code.GDHorizontalDemonObjects4, "SpikeDemon_Base": gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4, "SpikeDemon_Spike": gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4, "StalactiteDemon_Base": gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4, "StalactiteDemon_Spike": gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4, "LaserDemon_Beam": gdjs.Level4Code.GDLaserDemon_9595BeamObjects4, "LaserDemon_Base": gdjs.Level4Code.GDLaserDemon_9595BaseObjects4, "LaserDemon_BasePermanent": gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4, "LaserDemon_BeamPermanent": gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4, "LaserRing": gdjs.Level4Code.GDLaserRingObjects4, "LaserDemon_BeamSingle": gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4, "LaserDemon_BeamPermanentSingle": gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4, "TurretDemon_Base": gdjs.Level4Code.GDTurretDemon_9595BaseObjects4, "TurretDemon_Spike": gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4, "ShockwaveDemon_Base": gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4, "MiteDemon_Base": gdjs.Level4Code.GDMiteDemon_9595BaseObjects4, "MiteDemon_Mite": gdjs.Level4Code.GDMiteDemon_9595MiteObjects4, "StalagmiteDemon_Base": gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4, "StalagmiteDemon_Spike": gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDFlyingDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDFireDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDHorizontalDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BasePermanentObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserRingObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamSingleObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentSingleObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595SpikeObjects4Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level4Code.GDFlyingDemonObjects4, "FireDemon": gdjs.Level4Code.GDFireDemonObjects4, "HorizontalDemon": gdjs.Level4Code.GDHorizontalDemonObjects4, "SpikeDemon_Base": gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4, "SpikeDemon_Spike": gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4, "StalactiteDemon_Base": gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4, "StalactiteDemon_Spike": gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4, "LaserDemon_Beam": gdjs.Level4Code.GDLaserDemon_9595BeamObjects4, "LaserDemon_Base": gdjs.Level4Code.GDLaserDemon_9595BaseObjects4, "LaserDemon_BasePermanent": gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4, "LaserDemon_BeamPermanent": gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4, "LaserRing": gdjs.Level4Code.GDLaserRingObjects4, "LaserDemon_BeamSingle": gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4, "LaserDemon_BeamPermanentSingle": gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4, "TurretDemon_Base": gdjs.Level4Code.GDTurretDemon_9595BaseObjects4, "TurretDemon_Spike": gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4, "ShockwaveDemon_Base": gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4, "MiteDemon_Base": gdjs.Level4Code.GDMiteDemon_9595BaseObjects4, "MiteDemon_Mite": gdjs.Level4Code.GDMiteDemon_9595MiteObjects4, "StalagmiteDemon_Base": gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4, "StalagmiteDemon_Spike": gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDBloodParticlesObjects4Objects = Hashtable.newFrom({"BloodParticles": gdjs.Level4Code.GDBloodParticlesObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects4Objects = Hashtable.newFrom({"TurretDemon_Eye": gdjs.Level4Code.GDTurretDemon_9595EyeObjects4});
gdjs.Level4Code.eventsList3 = function(runtimeScene) {

{



}


{

/* Reuse gdjs.Level4Code.GDFireDemonObjects4 */
/* Reuse gdjs.Level4Code.GDFlyingDemonObjects4 */
/* Reuse gdjs.Level4Code.GDHorizontalDemonObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4 */
/* Reuse gdjs.Level4Code.GDLaserRingObjects4 */
/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects4 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595BaseObjects4 */
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Eye"), gdjs.Level4Code.GDTurretDemon_9595EyeObjects4);
/* Reuse gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickNearestObject(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects4Objects, (gdjs.RuntimeObject.getVariableNumber(((gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? ((gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length === 0 ) ? ((gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length === 0 ) ? ((gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserRingObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length === 0 ) ? ((gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length === 0 ) ? ((gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length === 0 ) ? ((gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDHorizontalDemonObjects4.length === 0 ) ? ((gdjs.Level4Code.GDFireDemonObjects4.length === 0 ) ? ((gdjs.Level4Code.GDFlyingDemonObjects4.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level4Code.GDFlyingDemonObjects4[0].getVariables()) : gdjs.Level4Code.GDFireDemonObjects4[0].getVariables()) : gdjs.Level4Code.GDHorizontalDemonObjects4[0].getVariables()) : gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[0].getVariables()) : gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserRingObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[0].getVariables()) : gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[0].getVariables()) : gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[0].getVariables()) : gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[0].getVariables()).get("OldPosition_X"))), (gdjs.RuntimeObject.getVariableNumber(((gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? ((gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length === 0 ) ? ((gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length === 0 ) ? ((gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserRingObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length === 0 ) ? ((gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length === 0 ) ? ((gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length === 0 ) ? ((gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length === 0 ) ? ((gdjs.Level4Code.GDHorizontalDemonObjects4.length === 0 ) ? ((gdjs.Level4Code.GDFireDemonObjects4.length === 0 ) ? ((gdjs.Level4Code.GDFlyingDemonObjects4.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level4Code.GDFlyingDemonObjects4[0].getVariables()) : gdjs.Level4Code.GDFireDemonObjects4[0].getVariables()) : gdjs.Level4Code.GDHorizontalDemonObjects4[0].getVariables()) : gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[0].getVariables()) : gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserRingObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[0].getVariables()) : gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[0].getVariables()) : gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[0].getVariables()) : gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[0].getVariables()) : gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[0].getVariables()) : gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[0].getVariables()).get("OldPosition_Y"))), false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDTurretDemon_9595EyeObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].setPosition(0,4000);
}
}}

}


};gdjs.Level4Code.eventsList4 = function(runtimeScene) {

{

/* Reuse gdjs.Level4Code.GDFireDemonObjects4 */
/* Reuse gdjs.Level4Code.GDFlyingDemonObjects4 */
/* Reuse gdjs.Level4Code.GDHorizontalDemonObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4 */
/* Reuse gdjs.Level4Code.GDLaserRingObjects4 */
/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects4 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{isConditionTrue_0 = ((( gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length === 0 ) ? (( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserRingObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDHorizontalDemonObjects4.length === 0 ) ? (( gdjs.Level4Code.GDFireDemonObjects4.length === 0 ) ? (( gdjs.Level4Code.GDFlyingDemonObjects4.length === 0 ) ? "" :gdjs.Level4Code.GDFlyingDemonObjects4[0].getName()) :gdjs.Level4Code.GDFireDemonObjects4[0].getName()) :gdjs.Level4Code.GDHorizontalDemonObjects4[0].getName()) :gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[0].getName()) :gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[0].getName()) :gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[0].getName()) :gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[0].getName()) :gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[0].getName()) :gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[0].getName()) :gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[0].getName()) :gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[0].getName()) :gdjs.Level4Code.GDLaserRingObjects4[0].getName()) :gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[0].getName()) :gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[0].getName()) :gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[0].getName()) :gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[0].getName()) :gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[0].getName()) :gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[0].getName()) :gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[0].getName()) :gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[0].getName()) :gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[0].getName()) == "TurretDemon_Base");
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList3(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList5 = function(runtimeScene) {

{

/* Reuse gdjs.Level4Code.GDFireDemonObjects4 */
/* Reuse gdjs.Level4Code.GDFlyingDemonObjects4 */
/* Reuse gdjs.Level4Code.GDHorizontalDemonObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4 */
/* Reuse gdjs.Level4Code.GDLaserRingObjects4 */
/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects4 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDFlyingDemonObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDFlyingDemonObjects4[i].getVariableBoolean(gdjs.Level4Code.GDFlyingDemonObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDFlyingDemonObjects4[k] = gdjs.Level4Code.GDFlyingDemonObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDFlyingDemonObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDFireDemonObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDFireDemonObjects4[i].getVariableBoolean(gdjs.Level4Code.GDFireDemonObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDFireDemonObjects4[k] = gdjs.Level4Code.GDFireDemonObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDFireDemonObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDHorizontalDemonObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDHorizontalDemonObjects4[i].getVariableBoolean(gdjs.Level4Code.GDHorizontalDemonObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDHorizontalDemonObjects4[k] = gdjs.Level4Code.GDHorizontalDemonObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDHorizontalDemonObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[i].getVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[k] = gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[k] = gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserRingObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserRingObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserRingObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserRingObjects4[k] = gdjs.Level4Code.GDLaserRingObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserRingObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[i].getVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[k] = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].getVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[k] = gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDFlyingDemonObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDFlyingDemonObjects4[i].getVariableBoolean(gdjs.Level4Code.GDFlyingDemonObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDFlyingDemonObjects4[k] = gdjs.Level4Code.GDFlyingDemonObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDFlyingDemonObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDFireDemonObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDFireDemonObjects4[i].getVariableBoolean(gdjs.Level4Code.GDFireDemonObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDFireDemonObjects4[k] = gdjs.Level4Code.GDFireDemonObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDFireDemonObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDHorizontalDemonObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDHorizontalDemonObjects4[i].getVariableBoolean(gdjs.Level4Code.GDHorizontalDemonObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDHorizontalDemonObjects4[k] = gdjs.Level4Code.GDHorizontalDemonObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDHorizontalDemonObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[i].getVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[k] = gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[k] = gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserRingObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserRingObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserRingObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserRingObjects4[k] = gdjs.Level4Code.GDLaserRingObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserRingObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[i].getVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[k] = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].getVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[k] = gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDFireDemonObjects4 */
/* Reuse gdjs.Level4Code.GDFlyingDemonObjects4 */
/* Reuse gdjs.Level4Code.GDHorizontalDemonObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4 */
/* Reuse gdjs.Level4Code.GDLaserRingObjects4 */
/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects4 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4 */
gdjs.Level4Code.GDBloodParticlesObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDBloodParticlesObjects4Objects, (( gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length === 0 ) ? (( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserRingObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDHorizontalDemonObjects4.length === 0 ) ? (( gdjs.Level4Code.GDFireDemonObjects4.length === 0 ) ? (( gdjs.Level4Code.GDFlyingDemonObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDFlyingDemonObjects4[0].getX()) :gdjs.Level4Code.GDFireDemonObjects4[0].getX()) :gdjs.Level4Code.GDHorizontalDemonObjects4[0].getX()) :gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[0].getX()) :gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[0].getX()) :gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[0].getX()) :gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[0].getX()) :gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[0].getX()) :gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[0].getX()) :gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[0].getX()) :gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[0].getX()) :gdjs.Level4Code.GDLaserRingObjects4[0].getX()) :gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[0].getX()) :gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[0].getX()) :gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[0].getX()) :gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[0].getX()) :gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[0].getX()) :gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[0].getX()) :gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[0].getX()) :gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[0].getX()) :gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[0].getX()) + (( gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length === 0 ) ? (( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserRingObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDHorizontalDemonObjects4.length === 0 ) ? (( gdjs.Level4Code.GDFireDemonObjects4.length === 0 ) ? (( gdjs.Level4Code.GDFlyingDemonObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDFlyingDemonObjects4[0].getWidth()) :gdjs.Level4Code.GDFireDemonObjects4[0].getWidth()) :gdjs.Level4Code.GDHorizontalDemonObjects4[0].getWidth()) :gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[0].getWidth()) :gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[0].getWidth()) :gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[0].getWidth()) :gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[0].getWidth()) :gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[0].getWidth()) :gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[0].getWidth()) :gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[0].getWidth()) :gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[0].getWidth()) :gdjs.Level4Code.GDLaserRingObjects4[0].getWidth()) :gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[0].getWidth()) :gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[0].getWidth()) :gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[0].getWidth()) :gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[0].getWidth()) :gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[0].getWidth()) :gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[0].getWidth()) :gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[0].getWidth()) :gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[0].getWidth()) :gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[0].getWidth()) / 2, (( gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length === 0 ) ? (( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserRingObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDHorizontalDemonObjects4.length === 0 ) ? (( gdjs.Level4Code.GDFireDemonObjects4.length === 0 ) ? (( gdjs.Level4Code.GDFlyingDemonObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDFlyingDemonObjects4[0].getY()) :gdjs.Level4Code.GDFireDemonObjects4[0].getY()) :gdjs.Level4Code.GDHorizontalDemonObjects4[0].getY()) :gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[0].getY()) :gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[0].getY()) :gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[0].getY()) :gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[0].getY()) :gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[0].getY()) :gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[0].getY()) :gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[0].getY()) :gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[0].getY()) :gdjs.Level4Code.GDLaserRingObjects4[0].getY()) :gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[0].getY()) :gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[0].getY()) :gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[0].getY()) :gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[0].getY()) :gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[0].getY()) :gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[0].getY()) :gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[0].getY()) :gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[0].getY()) :gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[0].getY()) + (( gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length === 0 ) ? (( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserRingObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length === 0 ) ? (( gdjs.Level4Code.GDHorizontalDemonObjects4.length === 0 ) ? (( gdjs.Level4Code.GDFireDemonObjects4.length === 0 ) ? (( gdjs.Level4Code.GDFlyingDemonObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDFlyingDemonObjects4[0].getHeight()) :gdjs.Level4Code.GDFireDemonObjects4[0].getHeight()) :gdjs.Level4Code.GDHorizontalDemonObjects4[0].getHeight()) :gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[0].getHeight()) :gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[0].getHeight()) :gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[0].getHeight()) :gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[0].getHeight()) :gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[0].getHeight()) :gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[0].getHeight()) :gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[0].getHeight()) :gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[0].getHeight()) :gdjs.Level4Code.GDLaserRingObjects4[0].getHeight()) :gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[0].getHeight()) :gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[0].getHeight()) :gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[0].getHeight()) :gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[0].getHeight()) :gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[0].getHeight()) :gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[0].getHeight()) :gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[0].getHeight()) :gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[0].getHeight()) :gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.Level4Code.GDFlyingDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFlyingDemonObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDFireDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFireDemonObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDHorizontalDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDHorizontalDemonObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserRingObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserRingObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[i].setPosition(0,4000);
}
}{for(var i = 0, len = gdjs.Level4Code.GDFlyingDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFlyingDemonObjects4[i].setVariableBoolean(gdjs.Level4Code.GDFlyingDemonObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDFireDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFireDemonObjects4[i].setVariableBoolean(gdjs.Level4Code.GDFireDemonObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDHorizontalDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDHorizontalDemonObjects4[i].setVariableBoolean(gdjs.Level4Code.GDHorizontalDemonObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[i].setVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[i].setVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserRingObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserRingObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserRingObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[i].setVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].setVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("HasBeenReaped"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[i].setVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[i].getVariables().get("HasBeenReaped"), true);
}
}
{ //Subevents
gdjs.Level4Code.eventsList4(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList6 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level4Code.GDFireDemonObjects4);
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.Level4Code.GDFlyingDemonObjects4);
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.Level4Code.GDHorizontalDemonObjects4);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level4Code.GDLaserDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BasePermanent"), gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.Level4Code.GDLaserDemon_9595BeamObjects4);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BeamPermanent"), gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BeamPermanentSingle"), gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BeamSingle"), gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4);
gdjs.copyArray(runtimeScene.getObjects("LaserRing"), gdjs.Level4Code.GDLaserRingObjects4);
gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Base"), gdjs.Level4Code.GDMiteDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Mite"), gdjs.Level4Code.GDMiteDemon_9595MiteObjects4);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Base"), gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalactiteDemon_Base"), gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalactiteDemon_Spike"), gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike"), gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Base"), gdjs.Level4Code.GDTurretDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Spike"), gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects4Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDFlyingDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDFireDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDHorizontalDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BasePermanentObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserRingObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamSingleObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentSingleObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595SpikeObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDFireDemonObjects4 */
/* Reuse gdjs.Level4Code.GDFlyingDemonObjects4 */
/* Reuse gdjs.Level4Code.GDHorizontalDemonObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4 */
/* Reuse gdjs.Level4Code.GDLaserRingObjects4 */
/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects4 */
/* Reuse gdjs.Level4Code.GDPlayerObjects4 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDFlyingDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDFireDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDHorizontalDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BasePermanentObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserRingObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamSingleObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentSingleObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595SpikeObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.Level4Code.eventsList5(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformRightObjects4ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformLeftObjects4ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformDownObjects4ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformUpObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformClockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformCounterclockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformClockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformCounterclockwiseObjects4Objects = Hashtable.newFrom({"HorizontalMovingPlatformRight": gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects4, "HorizontalMovingPlatformLeft": gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects4, "VerticalMovingPlatformDown": gdjs.Level4Code.GDVerticalMovingPlatformDownObjects4, "VerticalMovingPlatformUp": gdjs.Level4Code.GDVerticalMovingPlatformUpObjects4, "SpinningPlatformClockwise": gdjs.Level4Code.GDSpinningPlatformClockwiseObjects4, "SpinningPlatformCounterclockwise": gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects4, "SpinningRotatingPlatformClockwise": gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects4, "SpinningRotatingPlatformCounterclockwise": gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects4});
gdjs.Level4Code.userFunc0x169dc00 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var i = 0; i < objects.length; i++) {
    const CurrPlatform = objects[i];
    const CurrPlatformVariables = CurrPlatform.getVariables();
    const NewPlatform = runtimeScene.createObject(CurrPlatform.getName());
    const NewPlatformVariables = NewPlatform.getVariables();

    NewPlatform.setWidth(CurrPlatform.getWidth());
    NewPlatform.setHeight(CurrPlatform.getHeight());
    NewPlatform.setVariableNumber(NewPlatformVariables.get("OldPosition_X"), CurrPlatformVariables.get("OldPosition_X").getAsNumber());
    NewPlatform.setVariableNumber(NewPlatformVariables.get("OldPosition_Y"), CurrPlatformVariables.get("OldPosition_Y").getAsNumber());
    NewPlatform.setVariableNumber(NewPlatformVariables.get("OldAngle"), CurrPlatformVariables.get("OldAngle").getAsNumber());
    NewPlatform.setPosition(NewPlatformVariables.get("OldPosition_X").getAsNumber(), NewPlatformVariables.get("OldPosition_Y").getAsNumber());
    NewPlatform.setAngle(NewPlatformVariables.get("OldAngle").getAsNumber());

    CurrPlatform.deleteFromScene(CurrPlatform.getInstanceContainer());
}
};
gdjs.Level4Code.eventsList7 = function(runtimeScene) {

{

/* Reuse gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects4 */
/* Reuse gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects4 */
/* Reuse gdjs.Level4Code.GDSpinningPlatformClockwiseObjects4 */
/* Reuse gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects4 */
/* Reuse gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects4 */
/* Reuse gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects4 */
/* Reuse gdjs.Level4Code.GDVerticalMovingPlatformDownObjects4 */
/* Reuse gdjs.Level4Code.GDVerticalMovingPlatformUpObjects4 */

var objects = [];
objects.push.apply(objects,gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects4);
objects.push.apply(objects,gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects4);
objects.push.apply(objects,gdjs.Level4Code.GDVerticalMovingPlatformDownObjects4);
objects.push.apply(objects,gdjs.Level4Code.GDVerticalMovingPlatformUpObjects4);
objects.push.apply(objects,gdjs.Level4Code.GDSpinningPlatformClockwiseObjects4);
objects.push.apply(objects,gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects4);
objects.push.apply(objects,gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects4);
objects.push.apply(objects,gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects4);
gdjs.Level4Code.userFunc0x169dc00(runtimeScene, objects);

}


};gdjs.Level4Code.eventsList8 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatformLeft"), gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects4);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatformRight"), gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningPlatformClockwise"), gdjs.Level4Code.GDSpinningPlatformClockwiseObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningPlatformCounterclockwise"), gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningRotatingPlatformClockwise"), gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningRotatingPlatformCounterclockwise"), gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects4);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatformDown"), gdjs.Level4Code.GDVerticalMovingPlatformDownObjects4);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatformUp"), gdjs.Level4Code.GDVerticalMovingPlatformUpObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformRightObjects4ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformLeftObjects4ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformDownObjects4ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformUpObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformClockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformCounterclockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformClockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformCounterclockwiseObjects4Objects);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList7(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDFlyingDemonObjects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalDemonObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BasePermanentObjects5ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects5ObjectsGDgdjs_9546Level4Code_9546GDFireDemonObjects5Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level4Code.GDFlyingDemonObjects5, "HorizontalDemon": gdjs.Level4Code.GDHorizontalDemonObjects5, "SpikeDemon_Base": gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5, "StalactiteDemon_Base": gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5, "StalagmiteDemon_Base": gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5, "LaserDemon_Base": gdjs.Level4Code.GDLaserDemon_9595BaseObjects5, "LaserDemon_BasePermanent": gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects5, "ShockwaveDemon_Base": gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5, "MiteDemon_Base": gdjs.Level4Code.GDMiteDemon_9595BaseObjects5, "TurretDemon_Base": gdjs.Level4Code.GDTurretDemon_9595BaseObjects5, "TurretDemon_Eye": gdjs.Level4Code.GDTurretDemon_9595EyeObjects5, "FireDemon": gdjs.Level4Code.GDFireDemonObjects5});
gdjs.Level4Code.userFunc0x1086a70 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var i = 0; i < objects.length; i++) {
    // this check doesn't work with native GDevelop event conditions since objects[] seems to behave weirdly if you use it with pick all
    // and doesn't only pick one enemy at a time? Hence if we use conditions to check name = "FireDemon" this code will run for all enemies
    // after just a singular true with an actual fire demon
    if (objects[i].getVariables().get("IsKillable").getAsBoolean() || objects[i].getName() == "FireDemon") {
        const CurrEnemy = objects[i];
        const CurrEnemyVariables = CurrEnemy.getVariables();
        const NewEnemy = runtimeScene.createObject(CurrEnemy.getName());
        const NewEnemyVariables = NewEnemy.getVariables();

        NewEnemy.setWidth(CurrEnemy.getWidth());
        NewEnemy.setHeight(CurrEnemy.getHeight());
        NewEnemy.setVariableNumber(NewEnemyVariables.get("OldPosition_X"), CurrEnemyVariables.get("OldPosition_X").getAsNumber());
        NewEnemy.setVariableNumber(NewEnemyVariables.get("OldPosition_Y"), CurrEnemyVariables.get("OldPosition_Y").getAsNumber());
        NewEnemy.setVariableNumber(NewEnemyVariables.get("OldAngle"), CurrEnemyVariables.get("OldAngle").getAsNumber());
        NewEnemy.setVariableBoolean(NewEnemyVariables.get("HasBeenReaped"), CurrEnemyVariables.get("HasBeenReaped").getAsBoolean());

        // copy over changeable instance variables, as these changes are not "default" except at the beginning of scene
        // (uses object-wide default variable values when creating new object)
        if (NewEnemyVariables.has("FireFrequencyTime")) { // this sure is a way to check instanceof ShootingEnemies
            NewEnemy.setVariableNumber(NewEnemyVariables.get("FireFrequencyTime"), CurrEnemyVariables.get("FireFrequencyTime").getAsNumber());
            NewEnemy.setVariableNumber(NewEnemyVariables.get("FireOffsetTime"), CurrEnemyVariables.get("FireOffsetTime").getAsNumber());
            NewEnemy.setVariableBoolean(NewEnemyVariables.get("CanShootOffscreen"), CurrEnemyVariables.get("CanShootOffscreen").getAsBoolean());
            // GDEVELOP SUCKS
            if (NewEnemyVariables.has("ProjectileSpeed")) {
                NewEnemy.setVariableNumber(NewEnemyVariables.get("ProjectileSpeed"), CurrEnemyVariables.get("ProjectileSpeed").getAsNumber());
            }
            if (NewEnemyVariables.has("FixedAngle")) {
                NewEnemy.setVariableBoolean(NewEnemyVariables.get("FixedAngle"), CurrEnemyVariables.get("FixedAngle").getAsBoolean());
            }
            if (NewEnemyVariables.has("ProjectileLifetime")) {
                NewEnemy.setVariableNumber(NewEnemyVariables.get("ProjectileLifetime"), CurrEnemyVariables.get("ProjectileLifetime").getAsNumber());
            }
            if (NewEnemyVariables.has("ShockwavePower")) {
                NewEnemy.setVariableNumber(NewEnemyVariables.get("ShockwavePower"), CurrEnemyVariables.get("ShockwavePower").getAsNumber());
            }
            if (NewEnemyVariables.has("MiteMaxSpeed")) {
                NewEnemy.setVariableNumber(NewEnemyVariables.get("MiteMaxSpeed"), CurrEnemyVariables.get("MiteMaxSpeed").getAsNumber());
            }
        }
        
        NewEnemy.setPosition(NewEnemyVariables.get("OldPosition_X").getAsNumber(), NewEnemyVariables.get("OldPosition_Y").getAsNumber());
        NewEnemy.setAngle(NewEnemyVariables.get("OldAngle").getAsNumber());

        CurrEnemy.deleteFromScene(CurrEnemy.getInstanceContainer());
    }
}
};
gdjs.Level4Code.eventsList9 = function(runtimeScene) {

{

/* Reuse gdjs.Level4Code.GDFireDemonObjects5 */
/* Reuse gdjs.Level4Code.GDFlyingDemonObjects5 */
/* Reuse gdjs.Level4Code.GDHorizontalDemonObjects5 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects5 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects5 */
/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects5 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595BaseObjects5 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595EyeObjects5 */

var objects = [];
objects.push.apply(objects,gdjs.Level4Code.GDFlyingDemonObjects5);
objects.push.apply(objects,gdjs.Level4Code.GDHorizontalDemonObjects5);
objects.push.apply(objects,gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5);
objects.push.apply(objects,gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5);
objects.push.apply(objects,gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5);
objects.push.apply(objects,gdjs.Level4Code.GDLaserDemon_9595BaseObjects5);
objects.push.apply(objects,gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects5);
objects.push.apply(objects,gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5);
objects.push.apply(objects,gdjs.Level4Code.GDMiteDemon_9595BaseObjects5);
objects.push.apply(objects,gdjs.Level4Code.GDTurretDemon_9595BaseObjects5);
objects.push.apply(objects,gdjs.Level4Code.GDTurretDemon_9595EyeObjects5);
objects.push.apply(objects,gdjs.Level4Code.GDFireDemonObjects5);
gdjs.Level4Code.userFunc0x1086a70(runtimeScene, objects);

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects5Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5, "StalactiteDemon_Base": gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5, "StalagmiteDemon_Base": gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5, "ShockwaveDemon_Base": gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5, "MiteDemon_Base": gdjs.Level4Code.GDMiteDemon_9595BaseObjects5, "TurretDemon_Eye": gdjs.Level4Code.GDTurretDemon_9595EyeObjects5});
gdjs.Level4Code.eventsList10 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.Level4Code.GDMiteDemon_9595BaseObjects5, gdjs.Level4Code.GDMiteDemon_9595BaseObjects6);

gdjs.copyArray(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5, gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects6);

gdjs.copyArray(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5, gdjs.Level4Code.GDSpikeDemon_9595BaseObjects6);

gdjs.copyArray(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5, gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects6);

gdjs.copyArray(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5, gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects6);

gdjs.copyArray(gdjs.Level4Code.GDTurretDemon_9595EyeObjects5, gdjs.Level4Code.GDTurretDemon_9595EyeObjects6);

{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects6[i].setVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects6[i].getVariables().get("NeedsOffsetReset"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects6[i].setVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects6[i].getVariables().get("NeedsOffsetReset"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects6[i].setVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects6[i].getVariables().get("NeedsOffsetReset"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects6[i].setVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects6[i].getVariables().get("NeedsOffsetReset"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects6[i].setVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects6[i].getVariables().get("NeedsOffsetReset"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects6[i].setVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595EyeObjects6[i].getVariables().get("NeedsOffsetReset"), true);
}
}{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects6[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects6[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects6[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects6[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects6[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects6[i].resetTimer("FireTimer");
}
}}

}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects5 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595EyeObjects5 */
{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects5[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects5[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects5[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects5[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5[i].getBehavior("Opacity").setOpacity(60);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5[i].getBehavior("Opacity").setOpacity(60);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5[i].getBehavior("Opacity").setOpacity(60);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5[i].getBehavior("Opacity").setOpacity(60);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects5[i].getBehavior("Opacity").setOpacity(60);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects5[i].getBehavior("Opacity").setOpacity(60);
}
}{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5[i].setVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5[i].getVariables().get("CurrentlyChargingUp"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5[i].setVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5[i].getVariables().get("CurrentlyChargingUp"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5[i].setVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5[i].getVariables().get("CurrentlyChargingUp"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5[i].setVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5[i].getVariables().get("CurrentlyChargingUp"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects5[i].setVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects5[i].getVariables().get("CurrentlyChargingUp"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects5[i].setVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595EyeObjects5[i].getVariables().get("CurrentlyChargingUp"), false);
}
}}

}


};gdjs.Level4Code.eventsList11 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5, gdjs.Level4Code.GDLaserDemon_9595BaseObjects6);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariableNumber(gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(1)) == 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects6 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(4), false);
}
}}

}


{

/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects5 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariableNumber(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(1)) != 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects5 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(4), true);
}
}}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamSingleObjects4Objects = Hashtable.newFrom({"LaserDemon_Beam": gdjs.Level4Code.GDLaserDemon_9595BeamObjects4, "LaserDemon_BeamSingle": gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4});
gdjs.Level4Code.eventsList12 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedUp"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedUp"), false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedDown"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedDown"), false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].resetTimer("FireTimer");
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].setPosition(0,4000);
}
}}

}


};gdjs.Level4Code.eventsList13 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level4Code.GDLaserDemon_9595BaseObjects5);
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(2), false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(3), false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(4), true);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].resetTimer("FireTimer");
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getBehavior("Tween").stopTween("ChargeUp", false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getBehavior("Tween").stopTween("ChargeDown", false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getBehavior("Opacity").setOpacity(50);
}
}
{ //Subevents
gdjs.Level4Code.eventsList11(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.Level4Code.GDLaserDemon_9595BeamObjects4);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BeamSingle"), gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamSingleObjects4Objects);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList12(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList14 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level4Code.GDFireDemonObjects5);
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.Level4Code.GDFlyingDemonObjects5);
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.Level4Code.GDHorizontalDemonObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level4Code.GDLaserDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BasePermanent"), gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects5);
gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Base"), gdjs.Level4Code.GDMiteDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Base"), gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalactiteDemon_Base"), gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Base"), gdjs.Level4Code.GDTurretDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Eye"), gdjs.Level4Code.GDTurretDemon_9595EyeObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDFlyingDemonObjects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalDemonObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BasePermanentObjects5ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects5ObjectsGDgdjs_9546Level4Code_9546GDFireDemonObjects5Objects);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList9(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Base"), gdjs.Level4Code.GDMiteDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Base"), gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalactiteDemon_Base"), gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Eye"), gdjs.Level4Code.GDTurretDemon_9595EyeObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects5Objects);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList10(runtimeScene);} //End of subevents
}

}


{


gdjs.Level4Code.eventsList13(runtimeScene);
}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595ShockwaveObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4, "StalactiteDemon_Spike": gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4, "StalagmiteDemon_Spike": gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4, "TurretDemon_Spike": gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4, "ShockwaveDemon_Shockwave": gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4, "MiteDemon_Mite": gdjs.Level4Code.GDMiteDemon_9595MiteObjects4});
gdjs.Level4Code.eventsList15 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects4 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level4Code.eventsList16 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Mite"), gdjs.Level4Code.GDMiteDemon_9595MiteObjects4);
gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Shockwave"), gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalactiteDemon_Spike"), gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike"), gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Spike"), gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595SpikeObjects4ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595ShockwaveObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4Objects);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList15(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList17 = function(runtimeScene) {

{


gdjs.Level4Code.eventsList8(runtimeScene);
}


{


gdjs.Level4Code.eventsList14(runtimeScene);
}


{


gdjs.Level4Code.eventsList16(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.Level4Code.GDPlayerObjects3 */
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects3[i].setVariableBoolean(gdjs.Level4Code.GDPlayerObjects3[i].getVariables().getFromIndex(3), false);
}
}}

}


};gdjs.Level4Code.eventsList18 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects3[i].getVariableBoolean(gdjs.Level4Code.GDPlayerObjects3[i].getVariables().getFromIndex(3), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects3[k] = gdjs.Level4Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33406396);
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList17(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList19 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects4[i].getY() > gdjs.evtTools.camera.getCameraBorderBottom(runtimeScene, "", 0) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects4[k] = gdjs.Level4Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDPlayerObjects4 */
{gdjs.evtsExt__Player__TriggerDeath.func(runtimeScene, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.Level4Code.eventsList6(runtimeScene);
}


{


gdjs.Level4Code.eventsList18(runtimeScene);
}


};gdjs.Level4Code.eventsList20 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "AssetDev/Audio/Heartbeat_Amplified.wav", 2, true, 100, 1);
}{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects4[i].getVariableNumber(gdjs.Level4Code.GDPlayerObjects4[i].getVariables().getFromIndex(0)) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects4[k] = gdjs.Level4Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 60);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.getSoundOnChannelVolume(runtimeScene, 2) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects3[i].getVariableNumber(gdjs.Level4Code.GDPlayerObjects3[i].getVariables().getFromIndex(0)) > 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects3[k] = gdjs.Level4Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects3.length = k;
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 0);
}}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects2});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDDustParticleObjects2Objects = Hashtable.newFrom({"DustParticle": gdjs.Level4Code.GDDustParticleObjects2});
gdjs.Level4Code.eventsList21 = function(runtimeScene) {

{


gdjs.Level4Code.eventsList20(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects3[i].getBehavior("PlatformerObject").isJumping() ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects3[k] = gdjs.Level4Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33460884);
}
}
if (isConditionTrue_0) {
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtsExt__Player__IsSteppingOnFloor.func(runtimeScene, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects2Objects, "PlatformerObject", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33461364);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDPlayerObjects2 */
gdjs.Level4Code.GDDustParticleObjects2.length = 0;

{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "grass.mp3", 1, false, 20, gdjs.randomFloatInRange(0.7, 1.2));
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDDustParticleObjects2Objects, (( gdjs.Level4Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects2[0].getAABBCenterX()), (( gdjs.Level4Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects2[0].getAABBBottom()), "");
}{for(var i = 0, len = gdjs.Level4Code.GDDustParticleObjects2.length ;i < len;++i) {
    gdjs.Level4Code.GDDustParticleObjects2[i].setZOrder(-(1));
}
}{for(var i = 0, len = gdjs.Level4Code.GDDustParticleObjects2.length ;i < len;++i) {
    gdjs.Level4Code.GDDustParticleObjects2[i].setAngle(270);
}
}}

}


};gdjs.Level4Code.eventsList22 = function(runtimeScene) {

{


gdjs.Level4Code.eventsList0(runtimeScene);
}


{


gdjs.Level4Code.eventsList2(runtimeScene);
}


{


gdjs.Level4Code.eventsList19(runtimeScene);
}


{


gdjs.Level4Code.eventsList21(runtimeScene);
}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDFlyingDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDHorizontalDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BasePermanentObjects4ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects4ObjectsGDgdjs_9546Level4Code_9546GDFireDemonObjects4Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level4Code.GDFlyingDemonObjects4, "HorizontalDemon": gdjs.Level4Code.GDHorizontalDemonObjects4, "SpikeDemon_Base": gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4, "StalactiteDemon_Base": gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4, "StalagmiteDemon_Base": gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4, "LaserDemon_Base": gdjs.Level4Code.GDLaserDemon_9595BaseObjects4, "LaserDemon_BasePermanent": gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4, "ShockwaveDemon_Base": gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4, "MiteDemon_Base": gdjs.Level4Code.GDMiteDemon_9595BaseObjects4, "TurretDemon_Base": gdjs.Level4Code.GDTurretDemon_9595BaseObjects4, "TurretDemon_Eye": gdjs.Level4Code.GDTurretDemon_9595EyeObjects4, "FireDemon": gdjs.Level4Code.GDFireDemonObjects4});
gdjs.Level4Code.eventsList23 = function(runtimeScene) {

{

/* Reuse gdjs.Level4Code.GDFireDemonObjects4 */
/* Reuse gdjs.Level4Code.GDFlyingDemonObjects4 */
/* Reuse gdjs.Level4Code.GDHorizontalDemonObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4 */
/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595EyeObjects4 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDFlyingDemonObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDFlyingDemonObjects4[i].getVariableBoolean(gdjs.Level4Code.GDFlyingDemonObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDFlyingDemonObjects4[k] = gdjs.Level4Code.GDFlyingDemonObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDFlyingDemonObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDHorizontalDemonObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDHorizontalDemonObjects4[i].getVariableBoolean(gdjs.Level4Code.GDHorizontalDemonObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDHorizontalDemonObjects4[k] = gdjs.Level4Code.GDHorizontalDemonObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDHorizontalDemonObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[k] = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDFireDemonObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDFireDemonObjects4[i].getVariableBoolean(gdjs.Level4Code.GDFireDemonObjects4[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDFireDemonObjects4[k] = gdjs.Level4Code.GDFireDemonObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDFireDemonObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDFireDemonObjects4 */
/* Reuse gdjs.Level4Code.GDFlyingDemonObjects4 */
/* Reuse gdjs.Level4Code.GDHorizontalDemonObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4 */
/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595EyeObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDFlyingDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFlyingDemonObjects4[i].returnVariable(gdjs.Level4Code.GDFlyingDemonObjects4[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDFlyingDemonObjects4[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDHorizontalDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDHorizontalDemonObjects4[i].returnVariable(gdjs.Level4Code.GDHorizontalDemonObjects4[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDHorizontalDemonObjects4[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].returnVariable(gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].returnVariable(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDFireDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFireDemonObjects4[i].returnVariable(gdjs.Level4Code.GDFireDemonObjects4[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDFireDemonObjects4[i].getPointX("")));
}
}{for(var i = 0, len = gdjs.Level4Code.GDFlyingDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFlyingDemonObjects4[i].returnVariable(gdjs.Level4Code.GDFlyingDemonObjects4[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDFlyingDemonObjects4[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDHorizontalDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDHorizontalDemonObjects4[i].returnVariable(gdjs.Level4Code.GDHorizontalDemonObjects4[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDHorizontalDemonObjects4[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].returnVariable(gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].returnVariable(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDFireDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFireDemonObjects4[i].returnVariable(gdjs.Level4Code.GDFireDemonObjects4[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDFireDemonObjects4[i].getPointY("")));
}
}{for(var i = 0, len = gdjs.Level4Code.GDFlyingDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFlyingDemonObjects4[i].returnVariable(gdjs.Level4Code.GDFlyingDemonObjects4[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDFlyingDemonObjects4[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDHorizontalDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDHorizontalDemonObjects4[i].returnVariable(gdjs.Level4Code.GDHorizontalDemonObjects4[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDHorizontalDemonObjects4[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].returnVariable(gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].returnVariable(gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDTurretDemon_9595BaseObjects4[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].returnVariable(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDFireDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFireDemonObjects4[i].returnVariable(gdjs.Level4Code.GDFireDemonObjects4[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDFireDemonObjects4[i].getAngle()));
}
}}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformRightObjects3ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformLeftObjects3ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformDownObjects3ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformUpObjects3ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformClockwiseObjects3ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformCounterclockwiseObjects3ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformClockwiseObjects3ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformCounterclockwiseObjects3Objects = Hashtable.newFrom({"HorizontalMovingPlatformRight": gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3, "HorizontalMovingPlatformLeft": gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3, "VerticalMovingPlatformDown": gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3, "VerticalMovingPlatformUp": gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3, "SpinningPlatformClockwise": gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3, "SpinningPlatformCounterclockwise": gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3, "SpinningRotatingPlatformClockwise": gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3, "SpinningRotatingPlatformCounterclockwise": gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3});
gdjs.Level4Code.eventsList24 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3 */
/* Reuse gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3 */
/* Reuse gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3 */
/* Reuse gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3 */
/* Reuse gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3 */
/* Reuse gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3 */
/* Reuse gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3 */
/* Reuse gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3 */
{for(var i = 0, len = gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3[i].returnVariable(gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3[i].returnVariable(gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3[i].returnVariable(gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3[i].returnVariable(gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3[i].returnVariable(gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3[i].returnVariable(gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3[i].returnVariable(gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3[i].getPointX("")));
}
for(var i = 0, len = gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3[i].returnVariable(gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3[i].getVariables().get("OldPosition_X")).setNumber((gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3[i].getPointX("")));
}
}{for(var i = 0, len = gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3[i].returnVariable(gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3[i].returnVariable(gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3[i].returnVariable(gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3[i].returnVariable(gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3[i].returnVariable(gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3[i].returnVariable(gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3[i].returnVariable(gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3[i].getPointY("")));
}
for(var i = 0, len = gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3[i].returnVariable(gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3[i].getPointY("")));
}
}{for(var i = 0, len = gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3[i].returnVariable(gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3[i].returnVariable(gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3[i].returnVariable(gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3[i].returnVariable(gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3[i].returnVariable(gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3[i].returnVariable(gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3[i].returnVariable(gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3[i].getAngle()));
}
for(var i = 0, len = gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3[i].returnVariable(gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3[i].getVariables().get("OldAngle")).setNumber((gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3[i].getAngle()));
}
}}

}


};gdjs.Level4Code.eventsList25 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level4Code.GDFireDemonObjects4);
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.Level4Code.GDFlyingDemonObjects4);
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.Level4Code.GDHorizontalDemonObjects4);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level4Code.GDLaserDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BasePermanent"), gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4);
gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Base"), gdjs.Level4Code.GDMiteDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Base"), gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalactiteDemon_Base"), gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Base"), gdjs.Level4Code.GDTurretDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Eye"), gdjs.Level4Code.GDTurretDemon_9595EyeObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDFlyingDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDHorizontalDemonObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BasePermanentObjects4ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects4ObjectsGDgdjs_9546Level4Code_9546GDFireDemonObjects4Objects);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList23(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatformLeft"), gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatformRight"), gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3);
gdjs.copyArray(runtimeScene.getObjects("SpinningPlatformClockwise"), gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3);
gdjs.copyArray(runtimeScene.getObjects("SpinningPlatformCounterclockwise"), gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3);
gdjs.copyArray(runtimeScene.getObjects("SpinningRotatingPlatformClockwise"), gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3);
gdjs.copyArray(runtimeScene.getObjects("SpinningRotatingPlatformCounterclockwise"), gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatformDown"), gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatformUp"), gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformRightObjects3ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformLeftObjects3ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformDownObjects3ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformUpObjects3ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformClockwiseObjects3ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformCounterclockwiseObjects3ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformClockwiseObjects3ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformCounterclockwiseObjects3Objects);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList24(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList26 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList25(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects3ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects3ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects3ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects3ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects3ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects3Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3, "StalactiteDemon_Base": gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3, "StalagmiteDemon_Base": gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3, "ShockwaveDemon_Base": gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3, "MiteDemon_Base": gdjs.Level4Code.GDMiteDemon_9595BaseObjects3, "TurretDemon_Eye": gdjs.Level4Code.GDTurretDemon_9595EyeObjects3});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects4Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4, "StalactiteDemon_Base": gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4, "StalagmiteDemon_Base": gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4, "ShockwaveDemon_Base": gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4, "MiteDemon_Base": gdjs.Level4Code.GDMiteDemon_9595BaseObjects4, "TurretDemon_Eye": gdjs.Level4Code.GDTurretDemon_9595EyeObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects4});
gdjs.Level4Code.userFunc0x16a18b8 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
function ShootSpikeDemon(SpikeDemonBase) {
    const NumSpikes = 6;
    const SpikeScale = 64;
    const SpikeDemonBaseAngle = SpikeDemonBase.getAngle();

    for (var i = 0; i < NumSpikes; i++) {
        var SpikeAngle = (360/NumSpikes)*i + SpikeDemonBaseAngle;

        var Spike = runtimeScene.createObject("SpikeDemon_Spike");
        Spike.setWidth(SpikeScale);
        Spike.setHeight(SpikeScale);
        // doesn't work without this print statement (gives time for renderer to update() maybe?)
        console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight());
        Spike.setPosition(SpikeDemonBase.x + SpikeDemonBase.getWidth() / 3.6,
                SpikeDemonBase.y + SpikeDemonBase.getHeight() / 3.2);
        Spike.setAngle(SpikeAngle);
        
        Spike.setLayer("Base Layer");
    }
}

function ShootStalactiteDemon(StalactiteDemonBase) {
    const SpikeLength = 96;

    var Spike = runtimeScene.createObject("StalactiteDemon_Spike");
    Spike.setWidth(SpikeLength);
    Spike.setHeight(SpikeLength / 2);
    // doesn't work without this print statement (gives time for renderer to update() maybe?)
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight());
    Spike.setPosition(StalactiteDemonBase.x + StalactiteDemonBase.getWidth() / 3,
            StalactiteDemonBase.y + StalactiteDemonBase.getHeight() - StalactiteDemonBase.getHeight() / 3);
    Spike.setAngle(90);

    Spike.setLayer("Base Layer");
}

function ShootStalagmiteDemon(StalagmiteDemonBase) {
    const SpikeLength = 96;
    
    var Spike = runtimeScene.createObject("StalagmiteDemon_Spike");
    Spike.setWidth(SpikeLength);
    Spike.setHeight(SpikeLength / 2);
    // doesn't work without this print statement (gives time for renderer to update() maybe?)
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight());
    Spike.setPosition(StalagmiteDemonBase.x + StalagmiteDemonBase.getWidth() / 3,
            StalagmiteDemonBase.y);
    Spike.setAngle(270);

    Spike.setLayer("Base Layer");
}

function ShootTurretDemon(TurretDemonEye) {
    const SpikeScale = 64;
    const Player = runtimeScene.getObjects("Player")[0];

    var Spike = runtimeScene.createObject("TurretDemon_Spike");
    Spike.setWidth(SpikeScale);
    Spike.setHeight(SpikeScale);
    // doesn't work without this print statement (gives time for renderer to update() maybe?)
    console.log(Spike.getX() + "\n" + Spike.getY() + "\n" + Spike.getWidth() + "\n" + Spike.getHeight());
    Spike.setPosition(TurretDemonEye.x + TurretDemonEye.getWidth() / 2.5, TurretDemonEye.y + TurretDemonEye.getHeight() / 2.5);
    // point initially towards player; have to do calcs since rotateTowardsPosition with immediate rotation seems to not work
    Spike.setAngle(Math.atan((Spike.getCenterX() - Player.getCenterX()) / (Spike.getCenterY - Player.getCenterY())));
    
    Spike.getVariables().get("Lifetime").setNumber(TurretDemonEye.getVariables().get("ProjectileLifetime").getAsNumber());
    Spike.getVariables().get("Speed").setNumber(TurretDemonEye.getVariables().get("ProjectileSpeed").getAsNumber());
    Spike.resetTimer("TimeAlive");

    Spike.setLayer("Base Layer");
}

function ShootShockwaveDemon(ShockwaveDemonBase) {
    const ShockwaveScale = 384;
    const Player = runtimeScene.getObjects("Player")[0];
    const ShockwaveDemonBaseVariables = ShockwaveDemonBase.getVariables();
    const ShockwavePower = ShockwaveDemonBaseVariables.get("ShockwavePower").getAsNumber();

    var Shockwave = runtimeScene.createObject("ShockwaveDemon_Shockwave");
    Shockwave.setWidth(ShockwaveScale);
    Shockwave.setHeight(ShockwaveScale);
    // doesn't work without this print statement (gives time for renderer to update() maybe?)
    console.log(Shockwave.getX() + "\n" + Shockwave.getY() + "\n" + Shockwave.getWidth() + "\n" + Shockwave.getHeight());
    Shockwave.setPosition(ShockwaveDemonBase.x - ShockwaveDemonBase.getWidth() / 2,
            ShockwaveDemonBase.y - ShockwaveDemonBase.getHeight() / 2);

    if (ShockwaveDemonBaseVariables.get("FixedAngle").getAsBoolean()) {
        var AngleToShoot = ShockwaveDemonBase.getAngle() + 90;
        Shockwave.setAngle(180 + AngleToShoot);
        Shockwave.addForceTowardPosition(ShockwaveDemonBase.getX() + 10000 * Math.cos(AngleToShoot * Math.PI / 180),
                ShockwaveDemonBase.getY() + 10000 * Math.sin(AngleToShoot * Math.PI / 180), ShockwavePower, 1);
    } else {
        Shockwave.setAngle(180 + Shockwave.getAngleToObject(Player));
        Shockwave.addForceTowardPosition(Player.getX() + Player.getWidth() / 2, Player.getY() + Player.getHeight() / 2,
                ShockwavePower, 1);
    }
    
    Shockwave.getVariables().get("Lifetime").setNumber(ShockwaveDemonBaseVariables.get("ProjectileLifetime").getAsNumber());
    Shockwave.getVariables().get("Power").setNumber(ShockwaveDemonBaseVariables.get("ShockwavePower").getAsNumber());
    Shockwave.resetTimer("TimeAlive");

    Shockwave.setLayer("Base Layer");
}

function ShootMiteDemon(MiteDemonBase) {
    const MiteSize = 128;
    const Player = runtimeScene.getObjects("Player")[0];
    const MiteDemonBaseVariables = MiteDemonBase.getVariables();

    var Mite = runtimeScene.createObject("MiteDemon_Mite");
    Mite.setWidth(MiteSize);
    Mite.setHeight(MiteSize);
    // doesn't work without this print statement (gives time for renderer to update() maybe?)
    console.log(Mite.getX() + "\n" + Mite.getY() + "\n" + Mite.getWidth() + "\n" + Mite.getHeight());
    Mite.setPosition(MiteDemonBase.x + MiteDemonBase.getWidth() / 4,
            MiteDemonBase.y + MiteDemonBase.getHeight() / 3);

    if (Mite.getCenterX() < Player.getX()) {
        Mite.setAngle(0);
    } else {
        Mite.setAngle(180);
    }

    Mite.getVariables().get("Lifetime").setNumber(MiteDemonBaseVariables.get("ProjectileLifetime").getAsNumber());
    Mite.getVariables().get("MaxSpeed").setNumber(MiteDemonBaseVariables.get("MiteMaxSpeed").getAsNumber());
    Mite.resetTimer("TimeAlive");

    Mite.setLayer("Base Layer");
}

for (var x = 0; x < objects.length; x++) {
    // "Polymorphism"
    const ShootingEnemy = objects[x];
    const ShootingEnemyName = ShootingEnemy.getName();
    console.log(ShootingEnemyName)

    if (ShootingEnemyName === "SpikeDemon_Base") {
        ShootSpikeDemon(ShootingEnemy);
    } else if (ShootingEnemyName === "StalactiteDemon_Base") {
        ShootStalactiteDemon(ShootingEnemy);
    } else if (ShootingEnemyName === "StalagmiteDemon_Base") {
        ShootStalagmiteDemon(ShootingEnemy);
    } else if (ShootingEnemyName === "TurretDemon_Eye") {
        ShootTurretDemon(ShootingEnemy);
    } else if (ShootingEnemyName === "ShockwaveDemon_Base") {
        ShootShockwaveDemon(ShootingEnemy);
    } else if (ShootingEnemyName === "MiteDemon_Base") {
        ShootMiteDemon(ShootingEnemy);
    }
}

};
gdjs.Level4Code.eventsList27 = function(runtimeScene) {

{

/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595EyeObjects3 */

var objects = [];
objects.push.apply(objects,gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3);
objects.push.apply(objects,gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3);
objects.push.apply(objects,gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3);
objects.push.apply(objects,gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3);
objects.push.apply(objects,gdjs.Level4Code.GDMiteDemon_9595BaseObjects3);
objects.push.apply(objects,gdjs.Level4Code.GDTurretDemon_9595EyeObjects3);
gdjs.Level4Code.userFunc0x16a18b8(runtimeScene, objects);

}


};gdjs.Level4Code.eventsList28 = function(runtimeScene) {

{

/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects3 */
gdjs.Level4Code.GDPlayerObjects3.length = 0;

/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595EyeObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.Level4Code.GDMiteDemon_9595BaseObjects3_1final.length = 0;
gdjs.Level4Code.GDPlayerObjects3_1final.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3_1final.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3_1final.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3_1final.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3_1final.length = 0;
gdjs.Level4Code.GDTurretDemon_9595EyeObjects3_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.Level4Code.GDMiteDemon_9595BaseObjects3, gdjs.Level4Code.GDMiteDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3, gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3, gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3, gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3, gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDTurretDemon_9595EyeObjects3, gdjs.Level4Code.GDTurretDemon_9595EyeObjects4);

for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getBehavior("InOnScreen").IsOnScreen(0, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getBehavior("InOnScreen").IsOnScreen(0, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getBehavior("InOnScreen").IsOnScreen(0, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getBehavior("InOnScreen").IsOnScreen(0, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getBehavior("InOnScreen").IsOnScreen(0, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getBehavior("InOnScreen").IsOnScreen(0, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[k] = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects3_1final.indexOf(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[j]) === -1 )
            gdjs.Level4Code.GDMiteDemon_9595BaseObjects3_1final.push(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3_1final.indexOf(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[j]) === -1 )
            gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3_1final.push(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3_1final.indexOf(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[j]) === -1 )
            gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3_1final.push(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3_1final.indexOf(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[j]) === -1 )
            gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3_1final.push(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3_1final.indexOf(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[j]) === -1 )
            gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3_1final.push(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDTurretDemon_9595EyeObjects3_1final.indexOf(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[j]) === -1 )
            gdjs.Level4Code.GDTurretDemon_9595EyeObjects3_1final.push(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[j]);
    }
}
}
{
gdjs.copyArray(gdjs.Level4Code.GDMiteDemon_9595BaseObjects3, gdjs.Level4Code.GDMiteDemon_9595BaseObjects4);

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);
gdjs.copyArray(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3, gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3, gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3, gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3, gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDTurretDemon_9595EyeObjects3, gdjs.Level4Code.GDTurretDemon_9595EyeObjects4);

{let isConditionTrue_2 = false;
isConditionTrue_2 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("CanShootOffscreen"), true) ) {
        isConditionTrue_2 = true;
        gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("CanShootOffscreen"), true) ) {
        isConditionTrue_2 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("CanShootOffscreen"), true) ) {
        isConditionTrue_2 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("CanShootOffscreen"), true) ) {
        isConditionTrue_2 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("CanShootOffscreen"), true) ) {
        isConditionTrue_2 = true;
        gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariables().get("CanShootOffscreen"), true) ) {
        isConditionTrue_2 = true;
        gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[k] = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length = k;
if (isConditionTrue_2) {
isConditionTrue_2 = false;
isConditionTrue_2 = gdjs.evtTools.object.distanceTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects4ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects4Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects4Objects, 4000, false);
}
isConditionTrue_1 = isConditionTrue_2;
}
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects3_1final.indexOf(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[j]) === -1 )
            gdjs.Level4Code.GDMiteDemon_9595BaseObjects3_1final.push(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDPlayerObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDPlayerObjects3_1final.indexOf(gdjs.Level4Code.GDPlayerObjects4[j]) === -1 )
            gdjs.Level4Code.GDPlayerObjects3_1final.push(gdjs.Level4Code.GDPlayerObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3_1final.indexOf(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[j]) === -1 )
            gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3_1final.push(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3_1final.indexOf(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[j]) === -1 )
            gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3_1final.push(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3_1final.indexOf(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[j]) === -1 )
            gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3_1final.push(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3_1final.indexOf(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[j]) === -1 )
            gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3_1final.push(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDTurretDemon_9595EyeObjects3_1final.indexOf(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[j]) === -1 )
            gdjs.Level4Code.GDTurretDemon_9595EyeObjects3_1final.push(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[j]);
    }
}
}
{
gdjs.copyArray(gdjs.Level4Code.GDMiteDemon_9595BaseObjects3_1final, gdjs.Level4Code.GDMiteDemon_9595BaseObjects3);
gdjs.copyArray(gdjs.Level4Code.GDPlayerObjects3_1final, gdjs.Level4Code.GDPlayerObjects3);
gdjs.copyArray(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3_1final, gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3);
gdjs.copyArray(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3_1final, gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3);
gdjs.copyArray(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3_1final, gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3);
gdjs.copyArray(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3_1final, gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3);
gdjs.copyArray(gdjs.Level4Code.GDTurretDemon_9595EyeObjects3_1final, gdjs.Level4Code.GDTurretDemon_9595EyeObjects3);
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList27(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList29 = function(runtimeScene) {

{



}


{

gdjs.copyArray(gdjs.Level4Code.GDMiteDemon_9595BaseObjects3, gdjs.Level4Code.GDMiteDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3, gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3, gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3, gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3, gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDTurretDemon_9595EyeObjects3, gdjs.Level4Code.GDTurretDemon_9595EyeObjects4);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariables().get("NeedsOffsetReset"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[k] = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("FireOffsetTime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("FireOffsetTime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("FireOffsetTime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("FireOffsetTime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("FireOffsetTime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariables().get("FireOffsetTime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[k] = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595EyeObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].setVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariables().get("NeedsOffsetReset"), false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].resetTimer("FireTimer");
}
}}

}


{



}


{

gdjs.copyArray(gdjs.Level4Code.GDMiteDemon_9595BaseObjects3, gdjs.Level4Code.GDMiteDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3, gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3, gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3, gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3, gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4);

gdjs.copyArray(gdjs.Level4Code.GDTurretDemon_9595EyeObjects3, gdjs.Level4Code.GDTurretDemon_9595EyeObjects4);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("CurrentlyChargingUp"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("CurrentlyChargingUp"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("CurrentlyChargingUp"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("CurrentlyChargingUp"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("CurrentlyChargingUp"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariables().get("CurrentlyChargingUp"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[k] = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("NeedsOffsetReset"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariables().get("NeedsOffsetReset"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[k] = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("FireFrequencyTime"))) - 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("FireFrequencyTime"))) - 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("FireFrequencyTime"))) - 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("FireFrequencyTime"))) - 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("FireFrequencyTime"))) - 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariables().get("FireFrequencyTime"))) - 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[k] = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length = k;
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595EyeObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "linear", 1, false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "linear", 1, false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "linear", 1, false);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "linear", 1, false);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "linear", 1, false);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "linear", 1, false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getVariables().get("CurrentlyChargingUp"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getVariables().get("CurrentlyChargingUp"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getVariables().get("CurrentlyChargingUp"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getVariables().get("CurrentlyChargingUp"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getVariables().get("CurrentlyChargingUp"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].setVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getVariables().get("CurrentlyChargingUp"), true);
}
}}

}


{



}


{

/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595EyeObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3[i].getVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3[i].getVariables().get("NeedsOffsetReset"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3[k] = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3[i].getVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3[i].getVariables().get("NeedsOffsetReset"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3[k] = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3[i].getVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3[i].getVariables().get("NeedsOffsetReset"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3[k] = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3[i].getVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3[i].getVariables().get("NeedsOffsetReset"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3[k] = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595BaseObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects3[i].getVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects3[i].getVariables().get("NeedsOffsetReset"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595BaseObjects3[k] = gdjs.Level4Code.GDMiteDemon_9595BaseObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595BaseObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595EyeObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595EyeObjects3[i].getVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595EyeObjects3[i].getVariables().get("NeedsOffsetReset"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595EyeObjects3[k] = gdjs.Level4Code.GDTurretDemon_9595EyeObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595EyeObjects3.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3[i].getVariables().get("FireFrequencyTime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3[k] = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3[i].getVariables().get("FireFrequencyTime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3[k] = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3[i].getVariables().get("FireFrequencyTime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3[k] = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3[i].getVariables().get("FireFrequencyTime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3[k] = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595BaseObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595BaseObjects3[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDMiteDemon_9595BaseObjects3[i].getVariables().get("FireFrequencyTime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595BaseObjects3[k] = gdjs.Level4Code.GDMiteDemon_9595BaseObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595BaseObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595EyeObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595EyeObjects3[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDTurretDemon_9595EyeObjects3[i].getVariables().get("FireFrequencyTime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595EyeObjects3[k] = gdjs.Level4Code.GDTurretDemon_9595EyeObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595EyeObjects3.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595EyeObjects3 */
{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3[i].getBehavior("Tween").addObjectOpacityTween2("ChargeDown", 100, "easeOutQuad", 0.3, false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3[i].getBehavior("Tween").addObjectOpacityTween2("ChargeDown", 100, "easeOutQuad", 0.3, false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3[i].getBehavior("Tween").addObjectOpacityTween2("ChargeDown", 100, "easeOutQuad", 0.3, false);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3[i].getBehavior("Tween").addObjectOpacityTween2("ChargeDown", 100, "easeOutQuad", 0.3, false);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects3[i].getBehavior("Tween").addObjectOpacityTween2("ChargeDown", 100, "easeOutQuad", 0.3, false);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects3[i].getBehavior("Tween").addObjectOpacityTween2("ChargeDown", 100, "easeOutQuad", 0.3, false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3[i].setVariableBoolean(gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3[i].getVariables().get("CurrentlyChargingUp"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3[i].setVariableBoolean(gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3[i].getVariables().get("CurrentlyChargingUp"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3[i].setVariableBoolean(gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3[i].getVariables().get("CurrentlyChargingUp"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3[i].setVariableBoolean(gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3[i].getVariables().get("CurrentlyChargingUp"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects3[i].setVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595BaseObjects3[i].getVariables().get("CurrentlyChargingUp"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects3[i].setVariableBoolean(gdjs.Level4Code.GDTurretDemon_9595EyeObjects3[i].getVariables().get("CurrentlyChargingUp"), false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects3[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects3[i].resetTimer("FireTimer");
}
}
{ //Subevents
gdjs.Level4Code.eventsList28(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList30 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Base"), gdjs.Level4Code.GDMiteDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Base"), gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalactiteDemon_Base"), gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Eye"), gdjs.Level4Code.GDTurretDemon_9595EyeObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].resetTimer("FireTimer");
}
}{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4[i].getBehavior("Opacity").setOpacity(100);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4[i].getBehavior("Opacity").setOpacity(100);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4[i].getBehavior("Opacity").setOpacity(100);
}
for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4[i].getBehavior("Opacity").setOpacity(100);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595BaseObjects4[i].getBehavior("Opacity").setOpacity(100);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects4[i].getBehavior("Opacity").setOpacity(100);
}
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Base"), gdjs.Level4Code.GDMiteDemon_9595BaseObjects3);
gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Base"), gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3);
gdjs.copyArray(runtimeScene.getObjects("StalactiteDemon_Base"), gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Base"), gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Eye"), gdjs.Level4Code.GDTurretDemon_9595EyeObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects3ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects3ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595BaseObjects3ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects3ObjectsGDgdjs_9546Level4Code_9546GDMiteDemon_95959595BaseObjects3ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects3Objects);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList29(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595SpikeObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595SpikeObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595SpikeObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595SpikeObjects5Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5, "StalactiteDemon_Spike": gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5, "StalagmiteDemon_Spike": gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5, "TurretDemon_Spike": gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDStaticPlatform1Objects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformRightObjects5ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform2Objects5ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform3Objects5ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformDownObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformClockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatformObjects5ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatform2Objects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformLeftObjects5ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformUpObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformCounterclockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformClockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformCounterclockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserRingObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BasePermanentObjects5Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.Level4Code.GDStaticPlatform1Objects5, "HorizontalMovingPlatformRight": gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects5, "StaticPlatform2": gdjs.Level4Code.GDStaticPlatform2Objects5, "StaticPlatform3": gdjs.Level4Code.GDStaticPlatform3Objects5, "VerticalMovingPlatformDown": gdjs.Level4Code.GDVerticalMovingPlatformDownObjects5, "SpinningRotatingPlatformClockwise": gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects5, "FlippingPlatform": gdjs.Level4Code.GDFlippingPlatformObjects5, "FlippingPlatform2": gdjs.Level4Code.GDFlippingPlatform2Objects5, "HorizontalMovingPlatformLeft": gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects5, "VerticalMovingPlatformUp": gdjs.Level4Code.GDVerticalMovingPlatformUpObjects5, "SpinningRotatingPlatformCounterclockwise": gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects5, "SpinningPlatformClockwise": gdjs.Level4Code.GDSpinningPlatformClockwiseObjects5, "SpinningPlatformCounterclockwise": gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects5, "LaserDemon_Beam": gdjs.Level4Code.GDLaserDemon_9595BeamObjects5, "LaserDemon_BeamPermanent": gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects5, "LaserRing": gdjs.Level4Code.GDLaserRingObjects5, "LaserDemon_Base": gdjs.Level4Code.GDLaserDemon_9595BaseObjects5, "LaserDemon_BasePermanent": gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects5});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDProjectileDeathParticlesObjects5Objects = Hashtable.newFrom({"ProjectileDeathParticles": gdjs.Level4Code.GDProjectileDeathParticlesObjects5});
gdjs.Level4Code.eventsList31 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level4Code.GDFlippingPlatformObjects5);
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level4Code.GDFlippingPlatform2Objects5);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatformLeft"), gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects5);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatformRight"), gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level4Code.GDLaserDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BasePermanent"), gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.Level4Code.GDLaserDemon_9595BeamObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BeamPermanent"), gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserRing"), gdjs.Level4Code.GDLaserRingObjects5);
gdjs.copyArray(gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4, gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5);

gdjs.copyArray(runtimeScene.getObjects("SpinningPlatformClockwise"), gdjs.Level4Code.GDSpinningPlatformClockwiseObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpinningPlatformCounterclockwise"), gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpinningRotatingPlatformClockwise"), gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpinningRotatingPlatformCounterclockwise"), gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects5);
gdjs.copyArray(gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4, gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5);

gdjs.copyArray(gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4, gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5);

gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.Level4Code.GDStaticPlatform1Objects5);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.Level4Code.GDStaticPlatform2Objects5);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.Level4Code.GDStaticPlatform3Objects5);
gdjs.copyArray(gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4, gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5);

gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatformDown"), gdjs.Level4Code.GDVerticalMovingPlatformDownObjects5);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatformUp"), gdjs.Level4Code.GDVerticalMovingPlatformUpObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595SpikeObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595SpikeObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalagmiteDemon_95959595SpikeObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595SpikeObjects5Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDStaticPlatform1Objects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformRightObjects5ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform2Objects5ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform3Objects5ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformDownObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformClockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatformObjects5ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatform2Objects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformLeftObjects5ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformUpObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformCounterclockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformClockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformCounterclockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserRingObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BasePermanentObjects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5 */
/* Reuse gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5 */
/* Reuse gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5 */
gdjs.Level4Code.GDProjectileDeathParticlesObjects5.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDProjectileDeathParticlesObjects5Objects, (( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5.length === 0 ) ? (( gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5[0].getPointX("")) :gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5[0].getPointX("")) :gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5[0].getPointX("")) :gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[0].getPointX("")) + (( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5.length === 0 ) ? (( gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5[0].getWidth()) :gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5[0].getWidth()) :gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5[0].getWidth()) :gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[0].getWidth()) / 2, (( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5.length === 0 ) ? (( gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5[0].getPointY("")) :gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5[0].getPointY("")) :gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5[0].getPointY("")) :gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[0].getPointY("")) + (( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5.length === 0 ) ? (( gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5.length === 0 ) ? (( gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5.length === 0 ) ? (( gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5[0].getHeight()) :gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5[0].getHeight()) :gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5[0].getHeight()) :gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[0].getHeight()) / 2, "Base Layer");
}{for(var i = 0, len = gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level4Code.eventsList32 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects3);
gdjs.copyArray(runtimeScene.getObjects("StalactiteDemon_Spike"), gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects3);
gdjs.copyArray(runtimeScene.getObjects("StalagmiteDemon_Spike"), gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects3);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Spike"), gdjs.Level4Code.GDTurretDemon_9595SpikeObjects3);

gdjs.Level4Code.forEachTotalCount4 = 0;
gdjs.Level4Code.forEachObjects4.length = 0;
gdjs.Level4Code.forEachCount0_4 = gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects3.length;
gdjs.Level4Code.forEachTotalCount4 += gdjs.Level4Code.forEachCount0_4;
gdjs.Level4Code.forEachObjects4.push.apply(gdjs.Level4Code.forEachObjects4,gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects3);
gdjs.Level4Code.forEachCount1_4 = gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects3.length;
gdjs.Level4Code.forEachTotalCount4 += gdjs.Level4Code.forEachCount1_4;
gdjs.Level4Code.forEachObjects4.push.apply(gdjs.Level4Code.forEachObjects4,gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects3);
gdjs.Level4Code.forEachCount2_4 = gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects3.length;
gdjs.Level4Code.forEachTotalCount4 += gdjs.Level4Code.forEachCount2_4;
gdjs.Level4Code.forEachObjects4.push.apply(gdjs.Level4Code.forEachObjects4,gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects3);
gdjs.Level4Code.forEachCount3_4 = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects3.length;
gdjs.Level4Code.forEachTotalCount4 += gdjs.Level4Code.forEachCount3_4;
gdjs.Level4Code.forEachObjects4.push.apply(gdjs.Level4Code.forEachObjects4,gdjs.Level4Code.GDTurretDemon_9595SpikeObjects3);
for (gdjs.Level4Code.forEachIndex4 = 0;gdjs.Level4Code.forEachIndex4 < gdjs.Level4Code.forEachTotalCount4;++gdjs.Level4Code.forEachIndex4) {
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length = 0;

gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length = 0;

gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length = 0;

gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length = 0;


if (gdjs.Level4Code.forEachIndex4 < gdjs.Level4Code.forEachCount0_4) {
    gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.push(gdjs.Level4Code.forEachObjects4[gdjs.Level4Code.forEachIndex4]);
}
else if (gdjs.Level4Code.forEachIndex4 < gdjs.Level4Code.forEachCount0_4+gdjs.Level4Code.forEachCount1_4) {
    gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.push(gdjs.Level4Code.forEachObjects4[gdjs.Level4Code.forEachIndex4]);
}
else if (gdjs.Level4Code.forEachIndex4 < gdjs.Level4Code.forEachCount0_4+gdjs.Level4Code.forEachCount1_4+gdjs.Level4Code.forEachCount2_4) {
    gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.push(gdjs.Level4Code.forEachObjects4[gdjs.Level4Code.forEachIndex4]);
}
else if (gdjs.Level4Code.forEachIndex4 < gdjs.Level4Code.forEachCount0_4+gdjs.Level4Code.forEachCount1_4+gdjs.Level4Code.forEachCount2_4+gdjs.Level4Code.forEachCount3_4) {
    gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.push(gdjs.Level4Code.forEachObjects4[gdjs.Level4Code.forEachIndex4]);
}
let isConditionTrue_0 = false;
if (true) {

{ //Subevents: 
gdjs.Level4Code.eventsList31(runtimeScene);} //Subevents end.
}
}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDBloodParticlesObjects5Objects = Hashtable.newFrom({"BloodParticles": gdjs.Level4Code.GDBloodParticlesObjects5});
gdjs.Level4Code.eventsList33 = function(runtimeScene) {

{



}


{

gdjs.copyArray(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4, gdjs.Level4Code.GDMiteDemon_9595MiteObjects5);

gdjs.copyArray(gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4, gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[i].getTimerElapsedTimeInSecondsOrNaN("TimeAlive") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[i].getVariables().get("Lifetime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[k] = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[i].getTimerElapsedTimeInSecondsOrNaN("TimeAlive") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[i].getVariables().get("Lifetime"))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[k] = gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects5 */
/* Reuse gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5 */
gdjs.Level4Code.GDBloodParticlesObjects5.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDBloodParticlesObjects5Objects, (( gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[0].getPointX("")) :gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[0].getPointX("")) + (( gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[0].getWidth()) :gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[0].getWidth()) / 2, (( gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[0].getPointY("")) :gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[0].getPointY("")) + (( gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length === 0 ) ? (( gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[0].getHeight()) :gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[0].getHeight()) / 2, "");
}{for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level4Code.eventsList34 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Mite"), gdjs.Level4Code.GDMiteDemon_9595MiteObjects3);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Spike"), gdjs.Level4Code.GDTurretDemon_9595SpikeObjects3);

gdjs.Level4Code.forEachTotalCount4 = 0;
gdjs.Level4Code.forEachObjects4.length = 0;
gdjs.Level4Code.forEachCount0_4 = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects3.length;
gdjs.Level4Code.forEachTotalCount4 += gdjs.Level4Code.forEachCount0_4;
gdjs.Level4Code.forEachObjects4.push.apply(gdjs.Level4Code.forEachObjects4,gdjs.Level4Code.GDTurretDemon_9595SpikeObjects3);
gdjs.Level4Code.forEachCount1_4 = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length;
gdjs.Level4Code.forEachTotalCount4 += gdjs.Level4Code.forEachCount1_4;
gdjs.Level4Code.forEachObjects4.push.apply(gdjs.Level4Code.forEachObjects4,gdjs.Level4Code.GDMiteDemon_9595MiteObjects3);
for (gdjs.Level4Code.forEachIndex4 = 0;gdjs.Level4Code.forEachIndex4 < gdjs.Level4Code.forEachTotalCount4;++gdjs.Level4Code.forEachIndex4) {
gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length = 0;

gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length = 0;


if (gdjs.Level4Code.forEachIndex4 < gdjs.Level4Code.forEachCount0_4) {
    gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.push(gdjs.Level4Code.forEachObjects4[gdjs.Level4Code.forEachIndex4]);
}
else if (gdjs.Level4Code.forEachIndex4 < gdjs.Level4Code.forEachCount0_4+gdjs.Level4Code.forEachCount1_4) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.push(gdjs.Level4Code.forEachObjects4[gdjs.Level4Code.forEachIndex4]);
}
let isConditionTrue_0 = false;
if (true) {

{ //Subevents: 
gdjs.Level4Code.eventsList33(runtimeScene);} //Subevents end.
}
}

}


};gdjs.Level4Code.eventsList35 = function(runtimeScene) {

};gdjs.Level4Code.eventsList36 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level4Code.GDFireDemonObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDFireDemonObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDFireDemonObjects4[i].getBehavior("Animation").getAnimationName() == "Fire" ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDFireDemonObjects4[k] = gdjs.Level4Code.GDFireDemonObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDFireDemonObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDFireDemonObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDFireDemonObjects4[i].getVariableBoolean(gdjs.Level4Code.GDFireDemonObjects4[i].getVariables().getFromIndex(1), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDFireDemonObjects4[k] = gdjs.Level4Code.GDFireDemonObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDFireDemonObjects4.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDFireDemonObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDFireDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFireDemonObjects4[i].setVariableBoolean(gdjs.Level4Code.GDFireDemonObjects4[i].getVariables().getFromIndex(1), false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDFireDemonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFireDemonObjects4[i].getBehavior("Effect").enableEffect("ChromaticAberration", true);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level4Code.GDFireDemonObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDFireDemonObjects3.length;i<l;++i) {
    if ( !(gdjs.Level4Code.GDFireDemonObjects3[i].getBehavior("Animation").getAnimationName() == "Fire") ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDFireDemonObjects3[k] = gdjs.Level4Code.GDFireDemonObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDFireDemonObjects3.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDFireDemonObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDFireDemonObjects3[i].getVariableBoolean(gdjs.Level4Code.GDFireDemonObjects3[i].getVariables().getFromIndex(1), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDFireDemonObjects3[k] = gdjs.Level4Code.GDFireDemonObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDFireDemonObjects3.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDFireDemonObjects3 */
{for(var i = 0, len = gdjs.Level4Code.GDFireDemonObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDFireDemonObjects3[i].setVariableBoolean(gdjs.Level4Code.GDFireDemonObjects3[i].getVariables().getFromIndex(1), true);
}
}{for(var i = 0, len = gdjs.Level4Code.GDFireDemonObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDFireDemonObjects3[i].getBehavior("Effect").enableEffect("ChromaticAberration", false);
}
}}

}


};gdjs.Level4Code.eventsList37 = function(runtimeScene) {

};gdjs.Level4Code.eventsList38 = function(runtimeScene) {

};gdjs.Level4Code.eventsList39 = function(runtimeScene) {

};gdjs.Level4Code.eventsList40 = function(runtimeScene) {

};gdjs.Level4Code.eventsList41 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level4Code.GDLaserDemon_9595BaseObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariableNumber(gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().getFromIndex(1)) == 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().getFromIndex(4), false);
}
}}

}


};gdjs.Level4Code.eventsList42 = function(runtimeScene) {

{



}


{

gdjs.copyArray(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5, gdjs.Level4Code.GDLaserDemon_9595BaseObjects6);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(2), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects6 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "linear", Math.max(gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(1).getAsNumber(), 0.15), false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(2), true);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(3), false);
}
}}

}


{



}


{

gdjs.copyArray(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5, gdjs.Level4Code.GDLaserDemon_9595BaseObjects6);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(3), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(4), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(0).getAsNumber() - 0.5 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length = k;
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects6 */
gdjs.copyArray(runtimeScene.getObjects("Test"), gdjs.Level4Code.GDTestObjects6);
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getBehavior("Tween").addObjectOpacityTween2("ChargeDown", 50, "linear", 0.5, false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(3), true);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(2), false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDTestObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDTestObjects6[i].getBehavior("Text").setText(gdjs.evtTools.common.toString(gdjs.randomFloat(1)));
}
}}

}


{



}


{

/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects5 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(3), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(2), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= (2 * gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(0).getAsNumber()) - gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(1).getAsNumber() - (1 - gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(1).getAsNumber()) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length = k;
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects5 */
gdjs.copyArray(runtimeScene.getObjects("Test2"), gdjs.Level4Code.GDTest2Objects5);
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "linear", 1, false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(2), true);
}
}{for(var i = 0, len = gdjs.Level4Code.GDTest2Objects5.length ;i < len;++i) {
    gdjs.Level4Code.GDTest2Objects5[i].getBehavior("Text").setText(gdjs.evtTools.common.toString(gdjs.randomFloat(1)));
}
}}

}


};gdjs.Level4Code.eventsList43 = function(runtimeScene) {

{



}


{

gdjs.copyArray(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5, gdjs.Level4Code.GDLaserDemon_9595BaseObjects6);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(2), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(1).getAsNumber() - 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects6 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "linear", 1, false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(2), true);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects6[i].getVariables().getFromIndex(3), false);
}
}}

}


{



}


{

/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects5 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(3), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(4), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(0).getAsNumber() - 0.5 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length = k;
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects5 */
gdjs.copyArray(runtimeScene.getObjects("Test"), gdjs.Level4Code.GDTestObjects5);
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getBehavior("Tween").addObjectOpacityTween2("ChargeDown", 50, "linear", 0.5, false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(3), true);
}
}{for(var i = 0, len = gdjs.Level4Code.GDTestObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDTestObjects5[i].getBehavior("Text").setText(gdjs.evtTools.common.toString(gdjs.randomFloat(1)));
}
}}

}


};gdjs.Level4Code.eventsList44 = function(runtimeScene) {

{

/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects4 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariableNumber(gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().getFromIndex(1)) != 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().getFromIndex(4), true);
}
}}

}


};gdjs.Level4Code.eventsList45 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level4Code.GDLaserDemon_9595BaseObjects5);
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].resetTimer("FireTimer");
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getBehavior("Opacity").setOpacity(50);
}
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level4Code.GDLaserDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariableNumber(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(1)) < 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length = k;
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList42(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level4Code.GDLaserDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariableNumber(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(1)) >= 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length = k;
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList43(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level4Code.GDLaserDemon_9595BaseObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(4), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(1).getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects5 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].getVariables().getFromIndex(4), false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects5[i].resetTimer("FireTimer");
}
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level4Code.GDLaserDemon_9595BaseObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= 2 * gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().getFromIndex(0).getAsNumber() - gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().getFromIndex(1).getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BaseObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].resetTimer("FireTimer");
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().getFromIndex(2), false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BaseObjects4[i].getVariables().getFromIndex(3), false);
}
}
{ //Subevents
gdjs.Level4Code.eventsList44(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamObjects3ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamSingleObjects3Objects = Hashtable.newFrom({"LaserDemon_Beam": gdjs.Level4Code.GDLaserDemon_9595BeamObjects3, "LaserDemon_BeamSingle": gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3});
gdjs.Level4Code.eventsList46 = function(runtimeScene) {

{



}


{

gdjs.copyArray(gdjs.Level4Code.GDLaserDemon_9595BeamObjects3, gdjs.Level4Code.GDLaserDemon_9595BeamObjects4);

gdjs.copyArray(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3, gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedUp"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedUp"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("FireOffsetTime").getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("FireOffsetTime").getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedUp"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedUp"), true);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].resetTimer("FireTimer");
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getBehavior("Opacity").setOpacity(60);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getBehavior("Opacity").setOpacity(60);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].setPosition(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("OrgPosition_X").getAsNumber(),gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("OrgPosition_Y").getAsNumber());
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].setPosition(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("OrgPosition_X").getAsNumber(),gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("OrgPosition_Y").getAsNumber());
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "linear", 0.15, false);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "linear", 0.15, false);
}
}}

}


{



}


{

gdjs.copyArray(gdjs.Level4Code.GDLaserDemon_9595BeamObjects3, gdjs.Level4Code.GDLaserDemon_9595BeamObjects4);

gdjs.copyArray(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3, gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedDown"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedDown"), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedUp"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedUp"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("OnOffTime").getAsNumber() - 0.3 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("OnOffTime").getAsNumber() - 0.3 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length = k;
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedDown"), true);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedDown"), true);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getBehavior("Tween").addObjectOpacityTween2("ChargeDown", 60, "linear", 0.3, false);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getBehavior("Tween").addObjectOpacityTween2("ChargeDown", 60, "linear", 0.3, false);
}
}}

}


{



}


{

gdjs.copyArray(gdjs.Level4Code.GDLaserDemon_9595BeamObjects3, gdjs.Level4Code.GDLaserDemon_9595BeamObjects4);

gdjs.copyArray(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3, gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedDown"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("CurrentlyChargingOrHasChargedDown"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getY() != 6000 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getY() != 6000 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().get("OnOffTime").getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().get("OnOffTime").getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[k] = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length = k;
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamObjects4 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].setPosition(0,4000);
}
}}

}


{



}


{

/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamObjects3 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[i].getVariables().get("CurrentlyChargingOrHasChargedDown"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[k] = gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[i].getVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[i].getVariables().get("CurrentlyChargingOrHasChargedDown"), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[k] = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= 2 * gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[i].getVariables().get("OnOffTime").getAsNumber() - gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[i].getVariables().get("FireOffsetTime").getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[k] = gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamObjects3.length = k;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[i].getTimerElapsedTimeInSecondsOrNaN("FireTimer") >= 2 * gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[i].getVariables().get("OnOffTime").getAsNumber() - gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[i].getVariables().get("FireOffsetTime").getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[k] = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamObjects3 */
/* Reuse gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[i].resetTimer("FireTimer");
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[i].getVariables().get("CurrentlyChargingOrHasChargedUp"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[i].getVariables().get("CurrentlyChargingOrHasChargedUp"), false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamObjects3[i].getVariables().get("CurrentlyChargingOrHasChargedDown"), false);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[i].setVariableBoolean(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3[i].getVariables().get("CurrentlyChargingOrHasChargedDown"), false);
}
}}

}


};gdjs.Level4Code.eventsList47 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.Level4Code.GDLaserDemon_9595BeamObjects4);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BeamSingle"), gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].resetTimer("FireTimer");
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].resetTimer("FireTimer");
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].setPosition(0,4000);
}
for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].setPosition(0,4000);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.Level4Code.GDLaserDemon_9595BeamObjects3);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BeamSingle"), gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamObjects3ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamSingleObjects3Objects);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList46(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList48 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.Level4Code.GDLaserDemon_9595BeamObjects4);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BeamSingle"), gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].returnVariable(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().getFromIndex(5)).setNumber((gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getPointX("")));
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].returnVariable(gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getVariables().getFromIndex(6)).setNumber((gdjs.Level4Code.GDLaserDemon_9595BeamObjects4[i].getPointY("")));
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].returnVariable(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().getFromIndex(5)).setNumber((gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getPointX("")));
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].returnVariable(gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getVariables().getFromIndex(6)).setNumber((gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4[i].getPointY("")));
}
}
{ //Subevents
gdjs.Level4Code.eventsList41(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.Level4Code.eventsList45(runtimeScene);
}


{



}


{


gdjs.Level4Code.eventsList47(runtimeScene);
}


};gdjs.Level4Code.asyncCallback33530740 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_BeamPermanent"), gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects7);

{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects7.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects7[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 2);
}
}}
gdjs.Level4Code.eventsList49 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects6) asyncObjectsList.addObject("LaserDemon_BeamPermanent", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.Level4Code.asyncCallback33530740(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level4Code.asyncCallback33528972 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_BeamPermanent"), gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects6);

{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects6[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 1);
}
}
{ //Subevents
gdjs.Level4Code.eventsList49(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.Level4Code.eventsList50 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects5) asyncObjectsList.addObject("LaserDemon_BeamPermanent", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.Level4Code.asyncCallback33528972(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level4Code.asyncCallback33529580 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_BeamPermanent"), gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects5);

{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects5[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.5);
}
}
{ //Subevents
gdjs.Level4Code.eventsList50(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.Level4Code.eventsList51 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
for (const obj of gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4) asyncObjectsList.addObject("LaserDemon_BeamPermanent", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.Level4Code.asyncCallback33529580(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level4Code.asyncCallback33532996 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_BeamPermanentSingle"), gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects7);

{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects7.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects7[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 2);
}
}}
gdjs.Level4Code.eventsList52 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects6) asyncObjectsList.addObject("LaserDemon_BeamPermanentSingle", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.Level4Code.asyncCallback33532996(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level4Code.asyncCallback33532468 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_BeamPermanentSingle"), gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects6);

{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects6[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 1);
}
}
{ //Subevents
gdjs.Level4Code.eventsList52(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.Level4Code.eventsList53 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects5) asyncObjectsList.addObject("LaserDemon_BeamPermanentSingle", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.Level4Code.asyncCallback33532468(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level4Code.asyncCallback33531812 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserDemon_BeamPermanentSingle"), gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects5);

{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects5[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.5);
}
}
{ //Subevents
gdjs.Level4Code.eventsList53(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.Level4Code.eventsList54 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
for (const obj of gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4) asyncObjectsList.addObject("LaserDemon_BeamPermanentSingle", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.Level4Code.asyncCallback33531812(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level4Code.asyncCallback33535020 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserRing"), gdjs.Level4Code.GDLaserRingObjects6);

{for(var i = 0, len = gdjs.Level4Code.GDLaserRingObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserRingObjects6[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 2);
}
}}
gdjs.Level4Code.eventsList55 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.Level4Code.GDLaserRingObjects5) asyncObjectsList.addObject("LaserRing", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.Level4Code.asyncCallback33535020(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level4Code.asyncCallback33534460 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserRing"), gdjs.Level4Code.GDLaserRingObjects5);

{for(var i = 0, len = gdjs.Level4Code.GDLaserRingObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserRingObjects5[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 1);
}
}
{ //Subevents
gdjs.Level4Code.eventsList55(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.Level4Code.eventsList56 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.Level4Code.GDLaserRingObjects4) asyncObjectsList.addObject("LaserRing", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.Level4Code.asyncCallback33534460(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level4Code.asyncCallback33534052 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("LaserRing"), gdjs.Level4Code.GDLaserRingObjects4);

{for(var i = 0, len = gdjs.Level4Code.GDLaserRingObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserRingObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.5);
}
}
{ //Subevents
gdjs.Level4Code.eventsList56(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.Level4Code.eventsList57 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
for (const obj of gdjs.Level4Code.GDLaserRingObjects3) asyncObjectsList.addObject("LaserRing", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.05), (runtimeScene) => (gdjs.Level4Code.asyncCallback33534052(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.Level4Code.eventsList58 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BasePermanent"), gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getBehavior("Opacity").setOpacity(40);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4[i].getBehavior("Tween").addObjectOpacityTween2("ChargeUp", 255, "easeInQuad", 0.15, false);
}
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BeamPermanent"), gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.25);
}
}
{ //Subevents
gdjs.Level4Code.eventsList51(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BeamPermanentSingle"), gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.25);
}
}
{ //Subevents
gdjs.Level4Code.eventsList54(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.Level4Code.GDLaserRingObjects3 */
{for(var i = 0, len = gdjs.Level4Code.GDLaserRingObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserRingObjects3[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "bloomScale", 0);
}
}{for(var i = 0, len = gdjs.Level4Code.GDLaserRingObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserRingObjects3[i].getBehavior("Effect").setEffectDoubleParameter("Bloom", "brightness", 0.25);
}
}
{ //Subevents
gdjs.Level4Code.eventsList57(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList59 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("LaserRing"), gdjs.Level4Code.GDLaserRingObjects3);
{for(var i = 0, len = gdjs.Level4Code.GDLaserRingObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDLaserRingObjects3[i].setZOrder(9999);
}
}
{ //Subevents
gdjs.Level4Code.eventsList58(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList60 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Spike"), gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[i].addForceTowardObject((gdjs.Level4Code.GDPlayerObjects4.length !== 0 ? gdjs.Level4Code.GDPlayerObjects4[0] : null), gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[i].getVariables().getFromIndex(1).getAsNumber(), 0);
}
}{for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4[i].rotateTowardPosition((( gdjs.Level4Code.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects4[0].getCenterXInScene()), (( gdjs.Level4Code.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects4[0].getCenterYInScene()), 90, runtimeScene);
}
}}

}


};gdjs.Level4Code.eventsList61 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects3);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Eye"), gdjs.Level4Code.GDTurretDemon_9595EyeObjects3);
{for(var i = 0, len = gdjs.Level4Code.GDTurretDemon_9595EyeObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDTurretDemon_9595EyeObjects3[i].rotateTowardPosition((( gdjs.Level4Code.GDPlayerObjects3.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects3[0].getCenterXInScene()), (( gdjs.Level4Code.GDPlayerObjects3.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects3[0].getCenterYInScene()), 0, runtimeScene);
}
}}

}


};gdjs.Level4Code.eventsList62 = function(runtimeScene) {

{


gdjs.Level4Code.eventsList60(runtimeScene);
}


{


gdjs.Level4Code.eventsList61(runtimeScene);
}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects5});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595ShockwaveObjects5Objects = Hashtable.newFrom({"ShockwaveDemon_Shockwave": gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5});
gdjs.Level4Code.eventsList63 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.Level4Code.GDPlayerObjects6, gdjs.Level4Code.GDPlayerObjects7);

{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects7.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects7[i].pauseTimer("CameraDecreaseSpeed");
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects7.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects7[i].resetTimer("CameraIncreaseSpeed");
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects7.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects7[i].unpauseTimer("CameraIncreaseSpeed");
}
}}

}


{



}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.Level4Code.GDPlayerObjects6 */
gdjs.copyArray(gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5, gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects6);

{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects6[i].getBehavior("PlatformerObject").setMaxSpeed((gdjs.Level4Code.GDPlayerObjects6[i].getBehavior("PlatformerObject").getMaxSpeed()) / ((gdjs.RuntimeObject.getVariableNumber(((gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects6.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects6[0].getVariables()).getFromIndex(0))) * 3));
}
}}

}


};gdjs.Level4Code.eventsList64 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.Level4Code.GDPlayerObjects6, gdjs.Level4Code.GDPlayerObjects7);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects7.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects7[i].getTimerElapsedTimeInSecondsOrNaN("CameraIncreaseSpeed") <= 0.5 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects7[k] = gdjs.Level4Code.GDPlayerObjects7[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects7.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDPlayerObjects7 */
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects7.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects7[i].getBehavior("SmoothCamera").SetLeftwardSpeed(0.9 + (gdjs.Level4Code.GDPlayerObjects7[i].getTimerElapsedTimeInSeconds("CameraIncreaseSpeed")) / 5, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects7.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects7[i].getBehavior("SmoothCamera").SetRightwardSpeed(0.9 + (gdjs.Level4Code.GDPlayerObjects7[i].getTimerElapsedTimeInSeconds("CameraIncreaseSpeed")) / 5, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects7.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects7[i].getBehavior("SmoothCamera").SetUpwardSpeed(0.75 + (gdjs.Level4Code.GDPlayerObjects7[i].getTimerElapsedTimeInSeconds("CameraIncreaseSpeed")) / 2, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects7.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects7[i].getBehavior("SmoothCamera").SetDownwardSpeed(0.9 + (gdjs.Level4Code.GDPlayerObjects7[i].getTimerElapsedTimeInSeconds("CameraIncreaseSpeed")) / 5, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{

/* Reuse gdjs.Level4Code.GDPlayerObjects6 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects6.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects6[i].getTimerElapsedTimeInSecondsOrNaN("CameraIncreaseSpeed") > 0.5 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects6[k] = gdjs.Level4Code.GDPlayerObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects6.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDPlayerObjects6 */
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects6[i].pauseTimer("CameraIncreaseSpeed");
}
}}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects5Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects5});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDStaticPlatform1Objects5ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform2Objects5ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform3Objects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformLeftObjects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformRightObjects5ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformUpObjects5ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformDownObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformClockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformCounterclockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformClockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformCounterclockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatformObjects5ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatform2Objects5Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.Level4Code.GDStaticPlatform1Objects5, "StaticPlatform2": gdjs.Level4Code.GDStaticPlatform2Objects5, "StaticPlatform3": gdjs.Level4Code.GDStaticPlatform3Objects5, "HorizontalMovingPlatformLeft": gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects5, "HorizontalMovingPlatformRight": gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects5, "VerticalMovingPlatformUp": gdjs.Level4Code.GDVerticalMovingPlatformUpObjects5, "VerticalMovingPlatformDown": gdjs.Level4Code.GDVerticalMovingPlatformDownObjects5, "SpinningPlatformClockwise": gdjs.Level4Code.GDSpinningPlatformClockwiseObjects5, "SpinningPlatformCounterclockwise": gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects5, "SpinningRotatingPlatformClockwise": gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects5, "SpinningRotatingPlatformCounterclockwise": gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects5, "FlippingPlatform": gdjs.Level4Code.GDFlippingPlatformObjects5, "FlippingPlatform2": gdjs.Level4Code.GDFlippingPlatform2Objects5});
gdjs.Level4Code.eventsList65 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.Level4Code.GDPlayerObjects5, gdjs.Level4Code.GDPlayerObjects6);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects6.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects6[i].getBehavior("PlatformerObject").isFalling() ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects6[k] = gdjs.Level4Code.GDPlayerObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects6.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDPlayerObjects6 */
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects6[i].getBehavior("PlatformerObject").setCurrentFallSpeed(0);
}
}}

}


{



}


{

gdjs.copyArray(gdjs.Level4Code.GDPlayerObjects5, gdjs.Level4Code.GDPlayerObjects6);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects6.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects6[i].getVariableBoolean(gdjs.Level4Code.GDPlayerObjects6[i].getVariables().getFromIndex(5), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects6[k] = gdjs.Level4Code.GDPlayerObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects6.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDPlayerObjects6 */
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects6[i].setVariableBoolean(gdjs.Level4Code.GDPlayerObjects6[i].getVariables().getFromIndex(5), true);
}
}
{ //Subevents
gdjs.Level4Code.eventsList63(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.copyArray(gdjs.Level4Code.GDPlayerObjects5, gdjs.Level4Code.GDPlayerObjects6);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects6.length;i<l;++i) {
    if ( !(gdjs.Level4Code.GDPlayerObjects6[i].timerPaused("CameraIncreaseSpeed")) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects6[k] = gdjs.Level4Code.GDPlayerObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects6.length = k;
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList64(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level4Code.GDFlippingPlatformObjects5);
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level4Code.GDFlippingPlatform2Objects5);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatformLeft"), gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects5);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatformRight"), gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects5);
/* Reuse gdjs.Level4Code.GDPlayerObjects5 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5 */
gdjs.copyArray(runtimeScene.getObjects("SpinningPlatformClockwise"), gdjs.Level4Code.GDSpinningPlatformClockwiseObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpinningPlatformCounterclockwise"), gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpinningRotatingPlatformClockwise"), gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpinningRotatingPlatformCounterclockwise"), gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects5);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.Level4Code.GDStaticPlatform1Objects5);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.Level4Code.GDStaticPlatform2Objects5);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.Level4Code.GDStaticPlatform3Objects5);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatformDown"), gdjs.Level4Code.GDVerticalMovingPlatformDownObjects5);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatformUp"), gdjs.Level4Code.GDVerticalMovingPlatformUpObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects5.length;i<l;++i) {
    if ( !(gdjs.Level4Code.GDPlayerObjects5[i].getBehavior("PlatformerObject").isFalling()) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects5[k] = gdjs.Level4Code.GDPlayerObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects5.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects5Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDStaticPlatform1Objects5ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform2Objects5ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform3Objects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformLeftObjects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformRightObjects5ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformUpObjects5ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformDownObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformClockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformCounterclockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformClockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformCounterclockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatformObjects5ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatform2Objects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = (gdjs.evtTools.common.mod((( gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5[0].getAngle()), 360) == 90);
}
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDPlayerObjects5 */
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects5[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects6Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects6});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595ShockwaveObjects6Objects = Hashtable.newFrom({"ShockwaveDemon_Shockwave": gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects6});
gdjs.Level4Code.eventsList66 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.Level4Code.GDPlayerObjects5, gdjs.Level4Code.GDPlayerObjects6);

{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects6[i].pauseTimer("CameraIncreaseSpeed");
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects6[i].resetTimer("CameraDecreaseSpeed");
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects6[i].unpauseTimer("CameraDecreaseSpeed");
}
}}

}


{



}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.Level4Code.GDPlayerObjects5 */
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects5[i].getBehavior("PlatformerObject").setMaxSpeed(600);
}
}}

}


};gdjs.Level4Code.eventsList67 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.Level4Code.GDPlayerObjects4, gdjs.Level4Code.GDPlayerObjects5);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects5[i].getTimerElapsedTimeInSecondsOrNaN("CameraDecreaseSpeed") > 0.5 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects5[k] = gdjs.Level4Code.GDPlayerObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects5.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects5[i].getTimerElapsedTimeInSecondsOrNaN("CameraDecreaseSpeed") <= 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects5[k] = gdjs.Level4Code.GDPlayerObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects5.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDPlayerObjects5 */
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects5[i].getBehavior("SmoothCamera").SetLeftwardSpeed(1 - ((gdjs.Level4Code.GDPlayerObjects5[i].getTimerElapsedTimeInSeconds("CameraDecreaseSpeed")) - 0.5) / 5, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects5[i].getBehavior("SmoothCamera").SetRightwardSpeed(1 - ((gdjs.Level4Code.GDPlayerObjects5[i].getTimerElapsedTimeInSeconds("CameraDecreaseSpeed")) - 0.5) / 5, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects5[i].getBehavior("SmoothCamera").SetUpwardSpeed(1 - ((gdjs.Level4Code.GDPlayerObjects5[i].getTimerElapsedTimeInSeconds("CameraDecreaseSpeed")) - 0.5) / 2, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects5[i].getBehavior("SmoothCamera").SetDownwardSpeed(1 - ((gdjs.Level4Code.GDPlayerObjects5[i].getTimerElapsedTimeInSeconds("CameraDecreaseSpeed")) - 0.5) / 5, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{

/* Reuse gdjs.Level4Code.GDPlayerObjects4 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects4[i].getTimerElapsedTimeInSecondsOrNaN("CameraDecreaseSpeed") > 1 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects4[k] = gdjs.Level4Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDPlayerObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].pauseTimer("CameraDecreaseSpeed");
}
}}

}


};gdjs.Level4Code.eventsList68 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects5);
gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Shockwave"), gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects5Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595ShockwaveObjects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDPlayerObjects5 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5 */
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects5[i].addPolarForce(180 + (( gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5[0].getAngle()), (gdjs.RuntimeObject.getVariableNumber(((gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5[0].getVariables()).getFromIndex(1))), 0);
}
}
{ //Subevents
gdjs.Level4Code.eventsList65(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects5);
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects5.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects5[i].getVariableBoolean(gdjs.Level4Code.GDPlayerObjects5[i].getVariables().getFromIndex(5), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects5[k] = gdjs.Level4Code.GDPlayerObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects5.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.Level4Code.GDPlayerObjects5_1final.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.Level4Code.GDPlayerObjects5, gdjs.Level4Code.GDPlayerObjects6);

for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects6.length;i<l;++i) {
    if ( !(gdjs.Level4Code.GDPlayerObjects6[i].getY() > gdjs.evtTools.camera.getCameraBorderTop(runtimeScene, "", 0) + 100) ) {
        isConditionTrue_1 = true;
        gdjs.Level4Code.GDPlayerObjects6[k] = gdjs.Level4Code.GDPlayerObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects6.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level4Code.GDPlayerObjects6.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDPlayerObjects5_1final.indexOf(gdjs.Level4Code.GDPlayerObjects6[j]) === -1 )
            gdjs.Level4Code.GDPlayerObjects5_1final.push(gdjs.Level4Code.GDPlayerObjects6[j]);
    }
}
}
{
gdjs.copyArray(gdjs.Level4Code.GDPlayerObjects5, gdjs.Level4Code.GDPlayerObjects6);

gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Shockwave"), gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects6);
isConditionTrue_1 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects6Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595ShockwaveObjects6Objects, true, runtimeScene, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level4Code.GDPlayerObjects6.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDPlayerObjects5_1final.indexOf(gdjs.Level4Code.GDPlayerObjects6[j]) === -1 )
            gdjs.Level4Code.GDPlayerObjects5_1final.push(gdjs.Level4Code.GDPlayerObjects6[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects6.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5_1final.indexOf(gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects6[j]) === -1 )
            gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5_1final.push(gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects6[j]);
    }
}
}
{
gdjs.copyArray(gdjs.Level4Code.GDPlayerObjects5_1final, gdjs.Level4Code.GDPlayerObjects5);
gdjs.copyArray(gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5_1final, gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5);
}
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDPlayerObjects5 */
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects5[i].setVariableBoolean(gdjs.Level4Code.GDPlayerObjects5[i].getVariables().getFromIndex(5), false);
}
}
{ //Subevents
gdjs.Level4Code.eventsList66(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects4.length;i<l;++i) {
    if ( !(gdjs.Level4Code.GDPlayerObjects4[i].timerPaused("CameraDecreaseSpeed")) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects4[k] = gdjs.Level4Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList67(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects5Objects = Hashtable.newFrom({"MiteDemon_Mite": gdjs.Level4Code.GDMiteDemon_9595MiteObjects5});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595ShockwaveObjects5Objects = Hashtable.newFrom({"ShockwaveDemon_Shockwave": gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects5Objects = Hashtable.newFrom({"MiteDemon_Mite": gdjs.Level4Code.GDMiteDemon_9595MiteObjects5});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDStaticPlatform1Objects5ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform2Objects5ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform3Objects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformLeftObjects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformRightObjects5ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformUpObjects5ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformDownObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformClockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformCounterclockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformClockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformCounterclockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatformObjects5ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatform2Objects5Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.Level4Code.GDStaticPlatform1Objects5, "StaticPlatform2": gdjs.Level4Code.GDStaticPlatform2Objects5, "StaticPlatform3": gdjs.Level4Code.GDStaticPlatform3Objects5, "HorizontalMovingPlatformLeft": gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects5, "HorizontalMovingPlatformRight": gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects5, "VerticalMovingPlatformUp": gdjs.Level4Code.GDVerticalMovingPlatformUpObjects5, "VerticalMovingPlatformDown": gdjs.Level4Code.GDVerticalMovingPlatformDownObjects5, "SpinningPlatformClockwise": gdjs.Level4Code.GDSpinningPlatformClockwiseObjects5, "SpinningPlatformCounterclockwise": gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects5, "SpinningRotatingPlatformClockwise": gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects5, "SpinningRotatingPlatformCounterclockwise": gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects5, "FlippingPlatform": gdjs.Level4Code.GDFlippingPlatformObjects5, "FlippingPlatform2": gdjs.Level4Code.GDFlippingPlatform2Objects5});
gdjs.Level4Code.eventsList69 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.Level4Code.GDMiteDemon_9595MiteObjects5, gdjs.Level4Code.GDMiteDemon_9595MiteObjects6);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595MiteObjects6.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects6[i].getBehavior("PlatformerObject").isFalling() ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595MiteObjects6[k] = gdjs.Level4Code.GDMiteDemon_9595MiteObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595MiteObjects6.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects6 */
{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects6[i].getBehavior("PlatformerObject").setCurrentFallSpeed(0);
}
}}

}


{



}


{

gdjs.copyArray(gdjs.Level4Code.GDMiteDemon_9595MiteObjects5, gdjs.Level4Code.GDMiteDemon_9595MiteObjects6);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595MiteObjects6.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects6[i].getVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595MiteObjects6[i].getVariables().getFromIndex(6), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595MiteObjects6[k] = gdjs.Level4Code.GDMiteDemon_9595MiteObjects6[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595MiteObjects6.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects6 */
gdjs.copyArray(gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5, gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects6);

{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects6[i].setVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595MiteObjects6[i].getVariables().getFromIndex(6), true);
}
}{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects6.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects6[i].getBehavior("PlatformerObject").setMaxSpeed((gdjs.Level4Code.GDMiteDemon_9595MiteObjects6[i].getBehavior("PlatformerObject").getMaxSpeed()) / (gdjs.RuntimeObject.getVariableNumber(((gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects6.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects6[0].getVariables()).getFromIndex(0))));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level4Code.GDFlippingPlatformObjects5);
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level4Code.GDFlippingPlatform2Objects5);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatformLeft"), gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects5);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatformRight"), gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects5);
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects5 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5 */
gdjs.copyArray(runtimeScene.getObjects("SpinningPlatformClockwise"), gdjs.Level4Code.GDSpinningPlatformClockwiseObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpinningPlatformCounterclockwise"), gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpinningRotatingPlatformClockwise"), gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpinningRotatingPlatformCounterclockwise"), gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects5);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.Level4Code.GDStaticPlatform1Objects5);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.Level4Code.GDStaticPlatform2Objects5);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.Level4Code.GDStaticPlatform3Objects5);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatformDown"), gdjs.Level4Code.GDVerticalMovingPlatformDownObjects5);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatformUp"), gdjs.Level4Code.GDVerticalMovingPlatformUpObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length;i<l;++i) {
    if ( !(gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[i].getBehavior("PlatformerObject").isFalling()) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[k] = gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects5Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDStaticPlatform1Objects5ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform2Objects5ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform3Objects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformLeftObjects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformRightObjects5ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformUpObjects5ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformDownObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformClockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformCounterclockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformClockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformCounterclockwiseObjects5ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatformObjects5ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatform2Objects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = (gdjs.evtTools.common.mod((( gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5[0].getAngle()), 360) == 90);
}
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects5 */
{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4Objects = Hashtable.newFrom({"MiteDemon_Mite": gdjs.Level4Code.GDMiteDemon_9595MiteObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595ShockwaveObjects4Objects = Hashtable.newFrom({"ShockwaveDemon_Shockwave": gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4});
gdjs.Level4Code.eventsList70 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Mite"), gdjs.Level4Code.GDMiteDemon_9595MiteObjects5);
gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Shockwave"), gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects5Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595ShockwaveObjects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects5 */
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5 */
{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[i].addPolarForce(180 + (( gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5[0].getAngle()), (gdjs.RuntimeObject.getVariableNumber(((gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5[0].getVariables()).getFromIndex(1))), 0);
}
}
{ //Subevents
gdjs.Level4Code.eventsList69(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Mite"), gdjs.Level4Code.GDMiteDemon_9595MiteObjects4);
gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Shockwave"), gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].getVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].getVariables().getFromIndex(6), true) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595ShockwaveObjects4Objects, true, runtimeScene, false);
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].setVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].getVariables().getFromIndex(6), false);
}
}{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].getBehavior("PlatformerObject").setMaxSpeed(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].getVariables().getFromIndex(0).getAsNumber());
}
}}

}


};gdjs.Level4Code.eventsList71 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Shockwave"), gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4[i].getTimerElapsedTimeInSecondsOrNaN("TimeAlive") >= 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4[k] = gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4.length;i<l;++i) {
    if ( !(gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4[i].getBehavior("Tween").isPlaying("Despawn")) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4[k] = gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4[i].getBehavior("Tween").addObjectOpacityTween2("Despawn", 0, "linear", (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4[i].getVariables().getFromIndex(0))), false);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Shockwave"), gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3[i].getTimerElapsedTimeInSecondsOrNaN("TimeAlive") >= (gdjs.RuntimeObject.getVariableNumber(gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3[i].getVariables().getFromIndex(0))) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3[k] = gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3 */
{for(var i = 0, len = gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level4Code.eventsList72 = function(runtimeScene) {

{


gdjs.Level4Code.eventsList68(runtimeScene);
}


{


gdjs.Level4Code.eventsList70(runtimeScene);
}


{


gdjs.Level4Code.eventsList71(runtimeScene);
}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects5Objects = Hashtable.newFrom({"MiteDemon_Mite": gdjs.Level4Code.GDMiteDemon_9595MiteObjects5});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDFlyingDemonObjects5ObjectsGDgdjs_9546Level4Code_9546GDFireDemonObjects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalDemonObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595SpikeObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595SpikeObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BasePermanentObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserRingObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamSingleObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentSingleObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595SpikeObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects5ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects5Objects = Hashtable.newFrom({"FlyingDemon": gdjs.Level4Code.GDFlyingDemonObjects5, "FireDemon": gdjs.Level4Code.GDFireDemonObjects5, "HorizontalDemon": gdjs.Level4Code.GDHorizontalDemonObjects5, "SpikeDemon_Base": gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5, "SpikeDemon_Spike": gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5, "StalactiteDemon_Base": gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5, "StalactiteDemon_Spike": gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5, "LaserDemon_Beam": gdjs.Level4Code.GDLaserDemon_9595BeamObjects5, "LaserDemon_Base": gdjs.Level4Code.GDLaserDemon_9595BaseObjects5, "LaserDemon_BasePermanent": gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects5, "LaserDemon_BeamPermanent": gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects5, "LaserRing": gdjs.Level4Code.GDLaserRingObjects5, "LaserDemon_BeamSingle": gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects5, "LaserDemon_BeamPermanentSingle": gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects5, "TurretDemon_Base": gdjs.Level4Code.GDTurretDemon_9595BaseObjects5, "TurretDemon_Spike": gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5, "TurretDemon_Eye": gdjs.Level4Code.GDTurretDemon_9595EyeObjects5, "ShockwaveDemon_Base": gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDBloodParticlesObjects5Objects = Hashtable.newFrom({"BloodParticles": gdjs.Level4Code.GDBloodParticlesObjects5});
gdjs.Level4Code.eventsList73 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.Level4Code.GDFireDemonObjects5);
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.Level4Code.GDFlyingDemonObjects5);
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.Level4Code.GDHorizontalDemonObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Base"), gdjs.Level4Code.GDLaserDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BasePermanent"), gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_Beam"), gdjs.Level4Code.GDLaserDemon_9595BeamObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BeamPermanent"), gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BeamPermanentSingle"), gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserDemon_BeamSingle"), gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects5);
gdjs.copyArray(runtimeScene.getObjects("LaserRing"), gdjs.Level4Code.GDLaserRingObjects5);
gdjs.copyArray(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4, gdjs.Level4Code.GDMiteDemon_9595MiteObjects5);

gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Base"), gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalactiteDemon_Base"), gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("StalactiteDemon_Spike"), gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Base"), gdjs.Level4Code.GDTurretDemon_9595BaseObjects5);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Eye"), gdjs.Level4Code.GDTurretDemon_9595EyeObjects5);
gdjs.copyArray(runtimeScene.getObjects("TurretDemon_Spike"), gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects5Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDFlyingDemonObjects5ObjectsGDgdjs_9546Level4Code_9546GDFireDemonObjects5ObjectsGDgdjs_9546Level4Code_9546GDHorizontalDemonObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDSpikeDemon_95959595SpikeObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDStalactiteDemon_95959595SpikeObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BasePermanentObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserRingObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamSingleObjects5ObjectsGDgdjs_9546Level4Code_9546GDLaserDemon_95959595BeamPermanentSingleObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595BaseObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595SpikeObjects5ObjectsGDgdjs_9546Level4Code_9546GDTurretDemon_95959595EyeObjects5ObjectsGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595BaseObjects5Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects5 */
gdjs.Level4Code.GDBloodParticlesObjects5.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDBloodParticlesObjects5Objects, (( gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[0].getPointX("")) + (( gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[0].getWidth()) / 2, (( gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[0].getPointY("")) + (( gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length === 0 ) ? 0 :gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[0].getHeight()) / 2, "");
}{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects5[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4Objects = Hashtable.newFrom({"MiteDemon_Mite": gdjs.Level4Code.GDMiteDemon_9595MiteObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformLeftObjects4ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformRightObjects4ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformUpObjects4ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformDownObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformClockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformCounterclockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformClockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformCounterclockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatform2Objects4Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.Level4Code.GDStaticPlatform1Objects4, "StaticPlatform2": gdjs.Level4Code.GDStaticPlatform2Objects4, "StaticPlatform3": gdjs.Level4Code.GDStaticPlatform3Objects4, "HorizontalMovingPlatformLeft": gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects4, "HorizontalMovingPlatformRight": gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects4, "VerticalMovingPlatformUp": gdjs.Level4Code.GDVerticalMovingPlatformUpObjects4, "VerticalMovingPlatformDown": gdjs.Level4Code.GDVerticalMovingPlatformDownObjects4, "SpinningPlatformClockwise": gdjs.Level4Code.GDSpinningPlatformClockwiseObjects4, "SpinningPlatformCounterclockwise": gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects4, "SpinningRotatingPlatformClockwise": gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects4, "SpinningRotatingPlatformCounterclockwise": gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects4, "FlippingPlatform": gdjs.Level4Code.GDFlippingPlatformObjects4, "FlippingPlatform2": gdjs.Level4Code.GDFlippingPlatform2Objects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4Objects = Hashtable.newFrom({"MiteDemon_Mite": gdjs.Level4Code.GDMiteDemon_9595MiteObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDLadderObjects4Objects = Hashtable.newFrom({"Ladder": gdjs.Level4Code.GDLadderObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4Objects = Hashtable.newFrom({"MiteDemon_Mite": gdjs.Level4Code.GDMiteDemon_9595MiteObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595ShockwaveObjects4Objects = Hashtable.newFrom({"ShockwaveDemon_Shockwave": gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4});
gdjs.Level4Code.eventsList74 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.Level4Code.GDMiteDemon_9595MiteObjects3, gdjs.Level4Code.GDMiteDemon_9595MiteObjects4);

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].getCenterXInScene() < (( gdjs.Level4Code.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects4[0].getCenterXInScene()) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[k] = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects4 */
{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].setAngle(0);
}
}{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[i].getBehavior("PlatformerObject").simulateRightKey();
}
}}

}


{

/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects3 */
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i].getCenterXInScene() >= (( gdjs.Level4Code.GDPlayerObjects3.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects3[0].getCenterXInScene()) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[k] = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects3 */
{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i].setAngle(180);
}
}{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i].getBehavior("PlatformerObject").simulateLeftKey();
}
}}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects2Objects = Hashtable.newFrom({"MiteDemon_Mite": gdjs.Level4Code.GDMiteDemon_9595MiteObjects2});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDLadderObjects2Objects = Hashtable.newFrom({"Ladder": gdjs.Level4Code.GDLadderObjects2});
gdjs.Level4Code.eventsList75 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.Level4Code.GDMiteDemon_9595MiteObjects2, gdjs.Level4Code.GDMiteDemon_9595MiteObjects3);

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i].getCenterYInScene() <= (( gdjs.Level4Code.GDPlayerObjects3.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects3[0].getCenterYInScene()) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[k] = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects3 */
{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i].getBehavior("PlatformerObject").simulateDownKey();
}
}}

}


{

/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects2 */
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595MiteObjects2.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects2[i].getCenterYInScene() > (( gdjs.Level4Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects2[0].getCenterYInScene()) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595MiteObjects2[k] = gdjs.Level4Code.GDMiteDemon_9595MiteObjects2[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595MiteObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects2 */
{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects2.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects2[i].getBehavior("PlatformerObject").simulateUpKey();
}
}{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects2.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects2[i].getBehavior("PlatformerObject").simulateLadderKey();
}
}}

}


};gdjs.Level4Code.eventsList76 = function(runtimeScene) {

{



}


{

gdjs.Level4Code.GDFlippingPlatformObjects3.length = 0;

gdjs.Level4Code.GDFlippingPlatform2Objects3.length = 0;

gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3.length = 0;

gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3.length = 0;

gdjs.Level4Code.GDLadderObjects3.length = 0;

gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length = 0;

gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3.length = 0;

gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3.length = 0;

gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3.length = 0;

gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3.length = 0;

gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3.length = 0;

gdjs.Level4Code.GDStaticPlatform1Objects3.length = 0;

gdjs.Level4Code.GDStaticPlatform2Objects3.length = 0;

gdjs.Level4Code.GDStaticPlatform3Objects3.length = 0;

gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3.length = 0;

gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.Level4Code.GDFlippingPlatformObjects3_1final.length = 0;
gdjs.Level4Code.GDFlippingPlatform2Objects3_1final.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3_1final.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3_1final.length = 0;
gdjs.Level4Code.GDLadderObjects3_1final.length = 0;
gdjs.Level4Code.GDMiteDemon_9595MiteObjects3_1final.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3_1final.length = 0;
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3_1final.length = 0;
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3_1final.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3_1final.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3_1final.length = 0;
gdjs.Level4Code.GDStaticPlatform1Objects3_1final.length = 0;
gdjs.Level4Code.GDStaticPlatform2Objects3_1final.length = 0;
gdjs.Level4Code.GDStaticPlatform3Objects3_1final.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3_1final.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level4Code.GDFlippingPlatformObjects4);
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level4Code.GDFlippingPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatformLeft"), gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects4);
gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatformRight"), gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects4);
gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Mite"), gdjs.Level4Code.GDMiteDemon_9595MiteObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningPlatformClockwise"), gdjs.Level4Code.GDSpinningPlatformClockwiseObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningPlatformCounterclockwise"), gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningRotatingPlatformClockwise"), gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpinningRotatingPlatformCounterclockwise"), gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.Level4Code.GDStaticPlatform1Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.Level4Code.GDStaticPlatform2Objects4);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.Level4Code.GDStaticPlatform3Objects4);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatformDown"), gdjs.Level4Code.GDVerticalMovingPlatformDownObjects4);
gdjs.copyArray(runtimeScene.getObjects("VerticalMovingPlatformUp"), gdjs.Level4Code.GDVerticalMovingPlatformUpObjects4);
isConditionTrue_1 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDStaticPlatform1Objects4ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform2Objects4ObjectsGDgdjs_9546Level4Code_9546GDStaticPlatform3Objects4ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformLeftObjects4ObjectsGDgdjs_9546Level4Code_9546GDHorizontalMovingPlatformRightObjects4ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformUpObjects4ObjectsGDgdjs_9546Level4Code_9546GDVerticalMovingPlatformDownObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformClockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningPlatformCounterclockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformClockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDSpinningRotatingPlatformCounterclockwiseObjects4ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatformObjects4ObjectsGDgdjs_9546Level4Code_9546GDFlippingPlatform2Objects4Objects, false, runtimeScene, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level4Code.GDFlippingPlatformObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDFlippingPlatformObjects3_1final.indexOf(gdjs.Level4Code.GDFlippingPlatformObjects4[j]) === -1 )
            gdjs.Level4Code.GDFlippingPlatformObjects3_1final.push(gdjs.Level4Code.GDFlippingPlatformObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDFlippingPlatform2Objects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDFlippingPlatform2Objects3_1final.indexOf(gdjs.Level4Code.GDFlippingPlatform2Objects4[j]) === -1 )
            gdjs.Level4Code.GDFlippingPlatform2Objects3_1final.push(gdjs.Level4Code.GDFlippingPlatform2Objects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3_1final.indexOf(gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects4[j]) === -1 )
            gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3_1final.push(gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3_1final.indexOf(gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects4[j]) === -1 )
            gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3_1final.push(gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects3_1final.indexOf(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[j]) === -1 )
            gdjs.Level4Code.GDMiteDemon_9595MiteObjects3_1final.push(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDSpinningPlatformClockwiseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3_1final.indexOf(gdjs.Level4Code.GDSpinningPlatformClockwiseObjects4[j]) === -1 )
            gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3_1final.push(gdjs.Level4Code.GDSpinningPlatformClockwiseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3_1final.indexOf(gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects4[j]) === -1 )
            gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3_1final.push(gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3_1final.indexOf(gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects4[j]) === -1 )
            gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3_1final.push(gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3_1final.indexOf(gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects4[j]) === -1 )
            gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3_1final.push(gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDStaticPlatform1Objects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDStaticPlatform1Objects3_1final.indexOf(gdjs.Level4Code.GDStaticPlatform1Objects4[j]) === -1 )
            gdjs.Level4Code.GDStaticPlatform1Objects3_1final.push(gdjs.Level4Code.GDStaticPlatform1Objects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDStaticPlatform2Objects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDStaticPlatform2Objects3_1final.indexOf(gdjs.Level4Code.GDStaticPlatform2Objects4[j]) === -1 )
            gdjs.Level4Code.GDStaticPlatform2Objects3_1final.push(gdjs.Level4Code.GDStaticPlatform2Objects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDStaticPlatform3Objects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDStaticPlatform3Objects3_1final.indexOf(gdjs.Level4Code.GDStaticPlatform3Objects4[j]) === -1 )
            gdjs.Level4Code.GDStaticPlatform3Objects3_1final.push(gdjs.Level4Code.GDStaticPlatform3Objects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDVerticalMovingPlatformDownObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3_1final.indexOf(gdjs.Level4Code.GDVerticalMovingPlatformDownObjects4[j]) === -1 )
            gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3_1final.push(gdjs.Level4Code.GDVerticalMovingPlatformDownObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDVerticalMovingPlatformUpObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3_1final.indexOf(gdjs.Level4Code.GDVerticalMovingPlatformUpObjects4[j]) === -1 )
            gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3_1final.push(gdjs.Level4Code.GDVerticalMovingPlatformUpObjects4[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("Ladder"), gdjs.Level4Code.GDLadderObjects4);
gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Mite"), gdjs.Level4Code.GDMiteDemon_9595MiteObjects4);
isConditionTrue_1 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDLadderObjects4Objects, false, runtimeScene, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level4Code.GDLadderObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDLadderObjects3_1final.indexOf(gdjs.Level4Code.GDLadderObjects4[j]) === -1 )
            gdjs.Level4Code.GDLadderObjects3_1final.push(gdjs.Level4Code.GDLadderObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects3_1final.indexOf(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[j]) === -1 )
            gdjs.Level4Code.GDMiteDemon_9595MiteObjects3_1final.push(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Mite"), gdjs.Level4Code.GDMiteDemon_9595MiteObjects4);
gdjs.copyArray(runtimeScene.getObjects("ShockwaveDemon_Shockwave"), gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4);
isConditionTrue_1 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects4Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDShockwaveDemon_95959595ShockwaveObjects4Objects, false, runtimeScene, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects3_1final.indexOf(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[j]) === -1 )
            gdjs.Level4Code.GDMiteDemon_9595MiteObjects3_1final.push(gdjs.Level4Code.GDMiteDemon_9595MiteObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3_1final.indexOf(gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4[j]) === -1 )
            gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3_1final.push(gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4[j]);
    }
}
}
{
gdjs.copyArray(gdjs.Level4Code.GDFlippingPlatformObjects3_1final, gdjs.Level4Code.GDFlippingPlatformObjects3);
gdjs.copyArray(gdjs.Level4Code.GDFlippingPlatform2Objects3_1final, gdjs.Level4Code.GDFlippingPlatform2Objects3);
gdjs.copyArray(gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3_1final, gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3);
gdjs.copyArray(gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3_1final, gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3);
gdjs.copyArray(gdjs.Level4Code.GDLadderObjects3_1final, gdjs.Level4Code.GDLadderObjects3);
gdjs.copyArray(gdjs.Level4Code.GDMiteDemon_9595MiteObjects3_1final, gdjs.Level4Code.GDMiteDemon_9595MiteObjects3);
gdjs.copyArray(gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3_1final, gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3);
gdjs.copyArray(gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3_1final, gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3);
gdjs.copyArray(gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3_1final, gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3);
gdjs.copyArray(gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3_1final, gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3);
gdjs.copyArray(gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3_1final, gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3);
gdjs.copyArray(gdjs.Level4Code.GDStaticPlatform1Objects3_1final, gdjs.Level4Code.GDStaticPlatform1Objects3);
gdjs.copyArray(gdjs.Level4Code.GDStaticPlatform2Objects3_1final, gdjs.Level4Code.GDStaticPlatform2Objects3);
gdjs.copyArray(gdjs.Level4Code.GDStaticPlatform3Objects3_1final, gdjs.Level4Code.GDStaticPlatform3Objects3);
gdjs.copyArray(gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3_1final, gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3);
gdjs.copyArray(gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3_1final, gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3);
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList74(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Ladder"), gdjs.Level4Code.GDLadderObjects2);
gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Mite"), gdjs.Level4Code.GDMiteDemon_9595MiteObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDMiteDemon_95959595MiteObjects2Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDLadderObjects2Objects, false, runtimeScene, false);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList75(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList77 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Mite"), gdjs.Level4Code.GDMiteDemon_9595MiteObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i].getVariableBoolean(gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i].getVariables().getFromIndex(6), false) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[k] = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i].getBehavior("PlatformerObject").getMaxSpeed() != gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i].getVariables().getFromIndex(0).getAsNumber() ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[k] = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length = k;
}
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDMiteDemon_9595MiteObjects3 */
{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i].getBehavior("PlatformerObject").setMaxSpeed(gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i].getVariables().getFromIndex(0).getAsNumber());
}
}{for(var i = 0, len = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i].getBehavior("PlatformerObject").setLadderClimbingSpeed(gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[i].getVariables().getFromIndex(0).getAsNumber());
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("MiteDemon_Mite"), gdjs.Level4Code.GDMiteDemon_9595MiteObjects3);

for (gdjs.Level4Code.forEachIndex4 = 0;gdjs.Level4Code.forEachIndex4 < gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length;++gdjs.Level4Code.forEachIndex4) {
gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length = 0;


gdjs.Level4Code.forEachTemporary4 = gdjs.Level4Code.GDMiteDemon_9595MiteObjects3[gdjs.Level4Code.forEachIndex4];
gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.push(gdjs.Level4Code.forEachTemporary4);
let isConditionTrue_0 = false;
if (true) {

{ //Subevents: 
gdjs.Level4Code.eventsList73(runtimeScene);} //Subevents end.
}
}

}


{


gdjs.Level4Code.eventsList76(runtimeScene);
}


};gdjs.Level4Code.eventsList78 = function(runtimeScene) {

{


gdjs.Level4Code.eventsList26(runtimeScene);
}


{


gdjs.Level4Code.eventsList30(runtimeScene);
}


{


gdjs.Level4Code.eventsList32(runtimeScene);
}


{


gdjs.Level4Code.eventsList34(runtimeScene);
}


{


gdjs.Level4Code.eventsList35(runtimeScene);
}


{


gdjs.Level4Code.eventsList36(runtimeScene);
}


{


gdjs.Level4Code.eventsList37(runtimeScene);
}


{


gdjs.Level4Code.eventsList38(runtimeScene);
}


{


gdjs.Level4Code.eventsList39(runtimeScene);
}


{


gdjs.Level4Code.eventsList40(runtimeScene);
}


{


gdjs.Level4Code.eventsList48(runtimeScene);
}


{


gdjs.Level4Code.eventsList59(runtimeScene);
}


{


gdjs.Level4Code.eventsList62(runtimeScene);
}


{


gdjs.Level4Code.eventsList72(runtimeScene);
}


{


gdjs.Level4Code.eventsList77(runtimeScene);
}


};gdjs.Level4Code.eventsList79 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{/* Unknown instruction - skipped. */}{/* Unknown instruction - skipped. */}{/* Unknown instruction - skipped. */}{/* Unknown instruction - skipped. */}{/* Unknown instruction - skipped. */}{/* Unknown instruction - skipped. */}{/* Unknown instruction - skipped. */}{/* Unknown instruction - skipped. */}}

}


};gdjs.Level4Code.eventsList80 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList79(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList81 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") < 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33579308);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level4Code.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFlippingPlatformObjects4[i].rotateTowardAngle(0, 0, runtimeScene);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") < 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33580172);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level4Code.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFlippingPlatformObjects4[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(1, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") < 5;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33582004);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level4Code.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFlippingPlatformObjects4[i].rotateTowardAngle(-(80), 0, runtimeScene);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 5;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") < 6;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33582916);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level4Code.GDFlippingPlatformObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDFlippingPlatformObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFlippingPlatformObjects4[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(1, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 6;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33584236);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.Level4Code.GDFlippingPlatformObjects3);
{for(var i = 0, len = gdjs.Level4Code.GDFlippingPlatformObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDFlippingPlatformObjects3[i].rotateTowardAngle(0, 0, runtimeScene);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


};gdjs.Level4Code.eventsList82 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") < 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33586340);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level4Code.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.Level4Code.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.Level4Code.GDFlippingPlatform2Objects3[i].rotateTowardAngle(-(80), 0, runtimeScene);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") < 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33587500);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level4Code.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.Level4Code.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.Level4Code.GDFlippingPlatform2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(1, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") < 5;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33588668);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level4Code.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.Level4Code.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.Level4Code.GDFlippingPlatform2Objects3[i].rotateTowardAngle(0, 0, runtimeScene);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 5;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") < 6;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33589804);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level4Code.GDFlippingPlatform2Objects3);
{for(var i = 0, len = gdjs.Level4Code.GDFlippingPlatform2Objects3.length ;i < len;++i) {
    gdjs.Level4Code.GDFlippingPlatform2Objects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(1, 2, 2, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip2") >= 6;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33591148);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform2"), gdjs.Level4Code.GDFlippingPlatform2Objects2);
{for(var i = 0, len = gdjs.Level4Code.GDFlippingPlatform2Objects2.length ;i < len;++i) {
    gdjs.Level4Code.GDFlippingPlatform2Objects2[i].rotateTowardAngle(0, 0, runtimeScene);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip2");
}}

}


};gdjs.Level4Code.eventsList83 = function(runtimeScene) {

{


gdjs.Level4Code.eventsList80(runtimeScene);
}


{


gdjs.Level4Code.eventsList81(runtimeScene);
}


{


gdjs.Level4Code.eventsList82(runtimeScene);
}


};gdjs.Level4Code.eventsList84 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FrameRate"), gdjs.Level4Code.GDFrameRateObjects4);
gdjs.copyArray(runtimeScene.getObjects("HopeBar"), gdjs.Level4Code.GDHopeBarObjects4);
gdjs.copyArray(runtimeScene.getObjects("LivesBar"), gdjs.Level4Code.GDLivesBarObjects4);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDLivesBarObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLivesBarObjects4[i].SetValue((gdjs.RuntimeObject.getVariableNumber(((gdjs.Level4Code.GDPlayerObjects4.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level4Code.GDPlayerObjects4[0].getVariables()).getFromIndex(0))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.Level4Code.GDHopeBarObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDHopeBarObjects4[i].SetValue((gdjs.RuntimeObject.getVariableNumber(((gdjs.Level4Code.GDPlayerObjects4.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level4Code.GDPlayerObjects4[0].getVariables()).getFromIndex(2))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.Level4Code.GDFrameRateObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDFrameRateObjects4[i].getBehavior("Text").setText(gdjs.evtTools.common.toString(gdjs.evtTools.common.roundTo(1 / gdjs.evtTools.runtimeScene.getElapsedTimeInSeconds(runtimeScene), 2)));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "LevelUI", 0, 0, 0);
}{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "LevelEndScreen", 0, 0, 0);
}}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDCameraGlitchFixObjects4Objects = Hashtable.newFrom({"CameraGlitchFix": gdjs.Level4Code.GDCameraGlitchFixObjects4});
gdjs.Level4Code.eventsList85 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].pauseTimer("CameraIncreaseSpeed");
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].pauseTimer("CameraDecreaseSpeed");
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].getBehavior("SmoothCamera").SetLeftwardSpeed(0.9, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].getBehavior("SmoothCamera").SetRightwardSpeed(0.9, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].getBehavior("SmoothCamera").SetUpwardSpeed(0.75, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].getBehavior("SmoothCamera").SetDownwardSpeed(0.9, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.Level4Code.eventsList86 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("BoundaryJumpThrough"), gdjs.Level4Code.GDBoundaryJumpThroughObjects4);
gdjs.copyArray(runtimeScene.getObjects("LeftBoundary"), gdjs.Level4Code.GDLeftBoundaryObjects4);
gdjs.copyArray(runtimeScene.getObjects("RightBoundary"), gdjs.Level4Code.GDRightBoundaryObjects4);
gdjs.Level4Code.GDCameraGlitchFixObjects4.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDCameraGlitchFixObjects4Objects, 0, -(6000), "");
}{for(var i = 0, len = gdjs.Level4Code.GDLeftBoundaryObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDLeftBoundaryObjects4[i].getBehavior("Opacity").setOpacity(0);
}
for(var i = 0, len = gdjs.Level4Code.GDRightBoundaryObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDRightBoundaryObjects4[i].getBehavior("Opacity").setOpacity(0);
}
for(var i = 0, len = gdjs.Level4Code.GDBoundaryJumpThroughObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDBoundaryJumpThroughObjects4[i].getBehavior("Opacity").setOpacity(0);
}
for(var i = 0, len = gdjs.Level4Code.GDCameraGlitchFixObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDCameraGlitchFixObjects4[i].getBehavior("Opacity").setOpacity(0);
}
}
{ //Subevents
gdjs.Level4Code.eventsList85(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("BottomBoundary"), gdjs.Level4Code.GDBottomBoundaryObjects4);
gdjs.copyArray(runtimeScene.getObjects("LeftBoundary"), gdjs.Level4Code.GDLeftBoundaryObjects4);
gdjs.copyArray(runtimeScene.getObjects("RightBoundary"), gdjs.Level4Code.GDRightBoundaryObjects4);
gdjs.copyArray(runtimeScene.getObjects("TopBoundary"), gdjs.Level4Code.GDTopBoundaryObjects4);
{gdjs.evtTools.camera.clampCamera(runtimeScene, (( gdjs.Level4Code.GDLeftBoundaryObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDLeftBoundaryObjects4[0].getPointX("")) + (( gdjs.Level4Code.GDLeftBoundaryObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDLeftBoundaryObjects4[0].getWidth()), (( gdjs.Level4Code.GDTopBoundaryObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDTopBoundaryObjects4[0].getPointY("")) + (( gdjs.Level4Code.GDTopBoundaryObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDTopBoundaryObjects4[0].getHeight()), (( gdjs.Level4Code.GDRightBoundaryObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDRightBoundaryObjects4[0].getPointX("")), (( gdjs.Level4Code.GDBottomBoundaryObjects4.length === 0 ) ? 0 :gdjs.Level4Code.GDBottomBoundaryObjects4[0].getPointY("")), "", 0);
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("CameraGlitchFix"), gdjs.Level4Code.GDCameraGlitchFixObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects3);
{for(var i = 0, len = gdjs.Level4Code.GDCameraGlitchFixObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDCameraGlitchFixObjects3[i].setPosition((( gdjs.Level4Code.GDPlayerObjects3.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects3[0].getPointX("")) - 3000,(( gdjs.Level4Code.GDPlayerObjects3.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects3[0].getPointY("")) - 3000);
}
}}

}


};gdjs.Level4Code.eventsList87 = function(runtimeScene) {

{



}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "RandomNoiseTimer");
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("BackgroundPlants"), gdjs.Level4Code.GDBackgroundPlantsObjects3);
{for(var i = 0, len = gdjs.Level4Code.GDBackgroundPlantsObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDBackgroundPlantsObjects3[i].setWidth(gdjs.evtTools.camera.getCameraWidth(runtimeScene, "", 0));
}
}{for(var i = 0, len = gdjs.Level4Code.GDBackgroundPlantsObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDBackgroundPlantsObjects3[i].setXOffset(gdjs.evtTools.camera.getCameraBorderLeft(runtimeScene, "", 0) / 3 + 2400);
}
}}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects3});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPortalObjects3Objects = Hashtable.newFrom({"Portal": gdjs.Level4Code.GDPortalObjects3});
gdjs.Level4Code.mapOfEmptyGDPlayerObjects = Hashtable.newFrom({"Player": []});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPortalObjects2Objects = Hashtable.newFrom({"Portal": gdjs.Level4Code.GDPortalObjects2});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects2});
gdjs.Level4Code.eventsList88 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "door.aac", 0, true, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects3);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.Level4Code.GDPortalObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects3Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPortalObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33604012);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Portal/PortalInteract.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.getSceneInstancesCount((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level4Code.mapOfEmptyGDPlayerObjects) > 0;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects2);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.Level4Code.GDPortalObjects2);
{gdjs.evtsExt__VolumeFalloff__SetVolumeFalloff.func(runtimeScene, 0, "Sound", gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPortalObjects2Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects2Objects, 0, 100, 750, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level4Code.eventsList89 = function(runtimeScene) {

{


gdjs.Level4Code.eventsList84(runtimeScene);
}


{


gdjs.Level4Code.eventsList86(runtimeScene);
}


{


gdjs.Level4Code.eventsList87(runtimeScene);
}


{


gdjs.Level4Code.eventsList88(runtimeScene);
}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPortalObjects4Objects = Hashtable.newFrom({"Portal": gdjs.Level4Code.GDPortalObjects4});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects3});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPortalObjects3Objects = Hashtable.newFrom({"Portal": gdjs.Level4Code.GDPortalObjects3});
gdjs.Level4Code.eventsList90 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.Level4Code.GDPlayerObjects3, gdjs.Level4Code.GDPlayerObjects4);

{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects4[i].activateBehavior("PlatformerObject", false);
}
}}

}


{

/* Reuse gdjs.Level4Code.GDPlayerObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects3[i].getVariableNumber(gdjs.Level4Code.GDPlayerObjects3[i].getVariables().getFromIndex(0)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects3[k] = gdjs.Level4Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.Level4Code.GDPlayerObjects3 */
/* Reuse gdjs.Level4Code.GDPortalObjects3 */
{gdjs.evtsExt__Player__AnimateFallingIntoPortal.func(runtimeScene, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects3Objects, "Tween", gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPortalObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.Level4Code.eventsList91 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects4[i].getVariableNumber(gdjs.Level4Code.GDPlayerObjects4[i].getVariables().getFromIndex(0)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects4[k] = gdjs.Level4Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_DeathText"), gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects4);
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_RetryButton"), gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects4);
{for(var i = 0, len = gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects4[i].hide();
}
}{for(var i = 0, len = gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects4.length ;i < len;++i) {
    gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects4[i].hide();
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects3.length;i<l;++i) {
    if ( !(gdjs.Level4Code.GDPlayerObjects3[i].getVariableNumber(gdjs.Level4Code.GDPlayerObjects3[i].getVariables().getFromIndex(0)) > 0) ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects3[k] = gdjs.Level4Code.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_MemoryAcquired"), gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3);
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_ProceedButton"), gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects3);
{for(var i = 0, len = gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3[i].hide();
}
}{for(var i = 0, len = gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects3[i].hide();
}
}}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDUI_95959595Sinage_95959595BackgroundObjects2Objects = Hashtable.newFrom({"UI_Sinage_Background": gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects2});
gdjs.Level4Code.eventsList92 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33611516);
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList91(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_Background"), gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects2);
{gdjs.evtsExt__UserInterface__StretchToFillScreen.func(runtimeScene, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDUI_95959595Sinage_95959595BackgroundObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{}}

}


};gdjs.Level4Code.eventsList93 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_Background"), gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects3);
{gdjs.evtTools.camera.hideLayer(runtimeScene, "EndScreen");
}{for(var i = 0, len = gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects3.length ;i < len;++i) {
    gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects3[i].setOpacity(180);
}
}}

}


{



}


{

gdjs.Level4Code.GDPlayerObjects3.length = 0;

gdjs.Level4Code.GDPortalObjects3.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.Level4Code.GDPlayerObjects3_1final.length = 0;
gdjs.Level4Code.GDPortalObjects3_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.Level4Code.GDPortalObjects4);
isConditionTrue_1 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects4Objects, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPortalObjects4Objects, false, runtimeScene, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level4Code.GDPlayerObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDPlayerObjects3_1final.indexOf(gdjs.Level4Code.GDPlayerObjects4[j]) === -1 )
            gdjs.Level4Code.GDPlayerObjects3_1final.push(gdjs.Level4Code.GDPlayerObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.Level4Code.GDPortalObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDPortalObjects3_1final.indexOf(gdjs.Level4Code.GDPortalObjects4[j]) === -1 )
            gdjs.Level4Code.GDPortalObjects3_1final.push(gdjs.Level4Code.GDPortalObjects4[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects4);
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects4[i].getVariableNumber(gdjs.Level4Code.GDPlayerObjects4[i].getVariables().getFromIndex(0)) <= 0 ) {
        isConditionTrue_1 = true;
        gdjs.Level4Code.GDPlayerObjects4[k] = gdjs.Level4Code.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects4.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level4Code.GDPlayerObjects4.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDPlayerObjects3_1final.indexOf(gdjs.Level4Code.GDPlayerObjects4[j]) === -1 )
            gdjs.Level4Code.GDPlayerObjects3_1final.push(gdjs.Level4Code.GDPlayerObjects4[j]);
    }
}
}
{
gdjs.copyArray(gdjs.Level4Code.GDPlayerObjects3_1final, gdjs.Level4Code.GDPlayerObjects3);
gdjs.copyArray(gdjs.Level4Code.GDPortalObjects3_1final, gdjs.Level4Code.GDPortalObjects3);
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33607172);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.camera.showLayer(runtimeScene, "EndScreen");
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "UI");
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "");
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "Background");
}{gdjs.evtTools.sound.stopSoundOnChannel(runtimeScene, 2);
}
{ //Subevents
gdjs.Level4Code.eventsList90(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "EndScreen");
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList92(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects2});
gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Level4Code.GDPlayerObjects2});
gdjs.Level4Code.eventsList94 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "y");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33615236);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects2);
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects2[i].returnVariable(gdjs.Level4Code.GDPlayerObjects2[i].getVariables().getFromIndex(4)).setNumber(1);
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects2[i].setPosition(gdjs.Level4Code.GDPlayerObjects2[i].getVariables().getFromIndex(6).getAsNumber(),gdjs.Level4Code.GDPlayerObjects2[i].getVariables().getFromIndex(7).getAsNumber());
}
}{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects2Objects, ((gdjs.Level4Code.GDPlayerObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level4Code.GDPlayerObjects2[0].getVariables()).getFromIndex(6).getAsNumber(), ((gdjs.Level4Code.GDPlayerObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.Level4Code.GDPlayerObjects2[0].getVariables()).getFromIndex(7).getAsNumber(), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "u");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33616660);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects2);
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects2[i].returnVariable(gdjs.Level4Code.GDPlayerObjects2[i].getVariables().getFromIndex(4)).setNumber(1);
}
}{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.Level4Code.mapOfGDgdjs_9546Level4Code_9546GDPlayerObjects2Objects, (( gdjs.Level4Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects2[0].getPointX("")), (( gdjs.Level4Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Level4Code.GDPlayerObjects2[0].getPointY("")), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "i");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33617428);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects2);
{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects2[i].returnVariable(gdjs.Level4Code.GDPlayerObjects2[i].getVariables().getFromIndex(4)).setNumber(1 - gdjs.Level4Code.GDPlayerObjects2[i].getVariables().getFromIndex(4).getAsNumber());
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "n");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33618452);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MainMenu", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "r");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33618260);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, gdjs.evtTools.runtimeScene.getSceneName(runtimeScene), false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "l");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33619884);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "TestingLevel", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "m");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33620596);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Mindscape", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "t");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33621308);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Tutorial", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num1");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33621036);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level1", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num2");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33622756);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level2", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num3");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33622436);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level3", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num4");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33624116);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level4", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num5");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33624780);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level5", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num6");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33624556);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level6", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num7");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33626140);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level7", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num8");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33626804);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level8", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num9");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33626580);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level9", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num0");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(33628308);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level10", false);
}}

}


};gdjs.Level4Code.eventsList95 = function(runtimeScene) {

{


gdjs.Level4Code.eventsList22(runtimeScene);
}


{


gdjs.Level4Code.eventsList78(runtimeScene);
}


{


gdjs.Level4Code.eventsList83(runtimeScene);
}


{


gdjs.Level4Code.eventsList89(runtimeScene);
}


{


gdjs.Level4Code.eventsList93(runtimeScene);
}


{


gdjs.Level4Code.eventsList94(runtimeScene);
}


};gdjs.Level4Code.eventsList96 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects2[i].getVariableNumber(gdjs.Level4Code.GDPlayerObjects2[i].getVariables().getFromIndex(0)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects2[k] = gdjs.Level4Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Mindscape", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.Level4Code.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.Level4Code.GDPlayerObjects1[i].getVariableNumber(gdjs.Level4Code.GDPlayerObjects1[i].getVariables().getFromIndex(0)) <= 0 ) {
        isConditionTrue_0 = true;
        gdjs.Level4Code.GDPlayerObjects1[k] = gdjs.Level4Code.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.Level4Code.GDPlayerObjects1.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level4", false);
}}

}


};gdjs.Level4Code.eventsList97 = function(runtimeScene) {

{

gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "EndScreen");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_RetryButton"), gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects2);
for (var i = 0, k = 0, l = gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects2.length;i<l;++i) {
    if ( gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects2[k] = gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects2[i];
        ++k;
    }
}
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects2.length; j < jLen ; ++j) {
        if ( gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final.indexOf(gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects2[j]) === -1 )
            gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final.push(gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects2[j]);
    }
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "Space");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
gdjs.copyArray(gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects1_1final, gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects1);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.Level4Code.eventsList96(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList98 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 1;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance1.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance2.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 3;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance3.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)), 1);
}}

}


};gdjs.Level4Code.eventsList99 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Levels/5/AmbientLoop.ogg", true, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) - 16, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "RandomNoiseTimer") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("TimeBeforeNextRandomSFX"));
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("RandomSFXIndex").setNumber(gdjs.randomInRange(1, 3));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "RandomNoiseTimer");
}{runtimeScene.getScene().getVariables().get("TimeBeforeNextRandomSFX").setNumber(gdjs.randomFloatInRange(30, 240));
}
{ //Subevents
gdjs.Level4Code.eventsList98(runtimeScene);} //End of subevents
}

}


};gdjs.Level4Code.eventsList100 = function(runtimeScene) {

{



}


{


gdjs.Level4Code.eventsList95(runtimeScene);
}


{


gdjs.Level4Code.eventsList97(runtimeScene);
}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Memory"), gdjs.Level4Code.GDMemoryObjects1);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Level4Code.GDPlayerObjects1);
{for(var i = 0, len = gdjs.Level4Code.GDMemoryObjects1.length ;i < len;++i) {
    gdjs.Level4Code.GDMemoryObjects1[i].getBehavior("Animation").setAnimationName("Ocean");
}
}{for(var i = 0, len = gdjs.Level4Code.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Level4Code.GDPlayerObjects1[i].returnVariable(gdjs.Level4Code.GDPlayerObjects1[i].getVariables().getFromIndex(2)).setNumber(4);
}
}}

}


{


gdjs.Level4Code.eventsList99(runtimeScene);
}


};

gdjs.Level4Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Level4Code.GDBackgroundPlantsObjects1.length = 0;
gdjs.Level4Code.GDBackgroundPlantsObjects2.length = 0;
gdjs.Level4Code.GDBackgroundPlantsObjects3.length = 0;
gdjs.Level4Code.GDBackgroundPlantsObjects4.length = 0;
gdjs.Level4Code.GDBackgroundPlantsObjects5.length = 0;
gdjs.Level4Code.GDBackgroundPlantsObjects6.length = 0;
gdjs.Level4Code.GDBackgroundPlantsObjects7.length = 0;
gdjs.Level4Code.GDBackgroundPlantsObjects8.length = 0;
gdjs.Level4Code.GDPlayerObjects1.length = 0;
gdjs.Level4Code.GDPlayerObjects2.length = 0;
gdjs.Level4Code.GDPlayerObjects3.length = 0;
gdjs.Level4Code.GDPlayerObjects4.length = 0;
gdjs.Level4Code.GDPlayerObjects5.length = 0;
gdjs.Level4Code.GDPlayerObjects6.length = 0;
gdjs.Level4Code.GDPlayerObjects7.length = 0;
gdjs.Level4Code.GDPlayerObjects8.length = 0;
gdjs.Level4Code.GDFlyingDemonObjects1.length = 0;
gdjs.Level4Code.GDFlyingDemonObjects2.length = 0;
gdjs.Level4Code.GDFlyingDemonObjects3.length = 0;
gdjs.Level4Code.GDFlyingDemonObjects4.length = 0;
gdjs.Level4Code.GDFlyingDemonObjects5.length = 0;
gdjs.Level4Code.GDFlyingDemonObjects6.length = 0;
gdjs.Level4Code.GDFlyingDemonObjects7.length = 0;
gdjs.Level4Code.GDFlyingDemonObjects8.length = 0;
gdjs.Level4Code.GDFireDemonObjects1.length = 0;
gdjs.Level4Code.GDFireDemonObjects2.length = 0;
gdjs.Level4Code.GDFireDemonObjects3.length = 0;
gdjs.Level4Code.GDFireDemonObjects4.length = 0;
gdjs.Level4Code.GDFireDemonObjects5.length = 0;
gdjs.Level4Code.GDFireDemonObjects6.length = 0;
gdjs.Level4Code.GDFireDemonObjects7.length = 0;
gdjs.Level4Code.GDFireDemonObjects8.length = 0;
gdjs.Level4Code.GDCheckpointObjects1.length = 0;
gdjs.Level4Code.GDCheckpointObjects2.length = 0;
gdjs.Level4Code.GDCheckpointObjects3.length = 0;
gdjs.Level4Code.GDCheckpointObjects4.length = 0;
gdjs.Level4Code.GDCheckpointObjects5.length = 0;
gdjs.Level4Code.GDCheckpointObjects6.length = 0;
gdjs.Level4Code.GDCheckpointObjects7.length = 0;
gdjs.Level4Code.GDCheckpointObjects8.length = 0;
gdjs.Level4Code.GDStaticPlatform3Objects1.length = 0;
gdjs.Level4Code.GDStaticPlatform3Objects2.length = 0;
gdjs.Level4Code.GDStaticPlatform3Objects3.length = 0;
gdjs.Level4Code.GDStaticPlatform3Objects4.length = 0;
gdjs.Level4Code.GDStaticPlatform3Objects5.length = 0;
gdjs.Level4Code.GDStaticPlatform3Objects6.length = 0;
gdjs.Level4Code.GDStaticPlatform3Objects7.length = 0;
gdjs.Level4Code.GDStaticPlatform3Objects8.length = 0;
gdjs.Level4Code.GDStaticPlatform2Objects1.length = 0;
gdjs.Level4Code.GDStaticPlatform2Objects2.length = 0;
gdjs.Level4Code.GDStaticPlatform2Objects3.length = 0;
gdjs.Level4Code.GDStaticPlatform2Objects4.length = 0;
gdjs.Level4Code.GDStaticPlatform2Objects5.length = 0;
gdjs.Level4Code.GDStaticPlatform2Objects6.length = 0;
gdjs.Level4Code.GDStaticPlatform2Objects7.length = 0;
gdjs.Level4Code.GDStaticPlatform2Objects8.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects1.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects2.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects3.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects4.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects5.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects6.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects7.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformRightObjects8.length = 0;
gdjs.Level4Code.GDStaticPlatform1Objects1.length = 0;
gdjs.Level4Code.GDStaticPlatform1Objects2.length = 0;
gdjs.Level4Code.GDStaticPlatform1Objects3.length = 0;
gdjs.Level4Code.GDStaticPlatform1Objects4.length = 0;
gdjs.Level4Code.GDStaticPlatform1Objects5.length = 0;
gdjs.Level4Code.GDStaticPlatform1Objects6.length = 0;
gdjs.Level4Code.GDStaticPlatform1Objects7.length = 0;
gdjs.Level4Code.GDStaticPlatform1Objects8.length = 0;
gdjs.Level4Code.GDPortalObjects1.length = 0;
gdjs.Level4Code.GDPortalObjects2.length = 0;
gdjs.Level4Code.GDPortalObjects3.length = 0;
gdjs.Level4Code.GDPortalObjects4.length = 0;
gdjs.Level4Code.GDPortalObjects5.length = 0;
gdjs.Level4Code.GDPortalObjects6.length = 0;
gdjs.Level4Code.GDPortalObjects7.length = 0;
gdjs.Level4Code.GDPortalObjects8.length = 0;
gdjs.Level4Code.GDLadderObjects1.length = 0;
gdjs.Level4Code.GDLadderObjects2.length = 0;
gdjs.Level4Code.GDLadderObjects3.length = 0;
gdjs.Level4Code.GDLadderObjects4.length = 0;
gdjs.Level4Code.GDLadderObjects5.length = 0;
gdjs.Level4Code.GDLadderObjects6.length = 0;
gdjs.Level4Code.GDLadderObjects7.length = 0;
gdjs.Level4Code.GDLadderObjects8.length = 0;
gdjs.Level4Code.GDBloodParticlesObjects1.length = 0;
gdjs.Level4Code.GDBloodParticlesObjects2.length = 0;
gdjs.Level4Code.GDBloodParticlesObjects3.length = 0;
gdjs.Level4Code.GDBloodParticlesObjects4.length = 0;
gdjs.Level4Code.GDBloodParticlesObjects5.length = 0;
gdjs.Level4Code.GDBloodParticlesObjects6.length = 0;
gdjs.Level4Code.GDBloodParticlesObjects7.length = 0;
gdjs.Level4Code.GDBloodParticlesObjects8.length = 0;
gdjs.Level4Code.GDProjectileDeathParticlesObjects1.length = 0;
gdjs.Level4Code.GDProjectileDeathParticlesObjects2.length = 0;
gdjs.Level4Code.GDProjectileDeathParticlesObjects3.length = 0;
gdjs.Level4Code.GDProjectileDeathParticlesObjects4.length = 0;
gdjs.Level4Code.GDProjectileDeathParticlesObjects5.length = 0;
gdjs.Level4Code.GDProjectileDeathParticlesObjects6.length = 0;
gdjs.Level4Code.GDProjectileDeathParticlesObjects7.length = 0;
gdjs.Level4Code.GDProjectileDeathParticlesObjects8.length = 0;
gdjs.Level4Code.GDDoorParticlesObjects1.length = 0;
gdjs.Level4Code.GDDoorParticlesObjects2.length = 0;
gdjs.Level4Code.GDDoorParticlesObjects3.length = 0;
gdjs.Level4Code.GDDoorParticlesObjects4.length = 0;
gdjs.Level4Code.GDDoorParticlesObjects5.length = 0;
gdjs.Level4Code.GDDoorParticlesObjects6.length = 0;
gdjs.Level4Code.GDDoorParticlesObjects7.length = 0;
gdjs.Level4Code.GDDoorParticlesObjects8.length = 0;
gdjs.Level4Code.GDDustParticleObjects1.length = 0;
gdjs.Level4Code.GDDustParticleObjects2.length = 0;
gdjs.Level4Code.GDDustParticleObjects3.length = 0;
gdjs.Level4Code.GDDustParticleObjects4.length = 0;
gdjs.Level4Code.GDDustParticleObjects5.length = 0;
gdjs.Level4Code.GDDustParticleObjects6.length = 0;
gdjs.Level4Code.GDDustParticleObjects7.length = 0;
gdjs.Level4Code.GDDustParticleObjects8.length = 0;
gdjs.Level4Code.GDLivesBarObjects1.length = 0;
gdjs.Level4Code.GDLivesBarObjects2.length = 0;
gdjs.Level4Code.GDLivesBarObjects3.length = 0;
gdjs.Level4Code.GDLivesBarObjects4.length = 0;
gdjs.Level4Code.GDLivesBarObjects5.length = 0;
gdjs.Level4Code.GDLivesBarObjects6.length = 0;
gdjs.Level4Code.GDLivesBarObjects7.length = 0;
gdjs.Level4Code.GDLivesBarObjects8.length = 0;
gdjs.Level4Code.GDHopeBarObjects1.length = 0;
gdjs.Level4Code.GDHopeBarObjects2.length = 0;
gdjs.Level4Code.GDHopeBarObjects3.length = 0;
gdjs.Level4Code.GDHopeBarObjects4.length = 0;
gdjs.Level4Code.GDHopeBarObjects5.length = 0;
gdjs.Level4Code.GDHopeBarObjects6.length = 0;
gdjs.Level4Code.GDHopeBarObjects7.length = 0;
gdjs.Level4Code.GDHopeBarObjects8.length = 0;
gdjs.Level4Code.GDMemoryObjects1.length = 0;
gdjs.Level4Code.GDMemoryObjects2.length = 0;
gdjs.Level4Code.GDMemoryObjects3.length = 0;
gdjs.Level4Code.GDMemoryObjects4.length = 0;
gdjs.Level4Code.GDMemoryObjects5.length = 0;
gdjs.Level4Code.GDMemoryObjects6.length = 0;
gdjs.Level4Code.GDMemoryObjects7.length = 0;
gdjs.Level4Code.GDMemoryObjects8.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects1.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects2.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects3.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects4.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects5.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects6.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects7.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595DeathTextObjects8.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects1.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects2.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects3.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects4.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects5.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects6.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects7.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595MemoryAcquiredObjects8.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects1.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects2.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects3.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects4.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects5.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects6.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects7.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595LivesObjects8.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects1.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects2.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects3.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects4.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects5.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects6.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects7.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595HopeObjects8.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects1.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects2.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects3.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects4.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects5.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects6.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects7.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595RetryButtonObjects8.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects1.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects2.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects3.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects4.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects5.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects6.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects7.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595BackgroundObjects8.length = 0;
gdjs.Level4Code.GDHorizontalDemonObjects1.length = 0;
gdjs.Level4Code.GDHorizontalDemonObjects2.length = 0;
gdjs.Level4Code.GDHorizontalDemonObjects3.length = 0;
gdjs.Level4Code.GDHorizontalDemonObjects4.length = 0;
gdjs.Level4Code.GDHorizontalDemonObjects5.length = 0;
gdjs.Level4Code.GDHorizontalDemonObjects6.length = 0;
gdjs.Level4Code.GDHorizontalDemonObjects7.length = 0;
gdjs.Level4Code.GDHorizontalDemonObjects8.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects1.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects2.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects3.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects4.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects5.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects6.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects7.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595BaseObjects8.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects1.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects2.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects3.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects4.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects5.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects6.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects7.length = 0;
gdjs.Level4Code.GDSpikeDemon_9595SpikeObjects8.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects1.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects2.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects3.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects4.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects5.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects6.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects7.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformDownObjects8.length = 0;
gdjs.Level4Code.GDFlippingPlatformObjects1.length = 0;
gdjs.Level4Code.GDFlippingPlatformObjects2.length = 0;
gdjs.Level4Code.GDFlippingPlatformObjects3.length = 0;
gdjs.Level4Code.GDFlippingPlatformObjects4.length = 0;
gdjs.Level4Code.GDFlippingPlatformObjects5.length = 0;
gdjs.Level4Code.GDFlippingPlatformObjects6.length = 0;
gdjs.Level4Code.GDFlippingPlatformObjects7.length = 0;
gdjs.Level4Code.GDFlippingPlatformObjects8.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects1.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects2.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects3.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects4.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects5.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects6.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects7.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595BaseObjects8.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects1.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects2.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects3.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects4.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects5.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects6.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects7.length = 0;
gdjs.Level4Code.GDStalactiteDemon_9595SpikeObjects8.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects1.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects2.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects3.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects4.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects5.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects6.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects7.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595BaseObjects8.length = 0;
gdjs.Level4Code.GDFlippingPlatform2Objects1.length = 0;
gdjs.Level4Code.GDFlippingPlatform2Objects2.length = 0;
gdjs.Level4Code.GDFlippingPlatform2Objects3.length = 0;
gdjs.Level4Code.GDFlippingPlatform2Objects4.length = 0;
gdjs.Level4Code.GDFlippingPlatform2Objects5.length = 0;
gdjs.Level4Code.GDFlippingPlatform2Objects6.length = 0;
gdjs.Level4Code.GDFlippingPlatform2Objects7.length = 0;
gdjs.Level4Code.GDFlippingPlatform2Objects8.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects1.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects2.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects3.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects4.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects5.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects6.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects7.length = 0;
gdjs.Level4Code.GDUI_9595Sinage_9595ProceedButtonObjects8.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamObjects1.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamObjects2.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamObjects3.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamObjects4.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamObjects5.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamObjects6.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamObjects7.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamObjects8.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BaseObjects1.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BaseObjects2.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BaseObjects3.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BaseObjects4.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BaseObjects5.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BaseObjects6.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BaseObjects7.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BaseObjects8.length = 0;
gdjs.Level4Code.GDTestObjects1.length = 0;
gdjs.Level4Code.GDTestObjects2.length = 0;
gdjs.Level4Code.GDTestObjects3.length = 0;
gdjs.Level4Code.GDTestObjects4.length = 0;
gdjs.Level4Code.GDTestObjects5.length = 0;
gdjs.Level4Code.GDTestObjects6.length = 0;
gdjs.Level4Code.GDTestObjects7.length = 0;
gdjs.Level4Code.GDTestObjects8.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects1.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects2.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects3.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects4.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects5.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects6.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects7.length = 0;
gdjs.Level4Code.GDHorizontalMovingPlatformLeftObjects8.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects1.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects2.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects3.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects4.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects5.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects6.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects7.length = 0;
gdjs.Level4Code.GDVerticalMovingPlatformUpObjects8.length = 0;
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects1.length = 0;
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects2.length = 0;
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects3.length = 0;
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects4.length = 0;
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects5.length = 0;
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects6.length = 0;
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects7.length = 0;
gdjs.Level4Code.GDSpinningPlatformClockwiseObjects8.length = 0;
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects1.length = 0;
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects2.length = 0;
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects3.length = 0;
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects4.length = 0;
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects5.length = 0;
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects6.length = 0;
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects7.length = 0;
gdjs.Level4Code.GDSpinningPlatformCounterclockwiseObjects8.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects1.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects2.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects3.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects4.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects5.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects6.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects7.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformClockwiseObjects8.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects1.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects2.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects3.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects4.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects5.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects6.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects7.length = 0;
gdjs.Level4Code.GDSpinningRotatingPlatformCounterclockwiseObjects8.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects1.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects2.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects3.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects4.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects5.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects6.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects7.length = 0;
gdjs.Level4Code.GDStalagmiteDemon_9595SpikeObjects8.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects1.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects2.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects3.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects4.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects5.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects6.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects7.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BasePermanentObjects8.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects1.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects2.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects3.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects4.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects5.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects6.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects7.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentObjects8.length = 0;
gdjs.Level4Code.GDLaserRingObjects1.length = 0;
gdjs.Level4Code.GDLaserRingObjects2.length = 0;
gdjs.Level4Code.GDLaserRingObjects3.length = 0;
gdjs.Level4Code.GDLaserRingObjects4.length = 0;
gdjs.Level4Code.GDLaserRingObjects5.length = 0;
gdjs.Level4Code.GDLaserRingObjects6.length = 0;
gdjs.Level4Code.GDLaserRingObjects7.length = 0;
gdjs.Level4Code.GDLaserRingObjects8.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects1.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects2.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects3.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects4.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects5.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects6.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects7.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamSingleObjects8.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects1.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects2.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects3.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects4.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects5.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects6.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects7.length = 0;
gdjs.Level4Code.GDLaserDemon_9595BeamPermanentSingleObjects8.length = 0;
gdjs.Level4Code.GDTurretDemon_9595BaseObjects1.length = 0;
gdjs.Level4Code.GDTurretDemon_9595BaseObjects2.length = 0;
gdjs.Level4Code.GDTurretDemon_9595BaseObjects3.length = 0;
gdjs.Level4Code.GDTurretDemon_9595BaseObjects4.length = 0;
gdjs.Level4Code.GDTurretDemon_9595BaseObjects5.length = 0;
gdjs.Level4Code.GDTurretDemon_9595BaseObjects6.length = 0;
gdjs.Level4Code.GDTurretDemon_9595BaseObjects7.length = 0;
gdjs.Level4Code.GDTurretDemon_9595BaseObjects8.length = 0;
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects1.length = 0;
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects2.length = 0;
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects3.length = 0;
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects4.length = 0;
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects5.length = 0;
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects6.length = 0;
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects7.length = 0;
gdjs.Level4Code.GDTurretDemon_9595SpikeObjects8.length = 0;
gdjs.Level4Code.GDTurretDemon_9595EyeObjects1.length = 0;
gdjs.Level4Code.GDTurretDemon_9595EyeObjects2.length = 0;
gdjs.Level4Code.GDTurretDemon_9595EyeObjects3.length = 0;
gdjs.Level4Code.GDTurretDemon_9595EyeObjects4.length = 0;
gdjs.Level4Code.GDTurretDemon_9595EyeObjects5.length = 0;
gdjs.Level4Code.GDTurretDemon_9595EyeObjects6.length = 0;
gdjs.Level4Code.GDTurretDemon_9595EyeObjects7.length = 0;
gdjs.Level4Code.GDTurretDemon_9595EyeObjects8.length = 0;
gdjs.Level4Code.GDCameraGlitchFixObjects1.length = 0;
gdjs.Level4Code.GDCameraGlitchFixObjects2.length = 0;
gdjs.Level4Code.GDCameraGlitchFixObjects3.length = 0;
gdjs.Level4Code.GDCameraGlitchFixObjects4.length = 0;
gdjs.Level4Code.GDCameraGlitchFixObjects5.length = 0;
gdjs.Level4Code.GDCameraGlitchFixObjects6.length = 0;
gdjs.Level4Code.GDCameraGlitchFixObjects7.length = 0;
gdjs.Level4Code.GDCameraGlitchFixObjects8.length = 0;
gdjs.Level4Code.GDLeftBoundaryObjects1.length = 0;
gdjs.Level4Code.GDLeftBoundaryObjects2.length = 0;
gdjs.Level4Code.GDLeftBoundaryObjects3.length = 0;
gdjs.Level4Code.GDLeftBoundaryObjects4.length = 0;
gdjs.Level4Code.GDLeftBoundaryObjects5.length = 0;
gdjs.Level4Code.GDLeftBoundaryObjects6.length = 0;
gdjs.Level4Code.GDLeftBoundaryObjects7.length = 0;
gdjs.Level4Code.GDLeftBoundaryObjects8.length = 0;
gdjs.Level4Code.GDRightBoundaryObjects1.length = 0;
gdjs.Level4Code.GDRightBoundaryObjects2.length = 0;
gdjs.Level4Code.GDRightBoundaryObjects3.length = 0;
gdjs.Level4Code.GDRightBoundaryObjects4.length = 0;
gdjs.Level4Code.GDRightBoundaryObjects5.length = 0;
gdjs.Level4Code.GDRightBoundaryObjects6.length = 0;
gdjs.Level4Code.GDRightBoundaryObjects7.length = 0;
gdjs.Level4Code.GDRightBoundaryObjects8.length = 0;
gdjs.Level4Code.GDTopBoundaryObjects1.length = 0;
gdjs.Level4Code.GDTopBoundaryObjects2.length = 0;
gdjs.Level4Code.GDTopBoundaryObjects3.length = 0;
gdjs.Level4Code.GDTopBoundaryObjects4.length = 0;
gdjs.Level4Code.GDTopBoundaryObjects5.length = 0;
gdjs.Level4Code.GDTopBoundaryObjects6.length = 0;
gdjs.Level4Code.GDTopBoundaryObjects7.length = 0;
gdjs.Level4Code.GDTopBoundaryObjects8.length = 0;
gdjs.Level4Code.GDBottomBoundaryObjects1.length = 0;
gdjs.Level4Code.GDBottomBoundaryObjects2.length = 0;
gdjs.Level4Code.GDBottomBoundaryObjects3.length = 0;
gdjs.Level4Code.GDBottomBoundaryObjects4.length = 0;
gdjs.Level4Code.GDBottomBoundaryObjects5.length = 0;
gdjs.Level4Code.GDBottomBoundaryObjects6.length = 0;
gdjs.Level4Code.GDBottomBoundaryObjects7.length = 0;
gdjs.Level4Code.GDBottomBoundaryObjects8.length = 0;
gdjs.Level4Code.GDBoundaryJumpThroughObjects1.length = 0;
gdjs.Level4Code.GDBoundaryJumpThroughObjects2.length = 0;
gdjs.Level4Code.GDBoundaryJumpThroughObjects3.length = 0;
gdjs.Level4Code.GDBoundaryJumpThroughObjects4.length = 0;
gdjs.Level4Code.GDBoundaryJumpThroughObjects5.length = 0;
gdjs.Level4Code.GDBoundaryJumpThroughObjects6.length = 0;
gdjs.Level4Code.GDBoundaryJumpThroughObjects7.length = 0;
gdjs.Level4Code.GDBoundaryJumpThroughObjects8.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects1.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects2.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects3.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects4.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects5.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects6.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects7.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595BaseObjects8.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects1.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects2.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects3.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects4.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects5.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects6.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects7.length = 0;
gdjs.Level4Code.GDShockwaveDemon_9595ShockwaveObjects8.length = 0;
gdjs.Level4Code.GDMiteDemon_9595BaseObjects1.length = 0;
gdjs.Level4Code.GDMiteDemon_9595BaseObjects2.length = 0;
gdjs.Level4Code.GDMiteDemon_9595BaseObjects3.length = 0;
gdjs.Level4Code.GDMiteDemon_9595BaseObjects4.length = 0;
gdjs.Level4Code.GDMiteDemon_9595BaseObjects5.length = 0;
gdjs.Level4Code.GDMiteDemon_9595BaseObjects6.length = 0;
gdjs.Level4Code.GDMiteDemon_9595BaseObjects7.length = 0;
gdjs.Level4Code.GDMiteDemon_9595BaseObjects8.length = 0;
gdjs.Level4Code.GDMiteDemon_9595MiteObjects1.length = 0;
gdjs.Level4Code.GDMiteDemon_9595MiteObjects2.length = 0;
gdjs.Level4Code.GDMiteDemon_9595MiteObjects3.length = 0;
gdjs.Level4Code.GDMiteDemon_9595MiteObjects4.length = 0;
gdjs.Level4Code.GDMiteDemon_9595MiteObjects5.length = 0;
gdjs.Level4Code.GDMiteDemon_9595MiteObjects6.length = 0;
gdjs.Level4Code.GDMiteDemon_9595MiteObjects7.length = 0;
gdjs.Level4Code.GDMiteDemon_9595MiteObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595WObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595WObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595WObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595WObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595WObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595WObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595WObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595WObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595AObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595AObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595AObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595AObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595AObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595AObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595AObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595AObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595SObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595SObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595SObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595SObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595SObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595SObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595SObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595SObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595DObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595DObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595DObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595DObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595DObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595DObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595DObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595DObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595SpaceObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595SpaceObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595SpaceObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595SpaceObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595SpaceObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595SpaceObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595SpaceObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595SpaceObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595HealObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595HealObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595HealObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595HealObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595HealObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595HealObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595HealObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595HealObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595ProceedObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595ProceedObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595ProceedObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595ProceedObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595ProceedObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595ProceedObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595ProceedObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595ProceedObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595DeathObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595DeathObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595DeathObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595DeathObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595DeathObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595DeathObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595DeathObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595DeathObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595DownArrowObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595DownArrowObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595DownArrowObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595DownArrowObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595DownArrowObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595DownArrowObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595DownArrowObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595DownArrowObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595CheckpointObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595CheckpointObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595CheckpointObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595CheckpointObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595CheckpointObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595CheckpointObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595CheckpointObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595CheckpointObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595KillObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595KillObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595KillObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595KillObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595KillObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595KillObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595KillObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595KillObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595CollectObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595CollectObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595CollectObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595CollectObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595CollectObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595CollectObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595CollectObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595CollectObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595ArrowObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595ArrowObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595ArrowObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595ArrowObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595ArrowObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595ArrowObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595ArrowObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595ArrowObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595JumpObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595JumpObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595JumpObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595JumpObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595JumpObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595JumpObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595JumpObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595JumpObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595HoldObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595HoldObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595HoldObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595HoldObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595HoldObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595HoldObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595HoldObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595HoldObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595LongerJumpObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595LongerJumpObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595LongerJumpObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595LongerJumpObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595LongerJumpObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595LongerJumpObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595LongerJumpObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595LongerJumpObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595RestartObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595RestartObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595RestartObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595RestartObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595RestartObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595RestartObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595RestartObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595RestartObjects8.length = 0;
gdjs.Level4Code.GDSinage_9595PauseObjects1.length = 0;
gdjs.Level4Code.GDSinage_9595PauseObjects2.length = 0;
gdjs.Level4Code.GDSinage_9595PauseObjects3.length = 0;
gdjs.Level4Code.GDSinage_9595PauseObjects4.length = 0;
gdjs.Level4Code.GDSinage_9595PauseObjects5.length = 0;
gdjs.Level4Code.GDSinage_9595PauseObjects6.length = 0;
gdjs.Level4Code.GDSinage_9595PauseObjects7.length = 0;
gdjs.Level4Code.GDSinage_9595PauseObjects8.length = 0;
gdjs.Level4Code.GDTest2Objects1.length = 0;
gdjs.Level4Code.GDTest2Objects2.length = 0;
gdjs.Level4Code.GDTest2Objects3.length = 0;
gdjs.Level4Code.GDTest2Objects4.length = 0;
gdjs.Level4Code.GDTest2Objects5.length = 0;
gdjs.Level4Code.GDTest2Objects6.length = 0;
gdjs.Level4Code.GDTest2Objects7.length = 0;
gdjs.Level4Code.GDTest2Objects8.length = 0;
gdjs.Level4Code.GDFrameRateObjects1.length = 0;
gdjs.Level4Code.GDFrameRateObjects2.length = 0;
gdjs.Level4Code.GDFrameRateObjects3.length = 0;
gdjs.Level4Code.GDFrameRateObjects4.length = 0;
gdjs.Level4Code.GDFrameRateObjects5.length = 0;
gdjs.Level4Code.GDFrameRateObjects6.length = 0;
gdjs.Level4Code.GDFrameRateObjects7.length = 0;
gdjs.Level4Code.GDFrameRateObjects8.length = 0;

gdjs.Level4Code.eventsList100(runtimeScene);

return;

}

gdjs['Level4Code'] = gdjs.Level4Code;
