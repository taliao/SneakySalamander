
if (typeof gdjs.evtsExt__Player__CollideWithEnemy !== "undefined") {
  gdjs.evtsExt__Player__CollideWithEnemy.registeredGdjsCallbacks.forEach(callback =>
    gdjs._unregisterCallback(callback)
  );
}

gdjs.evtsExt__Player__CollideWithEnemy = {};
gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1_1final = [];

gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1= [];
gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2= [];
gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects3= [];
gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects4= [];
gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1= [];
gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2= [];
gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects3= [];
gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects4= [];


gdjs.evtsExt__Player__CollideWithEnemy.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595CollideWithEnemy_9546GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1});
gdjs.evtsExt__Player__CollideWithEnemy.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595CollideWithEnemy_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2});
gdjs.evtsExt__Player__CollideWithEnemy.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595CollideWithEnemy_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects3});
gdjs.evtsExt__Player__CollideWithEnemy.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595CollideWithEnemy_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2});
gdjs.evtsExt__Player__CollideWithEnemy.eventsList0 = function(runtimeScene, eventsFunctionContext) {

{



}


{

gdjs.copyArray(gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2, gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects3.length;i<l;++i) {
    if ( gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects3[i].getVariableBoolean(gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects3[i].getVariables().get("HasBeenReaped"), false) ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects3[k] = gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects3[i];
        ++k;
    }
}
gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects3 */
gdjs.copyArray(gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2, gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects3);

{gdjs.evtsExt__Player__UpdateLifeForce.func(runtimeScene, gdjs.evtsExt__Player__CollideWithEnemy.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595CollideWithEnemy_9546GDPlayerObjects3Objects, (gdjs.RuntimeObject.getVariableNumber(((gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects3.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects3[0].getVariables()).get("LifeForce"))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

/* Reuse gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2.length;i<l;++i) {
    if ( gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2[i].getVariableBoolean(gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2[i].getVariables().get("HasBeenReaped"), true) ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2[k] = gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2[i];
        ++k;
    }
}
gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2 */
/* Reuse gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2 */
{gdjs.evtsExt__Player__UpdateLifeForce.func(runtimeScene, gdjs.evtsExt__Player__CollideWithEnemy.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595CollideWithEnemy_9546GDPlayerObjects2Objects, 0.5 * (gdjs.RuntimeObject.getVariableNumber(((gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2[0].getVariables()).get("LifeForce"))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.evtsExt__Player__CollideWithEnemy.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595CollideWithEnemy_9546GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1});
gdjs.evtsExt__Player__CollideWithEnemy.eventsList1 = function(runtimeScene, eventsFunctionContext) {

{

gdjs.copyArray(eventsFunctionContext.getObjects("Player"), gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[i].getBehavior(eventsFunctionContext.getBehaviorName("Parameter")).isFalling() ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[k] = gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[i].getVariableBoolean(gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[i].getVariables().get("IsInShockwave"), false) ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[k] = gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2.length = k;
}
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1, gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2);

/* Reuse gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2 */
{for(var i = 0, len = gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2.length ;i < len;++i) {
    gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2[i].setVariableBoolean(gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2[i].getVariables().get("IsDead"), true);
}
}{gdjs.evtsExt__Player__Bounce.func(runtimeScene, gdjs.evtsExt__Player__CollideWithEnemy.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595CollideWithEnemy_9546GDPlayerObjects2Objects, eventsFunctionContext.getBehaviorName(""), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
{ //Subevents
gdjs.evtsExt__Player__CollideWithEnemy.eventsList0(runtimeScene, eventsFunctionContext);} //End of subevents
}

}


{

gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(eventsFunctionContext.getObjects("Player"), gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2);
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2.length;i<l;++i) {
    if ( !(gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[i].getBehavior(eventsFunctionContext.getBehaviorName("Parameter")).isFalling()) ) {
        isConditionTrue_1 = true;
        gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[k] = gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2.length; j < jLen ; ++j) {
        if ( gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1_1final.indexOf(gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[j]) === -1 )
            gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1_1final.push(gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[j]);
    }
}
}
{
gdjs.copyArray(eventsFunctionContext.getObjects("Player"), gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2);
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2.length;i<l;++i) {
    if ( !(gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[i].getVariableBoolean(gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[i].getVariables().get("IsInShockwave"), false)) ) {
        isConditionTrue_1 = true;
        gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[k] = gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2.length; j < jLen ; ++j) {
        if ( gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1_1final.indexOf(gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[j]) === -1 )
            gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1_1final.push(gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1_1final, gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1 */
{gdjs.evtsExt__Player__TriggerDeath.func(runtimeScene, gdjs.evtsExt__Player__CollideWithEnemy.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595CollideWithEnemy_9546GDPlayerObjects1Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.evtsExt__Player__CollideWithEnemy.eventsList2 = function(runtimeScene, eventsFunctionContext) {

{

gdjs.copyArray(eventsFunctionContext.getObjects("Enemy"), gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1.length;i<l;++i) {
    if ( gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1[i].getVariableBoolean(gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1[i].getVariables().get("IsKillable"), false) ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1[k] = gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1[i];
        ++k;
    }
}
gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(eventsFunctionContext.getObjects("Player"), gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1);
{gdjs.evtsExt__Player__TriggerDeath.func(runtimeScene, gdjs.evtsExt__Player__CollideWithEnemy.mapOfGDgdjs_9546evtsExt_9595_9595Player_9595_9595CollideWithEnemy_9546GDPlayerObjects1Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(eventsFunctionContext.getObjects("Enemy"), gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1.length;i<l;++i) {
    if ( gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1[i].getVariableBoolean(gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1[i].getVariables().get("IsKillable"), true) ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1[k] = gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1[i];
        ++k;
    }
}
gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1.length = k;
if (isConditionTrue_0) {

{ //Subevents
gdjs.evtsExt__Player__CollideWithEnemy.eventsList1(runtimeScene, eventsFunctionContext);} //End of subevents
}

}


};

gdjs.evtsExt__Player__CollideWithEnemy.func = function(runtimeScene, Player, Parameter, Enemy, parentEventsFunctionContext) {
var eventsFunctionContext = {
  _objectsMap: {
"Player": Player
, "Enemy": Enemy
},
  _objectArraysMap: {
"Player": gdjs.objectsListsToArray(Player)
, "Enemy": gdjs.objectsListsToArray(Enemy)
},
  _behaviorNamesMap: {
"Parameter": Parameter
},
  getObjects: function(objectName) {
    return eventsFunctionContext._objectArraysMap[objectName] || [];
  },
  getObjectsLists: function(objectName) {
    return eventsFunctionContext._objectsMap[objectName] || null;
  },
  getBehaviorName: function(behaviorName) {
    return eventsFunctionContext._behaviorNamesMap[behaviorName] || behaviorName;
  },
  createObject: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    if (objectsList) {
      const object = parentEventsFunctionContext ?
        parentEventsFunctionContext.createObject(objectsList.firstKey()) :
        runtimeScene.createObject(objectsList.firstKey());
      if (object) {
        objectsList.get(objectsList.firstKey()).push(object);
        eventsFunctionContext._objectArraysMap[objectName].push(object);
      }
      return object;    }
    return null;
  },
  getInstancesCountOnScene: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    let count = 0;
    if (objectsList) {
      for(const objectName in objectsList.items)
        count += parentEventsFunctionContext ?
parentEventsFunctionContext.getInstancesCountOnScene(objectName) :
        runtimeScene.getInstancesCountOnScene(objectName);
    }
    return count;
  },
  getLayer: function(layerName) {
    return runtimeScene.getLayer(layerName);
  },
  getArgument: function(argName) {
    return "";
  },
  getOnceTriggers: function() { return runtimeScene.getOnceTriggers(); }
};

gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects1.length = 0;
gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects2.length = 0;
gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects3.length = 0;
gdjs.evtsExt__Player__CollideWithEnemy.GDPlayerObjects4.length = 0;
gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects1.length = 0;
gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects2.length = 0;
gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects3.length = 0;
gdjs.evtsExt__Player__CollideWithEnemy.GDEnemyObjects4.length = 0;

gdjs.evtsExt__Player__CollideWithEnemy.eventsList2(runtimeScene, eventsFunctionContext);

return;
}

gdjs.evtsExt__Player__CollideWithEnemy.registeredGdjsCallbacks = [];