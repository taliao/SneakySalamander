#!/bin/bash

# Add npm modules to path
export PATH="/home/eliott/node_modules/.bin:$PATH"

# Enter build dir, start build
cd $1
echo "---- Building $1 ----"
yarn build -wl

echo "---- Packaging $1 ----"
zip -r dist/linux64.zip dist/linux-unpacked/
zip -r dist/win64.zip dist/win-unpacked/
