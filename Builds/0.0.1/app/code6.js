gdjs.TutorialCode = {};
gdjs.TutorialCode.GDPlayerObjects3_1final = [];

gdjs.TutorialCode.GDPortalObjects3_1final = [];

gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects1_1final = [];

gdjs.TutorialCode.GDBackgroundPlantsObjects1= [];
gdjs.TutorialCode.GDBackgroundPlantsObjects2= [];
gdjs.TutorialCode.GDBackgroundPlantsObjects3= [];
gdjs.TutorialCode.GDBackgroundPlantsObjects4= [];
gdjs.TutorialCode.GDBackgroundPlantsObjects5= [];
gdjs.TutorialCode.GDBackgroundPlantsObjects6= [];
gdjs.TutorialCode.GDLeftBoundaryObjects1= [];
gdjs.TutorialCode.GDLeftBoundaryObjects2= [];
gdjs.TutorialCode.GDLeftBoundaryObjects3= [];
gdjs.TutorialCode.GDLeftBoundaryObjects4= [];
gdjs.TutorialCode.GDLeftBoundaryObjects5= [];
gdjs.TutorialCode.GDLeftBoundaryObjects6= [];
gdjs.TutorialCode.GDRightBoundaryObjects1= [];
gdjs.TutorialCode.GDRightBoundaryObjects2= [];
gdjs.TutorialCode.GDRightBoundaryObjects3= [];
gdjs.TutorialCode.GDRightBoundaryObjects4= [];
gdjs.TutorialCode.GDRightBoundaryObjects5= [];
gdjs.TutorialCode.GDRightBoundaryObjects6= [];
gdjs.TutorialCode.GDTopBoundaryObjects1= [];
gdjs.TutorialCode.GDTopBoundaryObjects2= [];
gdjs.TutorialCode.GDTopBoundaryObjects3= [];
gdjs.TutorialCode.GDTopBoundaryObjects4= [];
gdjs.TutorialCode.GDTopBoundaryObjects5= [];
gdjs.TutorialCode.GDTopBoundaryObjects6= [];
gdjs.TutorialCode.GDBottomBoundaryObjects1= [];
gdjs.TutorialCode.GDBottomBoundaryObjects2= [];
gdjs.TutorialCode.GDBottomBoundaryObjects3= [];
gdjs.TutorialCode.GDBottomBoundaryObjects4= [];
gdjs.TutorialCode.GDBottomBoundaryObjects5= [];
gdjs.TutorialCode.GDBottomBoundaryObjects6= [];
gdjs.TutorialCode.GDBoundaryJumpThroughObjects1= [];
gdjs.TutorialCode.GDBoundaryJumpThroughObjects2= [];
gdjs.TutorialCode.GDBoundaryJumpThroughObjects3= [];
gdjs.TutorialCode.GDBoundaryJumpThroughObjects4= [];
gdjs.TutorialCode.GDBoundaryJumpThroughObjects5= [];
gdjs.TutorialCode.GDBoundaryJumpThroughObjects6= [];
gdjs.TutorialCode.GDSinage_9595ArrowObjects1= [];
gdjs.TutorialCode.GDSinage_9595ArrowObjects2= [];
gdjs.TutorialCode.GDSinage_9595ArrowObjects3= [];
gdjs.TutorialCode.GDSinage_9595ArrowObjects4= [];
gdjs.TutorialCode.GDSinage_9595ArrowObjects5= [];
gdjs.TutorialCode.GDSinage_9595ArrowObjects6= [];
gdjs.TutorialCode.GDSinage_9595JumpObjects1= [];
gdjs.TutorialCode.GDSinage_9595JumpObjects2= [];
gdjs.TutorialCode.GDSinage_9595JumpObjects3= [];
gdjs.TutorialCode.GDSinage_9595JumpObjects4= [];
gdjs.TutorialCode.GDSinage_9595JumpObjects5= [];
gdjs.TutorialCode.GDSinage_9595JumpObjects6= [];
gdjs.TutorialCode.GDSinage_9595CollectObjects1= [];
gdjs.TutorialCode.GDSinage_9595CollectObjects2= [];
gdjs.TutorialCode.GDSinage_9595CollectObjects3= [];
gdjs.TutorialCode.GDSinage_9595CollectObjects4= [];
gdjs.TutorialCode.GDSinage_9595CollectObjects5= [];
gdjs.TutorialCode.GDSinage_9595CollectObjects6= [];
gdjs.TutorialCode.GDSinage_9595KillObjects1= [];
gdjs.TutorialCode.GDSinage_9595KillObjects2= [];
gdjs.TutorialCode.GDSinage_9595KillObjects3= [];
gdjs.TutorialCode.GDSinage_9595KillObjects4= [];
gdjs.TutorialCode.GDSinage_9595KillObjects5= [];
gdjs.TutorialCode.GDSinage_9595KillObjects6= [];
gdjs.TutorialCode.GDSinage_9595CheckpointObjects1= [];
gdjs.TutorialCode.GDSinage_9595CheckpointObjects2= [];
gdjs.TutorialCode.GDSinage_9595CheckpointObjects3= [];
gdjs.TutorialCode.GDSinage_9595CheckpointObjects4= [];
gdjs.TutorialCode.GDSinage_9595CheckpointObjects5= [];
gdjs.TutorialCode.GDSinage_9595CheckpointObjects6= [];
gdjs.TutorialCode.GDSinage_9595DownArrowObjects1= [];
gdjs.TutorialCode.GDSinage_9595DownArrowObjects2= [];
gdjs.TutorialCode.GDSinage_9595DownArrowObjects3= [];
gdjs.TutorialCode.GDSinage_9595DownArrowObjects4= [];
gdjs.TutorialCode.GDSinage_9595DownArrowObjects5= [];
gdjs.TutorialCode.GDSinage_9595DownArrowObjects6= [];
gdjs.TutorialCode.GDSinage_9595DeathObjects1= [];
gdjs.TutorialCode.GDSinage_9595DeathObjects2= [];
gdjs.TutorialCode.GDSinage_9595DeathObjects3= [];
gdjs.TutorialCode.GDSinage_9595DeathObjects4= [];
gdjs.TutorialCode.GDSinage_9595DeathObjects5= [];
gdjs.TutorialCode.GDSinage_9595DeathObjects6= [];
gdjs.TutorialCode.GDSinage_9595ProceedObjects1= [];
gdjs.TutorialCode.GDSinage_9595ProceedObjects2= [];
gdjs.TutorialCode.GDSinage_9595ProceedObjects3= [];
gdjs.TutorialCode.GDSinage_9595ProceedObjects4= [];
gdjs.TutorialCode.GDSinage_9595ProceedObjects5= [];
gdjs.TutorialCode.GDSinage_9595ProceedObjects6= [];
gdjs.TutorialCode.GDSinage_9595SpaceObjects1= [];
gdjs.TutorialCode.GDSinage_9595SpaceObjects2= [];
gdjs.TutorialCode.GDSinage_9595SpaceObjects3= [];
gdjs.TutorialCode.GDSinage_9595SpaceObjects4= [];
gdjs.TutorialCode.GDSinage_9595SpaceObjects5= [];
gdjs.TutorialCode.GDSinage_9595SpaceObjects6= [];
gdjs.TutorialCode.GDSinage_9595WObjects1= [];
gdjs.TutorialCode.GDSinage_9595WObjects2= [];
gdjs.TutorialCode.GDSinage_9595WObjects3= [];
gdjs.TutorialCode.GDSinage_9595WObjects4= [];
gdjs.TutorialCode.GDSinage_9595WObjects5= [];
gdjs.TutorialCode.GDSinage_9595WObjects6= [];
gdjs.TutorialCode.GDSinage_9595AObjects1= [];
gdjs.TutorialCode.GDSinage_9595AObjects2= [];
gdjs.TutorialCode.GDSinage_9595AObjects3= [];
gdjs.TutorialCode.GDSinage_9595AObjects4= [];
gdjs.TutorialCode.GDSinage_9595AObjects5= [];
gdjs.TutorialCode.GDSinage_9595AObjects6= [];
gdjs.TutorialCode.GDSinage_9595DObjects1= [];
gdjs.TutorialCode.GDSinage_9595DObjects2= [];
gdjs.TutorialCode.GDSinage_9595DObjects3= [];
gdjs.TutorialCode.GDSinage_9595DObjects4= [];
gdjs.TutorialCode.GDSinage_9595DObjects5= [];
gdjs.TutorialCode.GDSinage_9595DObjects6= [];
gdjs.TutorialCode.GDSinage_9595SObjects1= [];
gdjs.TutorialCode.GDSinage_9595SObjects2= [];
gdjs.TutorialCode.GDSinage_9595SObjects3= [];
gdjs.TutorialCode.GDSinage_9595SObjects4= [];
gdjs.TutorialCode.GDSinage_9595SObjects5= [];
gdjs.TutorialCode.GDSinage_9595SObjects6= [];
gdjs.TutorialCode.GDPlayerObjects1= [];
gdjs.TutorialCode.GDPlayerObjects2= [];
gdjs.TutorialCode.GDPlayerObjects3= [];
gdjs.TutorialCode.GDPlayerObjects4= [];
gdjs.TutorialCode.GDPlayerObjects5= [];
gdjs.TutorialCode.GDPlayerObjects6= [];
gdjs.TutorialCode.GDFlyingDemonObjects1= [];
gdjs.TutorialCode.GDFlyingDemonObjects2= [];
gdjs.TutorialCode.GDFlyingDemonObjects3= [];
gdjs.TutorialCode.GDFlyingDemonObjects4= [];
gdjs.TutorialCode.GDFlyingDemonObjects5= [];
gdjs.TutorialCode.GDFlyingDemonObjects6= [];
gdjs.TutorialCode.GDFireDemonObjects1= [];
gdjs.TutorialCode.GDFireDemonObjects2= [];
gdjs.TutorialCode.GDFireDemonObjects3= [];
gdjs.TutorialCode.GDFireDemonObjects4= [];
gdjs.TutorialCode.GDFireDemonObjects5= [];
gdjs.TutorialCode.GDFireDemonObjects6= [];
gdjs.TutorialCode.GDCheckpointObjects1= [];
gdjs.TutorialCode.GDCheckpointObjects2= [];
gdjs.TutorialCode.GDCheckpointObjects3= [];
gdjs.TutorialCode.GDCheckpointObjects4= [];
gdjs.TutorialCode.GDCheckpointObjects5= [];
gdjs.TutorialCode.GDCheckpointObjects6= [];
gdjs.TutorialCode.GDStaticPlatform3Objects1= [];
gdjs.TutorialCode.GDStaticPlatform3Objects2= [];
gdjs.TutorialCode.GDStaticPlatform3Objects3= [];
gdjs.TutorialCode.GDStaticPlatform3Objects4= [];
gdjs.TutorialCode.GDStaticPlatform3Objects5= [];
gdjs.TutorialCode.GDStaticPlatform3Objects6= [];
gdjs.TutorialCode.GDStaticPlatform2Objects1= [];
gdjs.TutorialCode.GDStaticPlatform2Objects2= [];
gdjs.TutorialCode.GDStaticPlatform2Objects3= [];
gdjs.TutorialCode.GDStaticPlatform2Objects4= [];
gdjs.TutorialCode.GDStaticPlatform2Objects5= [];
gdjs.TutorialCode.GDStaticPlatform2Objects6= [];
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects1= [];
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects2= [];
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects3= [];
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4= [];
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects5= [];
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects6= [];
gdjs.TutorialCode.GDStaticPlatform1Objects1= [];
gdjs.TutorialCode.GDStaticPlatform1Objects2= [];
gdjs.TutorialCode.GDStaticPlatform1Objects3= [];
gdjs.TutorialCode.GDStaticPlatform1Objects4= [];
gdjs.TutorialCode.GDStaticPlatform1Objects5= [];
gdjs.TutorialCode.GDStaticPlatform1Objects6= [];
gdjs.TutorialCode.GDPortalObjects1= [];
gdjs.TutorialCode.GDPortalObjects2= [];
gdjs.TutorialCode.GDPortalObjects3= [];
gdjs.TutorialCode.GDPortalObjects4= [];
gdjs.TutorialCode.GDPortalObjects5= [];
gdjs.TutorialCode.GDPortalObjects6= [];
gdjs.TutorialCode.GDLadderObjects1= [];
gdjs.TutorialCode.GDLadderObjects2= [];
gdjs.TutorialCode.GDLadderObjects3= [];
gdjs.TutorialCode.GDLadderObjects4= [];
gdjs.TutorialCode.GDLadderObjects5= [];
gdjs.TutorialCode.GDLadderObjects6= [];
gdjs.TutorialCode.GDHeartObjects1= [];
gdjs.TutorialCode.GDHeartObjects2= [];
gdjs.TutorialCode.GDHeartObjects3= [];
gdjs.TutorialCode.GDHeartObjects4= [];
gdjs.TutorialCode.GDHeartObjects5= [];
gdjs.TutorialCode.GDHeartObjects6= [];
gdjs.TutorialCode.GDMonsterParticlesObjects1= [];
gdjs.TutorialCode.GDMonsterParticlesObjects2= [];
gdjs.TutorialCode.GDMonsterParticlesObjects3= [];
gdjs.TutorialCode.GDMonsterParticlesObjects4= [];
gdjs.TutorialCode.GDMonsterParticlesObjects5= [];
gdjs.TutorialCode.GDMonsterParticlesObjects6= [];
gdjs.TutorialCode.GDSpikeParticlesObjects1= [];
gdjs.TutorialCode.GDSpikeParticlesObjects2= [];
gdjs.TutorialCode.GDSpikeParticlesObjects3= [];
gdjs.TutorialCode.GDSpikeParticlesObjects4= [];
gdjs.TutorialCode.GDSpikeParticlesObjects5= [];
gdjs.TutorialCode.GDSpikeParticlesObjects6= [];
gdjs.TutorialCode.GDDoorParticlesObjects1= [];
gdjs.TutorialCode.GDDoorParticlesObjects2= [];
gdjs.TutorialCode.GDDoorParticlesObjects3= [];
gdjs.TutorialCode.GDDoorParticlesObjects4= [];
gdjs.TutorialCode.GDDoorParticlesObjects5= [];
gdjs.TutorialCode.GDDoorParticlesObjects6= [];
gdjs.TutorialCode.GDDustParticleObjects1= [];
gdjs.TutorialCode.GDDustParticleObjects2= [];
gdjs.TutorialCode.GDDustParticleObjects3= [];
gdjs.TutorialCode.GDDustParticleObjects4= [];
gdjs.TutorialCode.GDDustParticleObjects5= [];
gdjs.TutorialCode.GDDustParticleObjects6= [];
gdjs.TutorialCode.GDLivesBarObjects1= [];
gdjs.TutorialCode.GDLivesBarObjects2= [];
gdjs.TutorialCode.GDLivesBarObjects3= [];
gdjs.TutorialCode.GDLivesBarObjects4= [];
gdjs.TutorialCode.GDLivesBarObjects5= [];
gdjs.TutorialCode.GDLivesBarObjects6= [];
gdjs.TutorialCode.GDLifeForceBarObjects1= [];
gdjs.TutorialCode.GDLifeForceBarObjects2= [];
gdjs.TutorialCode.GDLifeForceBarObjects3= [];
gdjs.TutorialCode.GDLifeForceBarObjects4= [];
gdjs.TutorialCode.GDLifeForceBarObjects5= [];
gdjs.TutorialCode.GDLifeForceBarObjects6= [];
gdjs.TutorialCode.GDMemoryObjects1= [];
gdjs.TutorialCode.GDMemoryObjects2= [];
gdjs.TutorialCode.GDMemoryObjects3= [];
gdjs.TutorialCode.GDMemoryObjects4= [];
gdjs.TutorialCode.GDMemoryObjects5= [];
gdjs.TutorialCode.GDMemoryObjects6= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects1= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects2= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects3= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects4= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects5= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects6= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects1= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects2= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects4= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects5= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects6= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects1= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects2= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects3= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects4= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects5= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects6= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LifeForceObjects1= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LifeForceObjects2= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LifeForceObjects3= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LifeForceObjects4= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LifeForceObjects5= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595LifeForceObjects6= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects1= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects3= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects4= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects5= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects6= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects1= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects2= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects3= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects4= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects5= [];
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects6= [];
gdjs.TutorialCode.GDHorizontalDemonObjects1= [];
gdjs.TutorialCode.GDHorizontalDemonObjects2= [];
gdjs.TutorialCode.GDHorizontalDemonObjects3= [];
gdjs.TutorialCode.GDHorizontalDemonObjects4= [];
gdjs.TutorialCode.GDHorizontalDemonObjects5= [];
gdjs.TutorialCode.GDHorizontalDemonObjects6= [];
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects1= [];
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects2= [];
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3= [];
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4= [];
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5= [];
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects6= [];
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects1= [];
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects2= [];
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3= [];
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects4= [];
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects5= [];
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects6= [];
gdjs.TutorialCode.GDVerticalMovingPlatformObjects1= [];
gdjs.TutorialCode.GDVerticalMovingPlatformObjects2= [];
gdjs.TutorialCode.GDVerticalMovingPlatformObjects3= [];
gdjs.TutorialCode.GDVerticalMovingPlatformObjects4= [];
gdjs.TutorialCode.GDVerticalMovingPlatformObjects5= [];
gdjs.TutorialCode.GDVerticalMovingPlatformObjects6= [];
gdjs.TutorialCode.GDSpinningMovingPlatformObjects1= [];
gdjs.TutorialCode.GDSpinningMovingPlatformObjects2= [];
gdjs.TutorialCode.GDSpinningMovingPlatformObjects3= [];
gdjs.TutorialCode.GDSpinningMovingPlatformObjects4= [];
gdjs.TutorialCode.GDSpinningMovingPlatformObjects5= [];
gdjs.TutorialCode.GDSpinningMovingPlatformObjects6= [];
gdjs.TutorialCode.GDFlippingPlatformObjects1= [];
gdjs.TutorialCode.GDFlippingPlatformObjects2= [];
gdjs.TutorialCode.GDFlippingPlatformObjects3= [];
gdjs.TutorialCode.GDFlippingPlatformObjects4= [];
gdjs.TutorialCode.GDFlippingPlatformObjects5= [];
gdjs.TutorialCode.GDFlippingPlatformObjects6= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects1= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects2= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects5= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects6= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects1= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects2= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects5= [];
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects6= [];


gdjs.TutorialCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.TutorialCode.GDFlippingPlatformObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDFlippingPlatformObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDFlippingPlatformObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(0.2, 1, 1, 1, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 2;
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 3;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.TutorialCode.GDFlippingPlatformObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDFlippingPlatformObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDFlippingPlatformObjects3[i].rotateTowardAngle(-(80), 0, runtimeScene);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "PlatformFlip") >= 6;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlippingPlatform"), gdjs.TutorialCode.GDFlippingPlatformObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDFlippingPlatformObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDFlippingPlatformObjects3[i].rotateTowardAngle(0, 0, runtimeScene);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "PlatformFlip");
}}

}


};gdjs.TutorialCode.eventsList2 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList1(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects2});
gdjs.TutorialCode.eventsList3 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "w");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects3[i].getBehavior("PlatformerObject").simulateControl("Up");
}
}{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects3[i].getBehavior("PlatformerObject").simulateControl("Ladder");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "a");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects3[i].getBehavior("PlatformerObject").simulateControl("Left");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "d");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects3[i].getBehavior("PlatformerObject").simulateControl("Right");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects3[i].getBehavior("PlatformerObject").simulateControl("Jump");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "s");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects3[i].getBehavior("PlatformerObject").simulateControl("Down");
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "g");
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects2);
{gdjs.evtsExt__Player__HealPlayer.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList4 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList3(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects4Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects4Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects4Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects4Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects4Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects4Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4});
gdjs.TutorialCode.eventsList5 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.TutorialCode.GDFlyingDemonObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFlyingDemonObjects4 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.TutorialCode.GDFireDemonObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFireDemonObjects4 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.TutorialCode.GDHorizontalDemonObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDHorizontalDemonObjects4 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4 */
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects3[i].setVariableBoolean(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(4), false);
}
}}

}


};gdjs.TutorialCode.eventsList6 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "AssetDev/Audio/Heartbeat_Amplified.wav", 2, true, 100, 1);
}{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects4[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects4[i].getVariables().getFromIndex(1)) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects4[k] = gdjs.TutorialCode.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.setSoundOnChannelVolume(runtimeScene, 2, 60);
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDDustParticleObjects3Objects = Hashtable.newFrom({"DustParticle": gdjs.TutorialCode.GDDustParticleObjects3});
gdjs.TutorialCode.eventsList7 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList6(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects4[i].getBehavior("PlatformerObject").isJumping() ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects4[k] = gdjs.TutorialCode.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21209932);
}
}
if (isConditionTrue_0) {
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtsExt__Player__IsSteppingOnFloor.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, "PlatformerObject", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21210212);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
gdjs.TutorialCode.GDDustParticleObjects3.length = 0;

{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "grass.mp3", 1, false, 20, gdjs.randomFloatInRange(0.7, 1.2));
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDDustParticleObjects3Objects, (( gdjs.TutorialCode.GDPlayerObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDPlayerObjects3[0].getAABBCenterX()), (( gdjs.TutorialCode.GDPlayerObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDPlayerObjects3[0].getAABBBottom()), "");
}{for(var i = 0, len = gdjs.TutorialCode.GDDustParticleObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDDustParticleObjects3[i].setZOrder(-(1));
}
}{for(var i = 0, len = gdjs.TutorialCode.GDDustParticleObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDDustParticleObjects3[i].setAngle(270);
}
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDCheckpointObjects3Objects = Hashtable.newFrom({"Checkpoint": gdjs.TutorialCode.GDCheckpointObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDCheckpointObjects4Objects = Hashtable.newFrom({"Checkpoint": gdjs.TutorialCode.GDCheckpointObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.eventsList8 = function(runtimeScene) {

{



}


{

gdjs.copyArray(gdjs.TutorialCode.GDCheckpointObjects3, gdjs.TutorialCode.GDCheckpointObjects4);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDCheckpointObjects4Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDCheckpointObjects4 */
{for(var i = 0, len = gdjs.TutorialCode.GDCheckpointObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDCheckpointObjects4[i].getBehavior("Animation").setAnimationName("Inactive");
}
}}

}


{



}


{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.TutorialCode.GDCheckpointObjects3 */
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, (( gdjs.TutorialCode.GDCheckpointObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDCheckpointObjects3[0].getPointX("")), (( gdjs.TutorialCode.GDCheckpointObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDCheckpointObjects3[0].getPointY("")), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.TutorialCode.GDCheckpointObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDCheckpointObjects3[i].getBehavior("Animation").setAnimationName("Activate");
}
}}

}


};gdjs.TutorialCode.eventsList9 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
{gdjs.evtsExt__Checkpoints__SaveCheckpoint.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, (( gdjs.TutorialCode.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDPlayerObjects4[0].getPointX("")), (( gdjs.TutorialCode.GDPlayerObjects4.length === 0 ) ? 0 :gdjs.TutorialCode.GDPlayerObjects4[0].getPointY("")), "Checkpoint", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Checkpoint"), gdjs.TutorialCode.GDCheckpointObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDCheckpointObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDCheckpointObjects3.length;i<l;++i) {
    if ( !(gdjs.TutorialCode.GDCheckpointObjects3[i].isCurrentAnimationName("Activate")) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDCheckpointObjects3[k] = gdjs.TutorialCode.GDCheckpointObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDCheckpointObjects3.length = k;
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Checkpoint/Activate.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 1);
}
{ //Subevents
gdjs.TutorialCode.eventsList8(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHeartObjects3Objects = Hashtable.newFrom({"Heart": gdjs.TutorialCode.GDHeartObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeParticlesObjects3Objects = Hashtable.newFrom({"SpikeParticles": gdjs.TutorialCode.GDSpikeParticlesObjects3});
gdjs.TutorialCode.eventsList10 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Heart"), gdjs.TutorialCode.GDHeartObjects3);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHeartObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDHeartObjects3 */
gdjs.TutorialCode.GDSpikeParticlesObjects3.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeParticlesObjects3Objects, (( gdjs.TutorialCode.GDHeartObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDHeartObjects3[0].getCenterXInScene()), (( gdjs.TutorialCode.GDHeartObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDHeartObjects3[0].getCenterYInScene()), "");
}{for(var i = 0, len = gdjs.TutorialCode.GDHeartObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDHeartObjects3[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(100);
}{gdjs.evtTools.sound.playSound(runtimeScene, "AssetDev/Audio/Squelch.wav", false, 50, 1);
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects2});
gdjs.TutorialCode.eventsList11 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects2[i].getY() > gdjs.evtTools.camera.getCameraBorderBottom(runtimeScene, "", 0) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects2[k] = gdjs.TutorialCode.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects2 */
{gdjs.evtsExt__Player__TriggerDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList12 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects3[i].getVariableBoolean(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(4), true) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects3[k] = gdjs.TutorialCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList5(runtimeScene);} //End of subevents
}

}


{


gdjs.TutorialCode.eventsList7(runtimeScene);
}


{


gdjs.TutorialCode.eventsList9(runtimeScene);
}


{


gdjs.TutorialCode.eventsList10(runtimeScene);
}


{


gdjs.TutorialCode.eventsList11(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.TutorialCode.GDMonsterParticlesObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects2});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects2Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects2});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects2});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects2Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects2});
gdjs.TutorialCode.eventsList13 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.TutorialCode.GDFlyingDemonObjects2);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects2Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFlyingDemonObjects2 */
/* Reuse gdjs.TutorialCode.GDPlayerObjects2 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList14 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.TutorialCode.GDFlyingDemonObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.TutorialCode.GDFlyingDemonObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFlyingDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.TutorialCode.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.TutorialCode.eventsList13(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.TutorialCode.GDMonsterParticlesObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects2});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects2Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects2});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects2});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects2Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects2});
gdjs.TutorialCode.eventsList15 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.TutorialCode.GDFireDemonObjects2);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects2Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFireDemonObjects2 */
/* Reuse gdjs.TutorialCode.GDPlayerObjects2 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList16 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.TutorialCode.GDFireDemonObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.TutorialCode.GDFireDemonObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFireDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.TutorialCode.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.TutorialCode.GDFireDemonObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDFireDemonObjects3.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDFireDemonObjects3[i].getBehavior("Animation").getAnimationName() == "Fire" ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDFireDemonObjects3[k] = gdjs.TutorialCode.GDFireDemonObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDFireDemonObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFireDemonObjects3 */
{for(var i = 0, len = gdjs.TutorialCode.GDFireDemonObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDFireDemonObjects3[i].returnVariable(gdjs.TutorialCode.GDFireDemonObjects3[i].getVariables().getFromIndex(2)).setNumber(0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.TutorialCode.GDFireDemonObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDFireDemonObjects3.length;i<l;++i) {
    if ( !(gdjs.TutorialCode.GDFireDemonObjects3[i].getBehavior("Animation").getAnimationName() == "Fire") ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDFireDemonObjects3[k] = gdjs.TutorialCode.GDFireDemonObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDFireDemonObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDFireDemonObjects3 */
{for(var i = 0, len = gdjs.TutorialCode.GDFireDemonObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDFireDemonObjects3[i].returnVariable(gdjs.TutorialCode.GDFireDemonObjects3[i].getVariables().getFromIndex(2)).setNumber(1);
}
}}

}


{


gdjs.TutorialCode.eventsList15(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.TutorialCode.GDMonsterParticlesObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects2});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects2Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects2});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects2});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects2Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects2});
gdjs.TutorialCode.eventsList17 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.TutorialCode.GDHorizontalDemonObjects2);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects2Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDHorizontalDemonObjects2 */
/* Reuse gdjs.TutorialCode.GDPlayerObjects2 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList18 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.TutorialCode.GDHorizontalDemonObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.TutorialCode.GDHorizontalDemonObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDHorizontalDemonObjects3 */
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.TutorialCode.GDMonsterParticlesObjects3);
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.TutorialCode.eventsList17(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.TutorialCode.GDMonsterParticlesObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.TutorialCode.GDMonsterParticlesObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"SpikeDemon_Spike": gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3});
gdjs.TutorialCode.eventsList19 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects4 */
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.userFunc0x106e170 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
var NumSpikes = 6;
var SpikeScale = 64;

for (var x = 0; x < objects.length; x++) {
    var SpikeDemonBaseInstnace = objects[x];

    for (var i = 0; i < NumSpikes; i++) {
        var SpikeAngle = (360/NumSpikes)*i;

        const Spike = runtimeScene.createObject("SpikeDemon_Spike");
        Spike.setWidth(SpikeScale);
        Spike.setHeight(SpikeScale);
        var CenterX = SpikeDemonBaseInstnace.x + SpikeDemonBaseInstnace.getWidth()/4;
        var CenterY = SpikeDemonBaseInstnace.y + SpikeDemonBaseInstnace.getHeight()/4;
        Spike.setPosition(CenterX, CenterY);
        Spike.setAngle(SpikeAngle);
        
        Spike.setLayer("Base Layer");


    }
}
};
gdjs.TutorialCode.eventsList20 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4);

var objects = [];
objects.push.apply(objects,gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4);
gdjs.TutorialCode.userFunc0x106e170(runtimeScene, objects);

}


};gdjs.TutorialCode.eventsList21 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(1.5, 3, 3, 2, 0, 0.04, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.eventsList22 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy") >= 8;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy");
}
{ //Subevents
gdjs.TutorialCode.eventsList20(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "SpikeEnemy") >= 5;
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList21(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList23 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects3Objects);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.TutorialCode.GDMonsterParticlesObjects3);
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3 */
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Spike"), gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects3Objects);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.TutorialCode.GDMonsterParticlesObjects3);
/* Reuse gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3 */
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595SpikeObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


gdjs.TutorialCode.eventsList19(runtimeScene);
}


{


gdjs.TutorialCode.eventsList22(runtimeScene);
}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "SpikeEnemy");
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects = Hashtable.newFrom({"MonsterParticles": gdjs.TutorialCode.GDMonsterParticlesObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStaticPlatform1Objects3ObjectsGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects3ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform2Objects3ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform3Objects3Objects = Hashtable.newFrom({"StaticPlatform1": gdjs.TutorialCode.GDStaticPlatform1Objects3, "HorizontalMovingPlatform": gdjs.TutorialCode.GDHorizontalMovingPlatformObjects3, "StaticPlatform2": gdjs.TutorialCode.GDStaticPlatform2Objects3, "StaticPlatform3": gdjs.TutorialCode.GDStaticPlatform3Objects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeParticlesObjects3Objects = Hashtable.newFrom({"SpikeParticles": gdjs.TutorialCode.GDSpikeParticlesObjects3});
gdjs.TutorialCode.eventsList24 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{/* Unknown object - skipped. */}}

}


{

gdjs.copyArray(runtimeScene.getObjects("HorizontalMovingPlatform"), gdjs.TutorialCode.GDHorizontalMovingPlatformObjects3);
/* Reuse gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3 */
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform1"), gdjs.TutorialCode.GDStaticPlatform1Objects3);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform2"), gdjs.TutorialCode.GDStaticPlatform2Objects3);
gdjs.copyArray(runtimeScene.getObjects("StaticPlatform3"), gdjs.TutorialCode.GDStaticPlatform3Objects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStaticPlatform1Objects3ObjectsGDgdjs_9546TutorialCode_9546GDHorizontalMovingPlatformObjects3ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform2Objects3ObjectsGDgdjs_9546TutorialCode_9546GDStaticPlatform3Objects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3 */
gdjs.TutorialCode.GDSpikeParticlesObjects3.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeParticlesObjects3Objects, (( gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3[0].getPointX("")), (( gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3[0].getPointY("")), "");
}{for(var i = 0, len = gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects4Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Spike": gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3});
gdjs.TutorialCode.eventsList25 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects4Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects4 */
/* Reuse gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects4Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike"), gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
/* Reuse gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3 */
{gdjs.evtsExt__Player__CollideWithEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, "PlatformerObject", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.userFunc0x11d55e8 = function GDJSInlineCode(runtimeScene, objects) {
"use strict";
for (var x = 0; x < objects.length; x++) {
    var Stalagtite = objects[x];
    const Spike = runtimeScene.createObject("StalagtiteDemon_Spike");
    Spike.setWidth(96);
    Spike.setHeight(48);
    var CenterX = Stalagtite.x + Stalagtite.getWidth()/3;
    var CenterY = Stalagtite.y + Stalagtite.getHeight() - Stalagtite.getHeight()/3;
    Spike.setPosition(CenterX, CenterY);
    Spike.setAngle(90);
    Spike.setLayer("Base Layer");
}
};
gdjs.TutorialCode.eventsList26 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4);

var objects = [];
objects.push.apply(objects,gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4);
gdjs.TutorialCode.userFunc0x11d55e8(runtimeScene, objects);

}


};gdjs.TutorialCode.eventsList27 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy") >= 6;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}
{ //Subevents
gdjs.TutorialCode.eventsList26(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "StalagtiteEnemy") >= 4;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3[i].getBehavior("ShakeObject_PositionAngleScale").ShakeObject_PositionAngleScale(1.5, 1, 1, 1, 0, 0.02, false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.eventsList28 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3);
{gdjs.evtsExt__Enemy__SetEarlyResetPosition.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects3Objects);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("MonsterParticles"), gdjs.TutorialCode.GDMonsterParticlesObjects3);
/* Reuse gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3 */
{gdjs.evtsExt__Enemy__HandleDeath.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDMonsterParticlesObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Spike"), gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.pickAllObjects((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595SpikeObjects3Objects);
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList24(runtimeScene);} //End of subevents
}

}


{


gdjs.TutorialCode.eventsList25(runtimeScene);
}


{


gdjs.TutorialCode.eventsList27(runtimeScene);
}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "StalagtiteEnemy");
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects4Objects = Hashtable.newFrom({"Portal": gdjs.TutorialCode.GDPortalObjects4});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects3Objects = Hashtable.newFrom({"Portal": gdjs.TutorialCode.GDPortalObjects3});
gdjs.TutorialCode.eventsList29 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(gdjs.TutorialCode.GDPlayerObjects3, gdjs.TutorialCode.GDPlayerObjects4);

{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects4[i].activateBehavior("PlatformerObject", false);
}
}}

}


{

/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects3[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects3[k] = gdjs.TutorialCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.TutorialCode.GDPlayerObjects3 */
/* Reuse gdjs.TutorialCode.GDPortalObjects3 */
{gdjs.evtsExt__Player__AnimateFallingIntoPortal.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, "Tween", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList30 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList29(runtimeScene);
}


};gdjs.TutorialCode.eventsList31 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects4[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects4[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects4[k] = gdjs.TutorialCode.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects4.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_DeathText"), gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects4[i].hide();
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects3.length;i<l;++i) {
    if ( !(gdjs.TutorialCode.GDPlayerObjects3[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(1)) > 0) ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects3[k] = gdjs.TutorialCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects3.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_MemoryAcquired"), gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3[i].hide();
}
}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDUI_95959595Sinage_95959595BackgroundObjects2Objects = Hashtable.newFrom({"UI_Sinage_Background": gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects2});
gdjs.TutorialCode.eventsList32 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21251092);
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList31(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_Background"), gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects2);
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_RetryButton"), gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2);
{gdjs.evtsExt__UserInterface__StretchToFillScreen.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDUI_95959595Sinage_95959595BackgroundObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{for(var i = 0, len = gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects2.length ;i < len;++i) {
    gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects2[i].setCenterXInScene(gdjs.evtTools.camera.getCameraX(runtimeScene, "EndScreen", 0));
}
for(var i = 0, len = gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2.length ;i < len;++i) {
    gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2[i].setCenterXInScene(gdjs.evtTools.camera.getCameraX(runtimeScene, "EndScreen", 0));
}
}}

}


};gdjs.TutorialCode.eventsList33 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList32(runtimeScene);
}


};gdjs.TutorialCode.eventsList34 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_Background"), gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects3);
{gdjs.evtTools.camera.hideLayer(runtimeScene, "EndScreen");
}{for(var i = 0, len = gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects3[i].setOpacity(180);
}
}}

}


{



}


{

gdjs.TutorialCode.GDPlayerObjects3.length = 0;

gdjs.TutorialCode.GDPortalObjects3.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.TutorialCode.GDPlayerObjects3_1final.length = 0;
gdjs.TutorialCode.GDPortalObjects3_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.TutorialCode.GDPortalObjects4);
isConditionTrue_1 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects4Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects4Objects, false, runtimeScene, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.TutorialCode.GDPlayerObjects4.length; j < jLen ; ++j) {
        if ( gdjs.TutorialCode.GDPlayerObjects3_1final.indexOf(gdjs.TutorialCode.GDPlayerObjects4[j]) === -1 )
            gdjs.TutorialCode.GDPlayerObjects3_1final.push(gdjs.TutorialCode.GDPlayerObjects4[j]);
    }
    for (let j = 0, jLen = gdjs.TutorialCode.GDPortalObjects4.length; j < jLen ; ++j) {
        if ( gdjs.TutorialCode.GDPortalObjects3_1final.indexOf(gdjs.TutorialCode.GDPortalObjects4[j]) === -1 )
            gdjs.TutorialCode.GDPortalObjects3_1final.push(gdjs.TutorialCode.GDPortalObjects4[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects4);
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects4.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects4[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects4[i].getVariables().getFromIndex(1)) <= 0 ) {
        isConditionTrue_1 = true;
        gdjs.TutorialCode.GDPlayerObjects4[k] = gdjs.TutorialCode.GDPlayerObjects4[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects4.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.TutorialCode.GDPlayerObjects4.length; j < jLen ; ++j) {
        if ( gdjs.TutorialCode.GDPlayerObjects3_1final.indexOf(gdjs.TutorialCode.GDPlayerObjects4[j]) === -1 )
            gdjs.TutorialCode.GDPlayerObjects3_1final.push(gdjs.TutorialCode.GDPlayerObjects4[j]);
    }
}
}
{
gdjs.copyArray(gdjs.TutorialCode.GDPlayerObjects3_1final, gdjs.TutorialCode.GDPlayerObjects3);
gdjs.copyArray(gdjs.TutorialCode.GDPortalObjects3_1final, gdjs.TutorialCode.GDPortalObjects3);
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21247140);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.camera.showLayer(runtimeScene, "EndScreen");
}
{ //Subevents
gdjs.TutorialCode.eventsList30(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "EndScreen");
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList33(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList35 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("BoundaryJumpThrough"), gdjs.TutorialCode.GDBoundaryJumpThroughObjects4);
gdjs.copyArray(runtimeScene.getObjects("LeftBoundary"), gdjs.TutorialCode.GDLeftBoundaryObjects4);
gdjs.copyArray(runtimeScene.getObjects("RightBoundary"), gdjs.TutorialCode.GDRightBoundaryObjects4);
{for(var i = 0, len = gdjs.TutorialCode.GDLeftBoundaryObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDLeftBoundaryObjects4[i].hide();
}
for(var i = 0, len = gdjs.TutorialCode.GDRightBoundaryObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDRightBoundaryObjects4[i].hide();
}
for(var i = 0, len = gdjs.TutorialCode.GDBoundaryJumpThroughObjects4.length ;i < len;++i) {
    gdjs.TutorialCode.GDBoundaryJumpThroughObjects4[i].hide();
}
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("BottomBoundary"), gdjs.TutorialCode.GDBottomBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("LeftBoundary"), gdjs.TutorialCode.GDLeftBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("RightBoundary"), gdjs.TutorialCode.GDRightBoundaryObjects3);
gdjs.copyArray(runtimeScene.getObjects("TopBoundary"), gdjs.TutorialCode.GDTopBoundaryObjects3);
{gdjs.evtTools.camera.clampCamera(runtimeScene, (( gdjs.TutorialCode.GDLeftBoundaryObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDLeftBoundaryObjects3[0].getPointX("")) + (( gdjs.TutorialCode.GDLeftBoundaryObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDLeftBoundaryObjects3[0].getWidth()), (( gdjs.TutorialCode.GDTopBoundaryObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDTopBoundaryObjects3[0].getPointY("")) + (( gdjs.TutorialCode.GDTopBoundaryObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDTopBoundaryObjects3[0].getHeight()), (( gdjs.TutorialCode.GDRightBoundaryObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDRightBoundaryObjects3[0].getPointX("")), (( gdjs.TutorialCode.GDBottomBoundaryObjects3.length === 0 ) ? 0 :gdjs.TutorialCode.GDBottomBoundaryObjects3[0].getPointY("")), "", 0);
}}

}


};gdjs.TutorialCode.eventsList36 = function(runtimeScene) {

{



}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "RandomNoiseTimer");
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("BackgroundPlants"), gdjs.TutorialCode.GDBackgroundPlantsObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDBackgroundPlantsObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDBackgroundPlantsObjects3[i].setWidth(gdjs.evtTools.camera.getCameraWidth(runtimeScene, "", 0));
}
}{for(var i = 0, len = gdjs.TutorialCode.GDBackgroundPlantsObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDBackgroundPlantsObjects3[i].setXOffset(gdjs.evtTools.camera.getCameraBorderLeft(runtimeScene, "", 0) / 3 + 780);
}
}}

}


};gdjs.TutorialCode.eventsList37 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{/* Unknown object - skipped. */}}

}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects3Objects = Hashtable.newFrom({"Portal": gdjs.TutorialCode.GDPortalObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects2Objects = Hashtable.newFrom({"Portal": gdjs.TutorialCode.GDPortalObjects2});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.TutorialCode.GDPlayerObjects2});
gdjs.TutorialCode.eventsList38 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "door.aac", 0, true, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.TutorialCode.GDPortalObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects3Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects3Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21261092);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Portal/PortalInteract.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 1);
}}

}


{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects2);
gdjs.copyArray(runtimeScene.getObjects("Portal"), gdjs.TutorialCode.GDPortalObjects2);
{gdjs.evtsExt__VolumeFalloff__SetVolumeFalloff.func(runtimeScene, 0, "Sound", gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPortalObjects2Objects, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDPlayerObjects2Objects, 0, 100, 750, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


};gdjs.TutorialCode.eventsList39 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList35(runtimeScene);
}


{


gdjs.TutorialCode.eventsList36(runtimeScene);
}


{


gdjs.TutorialCode.eventsList37(runtimeScene);
}


{


gdjs.TutorialCode.eventsList38(runtimeScene);
}


};gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects = Hashtable.newFrom({"FlyingDemon": gdjs.TutorialCode.GDFlyingDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects = Hashtable.newFrom({"FireDemon": gdjs.TutorialCode.GDFireDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects = Hashtable.newFrom({"HorizontalDemon": gdjs.TutorialCode.GDHorizontalDemonObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"SpikeDemon_Base": gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3});
gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects3Objects = Hashtable.newFrom({"StalagtiteDemon_Base": gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3});
gdjs.TutorialCode.eventsList40 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Tilde");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21263060);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("FireDemon"), gdjs.TutorialCode.GDFireDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("FlyingDemon"), gdjs.TutorialCode.GDFlyingDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("HorizontalDemon"), gdjs.TutorialCode.GDHorizontalDemonObjects3);
gdjs.copyArray(runtimeScene.getObjects("SpikeDemon_Base"), gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3);
gdjs.copyArray(runtimeScene.getObjects("StalagtiteDemon_Base"), gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3);
{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFlyingDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDFireDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDHorizontalDemonObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDSpikeDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtsExt__Enemy__ResetEnemy.func(runtimeScene, gdjs.TutorialCode.mapOfGDgdjs_9546TutorialCode_9546GDStalagtiteDemon_95959595BaseObjects3Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "i");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21263972);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects3);
{for(var i = 0, len = gdjs.TutorialCode.GDPlayerObjects3.length ;i < len;++i) {
    gdjs.TutorialCode.GDPlayerObjects3[i].returnVariable(gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(5)).setNumber(1 - gdjs.TutorialCode.GDPlayerObjects3[i].getVariables().getFromIndex(5).getAsNumber());
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Escape");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21265524);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, gdjs.evtTools.runtimeScene.getSceneName(runtimeScene), false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "l");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21265596);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "TestingLevel", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "m");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21266964);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Mindscape", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num1");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21267724);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Tutorial", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num2");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21267404);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level1", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num3");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21269084);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level2", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num4");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21269748);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level3", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num5");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21270508);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level4", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num6");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21270188);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level5", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num7");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(21271868);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level6", false);
}}

}


};gdjs.TutorialCode.eventsList41 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("LifeForceBar"), gdjs.TutorialCode.GDLifeForceBarObjects1);
gdjs.copyArray(runtimeScene.getObjects("LivesBar"), gdjs.TutorialCode.GDLivesBarObjects1);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects1);
{for(var i = 0, len = gdjs.TutorialCode.GDLivesBarObjects1.length ;i < len;++i) {
    gdjs.TutorialCode.GDLivesBarObjects1[i].SetValue((gdjs.RuntimeObject.getVariableNumber(((gdjs.TutorialCode.GDPlayerObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.TutorialCode.GDPlayerObjects1[0].getVariables()).getFromIndex(1))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.TutorialCode.GDLifeForceBarObjects1.length ;i < len;++i) {
    gdjs.TutorialCode.GDLifeForceBarObjects1[i].SetValue((gdjs.RuntimeObject.getVariableNumber(((gdjs.TutorialCode.GDPlayerObjects1.length === 0 ) ? gdjs.VariablesContainer.badVariablesContainer : gdjs.TutorialCode.GDPlayerObjects1[0].getVariables()).getFromIndex(3))), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.TutorialCode.eventsList42 = function(runtimeScene) {

{


gdjs.TutorialCode.eventsList2(runtimeScene);
}


{


gdjs.TutorialCode.eventsList4(runtimeScene);
}


{


gdjs.TutorialCode.eventsList12(runtimeScene);
}


{


gdjs.TutorialCode.eventsList14(runtimeScene);
}


{


gdjs.TutorialCode.eventsList16(runtimeScene);
}


{


gdjs.TutorialCode.eventsList18(runtimeScene);
}


{


gdjs.TutorialCode.eventsList23(runtimeScene);
}


{


gdjs.TutorialCode.eventsList28(runtimeScene);
}


{


gdjs.TutorialCode.eventsList34(runtimeScene);
}


{


gdjs.TutorialCode.eventsList39(runtimeScene);
}


{


gdjs.TutorialCode.eventsList40(runtimeScene);
}


{


gdjs.TutorialCode.eventsList41(runtimeScene);
}


};gdjs.TutorialCode.eventsList43 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects2[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects2[i].getVariables().getFromIndex(1)) > 0 ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects2[k] = gdjs.TutorialCode.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Mindscape", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.TutorialCode.GDPlayerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDPlayerObjects1[i].getVariableNumber(gdjs.TutorialCode.GDPlayerObjects1[i].getVariables().getFromIndex(1)) <= 0 ) {
        isConditionTrue_0 = true;
        gdjs.TutorialCode.GDPlayerObjects1[k] = gdjs.TutorialCode.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.TutorialCode.GDPlayerObjects1.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Tutorial", false);
}}

}


};gdjs.TutorialCode.eventsList44 = function(runtimeScene) {

{

gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.camera.layerIsVisible(runtimeScene, "EndScreen");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("UI_Sinage_RetryButton"), gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2);
for (var i = 0, k = 0, l = gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2.length;i<l;++i) {
    if ( gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2[k] = gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2[i];
        ++k;
    }
}
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2.length; j < jLen ; ++j) {
        if ( gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects1_1final.indexOf(gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2[j]) === -1 )
            gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects1_1final.push(gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2[j]);
    }
}
}
{
isConditionTrue_1 = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "Space");
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
}
}
{
gdjs.copyArray(gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects1_1final, gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects1);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.TutorialCode.eventsList43(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList45 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 1;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance1.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance2.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("RandomSFXIndex")) == 3;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/RandomSounds/RandomAmbiance3.wav", false, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 1);
}}

}


};gdjs.TutorialCode.eventsList46 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Assets/Sounds/Levels/0/AmbientLoop.ogg", true, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)), 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "RandomNoiseTimer") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("TimeBeforeNextRandomSFX"));
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("RandomSFXIndex").setNumber(gdjs.randomInRange(1, 3));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "RandomNoiseTimer");
}{runtimeScene.getScene().getVariables().get("TimeBeforeNextRandomSFX").setNumber(gdjs.randomFloatInRange(30, 240));
}
{ //Subevents
gdjs.TutorialCode.eventsList45(runtimeScene);} //End of subevents
}

}


};gdjs.TutorialCode.eventsList47 = function(runtimeScene) {

{



}


{


gdjs.TutorialCode.eventsList42(runtimeScene);
}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "LevelUI", 0, 0, 0);
}{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "LevelEndScreen", 0, 0, 0);
}}

}


{


gdjs.TutorialCode.eventsList44(runtimeScene);
}


{


gdjs.TutorialCode.eventsList46(runtimeScene);
}


};

gdjs.TutorialCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.TutorialCode.GDBackgroundPlantsObjects1.length = 0;
gdjs.TutorialCode.GDBackgroundPlantsObjects2.length = 0;
gdjs.TutorialCode.GDBackgroundPlantsObjects3.length = 0;
gdjs.TutorialCode.GDBackgroundPlantsObjects4.length = 0;
gdjs.TutorialCode.GDBackgroundPlantsObjects5.length = 0;
gdjs.TutorialCode.GDBackgroundPlantsObjects6.length = 0;
gdjs.TutorialCode.GDLeftBoundaryObjects1.length = 0;
gdjs.TutorialCode.GDLeftBoundaryObjects2.length = 0;
gdjs.TutorialCode.GDLeftBoundaryObjects3.length = 0;
gdjs.TutorialCode.GDLeftBoundaryObjects4.length = 0;
gdjs.TutorialCode.GDLeftBoundaryObjects5.length = 0;
gdjs.TutorialCode.GDLeftBoundaryObjects6.length = 0;
gdjs.TutorialCode.GDRightBoundaryObjects1.length = 0;
gdjs.TutorialCode.GDRightBoundaryObjects2.length = 0;
gdjs.TutorialCode.GDRightBoundaryObjects3.length = 0;
gdjs.TutorialCode.GDRightBoundaryObjects4.length = 0;
gdjs.TutorialCode.GDRightBoundaryObjects5.length = 0;
gdjs.TutorialCode.GDRightBoundaryObjects6.length = 0;
gdjs.TutorialCode.GDTopBoundaryObjects1.length = 0;
gdjs.TutorialCode.GDTopBoundaryObjects2.length = 0;
gdjs.TutorialCode.GDTopBoundaryObjects3.length = 0;
gdjs.TutorialCode.GDTopBoundaryObjects4.length = 0;
gdjs.TutorialCode.GDTopBoundaryObjects5.length = 0;
gdjs.TutorialCode.GDTopBoundaryObjects6.length = 0;
gdjs.TutorialCode.GDBottomBoundaryObjects1.length = 0;
gdjs.TutorialCode.GDBottomBoundaryObjects2.length = 0;
gdjs.TutorialCode.GDBottomBoundaryObjects3.length = 0;
gdjs.TutorialCode.GDBottomBoundaryObjects4.length = 0;
gdjs.TutorialCode.GDBottomBoundaryObjects5.length = 0;
gdjs.TutorialCode.GDBottomBoundaryObjects6.length = 0;
gdjs.TutorialCode.GDBoundaryJumpThroughObjects1.length = 0;
gdjs.TutorialCode.GDBoundaryJumpThroughObjects2.length = 0;
gdjs.TutorialCode.GDBoundaryJumpThroughObjects3.length = 0;
gdjs.TutorialCode.GDBoundaryJumpThroughObjects4.length = 0;
gdjs.TutorialCode.GDBoundaryJumpThroughObjects5.length = 0;
gdjs.TutorialCode.GDBoundaryJumpThroughObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595ArrowObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595ArrowObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595ArrowObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595ArrowObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595ArrowObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595ArrowObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595JumpObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595JumpObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595JumpObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595JumpObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595JumpObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595JumpObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595CollectObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595CollectObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595CollectObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595CollectObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595CollectObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595CollectObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595KillObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595KillObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595KillObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595KillObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595KillObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595KillObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595CheckpointObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595CheckpointObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595CheckpointObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595CheckpointObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595CheckpointObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595CheckpointObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595DownArrowObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595DownArrowObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595DownArrowObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595DownArrowObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595DownArrowObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595DownArrowObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595DeathObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595DeathObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595DeathObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595DeathObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595DeathObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595DeathObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595ProceedObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595ProceedObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595ProceedObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595ProceedObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595ProceedObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595ProceedObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595SpaceObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595SpaceObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595SpaceObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595SpaceObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595SpaceObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595SpaceObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595WObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595WObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595WObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595WObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595WObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595WObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595AObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595AObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595AObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595AObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595AObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595AObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595DObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595DObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595DObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595DObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595DObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595DObjects6.length = 0;
gdjs.TutorialCode.GDSinage_9595SObjects1.length = 0;
gdjs.TutorialCode.GDSinage_9595SObjects2.length = 0;
gdjs.TutorialCode.GDSinage_9595SObjects3.length = 0;
gdjs.TutorialCode.GDSinage_9595SObjects4.length = 0;
gdjs.TutorialCode.GDSinage_9595SObjects5.length = 0;
gdjs.TutorialCode.GDSinage_9595SObjects6.length = 0;
gdjs.TutorialCode.GDPlayerObjects1.length = 0;
gdjs.TutorialCode.GDPlayerObjects2.length = 0;
gdjs.TutorialCode.GDPlayerObjects3.length = 0;
gdjs.TutorialCode.GDPlayerObjects4.length = 0;
gdjs.TutorialCode.GDPlayerObjects5.length = 0;
gdjs.TutorialCode.GDPlayerObjects6.length = 0;
gdjs.TutorialCode.GDFlyingDemonObjects1.length = 0;
gdjs.TutorialCode.GDFlyingDemonObjects2.length = 0;
gdjs.TutorialCode.GDFlyingDemonObjects3.length = 0;
gdjs.TutorialCode.GDFlyingDemonObjects4.length = 0;
gdjs.TutorialCode.GDFlyingDemonObjects5.length = 0;
gdjs.TutorialCode.GDFlyingDemonObjects6.length = 0;
gdjs.TutorialCode.GDFireDemonObjects1.length = 0;
gdjs.TutorialCode.GDFireDemonObjects2.length = 0;
gdjs.TutorialCode.GDFireDemonObjects3.length = 0;
gdjs.TutorialCode.GDFireDemonObjects4.length = 0;
gdjs.TutorialCode.GDFireDemonObjects5.length = 0;
gdjs.TutorialCode.GDFireDemonObjects6.length = 0;
gdjs.TutorialCode.GDCheckpointObjects1.length = 0;
gdjs.TutorialCode.GDCheckpointObjects2.length = 0;
gdjs.TutorialCode.GDCheckpointObjects3.length = 0;
gdjs.TutorialCode.GDCheckpointObjects4.length = 0;
gdjs.TutorialCode.GDCheckpointObjects5.length = 0;
gdjs.TutorialCode.GDCheckpointObjects6.length = 0;
gdjs.TutorialCode.GDStaticPlatform3Objects1.length = 0;
gdjs.TutorialCode.GDStaticPlatform3Objects2.length = 0;
gdjs.TutorialCode.GDStaticPlatform3Objects3.length = 0;
gdjs.TutorialCode.GDStaticPlatform3Objects4.length = 0;
gdjs.TutorialCode.GDStaticPlatform3Objects5.length = 0;
gdjs.TutorialCode.GDStaticPlatform3Objects6.length = 0;
gdjs.TutorialCode.GDStaticPlatform2Objects1.length = 0;
gdjs.TutorialCode.GDStaticPlatform2Objects2.length = 0;
gdjs.TutorialCode.GDStaticPlatform2Objects3.length = 0;
gdjs.TutorialCode.GDStaticPlatform2Objects4.length = 0;
gdjs.TutorialCode.GDStaticPlatform2Objects5.length = 0;
gdjs.TutorialCode.GDStaticPlatform2Objects6.length = 0;
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects1.length = 0;
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects2.length = 0;
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects3.length = 0;
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects4.length = 0;
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects5.length = 0;
gdjs.TutorialCode.GDHorizontalMovingPlatformObjects6.length = 0;
gdjs.TutorialCode.GDStaticPlatform1Objects1.length = 0;
gdjs.TutorialCode.GDStaticPlatform1Objects2.length = 0;
gdjs.TutorialCode.GDStaticPlatform1Objects3.length = 0;
gdjs.TutorialCode.GDStaticPlatform1Objects4.length = 0;
gdjs.TutorialCode.GDStaticPlatform1Objects5.length = 0;
gdjs.TutorialCode.GDStaticPlatform1Objects6.length = 0;
gdjs.TutorialCode.GDPortalObjects1.length = 0;
gdjs.TutorialCode.GDPortalObjects2.length = 0;
gdjs.TutorialCode.GDPortalObjects3.length = 0;
gdjs.TutorialCode.GDPortalObjects4.length = 0;
gdjs.TutorialCode.GDPortalObjects5.length = 0;
gdjs.TutorialCode.GDPortalObjects6.length = 0;
gdjs.TutorialCode.GDLadderObjects1.length = 0;
gdjs.TutorialCode.GDLadderObjects2.length = 0;
gdjs.TutorialCode.GDLadderObjects3.length = 0;
gdjs.TutorialCode.GDLadderObjects4.length = 0;
gdjs.TutorialCode.GDLadderObjects5.length = 0;
gdjs.TutorialCode.GDLadderObjects6.length = 0;
gdjs.TutorialCode.GDHeartObjects1.length = 0;
gdjs.TutorialCode.GDHeartObjects2.length = 0;
gdjs.TutorialCode.GDHeartObjects3.length = 0;
gdjs.TutorialCode.GDHeartObjects4.length = 0;
gdjs.TutorialCode.GDHeartObjects5.length = 0;
gdjs.TutorialCode.GDHeartObjects6.length = 0;
gdjs.TutorialCode.GDMonsterParticlesObjects1.length = 0;
gdjs.TutorialCode.GDMonsterParticlesObjects2.length = 0;
gdjs.TutorialCode.GDMonsterParticlesObjects3.length = 0;
gdjs.TutorialCode.GDMonsterParticlesObjects4.length = 0;
gdjs.TutorialCode.GDMonsterParticlesObjects5.length = 0;
gdjs.TutorialCode.GDMonsterParticlesObjects6.length = 0;
gdjs.TutorialCode.GDSpikeParticlesObjects1.length = 0;
gdjs.TutorialCode.GDSpikeParticlesObjects2.length = 0;
gdjs.TutorialCode.GDSpikeParticlesObjects3.length = 0;
gdjs.TutorialCode.GDSpikeParticlesObjects4.length = 0;
gdjs.TutorialCode.GDSpikeParticlesObjects5.length = 0;
gdjs.TutorialCode.GDSpikeParticlesObjects6.length = 0;
gdjs.TutorialCode.GDDoorParticlesObjects1.length = 0;
gdjs.TutorialCode.GDDoorParticlesObjects2.length = 0;
gdjs.TutorialCode.GDDoorParticlesObjects3.length = 0;
gdjs.TutorialCode.GDDoorParticlesObjects4.length = 0;
gdjs.TutorialCode.GDDoorParticlesObjects5.length = 0;
gdjs.TutorialCode.GDDoorParticlesObjects6.length = 0;
gdjs.TutorialCode.GDDustParticleObjects1.length = 0;
gdjs.TutorialCode.GDDustParticleObjects2.length = 0;
gdjs.TutorialCode.GDDustParticleObjects3.length = 0;
gdjs.TutorialCode.GDDustParticleObjects4.length = 0;
gdjs.TutorialCode.GDDustParticleObjects5.length = 0;
gdjs.TutorialCode.GDDustParticleObjects6.length = 0;
gdjs.TutorialCode.GDLivesBarObjects1.length = 0;
gdjs.TutorialCode.GDLivesBarObjects2.length = 0;
gdjs.TutorialCode.GDLivesBarObjects3.length = 0;
gdjs.TutorialCode.GDLivesBarObjects4.length = 0;
gdjs.TutorialCode.GDLivesBarObjects5.length = 0;
gdjs.TutorialCode.GDLivesBarObjects6.length = 0;
gdjs.TutorialCode.GDLifeForceBarObjects1.length = 0;
gdjs.TutorialCode.GDLifeForceBarObjects2.length = 0;
gdjs.TutorialCode.GDLifeForceBarObjects3.length = 0;
gdjs.TutorialCode.GDLifeForceBarObjects4.length = 0;
gdjs.TutorialCode.GDLifeForceBarObjects5.length = 0;
gdjs.TutorialCode.GDLifeForceBarObjects6.length = 0;
gdjs.TutorialCode.GDMemoryObjects1.length = 0;
gdjs.TutorialCode.GDMemoryObjects2.length = 0;
gdjs.TutorialCode.GDMemoryObjects3.length = 0;
gdjs.TutorialCode.GDMemoryObjects4.length = 0;
gdjs.TutorialCode.GDMemoryObjects5.length = 0;
gdjs.TutorialCode.GDMemoryObjects6.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects1.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects2.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects3.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects4.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects5.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595DeathTextObjects6.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects1.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects2.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects3.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects4.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects5.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595MemoryAcquiredObjects6.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects1.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects2.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects3.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects4.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects5.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LivesObjects6.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LifeForceObjects1.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LifeForceObjects2.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LifeForceObjects3.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LifeForceObjects4.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LifeForceObjects5.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595LifeForceObjects6.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects1.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects2.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects3.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects4.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects5.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595RetryButtonObjects6.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects1.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects2.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects3.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects4.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects5.length = 0;
gdjs.TutorialCode.GDUI_9595Sinage_9595BackgroundObjects6.length = 0;
gdjs.TutorialCode.GDHorizontalDemonObjects1.length = 0;
gdjs.TutorialCode.GDHorizontalDemonObjects2.length = 0;
gdjs.TutorialCode.GDHorizontalDemonObjects3.length = 0;
gdjs.TutorialCode.GDHorizontalDemonObjects4.length = 0;
gdjs.TutorialCode.GDHorizontalDemonObjects5.length = 0;
gdjs.TutorialCode.GDHorizontalDemonObjects6.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects1.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects2.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects3.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects4.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects5.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595BaseObjects6.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects1.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects2.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects3.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects4.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects5.length = 0;
gdjs.TutorialCode.GDSpikeDemon_9595SpikeObjects6.length = 0;
gdjs.TutorialCode.GDVerticalMovingPlatformObjects1.length = 0;
gdjs.TutorialCode.GDVerticalMovingPlatformObjects2.length = 0;
gdjs.TutorialCode.GDVerticalMovingPlatformObjects3.length = 0;
gdjs.TutorialCode.GDVerticalMovingPlatformObjects4.length = 0;
gdjs.TutorialCode.GDVerticalMovingPlatformObjects5.length = 0;
gdjs.TutorialCode.GDVerticalMovingPlatformObjects6.length = 0;
gdjs.TutorialCode.GDSpinningMovingPlatformObjects1.length = 0;
gdjs.TutorialCode.GDSpinningMovingPlatformObjects2.length = 0;
gdjs.TutorialCode.GDSpinningMovingPlatformObjects3.length = 0;
gdjs.TutorialCode.GDSpinningMovingPlatformObjects4.length = 0;
gdjs.TutorialCode.GDSpinningMovingPlatformObjects5.length = 0;
gdjs.TutorialCode.GDSpinningMovingPlatformObjects6.length = 0;
gdjs.TutorialCode.GDFlippingPlatformObjects1.length = 0;
gdjs.TutorialCode.GDFlippingPlatformObjects2.length = 0;
gdjs.TutorialCode.GDFlippingPlatformObjects3.length = 0;
gdjs.TutorialCode.GDFlippingPlatformObjects4.length = 0;
gdjs.TutorialCode.GDFlippingPlatformObjects5.length = 0;
gdjs.TutorialCode.GDFlippingPlatformObjects6.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects1.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects2.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects3.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects4.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects5.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595BaseObjects6.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects1.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects2.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects3.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects4.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects5.length = 0;
gdjs.TutorialCode.GDStalagtiteDemon_9595SpikeObjects6.length = 0;

gdjs.TutorialCode.eventsList47(runtimeScene);

return;

}

gdjs['TutorialCode'] = gdjs.TutorialCode;
