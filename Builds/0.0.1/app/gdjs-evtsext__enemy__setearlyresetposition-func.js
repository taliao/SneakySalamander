
if (typeof gdjs.evtsExt__Enemy__SetEarlyResetPosition !== "undefined") {
  gdjs.evtsExt__Enemy__SetEarlyResetPosition.registeredGdjsCallbacks.forEach(callback =>
    gdjs._unregisterCallback(callback)
  );
}

gdjs.evtsExt__Enemy__SetEarlyResetPosition = {};
gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1= [];
gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects2= [];


gdjs.evtsExt__Enemy__SetEarlyResetPosition.eventsList0 = function(runtimeScene, eventsFunctionContext) {

{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1 */
{for(var i = 0, len = gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1[i].setVariableBoolean(gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1[i].getVariables().get("HasPosition"), true);
}
}{for(var i = 0, len = gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1[i].returnVariable(gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1[i].getVariables().get("OldPosition_X")).setNumber((gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1[i].getX()));
}
}{for(var i = 0, len = gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1[i].returnVariable(gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1[i].getVariables().get("OldPosition_Y")).setNumber((gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1[i].getY()));
}
}}

}


};gdjs.evtsExt__Enemy__SetEarlyResetPosition.eventsList1 = function(runtimeScene, eventsFunctionContext) {

{

gdjs.copyArray(eventsFunctionContext.getObjects("Enemy"), gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1.length;i<l;++i) {
    if ( gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1[i].getVariableBoolean(gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1[i].getVariables().get("DeferAcquireResetPosition"), true) ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1[k] = gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1[i];
        ++k;
    }
}
gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1.length = k;
if (isConditionTrue_0) {

{ //Subevents
gdjs.evtsExt__Enemy__SetEarlyResetPosition.eventsList0(runtimeScene, eventsFunctionContext);} //End of subevents
}

}


};

gdjs.evtsExt__Enemy__SetEarlyResetPosition.func = function(runtimeScene, Enemy, parentEventsFunctionContext) {
var eventsFunctionContext = {
  _objectsMap: {
"Enemy": Enemy
},
  _objectArraysMap: {
"Enemy": gdjs.objectsListsToArray(Enemy)
},
  _behaviorNamesMap: {
},
  getObjects: function(objectName) {
    return eventsFunctionContext._objectArraysMap[objectName] || [];
  },
  getObjectsLists: function(objectName) {
    return eventsFunctionContext._objectsMap[objectName] || null;
  },
  getBehaviorName: function(behaviorName) {
    return eventsFunctionContext._behaviorNamesMap[behaviorName] || behaviorName;
  },
  createObject: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    if (objectsList) {
      const object = parentEventsFunctionContext ?
        parentEventsFunctionContext.createObject(objectsList.firstKey()) :
        runtimeScene.createObject(objectsList.firstKey());
      if (object) {
        objectsList.get(objectsList.firstKey()).push(object);
        eventsFunctionContext._objectArraysMap[objectName].push(object);
      }
      return object;    }
    return null;
  },
  getInstancesCountOnScene: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    let count = 0;
    if (objectsList) {
      for(const objectName in objectsList.items)
        count += parentEventsFunctionContext ?
parentEventsFunctionContext.getInstancesCountOnScene(objectName) :
        runtimeScene.getInstancesCountOnScene(objectName);
    }
    return count;
  },
  getLayer: function(layerName) {
    return runtimeScene.getLayer(layerName);
  },
  getArgument: function(argName) {
    return "";
  },
  getOnceTriggers: function() { return runtimeScene.getOnceTriggers(); }
};

gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects1.length = 0;
gdjs.evtsExt__Enemy__SetEarlyResetPosition.GDEnemyObjects2.length = 0;

gdjs.evtsExt__Enemy__SetEarlyResetPosition.eventsList1(runtimeScene, eventsFunctionContext);

return;
}

gdjs.evtsExt__Enemy__SetEarlyResetPosition.registeredGdjsCallbacks = [];