
if (typeof gdjs.evtsExt__Enemy__ResetEnemy !== "undefined") {
  gdjs.evtsExt__Enemy__ResetEnemy.registeredGdjsCallbacks.forEach(callback =>
    gdjs._unregisterCallback(callback)
  );
}

gdjs.evtsExt__Enemy__ResetEnemy = {};
gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1= [];
gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects2= [];


gdjs.evtsExt__Enemy__ResetEnemy.eventsList0 = function(runtimeScene, eventsFunctionContext) {

{


let isConditionTrue_0 = false;
{
/* Reuse gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1 */
{for(var i = 0, len = gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[i].setPosition((gdjs.RuntimeObject.getVariableNumber(gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[i].getVariables().get("OldPosition_X"))),(gdjs.RuntimeObject.getVariableNumber(gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[i].getVariables().get("OldPosition_Y"))));
}
}{for(var i = 0, len = gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[i].setVariableBoolean(gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[i].getVariables().get("IsDead"), false);
}
}{for(var i = 0, len = gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1.length ;i < len;++i) {
    gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[i].setVariableBoolean(gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[i].getVariables().get("HasBeenReaped"), false);
}
}}

}


};gdjs.evtsExt__Enemy__ResetEnemy.eventsList1 = function(runtimeScene, eventsFunctionContext) {

{

gdjs.copyArray(eventsFunctionContext.getObjects("Enemy"), gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1.length;i<l;++i) {
    if ( gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[i].getVariableBoolean(gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[i].getVariables().get("IsDead"), true) ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[k] = gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[i];
        ++k;
    }
}
gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1.length;i<l;++i) {
    if ( gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[i].getVariableBoolean(gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[i].getVariables().get("HasBeenReaped"), true) ) {
        isConditionTrue_0 = true;
        gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[k] = gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1[i];
        ++k;
    }
}
gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1.length = k;
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.evtsExt__Enemy__ResetEnemy.eventsList0(runtimeScene, eventsFunctionContext);} //End of subevents
}

}


};

gdjs.evtsExt__Enemy__ResetEnemy.func = function(runtimeScene, Enemy, parentEventsFunctionContext) {
var eventsFunctionContext = {
  _objectsMap: {
"Enemy": Enemy
},
  _objectArraysMap: {
"Enemy": gdjs.objectsListsToArray(Enemy)
},
  _behaviorNamesMap: {
},
  getObjects: function(objectName) {
    return eventsFunctionContext._objectArraysMap[objectName] || [];
  },
  getObjectsLists: function(objectName) {
    return eventsFunctionContext._objectsMap[objectName] || null;
  },
  getBehaviorName: function(behaviorName) {
    return eventsFunctionContext._behaviorNamesMap[behaviorName] || behaviorName;
  },
  createObject: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    if (objectsList) {
      const object = parentEventsFunctionContext ?
        parentEventsFunctionContext.createObject(objectsList.firstKey()) :
        runtimeScene.createObject(objectsList.firstKey());
      if (object) {
        objectsList.get(objectsList.firstKey()).push(object);
        eventsFunctionContext._objectArraysMap[objectName].push(object);
      }
      return object;    }
    return null;
  },
  getInstancesCountOnScene: function(objectName) {
    const objectsList = eventsFunctionContext._objectsMap[objectName];
    let count = 0;
    if (objectsList) {
      for(const objectName in objectsList.items)
        count += parentEventsFunctionContext ?
parentEventsFunctionContext.getInstancesCountOnScene(objectName) :
        runtimeScene.getInstancesCountOnScene(objectName);
    }
    return count;
  },
  getLayer: function(layerName) {
    return runtimeScene.getLayer(layerName);
  },
  getArgument: function(argName) {
    return "";
  },
  getOnceTriggers: function() { return runtimeScene.getOnceTriggers(); }
};

gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects1.length = 0;
gdjs.evtsExt__Enemy__ResetEnemy.GDEnemyObjects2.length = 0;

gdjs.evtsExt__Enemy__ResetEnemy.eventsList1(runtimeScene, eventsFunctionContext);

return;
}

gdjs.evtsExt__Enemy__ResetEnemy.registeredGdjsCallbacks = [];